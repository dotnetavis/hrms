﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class HolidayCalender:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }      //primary key
        public string calendername { get; set; }
        public string calenderyear { get; set; }
        public string holidaycolor { get; set; }
        public string offcolor { get; set; }
        public string weeklyoff { get; set; }
        public int allowedleave { get; set; }
        public string calender_path { get; set; }
    }
}


