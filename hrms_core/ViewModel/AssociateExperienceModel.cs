﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class AssociateExperienceModel: AssociateExperience
    {
        public List<AssociateExperienceModel> AssociateExperienceModelList { get; set; }
        public List<DesignationModel> DesignationModelList { get; set; }
        public AssociateExperienceModel()
        {
            AssociateExperienceModelList = new List<AssociateExperienceModel>();
            DesignationModelList = new List<DesignationModel>();
        }
    }
}
