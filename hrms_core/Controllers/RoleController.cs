﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace hrms_core.Controllers
{
    public class RoleController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IRoleServices roleServices => new RoleServices(_db);
        public RoleController(ApplicationDbContext db) => _db = db;
        [HttpGet]
        public IActionResult Index(int? id)
        {
            RoleMasterModel model = new RoleMasterModel();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                model.PageMenuList = roleServices.GetMenuList();
                model.RoleMasterModelList = roleServices.GetRoleList();
                if (id > 0 && id != null)
                {
                  if (model.RoleMasterModelList != null && model.RoleMasterModelList.Count > 0)
                  {
                      var data = model.RoleMasterModelList.FirstOrDefault(x => x.id == id);
                      if (data != null)
                      {
                        model.id = data.id;
                        model.role_name = data.role_name;
                        model.role_description = data.role_description;
                      }
                    model.PageMenuList = new List<PageMenuModel>();
                    model.PageMenuList = roleServices.GetMenuList(id.Value);
                  }
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult Index(RoleMasterModel model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                if (model.id > 0)
                {
                    TempData["msg"] = roleServices.UpdateRole(model);
                }
                else
                {
                    TempData["msg"] = roleServices.SaveRole(model);
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("Index", "Role",new { id=0});
        }
        [HttpPost]
        public IActionResult DisableRole(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            RoleMasterModel model = new RoleMasterModel();
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            model.id = id;
            model.updatedby = 1;
            TempData["msg"] = roleServices.DisableRole(model);
            return Json(true);
        }
        [HttpPost]
        public IActionResult EnableRole(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            RoleMasterModel model = new RoleMasterModel();
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            model.id = id;
            model.updatedby = 1;
            TempData["msg"] = roleServices.EnableRole(model);
            return Json(true);
        }
    }
}