﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class LoginHistory
    {
        public int id { get; set; }
        public string associate_name { get; set; }
        public string associate_code { get; set; }
        public string associate_email { get; set; }
        public string associate_mobile { get; set; }
        public string device_ip { get; set; }
        public string device_name { get; set; }
        public string  device_type { get; set; }
        public string device_location { get; set; }
        public DateTime created_on { get; set; }
    }
}
