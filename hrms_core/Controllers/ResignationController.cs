﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace hrms_core.Controllers
{
    public class ResignationController : Controller
    {
        private readonly ApplicationDbContext _db;
        public IConfiguration Configuration { get; }
        private ILeaveServices leaveservices => new LeaveServices(_db);
        private IResignationServices resignationServices => new ResignationServices(_db);
        private ILoginServices loginServices => new LoginServices(_db);
        private ILeaveServices leaveServices => new LeaveServices(_db);

        public ResignationController(ApplicationDbContext options, IConfiguration configuration)
        {
            _db = options;
            Configuration = configuration;
        }

        [HttpGet]
        public IActionResult Index()
        {
            ResignationViewModel model = new ResignationViewModel();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                bool check = resignationServices.CheckResignation(SesMdl.associate_id);
                model.ExistResignation = true;
                if (check)
                {
                    model.ExistResignation = true;
                }
                else
                {
                    model.ExistResignation = false;
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult Index(ResignationViewModel model)
        {
            bool _result = false;
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.associate_id = SesMdl.associate_id;
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                int Res = resignationServices.SaveResignation(model);
                if (Res == -1)
                    TempData["msg"] = MessageHelper.Resignation_AddError;
                else if (Res > 0)
                {
                    TempData["msg"] = MessageHelper.Resignation_AddSuccess;

                    #region Resignation Mail Functionality
                    model.id = Res;
                    model.official_email = SesMdl.official_email;
                    model.hod_email = SesMdl.head_of_department_email;
                    model.username = _db.AssociateMasters.FirstOrDefault(x => x.associate_code == SesMdl.username).associate_name;
                    model.reporting_email = SesMdl.reporting_person_email;
                    model.additional_reporting_email = SesMdl.addition_reporting;
                    //model.lstDailyReportDetailModel = dailyServices.GetDARDetails(result);
                    _result = resignationServices.SendResignationMail(model, HttpContext, Configuration);
                    #endregion

                }
                else
                    TempData["msg"] = MessageHelper.OtherError;
                return RedirectToAction("Index", "Resignation");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpGet]
        public IActionResult ResignationBucket()
        {
            ResignationViewModel model = new ResignationViewModel();
            List<int> reporting_ids = new List<int>();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                reporting_ids = leaveServices.GetReportingData(SesMdl.associate_id);
                model.ResignationList = resignationServices.GetAllResignation();
                return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [HttpGet]
        public IActionResult ResignationApproval(string ResignationId)
        {
            ResignationViewModel model = new ResignationViewModel();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            int ResId = 0;
            if (!String.IsNullOrWhiteSpace(ResignationId))
            {
                try
                {
                    ResId = Convert.ToInt32(CommonUtility.GetDecryptMD5(ResignationId));
                }
                catch (Exception ex)
                {
                    CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                    ResId = 0;
                }
            }
            if (SesMdl != null)
            {
                try
                {
                    model = resignationServices.GetResignationById(ResId);
                    model.id = ResId;
                    ViewBag.ApproveReject = 0;
                    if (model != null)
                    {
                        if (model.is_approved == true)
                            ViewBag.ApproveReject = 1;
                        else if (model.is_approved == false)
                            ViewBag.ApproveReject = 2;
                        else
                            ViewBag.ApproveReject = 0;
                    }
                }
                catch (Exception ex)
                {
                    CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                }
                if (model != null)
                    return View(model);
                else
                    return RedirectToAction("Login", "Account");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [HttpPost]
        public IActionResult ResignationApproval(ResignationViewModel model, string CommandName)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                try
                {
                    bool _result = false;
                    model.createdby = SesMdl.associate_id;
                    model.createdbyname = SesMdl.associate_name;
                    model.approved_by_name= SesMdl.associate_name;
                    bool flag = true;
                    if (CommandName == "Ok")
                        model.is_approved = true;
                    else if (CommandName == "Reject")
                        model.is_approved = false;
                    else
                        flag = false;
                    if (flag)
                    {
                        List<string> EmailIds = new List<string>();
                        int AsId = resignationServices.ApproveRejectResignation(model);
                        if (AsId > 0 && model.is_approved == true)
                        {
                            TempData["msg"] = MessageHelper.Resignation_ApprovedSuccess;
                        }
                        else if (AsId > 0 && model.is_approved == false)
                        {
                            TempData["msg"] = MessageHelper.Resignation_RejectSuccess;
                        }
                        else
                        {
                            TempData["msg"] = MessageHelper.OtherError;
                        }
                        List<int> reporting_ids = new List<int>();
                        reporting_ids = leaveservices.GetReportingData(AsId);
                        string ResignationSender_Email = string.Empty;
                        var ResignationSender = _db.AssociateMains.FirstOrDefault(x => x.associate_id == AsId);
                        if (ResignationSender != null)
                        {
                            ResignationSender_Email = ResignationSender.official_email;
                        }
                        var associate_codes = (from master in _db.AssociateMasters.Where(x => reporting_ids.Contains(x.id))
                                               join main in _db.AssociateMains on master.id equals main.associate_id
                                               join user in _db.Users on master.associate_code equals user.username

                                               select new SessionModel()
                                               {
                                                   associate_id = master.id,
                                                   associate_code = master.associate_code,
                                                   password = user.userpass,
                                                   official_email = main.official_email,
                                                   is_super_admin = master.is_super_admin

                                               }
                                               ).ToList();
                        
                        if (associate_codes != null && associate_codes.Count > 0)
                        {
                            var associate_codes1 = associate_codes.Where(x => x.official_email != ResignationSender_Email).Distinct();
                            foreach (var item in associate_codes)
                            {
                                if (item != null)
                                {
                                    EmailIds.Add(item.official_email);
                                }
                            }
                        }

                        #region Resignation Mail Functionality
                        model.username = _db.AssociateMasters.FirstOrDefault(x => x.associate_code == SesMdl.username).associate_name;

                        //model.lstDailyReportDetailModel = dailyServices.GetDARDetails(result);
                        _result = resignationServices.SendResignationApprovalRejectionMail(model, HttpContext, Configuration,EmailIds, ResignationSender_Email);
                        #endregion

                    }
                }
                catch (Exception ex)
                {
                    CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                }
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [HttpGet]
        public JsonResult GetResignationById(int ResignationId)
        {
            ResignationViewModel model = new ResignationViewModel();
            try
            {
                model.ResignationList = resignationServices.GetResignationId(ResignationId);
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return Json(model.ResignationList);
        }
        [HttpGet]
        public IActionResult Login(string ResignationId)
        {
            ResignationViewModel model = new ResignationViewModel();
            int ResId = 0;
            if (!String.IsNullOrWhiteSpace(ResignationId))
            {
                ResId = Convert.ToInt32(CommonUtility.GetDecryptMD5(ResignationId));
            }

            model.id = ResId;
            model.encResignationID = ResignationId;
            return View(model);
        }
        [HttpPost]
        public IActionResult Login(ResignationViewModel model)
        {
            string EncResignationId = string.Empty;
            int associate_id = 0;
            if (model.id > 0)
            {
                EncResignationId = CommonUtility.MD5Convertor(Convert.ToString(model.id));
                SessionModel sessionModel = new SessionModel();
                List<int> reporting_ids = new List<int>();
                string password = CommonUtility.MD5Convertor(model.userpass);
                //int associate_id = _db.LeaveApplications.FirstOrDefault(x => x.id == model.id).createdby;
                var data = _db.resignations.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    associate_id = data.associate_id;
                }
                reporting_ids = leaveservices.GetReportingData(associate_id);
                var associate_codes = (from master in _db.AssociateMasters.Where(x => reporting_ids.Contains(x.id))
                                       join main in _db.AssociateMains on master.id equals main.associate_id
                                       join user in _db.Users on master.associate_code equals user.username

                                       select new SessionModel()
                                       {
                                           associate_id = master.id,
                                           associate_code = master.associate_code,
                                           password = user.userpass,
                                           official_email = main.official_email,
                                           is_super_admin = master.is_super_admin

                                       }
                                       ).ToList();
                if (associate_codes != null && associate_codes.Count != 0)
                {
                    bool Count = false;
                    foreach (var item in associate_codes)
                    {
                        if ((item.associate_code == model.username || item.official_email == model.username) && item.password == password)
                        {
                            sessionModel = loginServices.GetAllSessionData(item.associate_code, item.is_super_admin).FirstOrDefault();
                            HttpContext.Session.SetObjectAsJson(SessionVariables.SessionData, sessionModel);
                            return RedirectToAction("ResignationApproval", "Resignation", new { ResignationId = EncResignationId });
                        }
                        else
                        {
                            Count = true;
                        }
                    }
                    if (Count)
                    {
                        TempData["msg"] = "Unauthorized User";
                        return RedirectToAction("Login", "Resignation", new { LeaveID = EncResignationId });
                    }
                }
            }
            TempData["msg"] = "Unauthorized User";
            return RedirectToAction("Login", "Resignation", new { LeaveID = model.encResignationID });


        }
    }
}