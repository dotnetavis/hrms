﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class PolicyVM : PolicyModel
    {
        public List<PolicyVM> PolicyList { get; set; }
        public List<AssociateMasterModel> AssociateList { get; set; }
        public List<DepartmentModel> DepartmentList { get; set; }
        public PolicyVM()
        {
            PolicyList = new List<PolicyVM>();
            DepartmentList = new List<DepartmentModel>();
            AssociateList = new List<AssociateMasterModel>();                                               
        }
    }
}
