﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;


namespace hrms_core.ViewModel.Services
{
    public class PaySlipServices : IPaySlipServices
    {
        private readonly ApplicationDbContext _db;
        public PaySlipServices(ApplicationDbContext options)
        {
            _db = options;
        }

        public string SavePaySlip(PayslipModel model)
        {
            string Result = string.Empty;
            Payslip paySlip = new Payslip();
            try
            {
                //Check for Duplicate Record
                bool bool_sameNameExists = _db.Payslips.Any(x => (x.monthyear.Date.Month == model.monthyear.Date.Month) && (x.monthyear.Date.Year == model.monthyear.Date.Year) && (x.associate.id == model.associate.id));
                if (bool_sameNameExists)
                {
                    Result = MessageHelper.PaySlip_Duplicate;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            paySlip.monthyear = model.monthyear;
                            paySlip.updatedbyname = model.updatedbyname;
                            paySlip.createdbyname = model.createdbyname;
                            paySlip.isactive = true;
                            paySlip.createdon = DateTime.Now;
                            paySlip.updatedon = DateTime.Now;
                            paySlip.createdby = model.createdby;
                            paySlip.updatedby = model.updatedby; 
                            _db.Payslips.Add(paySlip);
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.PaySlip_AddSuccess;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.PaySlip_AddError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.PaySlip_AddError;
            }
            return Result;
        }


    }
}
