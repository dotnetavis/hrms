﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;


namespace hrms_core.Controllers
{
    public class QualificationController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IQualificationServices qualificationServices => 
            new QualificationServices(_db);
        public QualificationController(ApplicationDbContext options) => _db = options;

        [HttpGet]
        public IActionResult Index(int? id)
        {
            QualificationModel model = new QualificationModel();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                model.QualificationModelList = qualificationServices.GetQualification();
                if (id > 0 && id != null)
                {
                    if (model.QualificationModelList.Count > 0 && model.QualificationModelList != null)
                    {
                       var data = model.QualificationModelList.FirstOrDefault(x => x.id == id);
                       if (data != null)
                       {
                           model.id = data.id;
                           model.qualificationtext = data.qualificationtext;
                           model.createdby = data.createdby;
                           model.createdon = data.createdon;
                           model.updatedby = data.updatedby;
                           model.updatedon = data.updatedon;
                           model.isactive = data.isactive;
                       
                       }
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(QualificationModel model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                if (model.id > 0)
                {
                    TempData["msg"] = qualificationServices.UpdateQualification(model);
                }
                else
                {
                    TempData["msg"] = qualificationServices.SaveQualification(model);
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("Index", "Qualification", new { id = 0});
        }
       
        public IActionResult DisableQualification(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            QualificationModel model = new QualificationModel();
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            model.id = id;
            model.updatedby = 1;
            TempData["msg"] = qualificationServices.DisableQualification(model);
            return RedirectToAction("qualification", "Qualification");
        }
     
        public IActionResult EnableQualification(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            QualificationModel model = new QualificationModel();
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            model.id = id;
            model.updatedby = 1;
            TempData["msg"] = qualificationServices.EnableQualification(model);
            return RedirectToAction("qualification", "Qualification");
        }
    }
}