﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class ProjectMaster : CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage = "Project Name Required")]
        public string projecttext { get; set; }
        [Required(ErrorMessage = "Date Required")]
        [DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = false, DataFormatString = "dd/MM/yyyy")]
        public DateTime datestart { get; set; }
        [Required(ErrorMessage = "Date Required")]
        [DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = false, DataFormatString = "dd/MM/yyyy")]
        public DateTime expectedcompletiondate { get; set; }
        [Required(ErrorMessage = "Date Required")]
        [DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = false, DataFormatString = "dd/MM/yyyy")]
        public DateTime completiondate { get; set; }
        [Required(ErrorMessage = "Please Select Department")]
        [Range(1, 9999999999, ErrorMessage = "Please Select Department")]
        public int departmentid { get; set; }
        [ForeignKey("projectid")]
        public virtual ICollection<TaskMaster> ProjectMasters { get; set; }
    }
}

