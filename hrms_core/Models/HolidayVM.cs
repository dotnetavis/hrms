﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class HolidayVM:CommonAttributeModel
    {
        public HolidayCalender holidayCalender { get; set; }
        public HolidayCalenderDates holidayCalenderDates { get; set; }
    }
}
