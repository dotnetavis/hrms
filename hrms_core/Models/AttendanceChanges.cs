﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class AttendanceChanges
    {
        public int id { get; set; }
        public DateTime attendancedate { get; set; }
        public string beforeintime { get; set; }
        public string beforeouttime { get; set; }
        public string afterintime { get; set; }
        public string afterouttime { get; set; }
        public string associate_code { get; set; }
        public string associate_name { get; set; }
        public string changed_by { get; set; }
        public DateTime changed_on { get; set; }
    }
}
