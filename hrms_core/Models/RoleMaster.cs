﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class RoleMaster:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage ="Role Name Required")]
        public string role_name { get; set; }
        public string role_description { get; set; }
        [ForeignKey("role_id")]
        public virtual ICollection<RoleMenuDetails> RoleMenuDetails { get; set; }
        [ForeignKey("role_id")]
        public virtual ICollection<AssociateMain> AssociateMainDetails { get; set; }
    }
}
