﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class HolidayServices : IHolidayServices
    {
        private readonly ApplicationDbContext _db;
        public HolidayServices(ApplicationDbContext options)
        {
            _db = options;
        }
        public string SaveEditHolidayCalnderDetails(HolidayCalenderModel model)
        {
            string Result = string.Empty;
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    try
                    {
                        if (model.id > 0)
                        {
                            var Update = _db.HolidayCalenders.FirstOrDefault(x => x.id == model.id);
                            if (Update != null)
                            {
                                Update.id = model.id;
                                Update.holidaycolor = model.holidaycolor;
                                Update.calendername = model.calendername;
                                Update.calenderyear = model.calenderyear;
                                Update.offcolor = model.offcolor;
                                Update.weeklyoff = model.weeklyoff;
                                Update.allowedleave = model.allowedleave;
                                Update.calender_path = model.calender_path;
                                Update.updatedby = model.updatedby;
                                Update.createdby = model.createdby;
                                Update.createdbyname = model.createdbyname;
                                Update.updatedbyname = model.updatedbyname;
                                Update.updatedon = DateTime.Now;
                                Update.createdon = DateTime.Now;
                                Update.isactive = true;
                                _db.SaveChanges();
                            }
                        }
                        else
                        {
                            // Add
                            HolidayCalender holiday_obj = new HolidayCalender();
                            {
                                holiday_obj.holidaycolor = model.holidaycolor;
                                holiday_obj.calendername = model.calendername;
                                holiday_obj.calenderyear = model.calenderyear;
                                holiday_obj.createdby = model.createdby;
                                holiday_obj.offcolor = model.offcolor;
                                holiday_obj.weeklyoff = model.weeklyoff;
                                holiday_obj.allowedleave = model.allowedleave;
                                holiday_obj.calender_path = model.calender_path;
                                holiday_obj.updatedby = model.updatedby;
                                holiday_obj.createdbyname = model.createdbyname;
                                holiday_obj.updatedbyname = model.updatedbyname;
                                holiday_obj.updatedon = DateTime.Now;
                                holiday_obj.createdon = DateTime.Now;
                                holiday_obj.isactive = true;
                            }
                            _db.HolidayCalenders.Add(holiday_obj);
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.HolidayCalender_AddSuccess;
                        }
                    }
                    catch (Exception ex)
                    {
                        scope.Dispose();
                        CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                        Result = MessageHelper.HolidayCalender_AddError;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.HolidayCalender_AddError;
            }
            return Result;
        }

        public List<HolidayCalenderModel> getHolidayCalender()
        {
            HolidayCalenderModel model = new HolidayCalenderModel();
            try
            {
                model.HolidayCalenderModelList = _db.HolidayCalenders.Select(obj => new HolidayCalenderModel
                {
                    id = obj.id,
                    holidaycolor = obj.holidaycolor,
                    calendername = obj.calendername,
                    calenderyear = obj.calenderyear,
                    offcolor = obj.offcolor,
                    weeklyoff = obj.weeklyoff,
                    allowedleave = obj.allowedleave,
                    calender_path = obj.calender_path,
                    createdby = obj.createdby,
                    updatedby = obj.updatedby,
                    createdbyname = obj.createdbyname,
                    updatedbyname = obj.updatedbyname,
                    updatedon = obj.updatedon,
                    createdon = obj.createdon,
                    isactive = true
                }).ToList();
                return model.HolidayCalenderModelList;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return null;
            }
        }
    }
}








