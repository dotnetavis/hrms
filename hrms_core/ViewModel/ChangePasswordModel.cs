﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class ChangePasswordModel : UserModel
    {
        public List<ChangePasswordModel> ChangePasswordModelList { get; set; }
   
        public ChangePasswordModel()
        {
            ChangePasswordModelList = new List<ChangePasswordModel>();
        }
    }
}

