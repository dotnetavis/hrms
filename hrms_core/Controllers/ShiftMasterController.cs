﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace hrms_core.Controllers
{
    public class ShiftMasterController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IShiftServices shiftServices => new ShiftServices(_db);
        public ShiftMasterController(ApplicationDbContext options) => _db = options;


        [HttpGet]
        public IActionResult Shift()
        {
            // TODO : Check the Accessbility and do the needful
            return View();
        }
    }
}