﻿using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services
{
    public class EmailService : IEmailService
    {
        static bool mailSent = false;
        private readonly SendMail ec;

        public EmailService(IOptions<SendMail> emailConfig)
        {
            this.ec = emailConfig.Value;
        }

        public bool SendEmailAsync(String email, String subject, String _message)
        {
            try
            {
                // Command-line argument must be the SMTP host.
                SmtpClient client = new SmtpClient("smtp.gmail.com",587);
                // Specify the email sender.
                // Create a mailing address that includes a UTF8 character
                // in the display name.
                MailAddress from = new MailAddress("nagesh@avissol.com","Nagesh " + (char)0xD8 + " Srivastava",
                System.Text.Encoding.UTF8);
                // Set destinations for the email message.
                MailAddress to = new MailAddress("nagesh@avissol.com");
                // Specify the message content.
                MailMessage message = new MailMessage(from, to);
                message.Body = "This is a test email message sent by an application. ";
                // Include some non-ASCII characters in body and subject.
                string someArrows = new string(new char[] { '\u2190', '\u2191', '\u2192', '\u2193' });
                message.Body += Environment.NewLine + someArrows;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.Subject = "test message 1" + someArrows;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                // Set the method that is called back when the send operation ends.
                // The userState can be any object that allows your callback 
                // method to identify this send operation.
                // For this example, the userToken is a string constant.
                string userState = "test message1";
                client.SendAsync(message, userState);
                Console.WriteLine("Sending message... press c to cancel mail. Press any other key to exit.");
                string answer = Console.ReadLine();
                // If the user canceled the send, and mail hasn't been sent yet,
                // then cancel the pending operation.
                if (answer.StartsWith("c") && mailSent == false)
                {
                    client.SendAsyncCancel();
                }
                // Clean up.
                message.Dispose();

                
                return true;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }
    }
}
