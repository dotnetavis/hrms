﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services
{
    public class LoginServices : ILoginServices
    {

        private readonly ApplicationDbContext _db;
        public LoginServices(ApplicationDbContext options) => _db = options;

        public List<LoginModel> doLogin(LoginModel model)
        {

            try
            {
                if(!string.IsNullOrWhiteSpace(model.username) && !string.IsNullOrWhiteSpace(model.userpass))
                {
                    model.LoginModelList = (from user in _db.Users
                                            join masters in _db.AssociateMasters.Where(x => x.isactive == true) on user.username equals masters.associate_code
                                            join associate in _db.AssociateMains.Where(x => x.isactive == true) on masters.id equals associate.associate_id
                                            select new LoginModel
                                            {
                                                username = user.username,
                                                userpass = user.userpass,
                                                official_email = associate.official_email,
                                                is_super_admin = masters.is_super_admin
                                            }).Where(x => (x.username.ToLower() == model.username.ToLower() || x.official_email.ToLower() == model.username.ToLower()) && x.userpass == CommonUtility.MD5Convertor(model.userpass)).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.LoginModelList;
        }

        /// <summary>
        /// Get the User Data,
        /// Created By : Bilal 
        ///On Date: 05/07/2018
        /// </summary>
        /// <returns></returns>
        public List<LoginModel> GetUserData()
        {
            LoginModel model = new LoginModel();
            try
            {
                model.LoginModelList = _db.Users.Select(obj => new LoginModel
                {
                    id = obj.id,
                    username = obj.username,
                    userpass = obj.userpass
                }).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.LoginModelList;
        }

        /// <summary>
        /// Get the All User Data,
        /// Created By : Bilal 
        ///On Date: 10/07/2018
        /// </summary>
        /// <returns></returns>
        public List<SessionModel> GetAllSessionData(string UserName, bool IsSuperAdmin)
        {
            SessionModel model = new SessionModel();
            List<PageMenuModel> _lstMenu = new List<PageMenuModel>();
            try
            {
                List<RoleMenuDetailModel> _data = new List<RoleMenuDetailModel>();
                if (IsSuperAdmin)
                {
                    _data = (from page in _db.PageMenus
                             select new RoleMenuDetailModel()
                             {
                                 isactive = page.isactive,
                                 is_selected = true,
                                 ControllerName = page.controller_name,
                                 ActionName = page.action_name,
                                 ModuleName = page.module_name,
                                 MenuName = page.menu_name
                             }).OrderBy(x => x.ModuleName).ToList();
                }
                else
                {
                    _data = (from associate in _db.AssociateMasters.Where(x => x.associate_code == UserName)
                             join main in _db.AssociateMains on associate.id equals main.associate_id
                             join role in _db.role_menu_details.Where(x => x.is_selected == true) on main.role_id equals role.role_id
                             join page in _db.PageMenus on role.page_menu_id equals page.id
                             select new RoleMenuDetailModel()
                             {
                                 page_menu_id = role.page_menu_id,
                                 isactive = role.isactive,
                                 is_selected = role.is_selected,
                                 role_id = main.role_id,
                                 ControllerName = page.controller_name,
                                 ActionName = page.action_name,
                                 ModuleName = page.module_name,
                                 MenuName = page.menu_name
                             }).OrderBy(x => x.ModuleName).ToList();
                }

                if (_data != null && _data.Count > 0)
                {
                    foreach (var item in _data)
                    {
                        _lstMenu.Add(new PageMenuModel
                        {
                            menu_name = item.MenuName,
                            controller_name = item.ControllerName,
                            action_name = item.ActionName,
                            module_name = item.ModuleName,
                            is_selected = item.is_selected,
                            isactive = item.isactive
                        });
                    }
                }
                var data = (from mas in _db.AssociateMasters.Where(x => x.isactive == true)
                            join main in _db.AssociateMains.Where(x => x.isactive == true) on mas.id equals main.associate_id
                            join usr in _db.Users.Where(x => x.username == UserName) on mas.associate_code equals usr.username
                            join desig in _db.Designations on main.designation_id equals desig.id
                            join dept in _db.Departments on main.department_id equals dept.id
                            join role in _db.role_master on main.role_id equals role.id
                            from profile in _db.ProfilePages.Where(x => x.associate_id == mas.id).DefaultIfEmpty()
                            from addReport in _db.AssociateAdditionalInfos.Where(x => x.associate_id == main.reportingperson_id).DefaultIfEmpty()
                            select new SessionModel
                            {
                                ProfilePic = profile.image_path,
                                associate_id = mas.id,
                                user_id = usr.id,
                                associate_code = mas.associate_code,
                                username = usr.username,
                                associate_name = mas.associate_name,
                                designation_id = main.designation_id,
                                designation_text = desig.designationtext,
                                department_id = main.department_id,
                                department_text = dept.departmenttext,
                                reporting_person_id=main.reportingperson_id.Value,
                                //reporting_person_id = _db.AssociateMains.FirstOrDefault(x => x.associate_id == main.reportingperson_id && x.isactive == true).associate_id == 0 ? 0 : main.reportingperson_id.Value,
                                //reporting_person_email = _db.AssociateMains.FirstOrDefault(x => x.associate_id == main.reportingperson_id && x.isactive == true).official_email,
                                //head_of_department_email = _db.AssociateMains.FirstOrDefault(a => a.department_id == main.department_id && a.is_hod == true).official_email,
                                personal_email = main.personal_email,
                                official_email = main.official_email,
                                role_id = role.id,
                                role_name = role.role_name,
                                associate_since = mas.createdon,
                                PageMenuModelList = _data,
                            }).ToList();


                if (data != null)
                {
                    //managing reporting person detail with active state.
                    if (data.FirstOrDefault().reporting_person_id > 0)
                    {
                        var reportingPersonData = _db.AssociateMains.FirstOrDefault(x => x.associate_id == data.FirstOrDefault().reporting_person_id && x.isactive == true);
                        if (reportingPersonData != null)
                            data.FirstOrDefault().reporting_person_email = reportingPersonData.official_email;
                        else
                        {
                            data.FirstOrDefault().reporting_person_id = 0;
                            data.FirstOrDefault().reporting_person_email = "";
                        }
                    }
                    //managing head of department Details
                    if (data.FirstOrDefault().department_id > 0)
                    {
                        var headOfDepartmentDetails = _db.AssociateMains.FirstOrDefault(a => a.department_id == data.FirstOrDefault().department_id && a.is_hod == true && a.isactive == true);
                        if (headOfDepartmentDetails != null)
                            data.FirstOrDefault().head_of_department_email = headOfDepartmentDetails.official_email;
                        else
                            data.FirstOrDefault().head_of_department_email = "";
                    }
                    model.SessionModelList = data;
                    var additional = (from assoc in _db.AssociateMasters.Where(x => x.id == data.FirstOrDefault().associate_id)
                                      join addReport in _db.AssociateAdditionalInfos
                                      on assoc.id equals addReport.associate_id
                                      select new SessionModel
                                      {
                                          associate_name = assoc.associate_name,
                                          reporting_person_id = addReport.reporting_person_id
                                      }).ToList();
                    List<String> additional_Reporting = new List<string>();
                    if (additional != null && additional.Count > 0)
                    {
                        foreach (var item in additional)
                        {
                            if (item != null)
                            {
                                var _daa = _db.AssociateMains.FirstOrDefault(c => c.associate_id == item.reporting_person_id && c.isactive == true);
                                if (_daa != null)
                                {
                                    if (!String.IsNullOrWhiteSpace(_daa.official_email))
                                    {
                                        additional_Reporting.Add(_daa.official_email);
                                        CommonUtility.WriteMailLogs(_daa.official_email, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name));
                                    }

                                }
                            }
                        }
                        if (model.SessionModelList != null && model.SessionModelList.Count > 0)
                        {
                            model.SessionModelList.FirstOrDefault().addition_reporting = additional_Reporting;
                            int ReportingPersonId = model.SessionModelList.FirstOrDefault().reporting_person_id;
                            var ReportingPersonData = (from mas in _db.AssociateMasters.Where(x => x.id == ReportingPersonId && x.isactive == true)
                                                       join main in _db.AssociateMains.Where(x => x.isactive == true) on mas.id equals main.associate_id
                                                       select new SessionModel
                                                       {
                                                           associate_name = mas.associate_name,
                                                           official_email = main.official_email
                                                       }).ToList();
                            if (ReportingPersonData != null && ReportingPersonData.Count > 0)
                            {
                                model.SessionModelList.FirstOrDefault().reporting_person = ReportingPersonData.FirstOrDefault().associate_name;
                                model.SessionModelList.FirstOrDefault().reporting_person_email = ReportingPersonData.FirstOrDefault().official_email;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        if (model.SessionModelList != null && model.SessionModelList.Count > 0)
                        {
                            model.SessionModelList.FirstOrDefault().addition_reporting = additional_Reporting;
                            int ReportingPersonId = model.SessionModelList.FirstOrDefault().reporting_person_id;
                            var ReportingPersonData = (from mas in _db.AssociateMasters.Where(x => x.id == ReportingPersonId && x.isactive == true)
                                                       join main in _db.AssociateMains.Where(x => x.isactive == true) on mas.id equals main.associate_id
                                                       select new SessionModel
                                                       {
                                                           associate_name = mas.associate_name,
                                                           official_email = main.official_email
                                                       }).ToList();
                            if (ReportingPersonData != null && ReportingPersonData.Count > 0)
                            {
                                model.SessionModelList.FirstOrDefault().reporting_person = ReportingPersonData.FirstOrDefault().associate_name;
                                model.SessionModelList.FirstOrDefault().reporting_person_email = ReportingPersonData.FirstOrDefault().official_email;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.SessionModelList;
        }
    }
}
