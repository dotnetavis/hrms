﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IReasonOfLeavingServices
    {
       void getReasonOfLeavingMasterList();
       void insertReasonOfLeavingMasterList();
       void editReasonOfLeavingMasterList(int reasonid);
       void updateReasonOfLeavingMasterList();
       void updateStatusReasonOfLeavingMasterList();

    }
}
