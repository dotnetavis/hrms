﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class ProjectServices : IProjectServices
    {
        private readonly ApplicationDbContext _db;
        public ProjectServices(ApplicationDbContext options)
        {
            _db = options;
        }
        /// <summary>
        /// Get the Project Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        public List<ProjectMasterModel> GetProject(int AssociateId)
        {
            ProjectMasterModel model = new ProjectMasterModel();
            List<int> Associate_List = new List<int>();
            try
            {
                var data = _db.AssociateMains.Where(x => x.reportingperson_id == AssociateId).ToList();
                if(data!=null && data.Count>0)
                {
                    model.ProjectMasterModelList=(from project in _db.ProjectMasters.Where(x=>x.isactive ==true)
                                                  join assign_project in _db.projectdeptassigns.Where(x=>x.isactive==true && x.is_selected==true) on project.id equals assign_project.project_id 
                                                  join main in _db.AssociateMains.Where(x=>x.isactive==true && x.reportingperson_id== AssociateId) on assign_project.department_id equals main.department_id
                                                  select new ProjectMasterModel
                                                  {
                                                      id = project.id,
                                                      projecttext = project.projecttext,
                                                      completiondate = project.completiondate,
                                                      expectedcompletiondate = project.expectedcompletiondate,
                                                      datestart = project.datestart,
                                                      isactive = project.isactive,
                                                      createdon = project.createdon,
                                                      createdby = project.createdby,
                                                      updatedon = project.updatedon,
                                                      updatedby = project.updatedby,
                                                      createdbyname = project.createdbyname,
                                                      updatedbyname = project.updatedbyname
                                                  }).Distinct().OrderBy(x => x.projecttext).ToList();
                }
                else
                {
                    //model.ProjectMasterModelList = (from project in _db.ProjectMasters.Where(x => x.isactive == true)
                    //                                join task in _db.TaskMasters.Where(x => x.isactive == true) on project.id equals task.projectid
                    //                                join assign_task in _db.TaskAssigns.Where(x => x.isactive == true && x.associate_id == AssociateId && x.is_selected == true)
                    //                                 on task.id equals assign_task.task_id
                    //                                select new ProjectMasterModel
                    //                                {
                    //                                    id = project.id,
                    //                                    projecttext = project.projecttext,
                    //                                    associate_id = assign_task.associate_id,
                    //                                    completiondate = project.completiondate,
                    //                                    expectedcompletiondate = project.expectedcompletiondate,
                    //                                    datestart = project.datestart,
                    //                                    isactive = project.isactive,
                    //                                    createdon = project.createdon,
                    //                                    createdby = project.createdby,
                    //                                    updatedon = project.updatedon,
                    //                                    updatedby = project.updatedby,
                    //                                    createdbyname = project.createdbyname,
                    //                                    updatedbyname = project.updatedbyname
                    //                                }).Distinct().OrderBy(x => x.projecttext).ToList();
                    model.ProjectMasterModelList = (from project in _db.ProjectMasters.Where(x => x.isactive == true)
                                                    join assign_project in _db.projectdeptassigns.Where(x => x.isactive == true && x.is_selected==true) on project.id equals assign_project.project_id
                                                    join main in _db.AssociateMains.Where(x => x.isactive == true && x.associate_id == AssociateId) on assign_project.department_id equals main.department_id
                                                    select new ProjectMasterModel
                                                    {
                                                        id = project.id,
                                                        projecttext = project.projecttext,
                                                        completiondate = project.completiondate,
                                                        expectedcompletiondate = project.expectedcompletiondate,
                                                        datestart = project.datestart,
                                                        isactive = project.isactive,
                                                        createdon = project.createdon,
                                                        createdby = project.createdby,
                                                        updatedon = project.updatedon,
                                                        updatedby = project.updatedby,
                                                        createdbyname = project.createdbyname,
                                                        updatedbyname = project.updatedbyname
                                                    }).Distinct().OrderBy(x => x.projecttext).ToList();
                }
                
              
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.ProjectMasterModelList;
        }

        /// <summary>
        /// Save the Project Data,
        /// Created By : Bilal 
        ///On Date: 22/05/2018
        /// </summary>
        /// <returns></returns>
        public string SaveProject(ProjectMasterModel model)
        {
            string Result = string.Empty;
            ProjectMaster projectMaster = new ProjectMaster();
            try
            {
                //Check for Duplicate Record
                bool bool_sameNameExists = _db.ProjectMasters.Any(x => x.projecttext.ToLower().Trim() == model.projecttext.ToLower().Trim());
                if (bool_sameNameExists)
                {
                    Result = MessageHelper.Project_Duplicate;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            projectMaster.projecttext = model.projecttext;
                            projectMaster.datestart = model.datestart;
                            projectMaster.departmentid = model.departmentid;
                            projectMaster.expectedcompletiondate = model.expectedcompletiondate;
                            projectMaster.completiondate = model.completiondate;
                            projectMaster.isactive = true;
                            projectMaster.createdon = DateTime.Now;
                            projectMaster.createdby = model.createdby;
                            projectMaster.updatedon = DateTime.Now;
                            projectMaster.updatedby = model.updatedby;
                            projectMaster.createdbyname = model.createdbyname;
                            projectMaster.updatedbyname = model.updatedbyname;
                            _db.ProjectMasters.Add(projectMaster);
                            _db.SaveChanges();

                            foreach (var item in model.DepartmentList)
                            {
                                ProjectDeptAssign projectdept = new ProjectDeptAssign();
                                projectdept.createdby = model.createdby;
                                projectdept.createdbyname = model.createdbyname;
                                projectdept.createdon = model.createdon;
                                projectdept.updatedby = model.updatedby;
                                projectdept.updatedbyname = model.updatedbyname;
                                projectdept.updatedon = model.updatedon;
                                projectdept.isactive = true;
                                projectdept.project_id = projectMaster.id;
                                projectdept.department_id = item.id;
                                projectdept.is_selected = item.is_selected;
                                _db.projectdeptassigns.Add(projectdept);
                                _db.SaveChanges();
                            }

                            scope.Complete();
                            Result = MessageHelper.Project_AddSuccess;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.Project_AddError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Project_AddError;
            }
            return Result;
        }

        /// <summary>
        /// Update the Project Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        public string UpdateProject(ProjectMasterModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.ProjectMasters.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    //Check for Duplicate Record
                    bool bool_sameNameExists = _db.ProjectMasters.Any(x => x.projecttext.ToLower().Trim() == model.projecttext.ToLower().Trim() && x.id != model.id);
                    if (bool_sameNameExists)
                    {
                        Result = MessageHelper.Project_Duplicate;
                    }
                    else
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            try
                            {
                                data.projecttext = model.projecttext;
                                data.datestart = model.datestart;
                                //data.departmentid = model.departmentid;
                                data.expectedcompletiondate = model.expectedcompletiondate;
                                data.completiondate = model.completiondate;
                                data.createdon = model.createdon;
                                
                                data.createdby = model.createdby;
                                data.updatedon = DateTime.Now;
                                data.updatedby = model.updatedby;
                                data.createdbyname = model.createdbyname;
                                data.updatedbyname = model.updatedbyname;
                                _db.SaveChanges();

                                var _RecordForDelete = _db.projectdeptassigns.Where(x => x.project_id == data.id).ToList();
                                if (_RecordForDelete != null)
                                {
                                    _db.projectdeptassigns.RemoveRange(_RecordForDelete);
                                    _db.SaveChanges();
                                }
                                foreach (var item in model.DepartmentList)
                                {
                                    ProjectDeptAssign projectdept = new ProjectDeptAssign();
                                    projectdept.createdby = model.createdby;
                                    projectdept.createdbyname = model.createdbyname;
                                    projectdept.createdon = model.createdon;
                                    projectdept.updatedby = model.updatedby;
                                    projectdept.updatedbyname = model.updatedbyname;
                                    projectdept.updatedon = model.updatedon;
                                    projectdept.isactive = true;
                                    projectdept.project_id = data.id;
                                    projectdept.department_id = item.id;
                                    projectdept.is_selected = item.is_selected;
                                    _db.projectdeptassigns.Add(projectdept);
                                    _db.SaveChanges();
                                }

                                scope.Complete();
                                Result = MessageHelper.Project_AddSuccess;
                            }
                            catch (Exception ex)
                            {
                                scope.Dispose();
                                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                Result = MessageHelper.Project_AddError;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Project_AddError;
            }
            return Result;
        }

        /// <summary>
        /// Disable the Designation Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        public string DisableProject(ProjectMasterModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.ProjectMasters.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = false;
                            data.createdon = model.createdon;
                            data.createdby = model.createdby;
                            data.updatedon = DateTime.Now;
                            data.updatedby = model.updatedby;
                            data.createdbyname = model.createdbyname;
                            data.updatedbyname = model.updatedbyname;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Project_Disable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }
        /// <summary>
        /// Enable the Designation Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        public string EnableProject(ProjectMasterModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.ProjectMasters.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = true;
                            data.createdon = model.createdon;
                            data.createdby = model.createdby;
                            data.updatedon = DateTime.Now;
                            data.updatedby = model.updatedby;
                            data.createdbyname = model.createdbyname;
                            data.updatedbyname = model.updatedbyname;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Project_Enable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }

        public List<ProjectMasterModel> GetProjectList()
        {
            ProjectMasterModel model = new ProjectMasterModel();
            try
            {
                model.ProjectMasterModelList = (from project in _db.ProjectMasters


                                                select new ProjectMasterModel
                                                {
                                                    id = project.id,
                                                    projecttext = project.projecttext,
                                                    completiondate = project.completiondate,
                                                    expectedcompletiondate = project.expectedcompletiondate,
                                                    datestart = project.datestart,
                                                    isactive = project.isactive,
                                                    createdon = project.createdon,
                                                    createdby = project.createdby,
                                                    updatedon = project.updatedon,
                                                    updatedby = project.updatedby,
                                                    createdbyname = project.createdbyname,
                                                    updatedbyname = project.updatedbyname
                                                }).OrderByDescending(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.ProjectMasterModelList;
        }
    }
}
