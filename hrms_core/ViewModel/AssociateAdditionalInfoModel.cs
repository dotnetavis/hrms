﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class AssociateAdditionalInfoModel : AssociateAdditionalInfo
    {
        public List<AssociateAdditionalInfoModel> AssociateAdditionalInfoModelList { get; set; }
        public List<AssociateMasterModel> AssociateMasterModelList { get; set; }
        public AssociateAdditionalInfoModel()
        {
            AssociateAdditionalInfoModelList= new List<AssociateAdditionalInfoModel>();
            AssociateMasterModelList = new List<AssociateMasterModel>();
        }
    }
}
