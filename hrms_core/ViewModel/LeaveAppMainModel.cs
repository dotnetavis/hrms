﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class LeaveAppMainModel: LeaveAppMain
    {
        public List<LeaveAppMainModel> LeaveAppMainModelList { get; set; }
        public LeaveAppMainModel()
        {
            LeaveAppMainModelList = new List<LeaveAppMainModel>();
        }
        
    }
}
