﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class FnfModel: Fnf
    {
        public List<FnfModel> FnfModelList { get; set; }
        public FnfModel()
        {
            FnfModelList = new List<FnfModel>();
        }
    }
}
