﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IHeaderServices
    {
        List<HeaderModel> GetHeaderByAssociateId(int AssociateId);

        string Save_HeaderAssign(HeaderAssignModel model);
        string Update_HeaderAssign(HeaderAssignModel model);
        List<HeaderAssignModel> GetHeader_Assign(int associate_id);
        void GetLastPaySlpi_Id();
        void GetHeader_AssignByDeduction(int payslip_id);
        void Delete_HeaderAssign(int associate_id);
        void GetHeaders();
        void BindPaySlip();
        void BindPaySlipWithFilter(DateTime monthyear);
        void DeleteHeader(int headerid);
        void DeletePaySlip(int payslip_id);
        void SaveSalarySlip();
        void SaveSalarySlip_ammount();
        void CheakCurrentMonth_PaySlip(DateTime monthyear, int associate_id);
        void GetLeaveTaken(int payslip_id);
        void GetAbsent(int payslip_id, DateTime date_part);
        void DecryptPaySlip_Id(int payslip_id);
        List<PayElementModel> GetPayElementList();
        List<AssociateMasterModel> GetAllAssociates();
        List<HeaderModel> GetHeaderList();

        HeaderModel GetHearderById(int Id);


        /// <summary>
        /// Get the Header Data,
        /// Created By : Himanshu
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        List<HeaderModel> GetHeader();
        string SaveHeader(HeaderModel model);
        /// <summary>
        /// Save the Header Data,
        /// Created By : Himanshu
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        string UpdateHeader(HeaderModel model);

        /// <summary>
        /// Disable the Header Data,
        /// Created By : Himanshu 
        ///On Date: 18/06/2018
        /// </summary>
        /// <returns></returns>
        string DisableHeader(HeaderModel model);

        /// <summary>
        /// Enable the Header Data,
        /// Created By : Himanshu 
        ///On Date: 18/06/2018
        /// </summary>
        /// <returns></returns>
        string EnableHeader(HeaderModel model);
    }
}
