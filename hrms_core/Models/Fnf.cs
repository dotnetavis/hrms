﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class Fnf:CommonAttributeModel
    {
        [Key]
        public  int id { get; set; }     //primary key
        public virtual AssociateMaster associate { get; set; }    //foreign key
        public int desination_id { get; set; }
        public DateTime doj { get; set; }
        public DateTime dateof_resignation { get; set; }
        public DateTime dateof_relieving { get; set; }
        public int working_day_month { get; set; }
        public int notice_period { get; set; }
        public int notice_served { get; set; }
        public int short_notice_day { get; set; }
        public int leave_allowed { get; set; }
        public int leave_taken { get; set; }
        public int leave_deduction { get; set; }
        public int carry_home_salary { get; set; }
        public int payable_salary { get; set; }
        public int full_n_final_payment{ get; set; }
        

    }
}
