﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class ConferenceRoom : CommonAttributeModel
    {
        [Key]
        public int even_id { get; set; }
        [ForeignKey("associate_id")]
        public int associate_id { get; set; }
        [Required(ErrorMessage ="Event Subject is required")]
        public string subject { get; set; }
        public string description { get; set; }
        [Required(ErrorMessage ="Starting date is required")]
        public DateTime startsession { get; set; }
        [Required(ErrorMessage = "Start time is required")]
        public TimeSpan starttime { get; set; }
        [Required(ErrorMessage ="End time is required")]
        public TimeSpan endtime { get; set; }
        public string themecolor { get; set; }
        public bool isfullday { get; set; }
    }
}
