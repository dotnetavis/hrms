﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using hrms_core.ViewModel;
using System.Text;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;

namespace hrms_core.Controllers
{
    //this API should be running at evening.
    [Produces("application/json")]
    [Route("api/Auto")]
    public class RefreshLeaveMailController : Controller
    {
        private readonly ApplicationDbContext _db;
        public IConfiguration Configuration { get; }
        private ILeaveServices leaveservices => new LeaveServices(_db);
        public DateTime currentdate = DateTime.Now;
        public StringBuilder result = new StringBuilder();
        public RefreshLeaveMailController(ApplicationDbContext _Db, IConfiguration configuration)
        {
            _db = _Db;
            Configuration = configuration;
        }
        [HttpGet]
        public string Index()
        {
            CommonUtility.WriteMsgLogs("@@@ AUTOAPPROVE API START EXECUTION TIME" + currentdate.ToString(), "     ==>  API_RefreshLeaveMailController");
            LeaveApplicationModel model = new LeaveApplicationModel();
            model.LeaveApplicationModelList = (from dbleave in _db.LeaveApplications.Where(x=>x.isactive==true)
                                               join dbltype in _db.LeaveTypeMasters on dbleave.leavetype_id equals dbltype.id
                                               join dbmaster in _db.AssociateMasters.Where(x => x.isactive == true) on dbleave.createdby equals dbmaster.id
                                               select new LeaveApplicationModel()
                                               {
                                                   id = dbleave.id,
                                                   leaveappliedby = dbleave.leaveappliedby,
                                                   employeereason = dbleave.employeereason,
                                                   status = dbleave.status,
                                                   associatecode = dbmaster.associate_code,
                                                   associatename = dbmaster.associate_name,
                                                   createdby = dbleave.createdby,
                                                   leavetype_id = dbleave.leavetype_id,
                                                   notificationdate = dbleave.notificationdate,
                                                   startdate = dbleave.startdate,
                                                   enddate = dbleave.enddate,
                                                   startsession = dbleave.startsession,
                                                   endsession = dbleave.endsession,
                                                   requested_no_of_leaves = dbleave.requested_no_of_leaves,
                                                   createdbyname = dbleave.createdbyname,
                                                   updatedbyname = dbleave.updatedbyname,
                                                   createdon = dbleave.createdon,
                                                   updatedon = dbleave.updatedon,
                                                   isactive = dbleave.isactive,
                                                   leave_type_name = dbltype.leave_code
                                               }).Where(x => x.status == 0 && x.notificationdate <= currentdate && x.startdate < currentdate).ToList();
            if (model.LeaveApplicationModelList.Count != 0 && model.LeaveApplicationModelList != null)
            {
                try
                {
                    foreach (var item in model.LeaveApplicationModelList)
                    {
                        var ApprovedBy = _db.AssociateMasters.FirstOrDefault(x => x.associate_name == "Super Admin");
                        if (ApprovedBy != null)
                        {
                            item.approvedby = ApprovedBy.id;
                            item.associatename = ApprovedBy.associate_name;
                        }
                        var data = _db.LeaveApplications.FirstOrDefault(x => x.id == item.id);
                        if (data != null)
                        {
                            data.status = 1;
                            data.approvedby = item.approvedby;
                            data.employerreason = "Auto Approved";
                            _db.SaveChanges();
                        }
                        LeaveAppMainModel leave = new LeaveAppMainModel();
                        leave.isactive = item.isactive;
                        leave.leaveapp_id = item.id;
                        leave.createdby = item.createdby;
                        leave.updatedby = item.updatedby;
                        leave.updatedbyname = item.updatedbyname;
                        leave.createdbyname = item.createdbyname;
                        leave.createdon = item.createdon;
                        leave.updatedon = DateTime.Now;
                        leave.leave_date = item.notificationdate.Date;
                        leave.totalleave = item.requested_no_of_leaves;
                        leave.isactive = true;
                        bool result1 = leaveservices.SaveLeaveAppMains(leave);
                        List<int> reporting_ids = new List<int>();
                        int associate_id = _db.LeaveApplications.FirstOrDefault(x => x.id == item.id).createdby;
                        reporting_ids = leaveservices.GetReportingData(associate_id);
                        var associate_email = _db.AssociateMains.FirstOrDefault(x => x.associate_id == associate_id);
                        if (associate_email != null)
                        {
                            item.employer_associate_email = associate_email.official_email;
                            item.official_email = associate_email.official_email;

                            item.hod_email = _db.AssociateMains.FirstOrDefault(x => x.associate_id == associate_email.reportingperson_id).official_email;
                        }
                        var associate = _db.AssociateMasters.FirstOrDefault(x => x.id == associate_id);
                        if (associate != null)
                        {
                            item.employer_associate_name = associate.associate_name;
                            item.username = associate.associate_name;
                            item.employer_associate_code = associate.associate_code;
                        }

                        List<string> reporting_emails = new List<string>();
                        reporting_emails = _db.AssociateMains.Where(x => reporting_ids.Contains(x.associate_id)).Select(x => x.official_email).ToList();
                        if (reporting_emails != null && reporting_emails.Count > 0)
                        {
                            item.Mail_to_emails = reporting_emails;
                        }
                        item.employerreason = "Automatically Approved by the Admin.";
                        item.status_text = "Auto Approved";
                        string result = string.Empty;
                        bool mail_result = leaveservices.SendLeaveApprovalDisapprovalReport(item, HttpContext, Configuration);
                        if (mail_result)
                        {
                            result = "leave by " + item.associatename + " from " + item.startdate + " to " + item.endsession + " is Automatically Approved";
                            CommonUtility.WriteMsgLogs("leave by " + item.employer_associate_name + " from " + item.startdate + " to " + item.endsession + " is Automatically Approved", "API_RefreshLeaveMailController");
                        }
                        else
                        {
                            CommonUtility.WriteMsgLogs("leave by " + item.employer_associate_name + " from " + item.startdate + " to " + item.endsession + " can't be Approved", "API_RefreshLeaveMailController");
                        }

                    }
                }
                catch (Exception ex)
                {
                    CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                }


            }
            else
            {
                CommonUtility.WriteMsgLogs("No pending leaves for the day " + DateTime.Now.ToString(), "API_RefreshLeaveMailController");
            }
            CommonUtility.WriteMsgLogs("@@@ AUTOAPPROVE API END EXECUTION TIME" + DateTime.Now.ToString(), "     ==>  API_RefreshLeaveMailController");
            return result.ToString();
        }
    }
}