﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class Shift:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage = "Enter Shift Name")]
        public string shifttext { get; set; }
        public DateTime starttime { get; set; }
        public DateTime endtime { get; set; }
        public DateTime totaltime { get; set; }
        public DateTime breaktime { get; set; }
        public DateTime networkingtime { get; set; }
        public string tolerance { get; set; }
        public bool nightshift { get; set; }
    }
}