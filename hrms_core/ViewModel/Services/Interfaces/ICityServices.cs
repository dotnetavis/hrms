﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface ICityServices
    {
        /// <summary>
        /// Get the City Data,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        List<CityModel> GetCity();

        /// <summary>
        /// Get the City Data Only Active,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        List<CityModel> GetCity_Active();

        /// <summary>
        /// Get the City Data By StateId Only Active,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <param name="StateId"></param>
        /// <returns></returns>
        List<CityModel> GetCity_ByStateId(int StateId);
        string UpdateCity(CityModel model);
        string SaveCity(CityModel model);
        string DisableCity(CityModel model);
        string EnableCity(CityModel model);
    }
}
