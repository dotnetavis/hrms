﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IPayElementServices
    {
        void insertPayElement(int associate_id);
        void getPayElementList();
        void getPayElementListOnP_Id(int id);
        void insertSalarySlip();
        void getSalarySlip(int month_year, int associate_id, string username );
        void checkDuplicacySalarySlip(DateTime month_year);

        
    }
}
