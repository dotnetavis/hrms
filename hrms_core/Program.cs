﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace hrms_core
{
    public class Program
    {
        public const string VersionNo = "2.0.0";
        public const string VersionDate = "(18-October-2021 03:40 PM) ";
        public static void Main(string[] args)
        {
            var Configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: false, reloadOnChange: true).Build();
            Log.Logger = new LoggerConfiguration()
              .ReadFrom.Configuration(Configuration, "Serilog")
              //.WriteTo.Console()
              .WriteTo.File($"logs/Output.log", fileSizeLimitBytes: 10485760, rollOnFileSizeLimit: true, rollingInterval: RollingInterval.Day)
              .Enrich.FromLogContext()
              .Enrich.WithProperty("Version", "1.0.0")
              .CreateLogger();
            try
            {
                Log.Information($"({typeof(Program).Name} ==> {MethodBase.GetCurrentMethod().Name}) | Starting up");
                Log.Information("=================================================================================");
                Log.Information("::     :: ::::::::  ::     ::  ::::::");
                Log.Information("::     :: ::     :: :::   ::: ::    ::");
                Log.Information("::     :: ::     :: :::: :::: ::");
                Log.Information("::::::::: ::::::::  :: ::: ::  ::::::");
                Log.Information("::     :: ::   ::   ::     ::       ::");
                Log.Information("::     :: ::    ::  ::     :: ::    ::");
                Log.Information("::     :: ::     :: ::     ::  ::::::");
                Log.Information("=================================================================================");
                Log.Information($" Version : {VersionNo} {VersionDate}           Dated : {DateTime.Now}       ");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal($"({typeof(Program).Name} ==> {MethodBase.GetCurrentMethod().Name}) | {ex}");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }


        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args).UseSerilog().ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.ConfigureKestrel(c => c.AddServerHeader = false)
                .UseStartup<Startup>().UseKestrel(options =>
                {
                    if (Convert.ToBoolean(Startup.Configuration["ApplicationSetting:RunOnSocket"]))
                    {
                        options.ListenUnixSocket(Convert.ToString(Startup.Configuration["ApplicationSetting:SocketPath"]));
                    }
                    else
                    {
                        Log.Error($"({typeof(Program).Name} ==> {System.Reflection.MethodBase.GetCurrentMethod().Name}) | Application Running on  Port. Please Check the Appsetting.json File");
                        options.ListenAnyIP(Convert.ToInt32(Startup.Configuration["ApplicationSetting:HttpPort"]));
                    }
                });
            });
    }
}
