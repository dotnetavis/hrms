﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class UserModel : User
    {
        [MaxLength(50)]
        [DataType(DataType.Password)]
        [CompareAttribute("newpassword", ErrorMessage = "Password not mismatch.")]
        public string confirmnewpassword { get; set; }
        [Required]
        [MaxLength(50)]
        [DataType(DataType.Password)]
        public string newpassword { get; set; }
        public string official_email { get; set; }
        public List<UserModel> UserModelList { get; set; }
        public UserModel()
        {
            UserModelList = new List<UserModel>();
        }
    }
}
