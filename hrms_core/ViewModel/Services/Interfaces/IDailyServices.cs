﻿using hrms_core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IDailyServices
    {
        int SaveData(DARVM model);
        List<DailyActivityReportModel> GetAllDailyReportDetails();
        DailyActivityReportModel GetActivityReportById(int ID);
        int EditDailyReport(DARVM model);
        List<DailyReportDetailModel> GetAllDailyReport();
        DailyReportDetailModel GetActivityById(int ID);

        /// <summary>
        ///  Get Daily Activity Details Data,
        /// Created By : Bilal 
        ///On Date: 09/07/2018
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        List<DailyReportDetailModel> GetDARDetails(int ID);
        bool SendDARReport(DARVM userModel, HttpContext httpContext, IConfiguration configuration);
    }
}
