﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace hrms_core.Controllers
{
    [Produces("application/json")]
    [Route("api/Greeting")]
    public class UpCommingGreetingsController : Controller
    {
        private readonly ApplicationDbContext _Db;
        public IConfiguration Configuration { get; }
        private DateTime CurrentDate = DateTime.Now;
        private DateTime LastDate;
        
        public UpCommingGreetingsController(ApplicationDbContext _db,IConfiguration configuration)
        {
            _Db = _db;
            Configuration = configuration;
        }
        [HttpGet]
        public string Index()
        {
            MailMessage mail = new MailMessage();
            string str = string.Empty;
            str = CurrentDate.Date.ToString("dd-MM");
            var _emailData = _Db.EmailConfigurations.FirstOrDefault();
            //NetworkCredential net = new NetworkCredential(Configuration.GetConnectionString("emailaddress"), Configuration.GetConnectionString("emailpassword"));
            AssociatePersonalModel model = new AssociatePersonalModel();
            LastDate = CurrentDate.AddDays(15);
            model.AssociatePersonalModelListWithBirthday = (from personal in _Db.AssociatePersonals.Where(x => (x.dob.Date.Month == CurrentDate.Date.Month && x.dob.Date.Day==CurrentDate.Date.Day))
                                                join master in _Db.AssociateMasters on personal.associate_id equals master.id
                                                select new AssociatePersonalModel()
                                                {
                                                    userName = master.associate_name,
                                                    dob = personal.dob.Date,

                                                }).ToList();
            model.AssociatePersonalModelList = (from personal in _Db.AssociatePersonals.Where(x => (x.anniversary.Value.Date.Month == CurrentDate.Date.Month && x.anniversary.Value.Date.Day == CurrentDate.Date.Day))
                                                join master in _Db.AssociateMasters on personal.associate_id equals master.id
                                                select new AssociatePersonalModel()
                                                {
                                                    userName = master.associate_name,
                                                    anniversary = personal.anniversary.Value.Date

                                                }).ToList();

            if (_emailData != null)
            {
                string FromEmail = _emailData.email_send_from;
                string Pass = _emailData.password;
                SmtpClient smtp = new SmtpClient(_emailData.smtp);
                mail.From = new MailAddress(FromEmail);
                string[] emails = Configuration.GetValue<string>("ConnectionStrings:GreetingMail").Split(';');
                if (emails != null && emails.Count() > 0)
                {
                    foreach (var email in emails)
                    {
                        mail.To.Add(email);
                    }
                    if(model.AssociatePersonalModelList.Count()!=0 || model.AssociatePersonalModelListWithBirthday.Count()!=0)
                    {
                        mail.Subject = "Employers Greeting";
                        StringBuilder mailstring = new StringBuilder();
                        mailstring.AppendLine(string.Format("<b>" + "Dear HR," + "</b><br/>" + "Kindly go through the records of the employers who are having their birthday or anniversary Today.<br/><br/>"));
                        if (model.AssociatePersonalModelList.Count()!=0)
                        {
                            mailstring.Append(String.Format("<table border='1'><tr><th> EMPLOYER NAME </th><th>ANNIVERSARY DATE</th></tr>"));
                            foreach (var item in model.AssociatePersonalModelList)
                            {
                                mailstring.Append(String.Format("<tr><td>{0}</td><td>{1}</td></tr>", item.userName, item.anniversary.Value.ToString("dd-MMM-yyyy")));
                            }
                            mailstring.Append("</table></br>");
                        }
                        if (model.AssociatePersonalModelListWithBirthday.Count() != 0)
                        {
                            mailstring.Append(String.Format("<table border='1'><tr><th> EMPLOYER NAME </th><th> DATE OF BIRTH </th></tr>"));
                            foreach (var item in model.AssociatePersonalModelListWithBirthday)
                            {
                                mailstring.Append(String.Format("<tr><td>{0}</td><td>{1}</td></tr>", item.userName, item.dob.ToString("dd-MMM-yyyy")));
                            }
                            mailstring.Append("</table></br>");
                        }
                        mailstring.Append("<b>Thanks and Regards,</b><br/><b>Development Team</b>");

                        mail.IsBodyHtml = true;
                        mail.Body = mailstring.ToString();
                        smtp.Host = _emailData.smtp;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.UseDefaultCredentials = false;
                        smtp.Port = _emailData.port;
                        smtp.Credentials = new System.Net.NetworkCredential(FromEmail, Pass);
                        smtp.EnableSsl = _emailData.ssl;
                        System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                 System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };
                        smtp.Send(mail);
                        str = MessageHelper.EmailSent;
                        CommonUtility.WriteMsgLogs(str, "API_UpCommingGreetings");
                        return str;
                    }
                    else
                    {
                        str = "no greeting available for the day";
                        CommonUtility.WriteMsgLogs(str, "API_UpCommingGreetings");
                        return str;
                    }
                    
                }
                else
                {
                    str = "no greeting mail id present in appsettings file";
                    CommonUtility.WriteMsgLogs(str, "API_UpCommingGreetings");
                    return str;
                }
                
            }
            else
            {
                str = "no data present in EmailConfigurations";
                CommonUtility.WriteMsgLogs(str, "API_UpCommingGreetings");
                return str;
            }


        }
    }
}