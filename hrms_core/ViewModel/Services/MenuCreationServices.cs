﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services
{
    public class MenuCreationServices : IMenuCreationServices
    {
        private readonly ApplicationDbContext _db;
        public MenuCreationServices (ApplicationDbContext options)
        {
            _db = options;
        }
        /// <summary>
        /// addmenus
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool AddMenu(MenuCreationVM model)
        {
            try
            {
                // duplicacy check
                bool bool_sameNameExists = _db.PageMenus.Any(x => x.menu_name.ToLower().Trim() == model.menu_name.ToLower().Trim());
                if (bool_sameNameExists)
                {
                    return false;
                }
                else
                {
                    PageMenu menu_obj = new PageMenu()
                    {
                        menu_name = model.menu_name,
                        controller_name = model.controller_name,
                        action_name = model.action_name,
                        module_name = model.module_name,
                        isactive=true,
                        createdby=1,
                        updatedby=1,
                        createdbyname=model.createdbyname,
                        updatedbyname=model.updatedbyname,
                        createdon=DateTime.Now,
                        updatedon=DateTime.Now

                    };

                    _db.PageMenus.Add(menu_obj);
                    _db.SaveChanges();
                    return true;
                }
                
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return false;

            }
        }
      
        
        
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public MenuCreationVM GetAllMenus()
        {
            {
                MenuCreationVM menuModel = new MenuCreationVM();
                try
                {
                    menuModel.menulist = _db.PageMenus
                        .Select(x => new MenuCreationVM
                        {
                            menu_name =menuModel.menu_name,
                            controller_name = menuModel.controller_name,
                            action_name = menuModel.action_name,
                            module_name = menuModel.module_name,

                        }).ToList();
                }
                catch (Exception ex)
                {
                    CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                }
                return menuModel;
            }
        }
         
        
        
        /// <summary>
         /// 
         /// </summary>
         /// <param name="menu_id"></param>
         /// <returns></returns>
        public MenuCreationVM GetMenuById(int menu_id)
        {

            MenuCreationVM menu_obj = new MenuCreationVM();
            try
            {
                var Data = _db.PageMenus.FirstOrDefault(x => x.id == menu_id);
                if (Data != null)
                {
                    menu_obj.menu_name = Data.menu_name;
                    menu_obj.action_name = Data.action_name;
                    menu_obj.controller_name = Data.controller_name;
                    menu_obj.menu_id = Data.id;
                    
                }
                return menu_obj;
            }
           catch(Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return null;
        }
    }
}

