﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class AssociatePersonalModel: AssociatePersonal
    {
        public List<AssociatePersonalModel> AssociatePersonalModelList { get; set; }
        public List<AssociatePersonalModel> AssociatePersonalModelListWithBirthday { get; set; }
        public List<CountryModel> AssociatePresentCountryList { get; set; }
        public List<StateModel> AssociatePresentStateList { get; set; }
        public string userName { get; set; }
        public AssociatePersonalModel()
        {
            AssociatePersonalModelList = new List<AssociatePersonalModel>();
            AssociatePersonalModelListWithBirthday = new List<AssociatePersonalModel>();
            AssociatePresentCountryList = new List<CountryModel>();
            AssociatePresentStateList = new List<StateModel>();
        }
    }
}
