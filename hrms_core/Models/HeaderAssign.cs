﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class HeaderAssign:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }                               //primary key
        [ForeignKey("associate_id")]
        public  int associate_id { get; set; }    //foreign key
        [ForeignKey("header_id")]
        public int header_id { get; set; }                 //foreign key
      
        public bool isdeduction { get; set; }
        public bool is_selected { get; set; }

    }
}
