﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class CommonAttributeModel
    {
        [DataType(DataType.DateTime)]
        [Display(Name = "Created On")]
        public DateTime createdon { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Updated On")]
        public DateTime updatedon { get; set; }

        [Display(Name = "Created By")]
        public int createdby { get; set; }

        [Display(Name = "Updated By")]
        public int updatedby { get; set; }

        [Display(Name = "Created By Name")]
        public string createdbyname { get; set; }

        [Display(Name = "Updated By Name")]
        public string updatedbyname { get; set; }

        [DefaultValue(true)]
        [Display(Name = "Is Active")]
        public bool? isactive { get; set; }
    }
}
