﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class ResignationServices : IResignationServices
    {
        private readonly ApplicationDbContext _db;
        public ResignationServices(ApplicationDbContext options) => _db = options;
        /// <summary>
        /// Get the Resignation All Data,
        /// Created By : Bilal 
        ///On Date: 02/11/2018
        /// </summary>
        /// <returns></returns>
        public List<ResignationVM> GetAllResignation()
        {
            ResignationViewModel model = new ResignationViewModel();
            try
            {
                model.ResignationVMList = (from res in _db.resignations.Where(x => x.isactive == true)
                                           join mas in _db.AssociateMasters.Where(x => x.isactive == true) on res.associate_id equals mas.id
                                           from main in _db.AssociateMains.Where(x => x.associate_id == res.associate_id).DefaultIfEmpty()
                                           from dep in _db.Departments.Where(x => x.id == main.department_id).DefaultIfEmpty()
                                           select new ResignationVM
                                           {
                                               id = res.id,
                                               resignation_text = res.resignation_text,
                                               username = mas.associate_name,
                                               official_email = main.official_email,
                                               department_name = dep.departmenttext,
                                               DOJ = main.createdon,
                                               createdon = res.createdon,
                                               approved_by = res.approved_by,
                                               is_approved = res.is_approved,
                                               approved_by_name = res.approved_by_name,
                                               reply_comment = res.reply_comment
                                           })
                    .OrderByDescending(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.ResignationVMList;
        }

        /// <summary>
        /// Get the Resignation Data by UserId,
        /// Created By : Bilal 
        ///On Date: 02/11/2018
        /// </summary>
        /// <returns></returns>
        public List<ResignationVM> GetResignationByUser(int UserId)
        {
            ResignationViewModel model = new ResignationViewModel();
            try
            {
                model.ResignationList = _db.resignations.Where(x => x.associate_id == UserId)
                    .Select(obj => new ResignationVM
                    {
                        id = obj.id,
                        resignation_text = obj.resignation_text,
                        associate_id = obj.associate_id,
                        approved_by = obj.approved_by,
                        createdon = obj.createdon,
                        createdby = obj.createdby,
                        createdbyname = obj.createdbyname,
                        updatedon = obj.updatedon,
                        updatedby = obj.updatedby,
                        updatedbyname = obj.updatedbyname,
                        isactive = obj.isactive,
                        is_approved = obj.is_approved,
                        approved_by_name = obj.approved_by_name,
                        reply_comment = obj.reply_comment

                    }).OrderByDescending(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.ResignationList;
        }

        /// <summary>
        /// Save Resignation Data,
        /// Created By : Bilal 
        ///On Date: 02/11/2018
        /// </summary>
        /// <returns></returns>
        public int SaveResignation(ResignationVM model)
        {
            int _resignationId = 0;
            string Result = string.Empty;
            Resignation resignation = new Resignation();
            try
            {
                //Check for Duplicate Record
                bool bool_sameNameExists = _db.resignations.Where(x => x.isactive == true).Any(x => x.associate_id == model.associate_id);
                if (bool_sameNameExists)
                {
                    Result = MessageHelper.Resignation_Duplicate;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            resignation.resignation_text = model.resignation_text;
                            resignation.associate_id = model.associate_id;
                            resignation.isactive = true;
                            resignation.createdon = DateTime.Now;
                            resignation.updatedon = DateTime.Now;
                            resignation.createdby = model.createdby;
                            resignation.updatedby = model.updatedby;
                            resignation.updatedbyname = model.updatedbyname;
                            resignation.createdbyname = model.createdbyname;
                            _db.resignations.Add(resignation);
                            _db.SaveChanges();
                            _resignationId = resignation.id;
                            scope.Complete();
                            Result = MessageHelper.Resignation_AddSuccess;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.Resignation_AddError;
                            _resignationId = -1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
                _resignationId = -1;
            }
            return _resignationId;
        }

        /// <summary>
        /// Check Your Existing Resignation,
        /// Created By : Bilal 
        ///On Date: 02/11/2018
        /// </summary>
        /// <returns></returns>
        public bool CheckResignation(int AssociateId)
        {
            bool flag = false;
            try
            {
                //Check for Duplicate Record
                bool bool_sameNameExists = _db.resignations.Where(x => x.isactive == true).Any(x => x.associate_id == AssociateId);
                if (bool_sameNameExists)
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Send Resignation Mail,
        /// Created By : Bilal 
        ///On Date: 02/11/2018
        /// </summary>
        /// <returns></returns>
        public bool SendResignationMail(ResignationVM model, HttpContext httpContext, IConfiguration configuration)
        {
            string str = string.Empty;
            bool result = false;
            var _emailData = _db.EmailConfigurations.FirstOrDefault();
            MailMessage mail = new MailMessage();
            try
            {

                if (_emailData != null)
                {
                    string FromEmail = _emailData.email_send_from;
                    string Pass = _emailData.password;

                    SmtpClient SmtpServer = new SmtpClient(_emailData.smtp);
                    StringBuilder URL = new StringBuilder();
                    var EncResignationId = CommonUtility.MD5Convertor(model.id.ToString());
                    URL.Append(String.Format("{0}://{1}", httpContext.Request.Scheme, httpContext.Request.Host.Host));

                    URL.Append(String.Format(":{0}", httpContext.Request.Host.Port));

                    URL.Append(String.Format("/{0}/{1}?ResignationId={2}", "Resignation", "Login", EncResignationId));

                    mail.From = new MailAddress(FromEmail, model.username);
                    if (model.reporting_email != null)
                    {
                        mail.To.Add(model.reporting_email);
                    }
                    if (model.hod_email != null)
                    {
                        mail.To.Add(model.hod_email);
                    }
                    string[] emails = configuration.GetValue<string>("ConnectionStrings:Resignation").Split(';');
                    if (emails != null && emails.Count() > 0)
                    {
                        foreach (var email in emails)
                        {
                            mail.CC.Add(email);
                        }
                    }
                    if (model.additional_reporting_email != null && model.additional_reporting_email.Count != 0)
                    {
                        foreach (var item in model.additional_reporting_email.Distinct())
                        {
                            mail.CC.Add(item);
                        }
                    }
                    if (model.official_email != null)
                    {
                        mail.CC.Add(model.official_email);
                    }
                    if (model.official_email != null)
                        mail.ReplyToList.Add(model.official_email);

                    mail.Subject = string.Format("Resignation Letter of {0}", model.username);
                    mail.IsBodyHtml = true;
                    mail.Body = Convert.ToString(model.resignation_text) + "<br/>Take action using below link.<br/><br/><a href='" + Convert.ToString(URL) + "'>Click Here<a/><br/><br/><i>Declaration:If you have already received this e-mail then ignore it.</i>";
                    SmtpServer.Host = _emailData.smtp;
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.Port = _emailData.port;
                    SmtpServer.Credentials = new System.Net.NetworkCredential(FromEmail, Pass);
                    SmtpServer.EnableSsl = _emailData.ssl;
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                 System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };
                    SmtpServer.Send(mail);
                    str = MessageHelper.EmailSent;
                    result = true;
                }
                else
                    result = false;

            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                result = false;
            }
            string status = string.Empty;
            if (result)
                status = "Success";
            else
                status = "Failed";
            CommonUtility.WriteMailLogs("From =>" + _emailData.email_send_from + " To=>" + mail.To + "CC=>" + mail.CC + " Date=>" + DateTime.Now.ToShortDateString() + " Body=>" + mail.Body + " Status=>" + status, "SendResignationMail");
            return result;
        }

        /// <summary>
        /// Send Resignation Approval Rejection Mail,
        /// Created By : Bilal 
        ///On Date: 13/11/2018
        /// </summary>
        /// <returns></returns>
        public bool SendResignationApprovalRejectionMail(ResignationViewModel model, HttpContext httpContext, IConfiguration configuration, List<string> CC_EmailId, string ResignAppliedBy_EMail)
        {
            string str = string.Empty;
            bool result = false;
            ResignationViewModel _resignationData = new ResignationViewModel();
            var _emailData = _db.EmailConfigurations.FirstOrDefault();
            MailMessage mail = new MailMessage();
            try
            {

                if (_emailData != null)
                {
                    string FromEmail = _emailData.email_send_from;
                    string Pass = _emailData.password;

                    SmtpClient SmtpServer = new SmtpClient(_emailData.smtp);
                    StringBuilder URL = new StringBuilder();
                    var EncResignationId = CommonUtility.MD5Convertor(model.id.ToString());
                    var ResignationData = _db.resignations.FirstOrDefault(x => x.id == model.id);
                    if (ResignationData != null)
                    {
                        _resignationData.id = ResignationData.id;
                        _resignationData.associate_id = ResignationData.associate_id;
                        _resignationData.createdbyname = ResignationData.createdbyname;
                        _resignationData.reply_comment = ResignationData.reply_comment;
                        _resignationData.approved_by = ResignationData.approved_by;
                        _resignationData.is_approved = ResignationData.is_approved;
                        _resignationData.approved_by_name = ResignationData.approved_by_name;
                    }
                    mail.From = new MailAddress(FromEmail, model.username);
                    mail.To.Add(ResignAppliedBy_EMail);

                    string[] emails = configuration.GetValue<string>("ConnectionStrings:Resignation").Split(';');
                    if (emails != null && emails.Count() > 0)
                    {
                        foreach (var email in emails)
                        {
                            mail.CC.Add(email);
                        }
                    }
                    if (CC_EmailId != null && CC_EmailId.Count != 0)
                    {
                        foreach (var item in CC_EmailId)
                        {
                            mail.CC.Add(item);
                        }
                    }
                    if (model.official_email != null)
                    {
                        mail.CC.Add(model.official_email);
                    }
                    if (model.official_email != null)
                        mail.ReplyToList.Add(model.official_email);

                    mail.Subject = string.Format("Resignation Status");
                    mail.IsBodyHtml = true;
                    if (_resignationData.is_approved == true)
                    {
                        mail.Body = "<p>Dear <strong>" + _resignationData.createdbyname + ",</strong></p><p>Your Resignation has been Approved.</p><p><strong>Comment :- </strong>" + _resignationData.reply_comment + "</p><br/><p><strong>Thanks & Regards,<strong></p><p>" + _resignationData.approved_by_name + "</p><br/><i>Declaration:If you have already received this e-mail then ignore it.</i>";
                    }
                    else if (_resignationData.is_approved == false)
                    {
                        mail.Body = "<p>Dear <strong>" + _resignationData.associate_name + ",</strong></p><p>Your Resignation has been Rejected.</p> <br/><p><strong>Comment :- </strong>" + _resignationData.reply_comment + "</p><br/><i>Declaration:If you have already received this e-mail then ignore it.</i>";
                    }
                    SmtpServer.Host = _emailData.smtp;
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.Port = _emailData.port;
                    SmtpServer.Credentials = new System.Net.NetworkCredential(FromEmail, Pass);
                    SmtpServer.EnableSsl = _emailData.ssl;
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                 System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };
                    SmtpServer.Send(mail);
                    str = MessageHelper.EmailSent;
                    result = true;
                }
                else
                    result = false;

            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                result = false;
            }
            string status = string.Empty;
            if (result)
                status = "Success";
            else
                status = "Failed";
            CommonUtility.WriteMailLogs("From =>" + _emailData.email_send_from + " To=>" + mail.To + "CC=>" + mail.CC + " Date=>" + DateTime.Now.ToShortDateString() + " Body=>" + mail.Body + " Status=>" + status, "SendResignationMail");
            return result;
        }

        /// <summary>
        /// Get the Resignation Data by ResignationId,
        /// Created By : Bilal 
        ///On Date: 05/11/2018
        /// </summary>
        /// <returns></returns>
        public List<ResignationVM> GetResignationId(int ResignationId)
        {
            ResignationViewModel model = new ResignationViewModel();
            try
            {
                model.ResignationList = _db.resignations.Where(x => x.id == ResignationId)
                    .Select(obj => new ResignationVM
                    {
                        id = obj.id,
                        resignation_text = obj.resignation_text,
                        associate_id = obj.associate_id,
                        approved_by = obj.approved_by,
                        createdon = obj.createdon,
                        createdby = obj.createdby,
                        createdbyname = obj.createdbyname,
                        updatedon = obj.updatedon,
                        updatedby = obj.updatedby,
                        updatedbyname = obj.updatedbyname,
                        isactive = obj.isactive,
                        is_approved = obj.is_approved,
                        approved_by_name=obj.approved_by_name,
                        reply_comment = obj.reply_comment

                    }).OrderByDescending(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.ResignationList;
        }

        /// <summary>
        /// Get the Resignation Data by ResignationId,
        /// Created By : Bilal 
        ///On Date: 13/11/2018
        /// </summary>
        /// <returns></returns>
        public ResignationViewModel GetResignationById(int ResignationId)
        {
            ResignationViewModel model = new ResignationViewModel();
            try
            {
                model = _db.resignations.Where(x => x.id == ResignationId)
                    .Select(obj => new ResignationViewModel
                    {
                        id = obj.id,
                        resignation_text = obj.resignation_text,
                        associate_id = obj.associate_id,
                        approved_by = obj.approved_by,
                        createdon = obj.createdon,
                        createdby = obj.createdby,
                        createdbyname = obj.createdbyname,
                        updatedon = obj.updatedon,
                        updatedby = obj.updatedby,
                        updatedbyname = obj.updatedbyname,
                        isactive = obj.isactive,
                        is_approved = obj.is_approved,
                        approved_by_name = obj.approved_by_name,
                        reply_comment = obj.reply_comment

                    }).OrderByDescending(x => x.updatedon).FirstOrDefault();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model;
        }

        /// <summary>
        /// Approve / Reject Resignation Data,
        /// Created By : Bilal 
        ///On Date: 13/11/2018
        /// </summary>
        /// <returns></returns>
        public int ApproveRejectResignation(ResignationViewModel model)
        {
            string Result = string.Empty;
            int AssociateId = 0;
            try
            {

                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    try
                    {
                        var resignation = _db.resignations.FirstOrDefault(x => x.id == model.id);
                        if (resignation != null)
                        {
                            AssociateId = resignation.associate_id;
                            resignation.updatedon = DateTime.Now;
                            resignation.approved_by = model.createdby;
                            resignation.is_approved = model.is_approved;
                            resignation.reply_comment = model.reply_comment;
                            resignation.approved_by_name = model.approved_by_name;
                            _db.SaveChanges();
                        }
                        if (model.is_approved == true)
                        {
                            Result = MessageHelper.Resignation_ApprovedSuccess;
                            NoticePeriod noticePeriod = new NoticePeriod();
                            noticePeriod.associate_id = resignation.associate_id;
                            noticePeriod.notice_start_date = DateTime.Now;
                            noticePeriod.notice_end_date = DateTime.Now.AddDays(model.NoticeDays);
                            noticePeriod.createdby = model.createdby;
                            noticePeriod.createdbyname = model.createdbyname;
                            noticePeriod.createdon = DateTime.Now;
                            _db.NoticePeriods.Add(noticePeriod);
                            _db.SaveChanges();
                        }
                        else
                            Result = MessageHelper.Resignation_RejectSuccess;
                        scope.Complete();
                    }
                    catch (Exception ex)
                    {
                        scope.Dispose();
                        CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                        Result = MessageHelper.Resignation_AddError;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return AssociateId;
        }
    }
}
