﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IQualificationServices
    {
        /// <summary>
        /// Get the Qualification Data,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        List<QualificationModel> GetQualification();
        string SaveQualification(QualificationModel model);
        string UpdateQualification(QualificationModel model);
        string DisableQualification(QualificationModel model);
        string EnableQualification(QualificationModel model);
        QualificationModel GetQualificationById(int ID);
        /// <summary>
        /// Get the Qualification Data Only Active,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        List<QualificationModel> GetQualification_Active();

        ///// <summary>
        ///// Save the Qualification Data,
        ///// Created By : Bilal 
        /////On Date: 31/05/2018
        ///// </summary>
        ///// <returns></returns>
        //string SaveQualification(QualificationModel model);

        ///// <summary>
        ///// Update the Qualification Data,
        ///// Created By : Bilal 
        /////On Date: 23/05/2018
        ///// </summary>
        ///// <returns></returns>
        //string UpdateQualification(QualificationModel model);

        ///// <summary>
        ///// Disable the Qualification Data,
        ///// Created By : Bilal 
        /////On Date: 23/05/2018
        ///// </summary>
        ///// <returns></returns>
        //string DisableQualification(QualificationModel model);

        ///// <summary>
        ///// Enable the Qualification Data,
        ///// Created By : Bilal 
        /////On Date: 23/05/2018
        ///// </summary>
        ///// <returns></returns>
        //string EnableQualification(QualificationModel model);

        ///// <summary>
        ///// Get the Qualification Data,
        ///// Created By : Bilal 
        /////On Date: 31/05/2018
        ///// </summary>
        ///// <returns></returns>
        //List<QualificationModel> GetQualification_Active();
    }
}
