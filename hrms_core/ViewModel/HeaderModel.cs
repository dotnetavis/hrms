﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class HeaderModel : Header
    {
        public List<HeaderModel> HeaderModelList { get; set; }
        public List<PayElementModel> PayElementList { get; set; }
        public List<AssociateMasterModel> AssociateList { get; set; }
        public List<HeaderAssignModel> HeaderAssignModelList { get; set; }
        public bool is_selected { get; set; }
        public int associate { get; set; }
        public HeaderModel()
        {
            HeaderModelList = new List<HeaderModel>();
            PayElementList = new List<PayElementModel>();
            AssociateList = new List<AssociateMasterModel>();
            HeaderAssignModelList = new List<HeaderAssignModel>();
        }
    }
}
