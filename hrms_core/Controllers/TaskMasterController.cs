﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.ViewModel;
using hrms_core.Models;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace hrms_core.Controllers
{
    public class TaskMasterController : Controller
    {
        private readonly ApplicationDbContext _db;
        private ITaskMasterServices taskmasterServices => new TaskMasterServices(_db);
        private IDepartmentServices departmentServices => new departmentServices(_db);
        private IProjectServices projectServices => new ProjectServices(_db);
        public TaskMasterController(ApplicationDbContext options) => _db = options;

        [HttpGet]
        public IActionResult Index(int? id, int? proId)
        {
            TaskMasterModel model = new TaskMasterModel();
            model.projectid = proId.HasValue == true ? proId.Value : 0;
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            model.TaskMasterModelList = taskmasterServices.GetTask_ByProjectID(proId.Value);
            model.DepartmentList = departmentServices.GetDepartmentList();
            model.ProjectList = projectServices.GetProjectList();
            model.ProjectList.Insert(0, new ProjectMasterModel { id = 0, projecttext = "--Select--" });
            model.DepartmentList.Insert(0, new DepartmentModel { id = 0, departmenttext = "--Select--" });
            if (proId > 0 && proId != null)
            {
                model.AssociateList = (from master in _db.AssociateMasters.Where(x => x.isactive == true && x.is_super_admin == false)
                                       join main in _db.AssociateMains.Where(x => x.department_id == SesMdl.department_id) on master.id equals main.associate_id
                                       select new AssociateMasterModel
                                       {
                                           id = main.associate_id,
                                           associate_name = master.associate_name
                                       }).ToList();
                if (model.TaskMasterModelList.Count > 0 && model.TaskMasterModelList != null)
                {
                    var data = model.ProjectList.FirstOrDefault(x => x.id == proId);
                    if (data != null)
                    {
                        model.expectedcompletiondate = DateTime.Now;
                        model.assigneddate = DateTime.Now;
                        model.projectid = data.id;
                        model.createdby = data.createdby;
                        model.createdon = data.createdon;
                        model.updatedby = data.updatedby;
                        model.updatedon = data.updatedon;
                        model.isactive = data.isactive;
                        model.actualcompletiondate = DateTime.Now;
                    }
                }
            }
            if (id > 0 && id != null)
            {
                if (model.TaskMasterModelList.Count > 0 && model.TaskMasterModelList != null)
                {

                    var data = model.TaskMasterModelList.FirstOrDefault(x => x.id == id);

                    if (data != null)
                    {
                        model.id = data.id;
                        model.tasktext = data.tasktext;
                        model.project_name = data.project_name;
                        model.assigneddate = data.assigneddate;
                        model.expectedcompletiondate = data.expectedcompletiondate;
                        model.actualcompletiondate = data.actualcompletiondate;
                        model.projectid = data.projectid;
                        model.createdby = data.createdby;
                        model.createdon = data.createdon;
                        model.updatedby = data.updatedby;
                        model.updatedon = data.updatedon;
                        model.isactive = data.isactive;
                        model.assignto = data.assignto;
                    }
                }
                //model.AssociateList = (from master in _db.AssociateMasters.Where(x => x.isactive == true && x.is_super_admin == false)
                //                    join main in _db.AssociateMains.Where(x => x.department_id == SesMdl.department_id) on master.id equals main.associate_id
                //                    join task in _db.TaskAssigns.Where(x => x.task_id == id).DefaultIfEmpty() on main.associate_id equals task.associate_id
                //                    select new AssociateMasterModel
                //                    {
                //                        id = main.associate_id,
                //                        associate_name = master.associate_name,
                //                        is_selected = task != null ? task.is_selected : false
                //                    }).ToList();
                //var list= _db.TaskAssigns.Where(x => x.task_id == id).Select(obj=>new AssociateMasterModel
                //                    {
                //                        id = obj.associate_id,
                //                        is_selected = obj.is_selected
                //                    }).ToList();
                var list = (from task in _db.TaskAssigns.Where(x => x.task_id == id)
                            join main in _db.AssociateMains
                            on task.associate_id equals main.associate_id
                            join master in _db.AssociateMasters
                            on main.associate_id equals master.id
                            select new AssociateMasterModel
                            {
                                id = task.associate_id,
                                associate_name = master.associate_name,
                                is_selected = task.is_selected

                            }).ToList();
                var finalList = list.Concat(model.AssociateList).GroupBy(x => x.id).Select(g => g.OrderByDescending(x => x.id).First()).ToList();
                if (finalList != null && finalList.Count > 0)
                {
                    model.AssociateList = finalList;
                }
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(TaskMasterModel model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                if (model.id > 0)
                {
                    TempData["msg"] = taskmasterServices.UpdateTask(model);
                }
                else
                {
                    TempData["msg"] = taskmasterServices.SaveTask(model);
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("Index", "TaskMaster", new { id = 0, proid = model.projectid });
        }

        public IActionResult DisableTask(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            TaskMasterModel model = new TaskMasterModel();
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            model.id = id;
            model.updatedby = 1;
            TempData["msg"] = taskmasterServices.DisableTask(model);
            return RedirectToAction("Index", "TaskMaster");
        }

        public IActionResult EnableTask(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            TaskMasterModel model = new TaskMasterModel();
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            model.id = id;
            model.updatedby = 1;
            TempData["msg"] = taskmasterServices.EnableTask(model);
            return RedirectToAction("Index", "TaskMaster");
        }

        public IActionResult Edit(int id)
        {
            // TODO : Implement the Edit Functionality for Task Edit
            return View();
        }

        [HttpPost]
        public IActionResult GetAssociates(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            var data = (from project in _db.ProjectMasters
                        join main in _db.AssociateMains
                        on project.departmentid equals main.department_id
                        join master in _db.AssociateMasters
                        on main.associate_id equals master.id
                        select new AssociateMasterModel
                        {
                            id = main.associate_id,
                            associate_name = master.associate_name
                        }).ToList();
            return Json(data);
        }
    }
}










































