﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class PageAssignModel: PageAssign
    {
        public List<PageAssignModel> PageAssignModelList { get; set; }
        public PageAssignModel()
        {
            PageAssignModelList = new List<PageAssignModel>();
        }
    }
}
