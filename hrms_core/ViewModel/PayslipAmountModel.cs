﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class PayslipAmountModel: PayslipAmount
    {
        public List<PayslipAmountModel> PayslipAmountModelList { get; set; }
        public PayslipAmountModel()
        {
            PayslipAmountModelList = new List<PayslipAmountModel>();
        }
    }
}
