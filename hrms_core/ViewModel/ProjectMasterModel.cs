﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class ProjectMasterModel: ProjectMaster
    {
        public List<ProjectMasterModel> ProjectMasterModelList { get; set; }
        public int department_id { get; set; }
        public int associate_id { get; set; }
        public string associate_name { get; set; }
        public int? reporting_id { get; set; }
        public List<DepartmentModel> DepartmentList { get; set; }
        public ProjectMasterModel()
        {
            DepartmentList = new List<DepartmentModel>();
            ProjectMasterModelList = new List<ProjectMasterModel>();
        }
    }
}
