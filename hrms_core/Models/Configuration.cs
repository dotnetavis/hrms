﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class Configuration:CommonAttributeModel
    {
        public int id { get; set; }
        [Required(ErrorMessage ="Required")]
        public string associate_prefix { get; set; }
        [Required(ErrorMessage = "Required")]
        [Range(1, 9, ErrorMessage = "Inavlid Input")]
        public int max_numeric_character { get; set; }
        [Required(ErrorMessage = "Required")]
        public string company_name { get; set; }
        [Required(ErrorMessage = "Required")]
        [Range(1000000000, 9999999999, ErrorMessage = "Inavlid Input")]
        public double contact_no { get; set; }
        [Required(ErrorMessage = "Required")]
        public string email_id { get; set; }
        [Required(ErrorMessage = "Required")]
        public string address { get; set; }
       
    }
}
