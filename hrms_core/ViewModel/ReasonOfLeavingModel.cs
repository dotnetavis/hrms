﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class ReasonOfLeavingModel: ReasonOfLeaving
    {
        public List<ReasonOfLeavingModel> ReasonOfLeavingModelList { get; set; }
        public ReasonOfLeavingModel()
        {
            ReasonOfLeavingModelList = new List<ReasonOfLeavingModel>();

        }
    }
}
