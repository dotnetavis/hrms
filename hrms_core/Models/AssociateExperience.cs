﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class AssociateExperience:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public string previous_company { get; set; }
        [ForeignKey("associate_id")]
        public int associate_id { get; set; }
        public DateTime from_date { get; set; }
        public DateTime to_date { get; set; }
        public int gross_salary { get; set; }
        public int carryhome_salary { get; set; }
        public int ctc { get; set; }
        public string extra_allowance { get; set; }
        [ForeignKey("designation_id")]
        public int designation_id { get; set; }
 }
    
}
