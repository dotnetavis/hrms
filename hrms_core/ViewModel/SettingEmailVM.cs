﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class SettingEmailVM
    {
        public virtual ConfigurationModel configurationModel { get; set; }
        public virtual EmailConfigurationModel emailConfigurationModel { get; set; }
        public SettingEmailVM()
        {
            configurationModel = new ConfigurationModel();
            emailConfigurationModel = new EmailConfigurationModel();
        }
    }
}
