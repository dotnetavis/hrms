﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class AssociateRelieve:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }             //primary key
        public virtual AssociateMaster associate { get; set; }   // foreign key
        [ForeignKey("employee_reason_id")]
        public int employee_reason_id { get; set; }    //foreign key
        [ForeignKey("employer_reason_id")]
        public int employer_reason_id { get; set; }    //foreign key
        public bool noticeforseperation { get; set; }
        public DateTime noticedate { get; set; }
        public int notice_duration { get; set; }
        public bool companyassetreturn { get; set; }
        public DateTime date_of_leaving { get; set; }
        public bool relieving_letter_given { get; set; }
        public DateTime relieving_letter_issue_date { get; set; }
        public bool experience_letter_given { get; set; }
        public DateTime experience_letter_issue_date { get; set; }
        public bool suitable_for_rehire { get; set; }
        public bool fullandfinaldone { get; set; }
    }
}
