﻿using System;
using System.Collections.Generic;
using System.Linq;
using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Transactions;
using System.Net;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Globalization;

namespace hrms_core.Controllers
{
    public class AccountController : Controller
    {
        private IHttpContextAccessor _accessor;
        private readonly ApplicationDbContext _db;
        public IConfiguration Configuration { get; }
        private ILoginServices loginServices => new LoginServices(_db);
        private IConfigurationServices configurationServices => new ConfigurationServices(_db);
        private IUserServices userServices => new UserServices(_db);
        private IAssociateServices associateServices => new AssociateServices(_db);
        public AccountController(ApplicationDbContext db, IConfiguration configuration, IHttpContextAccessor accessor)
        {
            Configuration = configuration;
            _accessor = accessor;
            _db = db;
        }

        private IForgotPasswordServices forgotPasswordServices => new ForgotPasswordServices(_db);
        private IChangePasswordServices changeservices => new ChangePasswordServices(_db);

        [HttpGet]
        public IActionResult Login()
        {
            LoginModel model = new LoginModel();
            var Configuration = _db.Configurations.ToList();
            var ConfigurationEmail = _db.EmailConfigurations.ToList();
            if ((Configuration != null && Configuration.Count > 0) && (ConfigurationEmail != null && ConfigurationEmail.Count > 0))
            {
                var loginlist = _db.Users.ToList();
                if (loginlist.Count == 0)
                {
                    int _associateId = 0;
                    string _associateCode = string.Empty;
                    string _associateName = string.Empty;
                    int _designationId = 0;
                    int _departmentId = 0;
                    int _qualificationId = 0;
                    int _roleId = 0;
                    string _associatePass = string.Empty;
                    string _associateOfficialEmail = string.Empty;
                    string _associatePersonalEmail = string.Empty;

                    #region AssociateMaster
                    AssociateMaster associateMaster = new AssociateMaster
                    {
                        associate_code = associateServices.GetAssociateCode(),
                        createdon = DateTime.Now,
                        updatedon = DateTime.Now,
                        createdby = model.createdby,
                        updatedby = model.updatedby,
                        createdbyname = model.createdbyname,
                        updatedbyname = model.updatedbyname,
                        isactive = true,
                        is_super_admin = true,
                        associate_name = "Super Admin",
                        application_date = DateTime.Now,
                        applicable_to = DateTime.Now
                    };
                    _db.AssociateMasters.Add(associateMaster);
                    _db.SaveChanges();
                    _associateId = associateMaster.id;
                    _associateCode = associateMaster.associate_code;
                    _associateName = associateMaster.associate_name;
                    #endregion

                    #region User
                    User user = new User
                    {
                        createdon = DateTime.Now,
                        updatedon = DateTime.Now,
                        createdby = _associateId,
                        updatedby = _associateId,
                        createdbyname = _associateName,
                        updatedbyname = _associateName,
                        isactive = true,
                        username = _associateCode
                    };
                    _associatePass = associateServices.GetSuperAdminPassword();
                    user.userpass = CommonUtility.MD5Convertor(_associatePass);//"admin@123";// CommonUtility.MD5Convertor(model.userpass);
                    _db.Users.Add(user);
                    _db.SaveChanges();
                    #endregion

                    #region Designation
                    Designation designation = new Designation
                    {
                        createdon = DateTime.Now,
                        updatedon = DateTime.Now,
                        createdby = _associateId,
                        updatedby = _associateId,
                        createdbyname = _associateName,
                        updatedbyname = _associateName,
                        isactive = true,
                        designationtext = "Super Admin Designation",
                        designationdesc = "Super Admin Designation"
                    };
                    _db.Designations.Add(designation);
                    _db.SaveChanges();
                    _designationId = designation.id;
                    #endregion

                    #region Department
                    Department department = new Department
                    {
                        createdon = DateTime.Now,
                        updatedon = DateTime.Now,
                        createdby = _associateId,
                        updatedby = _associateId,
                        createdbyname = _associateName,
                        updatedbyname = _associateName,
                        isactive = true,
                        departmenttext = "Super Admin Department"
                    };
                    _db.Departments.Add(department);
                    _db.SaveChanges();
                    _departmentId = department.id;
                    #endregion

                    #region Qualification
                    Qualification qualification = new Qualification
                    {
                        createdon = DateTime.Now,
                        updatedon = DateTime.Now,
                        createdby = _associateId,
                        updatedby = _associateId,
                        createdbyname = _associateName,
                        updatedbyname = _associateName,
                        isactive = true,
                        qualificationtext = "Super Admin Qualification"
                    };
                    _db.Qualifications.Add(qualification);
                    _db.SaveChanges();
                    _qualificationId = qualification.id;
                    #endregion

                    #region Page Menu Creation
                    List<PageMenu> lstPageMenu = new List<PageMenu>
                    {
                        new PageMenu() { module_name = "Configuration and Masters", menu_name = "Add Country", controller_name = "Address", action_name = "Country", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Configuration and Masters", menu_name = "Add State", controller_name = "Address", action_name = "State", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Configuration and Masters", menu_name = "Add City", controller_name = "Address", action_name = "City", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Configuration and Masters", menu_name = "Add Department", controller_name = "Department", action_name = "Index", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Configuration and Masters", menu_name = "Create Designation", controller_name = "Designation", action_name = "Index", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Configuration and Masters", menu_name = "Add Qualification", controller_name = "Qualification", action_name = "Index", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Configuration and Masters", menu_name = "Role with Menu Permission", controller_name = "Role", action_name = "Index", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Configuration and Masters", menu_name = "Configuration Setting", controller_name = "Home", action_name = "ConfigurationSetting", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Configuration and Masters", menu_name = "Login History", controller_name = "Home", action_name = "LoginHistory", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "DAR Panel", menu_name = "Create DAR", controller_name = "DAR", action_name = "CreateDAR", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "DAR Panel", menu_name = "DAR Bucket", controller_name = "DAR", action_name = "DARReport", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Leave Panel", menu_name = "Create Leave", controller_name = "Leave", action_name = "CreateLeave", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Leave Panel", menu_name = "Add Leave Type", controller_name = "Leave", action_name = "LeaveType", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Leave Panel", menu_name = "Opening Leave", controller_name = "Leave", action_name = "OpeningLeave", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Leave Panel", menu_name = "Leave Approve DisApprove Bucket", controller_name = "Leave", action_name = "LeaveApprovedDisApprovedBucket", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Payroll", menu_name = "Create Header", controller_name = "Payment", action_name = "Index", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Project and Task", menu_name = "Add Project", controller_name = "Project", action_name = "Index", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Project and Task", menu_name = "Create Task", controller_name = "TaskMaster", action_name = "Index", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Rewards", menu_name = "Create Award", controller_name = "EmployeeDashboard", action_name = "Employee", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Associate Panel", menu_name = "Add Associate", controller_name = "Associate", action_name = "Index", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true },
                        new PageMenu() { module_name = "Associate Panel", menu_name = "Associate Bucket", controller_name = "Associate", action_name = "AssociateBucket", createdon = DateTime.Now, updatedon = DateTime.Now, createdby = _associateId, updatedby = _associateId, createdbyname = _associateName, updatedbyname = _associateName, isactive = true }
                    };
                    _db.PageMenus.AddRange(lstPageMenu);
                    _db.SaveChanges();

                    #endregion

                    #region RoleMaster
                    RoleMaster roleMaster = new RoleMaster
                    {
                        createdon = DateTime.Now,
                        updatedon = DateTime.Now,
                        createdby = _associateId,
                        updatedby = _associateId,
                        createdbyname = _associateName,
                        updatedbyname = _associateName,
                        isactive = true,
                        role_name = "Super Admin Role",
                        role_description = "Super Admin Role"
                    };
                    _db.role_master.Add(roleMaster);
                    _db.SaveChanges();
                    _roleId = roleMaster.id;

                    #region Page Menu Permission
                    RoleMenuDetails roleMenuDetails;
                    var MenuList = _db.PageMenus.ToList();
                    if (MenuList != null && MenuList.Count > 0)
                    {
                        foreach (var item in MenuList)
                        {
                            roleMenuDetails = new RoleMenuDetails
                            {
                                createdon = DateTime.Now,
                                updatedon = DateTime.Now,
                                createdby = _associateId,
                                updatedby = _associateId,
                                createdbyname = _associateName,
                                updatedbyname = _associateName,
                                isactive = true,
                                page_menu_id = item.id,
                                role_id = _roleId,
                                is_selected = true
                            };
                            _db.role_menu_details.Add(roleMenuDetails);
                            _db.SaveChanges();
                        }
                    }
                    #endregion

                    #endregion

                    #region AssociateMain
                    AssociateMain associateMain = new AssociateMain
                    {
                        createdon = DateTime.Now,
                        updatedon = DateTime.Now,
                        createdby = _associateId,
                        updatedby = _associateId
                    };
                    associateMain.createdby = _associateId;
                    associateMain.updatedbyname = _associateName;
                    associateMain.createdbyname = _associateName;
                    associateMain.isactive = true;
                    associateMain.associate_id = _associateId;
                    associateMain.designation_id = _designationId;
                    associateMain.department_id = _departmentId;
                    associateMain.is_hod = false;
                    associateMain.extensionno = "";
                    associateMain.mobileno = Configuration == null ? "" : Convert.ToString(Configuration.FirstOrDefault().contact_no);
                    associateMain.official_email = Configuration == null ? "" : Configuration.FirstOrDefault().email_id;
                    associateMain.personal_email = Configuration == null ? "" : Configuration.FirstOrDefault().email_id;
                    _associatePersonalEmail = associateMain.personal_email;
                    associateMain.qualification_id = _qualificationId;
                    associateMain.shiftid = 0;
                    associateMain.applicable_leave = 0;
                    associateMain.role_id = _roleId;
                    associateMain.reportingperson_id = _associateId;
                    _db.AssociateMains.Add(associateMain);
                    _db.SaveChanges();
                    #endregion

                    #region Super Admin Welcome Mail
                    string Body = string.Empty;
                    string Subject = "Welcome " + _associateName + ", Your HRMS Credential !";
                    Body = "<strong>Dear " + _associateName + "</strong> \n";
                    Body += "Your User Name : " + _associateCode + " \n";
                    Body += "Your User Password : " + _associatePass + " \n </br>";
                    Body += "<strong>Regards</strong> </br> System Admin";
                    configurationServices.SendMail(_associatePersonalEmail, Subject, Body, _associateName);
                    #endregion
                }
                else
                {

                    if (HttpContext.Session.Keys.FirstOrDefault() != null)
                        HttpContext.Session.Remove(HttpContext.Session.Keys.FirstOrDefault());
                }
                return View();
            }
            else
            {
                return RedirectToAction("ConfigurationSetting", "Home");
            }
        }
        [HttpPost]
        public IActionResult Login(LoginModel model)
        {
            string associate_code = string.Empty;
            string _device = string.Empty;
            string DeviceName = string.Empty;
            string DeviceType = string.Empty;
            try
            {
                model.LoginModelList = loginServices.doLogin(model);
                if (model.LoginModelList.Count > 0 && model.LoginModelList != null)
                {
                    //TODO: Check the NULL Value
                    var _associate = model.LoginModelList.FirstOrDefault();
                    if (_associate != null)
                    {
                        associate_code = _associate.username;
                        SessionModel sessionModel = new SessionModel();
                        sessionModel = loginServices.GetAllSessionData(associate_code, _associate.is_super_admin).FirstOrDefault();
                        if (sessionModel != null)
                        {
                            HttpContext.Session.SetObjectAsJson(SessionVariables.SessionData, sessionModel);
                        }
                        else
                        {
                            TempData["msg"] = "Unauthorized User";
                            return RedirectToAction("Login", "Account");
                        }
                    }
                    #region Save Login History Details (Bilal - 27/08/2018)
                    SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
                    string hostName = Dns.GetHostName();
                    string myIP = string.Empty;
                    myIP = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                    if (myIP.IndexOf("::ffff:") != -1)
                    {
                        myIP = myIP.Replace("::ffff:", "");
                    }
                    LoginHistory loginHistory = new LoginHistory();
                    if (SesMdl != null)
                    {
                        loginHistory.associate_code = SesMdl.associate_code;
                        loginHistory.associate_email = SesMdl.official_email;
                        loginHistory.associate_name = SesMdl.associate_name;
                        loginHistory.device_ip = myIP;
                        string culture = CultureInfo.CurrentCulture.EnglishName;
                        _device = Convert.ToString(_accessor.HttpContext.Request.Headers["User-Agent"]);
                        string country = culture.Substring(culture.IndexOf('(') + 1, culture.LastIndexOf(')') - culture.IndexOf('(') - 1);
                        if (_device.ToString().IndexOf("Mobile") == -1)
                        {
                            DeviceType = "Desktop";
                        }
                        else
                        {
                            DeviceType = "Mobile";
                        }
                        loginHistory.device_location = country;
                        loginHistory.device_name = hostName;
                        loginHistory.device_type = DeviceType;
                        loginHistory.created_on = DateTime.Now;
                        _db.LoginHistory.Add(loginHistory);
                        _db.SaveChanges();
                    }
                    CommonUtility.WriteDeviceAndIPAddressLogs("Device: " + DeviceType + "; Host Name: " + hostName + "; IP: " + myIP, "Login");
                    #endregion
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    TempData["msg"] = "Unauthorized User";
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                TempData["msg"] = $"{ex.Message} Exception Occured!! Please contact to admin";
                return RedirectToAction("Login", "Account");
            }
        }
        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public IActionResult ForgotPassword(UserModel model, string CommandName)
        {
            if (CommandName == "Submit")
            {
                model.UserModelList = userServices.GetUserDetails(model);
                if (model.UserModelList.Count != 0 && model.UserModelList != null)
                {
                    bool pass = forgotPasswordServices.SendForgetPassword(model.UserModelList.FirstOrDefault(), HttpContext, Configuration);
                    if (pass == true)
                    {
                        TempData["Msg"] = "Password Reset Link is send to your official Email ID.";
                        return RedirectToAction("ForgotPassword", "Account");
                    }
                    else
                    {
                        TempData["Msg"] = "Invalid Username.";
                        return RedirectToAction("ForgotPassword", "Account");
                    }
                }
                else
                {
                    TempData["Msg"] = "Unauthorized User";
                    return RedirectToAction("ForgotPassword", "Account");

                }
            }
            else if (CommandName == "Cancel")
            {
                return RedirectToAction("Login", "Account");
            }

            return RedirectToAction("ForgotPassword", "Account");
        }
        [HttpGet]
        public IActionResult ResetPassword(string Guid)
        {
            UserModel model = new UserModel
            {
                guid = Guid
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult ResetPassword(UserModel model, string CommandName)
        {
            if (CommandName == "Submit")
            {
                //var data = userServices.GetUserDetailsByGUID(model.guid).FirstOrDefault();
                var data = _db.Users.Where(x => x.guid == model.guid).FirstOrDefault();
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.userpass = CommonUtility.MD5Convertor(model.confirmnewpassword);
                            data.guid = String.Empty;
                            _db.SaveChanges();
                            scope.Complete();
                            StringBuilder URL = new StringBuilder();
                            URL.Append(String.Format("{0}://{1}", _accessor.HttpContext.Request.Scheme, _accessor.HttpContext.Request.Host.Host));
                            URL.Append(String.Format(":{0}", _accessor.HttpContext.Request.Host.Port));
                            URL.Append(String.Format("/{0}/{1}", "Account", "Login"));

                            TempData["Msg"] = String.Format("{0} <a href='{1}'> Click here to Login</a>", MessageHelper.Forgot_PasswordCreated, URL.ToString());
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            TempData["Msg"] = "Error";
                        }
                    }
                    //data.FirstOrDefault().userpass = model.confirmnewpassword;
                    //_db.SaveChanges();
                }
                else
                {
                    TempData["Msg"] = "Forget Passowrd Link Expired.";
                }

            }

            else if (CommandName == "cancel")
            {
                return RedirectToAction("ResetPassword", "Account");
            }
            return View();
        }
        [HttpGet]
        public IActionResult ChangePass()
        {
            LoginModel login = HttpContext.Session.GetObjectFromJson<LoginModel>(SessionVariables.SessionData);
            if (login != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [HttpPost]
        public IActionResult ChangePass(ChangePasswordModel model)
        {
            UserModel user_obj = new UserModel();
            LoginModel login = HttpContext.Session.GetObjectFromJson<LoginModel>(SessionVariables.SessionData);
            model.username = login.username;
            user_obj = changeservices.CheckUserPassword(model).FirstOrDefault();
            if (user_obj.userpass == CommonUtility.MD5Convertor(model.userpass) && model.newpassword == model.confirmnewpassword)
            {
                bool result = changeservices.ChangePass(model);
                if (result == true)
                {
                    TempData["msg"] = "Password Changed";
                }
                else
                {
                    TempData["msg"] = "Error";
                }
            }
            return RedirectToAction("Login", "Account");
        }

        //public string GetIPAddress()
        //{
        //    //IPHostEntry Host = default(IPHostEntry);
        //    //string Hostname = null;
        //    //string IPAddress = string.Empty;
        //    //Hostname = System.Environment.MachineName;
        //    //Host = Dns.GetHostEntry(Hostname);
        //    //foreach (IPAddress IP in Host.AddressList)
        //    //{
        //    //    if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
        //    //    {
        //    //        IPAddress = Convert.ToString(IP);
        //    //    }
        //    //}
        //    //return IPAddress;
        //    System.Web.HttpContext context = System.Web.HttpContext.Current;
        //    string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        //    string ipaddress = string.Empty;
        //    ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        //    if (ipaddress == "" || ipaddress == null)
        //        ipaddress = Request.ServerVariables["REMOTE_ADDR"];
        //    return ipaddress;
        //}

        //public static string GetBrowserDetails()
        //{
        //    string browserDetails = string.Empty;
        //    System.Web.HttpBrowserCapabilities browser = HttpContext.Current.Request.Browser;
        //    browserDetails =
        //    "Name = " + browser.Browser + "," +
        //    "Type = " + browser.Type + ","
        //    + "Version = " + browser.Version + ","
        //    + "Major Version = " + browser.MajorVersion + ","
        //    + "Minor Version = " + browser.MinorVersion + ","
        //    + "Platform = " + browser.Platform + ","
        //    + "Is Beta = " + browser.Beta + ","
        //    + "Is Crawler = " + browser.Crawler + ","
        //    + "Is AOL = " + browser.AOL + ","
        //    + "Is Win16 = " + browser.Win16 + ","
        //    + "Is Win32 = " + browser.Win32 + ","
        //    + "Supports Frames = " + browser.Frames + ","
        //    + "Supports Tables = " + browser.Tables + ","
        //    + "Supports Cookies = " + browser.Cookies + ","
        //    + "Supports VBScript = " + browser.VBScript + ","
        //    + "Supports JavaScript = " + "," +
        //    browser.EcmaScriptVersion.ToString() + ","
        //    + "Supports Java Applets = " + browser.JavaApplets + ","
        //    + "Supports ActiveX Controls = " + browser.ActiveXControls
        //    + ","
        //    + "Supports JavaScript Version = " +
        //    browser["JavaScriptVersion"];
        //    return browserDetails;
        //}
    }
}
