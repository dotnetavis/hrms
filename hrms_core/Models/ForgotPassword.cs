﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class ForgotPassword:CommonAttributeModel
    {
        public string username;
        public string newpassword;
        public string oldpassword;
    }
}
