﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
  
        public class TaskAssignModel : TaskAssign
        {
            public List<TaskAssignModel> TaskAssignModelList { get; set; }
          
            public TaskAssignModel()
            {
                TaskAssignModelList = new List<TaskAssignModel>();
            }
        }
    }

