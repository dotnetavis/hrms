﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IConfigurationServices
    {
        /// <summary>
        /// Get the Configuration Data,
        /// Created By : Bilal 
        ///On Date: 18/05/2018
        /// </summary>
        /// <returns></returns>
        List<ConfigurationModel> GetConfigurationSettings();

        /// <summary>
        /// Save the Configuration Data,
        /// Created By : Bilal 
        ///On Date: 18/05/2018
        /// </summary>
        /// <returns></returns>
        string SaveConfigurationSettings(ConfigurationModel model);

        /// <summary>
        /// Update the Configuration Data,
        /// Created By : Bilal 
        ///On Date: 10/08/2018
        /// </summary>
        /// <returns></returns>
        string UpdateConfigurationSettings(ConfigurationModel model);

        /// <summary>
        /// Get the Email Configuration Data,
        /// Created By : Bilal 
        ///On Date: 18/05/2018
        /// </summary>
        /// <returns></returns>
        List<EmailConfigurationModel> GetConfigurationEmail();

        /// <summary>
        /// Save the Email Configuration Data,
        /// Created By : Bilal 
        ///On Date: 18/05/2018
        /// </summary>
        /// <returns></returns>
        string SaveEmailConfiguration(EmailConfigurationModel model);

        /// <summary>
        /// Update the Email Configuration Data,
        /// Created By : Bilal 
        ///On Date: 10/08/2018
        /// </summary>
        /// <returns></returns>
        string UpdateEmailConfiguration(EmailConfigurationModel model);

        /// <summary>
        /// Sent Email To SuperAdmin,
        /// Created By : Bilal 
        ///On Date: 02/08/2018
        /// </summary>
        /// <returns></returns>
        string SendMail(string ToMail, string Subject, string Body, string AssociateName);


    }
}
