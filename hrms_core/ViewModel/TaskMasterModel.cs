﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class TaskMasterModel: TaskMaster
    {
        public List<TaskMasterModel> TaskMasterModelList { get; set; }
        public string project_name { get; set; }
        public string associate_name { get; set; }
        public List<DepartmentModel> DepartmentList { get; set; }
        public List<ProjectMasterModel> ProjectList { get; set; }
        public List<ProjectDeptAssignModel> ProjectAssignList { get; set; }
        public List<TaskAssignModel> TaskAssignModelList { get; set; }
        public List<AssociateMasterModel> AssociateList { get; set; }
        public List<AssociateMainModel> AssociateMainList { get; set; }
        public bool is_selected { get; set; }
        public int? reporting_id { get; set; }
        public int associate_id { get; set; }

        public TaskMasterModel()
        {
            TaskMasterModelList = new List<TaskMasterModel>();
            DepartmentList = new List<DepartmentModel>();
            ProjectList = new List<ProjectMasterModel>();
            TaskAssignModelList = new List<TaskAssignModel>();
            AssociateList = new List<AssociateMasterModel>();
            ProjectAssignList = new List<ProjectDeptAssignModel>();
            AssociateMainList = new List<AssociateMainModel>();
        }

    }
}
