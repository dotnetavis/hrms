﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class QualificationModel: Qualification
    {
        public List<QualificationModel> QualificationModelList { get; set; }
        public QualificationModel()
        {
            QualificationModelList = new List<QualificationModel>();
        }

    }
}
