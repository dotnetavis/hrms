﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class AssociateSkillModel: AssociateSkill
    {
        public List<AssociateSkillModel> AssociateSkillModelList { get; set; }
        public AssociateSkillModel()
        {
            AssociateSkillModelList = new List<AssociateSkillModel>();
        }
    }
}
