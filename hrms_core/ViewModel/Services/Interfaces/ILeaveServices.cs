﻿using hrms_core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface ILeaveServices
    {
        void getactiveleavetype();
        void insertleaveapplication();
        void getleavestatus(String username, int associate_code);
        void getassociateandreportingemailid(int associate_code);
        void gethodemailid(int associate_code);
        void checkauthentication(int associate_code, int status);
        void getusername(int leaveapp_id);
        void reportinglogin(String username, int userpass);
        void getleavedetail(int leaveapp_id);
        void getleavedetaildirect(int leaveapp_id);
        void updateleavestatus(int leaveapp_id);
        void getLeaveDetailsExport(String leaveappliedby);
        void ExcludeHodId(int associate_code);
        void getApprovedLeaveList(String leaveappliedby);
        void getLeaveListOnSearchClick(String leaveappliedby, DateTime notificationdate);
        void getAllApprovedLeaveList();
        void GetLeaveDetailByDate();
        void getAllApprovedLeaveListSearch();
        void getAuthorizedPersonName(int associate_code);
        void LeaveGranted_DateExceed();
        void GetLeaveColumnHeader();
        void CheckLeave_ValidDateDate();
        void getLeavetypeMasterList();
        void insertLeavetypeMasterList();
        void editLeavetypeMasterList(int leaveid);
        void updateLeavetypeMasterList(int leaveid);
        void updateLeavetypeStatus(int leaveid);
        void getAppliedLeave(int reportingperson_id);
        void getAssociateid(int associate_code);
        void getLeaveApproveRejectDetail(int leaveapp_id);
        List<LeaveApplicationModel> GetLeave();
        int SaveDetails(LeaveApplicationModel model);
        List<DepartmentModel> GetDepartmentList();
        List<OpeningLeaveModel> GetOpeningLeave();
        string SaveLeaveType(LeaveTypeVM model);
        string UpdateLeaveType(LeaveTypeVM model);
        string SaveOpeningLeave(OpeningLeaveModel openingLeave);
        bool SendLeaveReport(LeaveApplicationModel model, HttpContext httpContext, IConfiguration configuration);
        List<int> GetReportingData(int associateid);
        List<LeaveApplicationModel> GetLeaveData(int leaveid);
        List<AssociateMasterModel> GetUsers(int associateid);
        bool SaveLeaveAppMains(LeaveAppMainModel model);
        bool SendLeaveApprovalDisapprovalReport(LeaveApplicationModel model, HttpContext httpContext, IConfiguration configuration);
        bool CancelLeave(LeaveApplicationModel model);
    }
}
