﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace hrms_core.ViewModel.Services.Interfaces
{
   public interface IForgotPasswordServices
    {
      bool SendForgetPassword(UserModel userModel, HttpContext httpContext, IConfiguration configuration);
    }
}
