﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using NUglify.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class LeaveServices : ILeaveServices
    {
        private readonly ApplicationDbContext _db;
        //  private IConfiguration configuration;
        public LeaveServices(ApplicationDbContext options)
        {
            //configuration = conf;
            _db = options;
        }

        /// <summary>
        /// Get the Leave Data,
        /// Created By : Himanshu
        /// </summary>
        /// <returns></returns>
        public List<LeaveApplicationModel> GetLeave()
        {
            LeaveApplicationModel model = new LeaveApplicationModel();
            try
            {
                model.LeaveApplicationModelList = (from dbleave in _db.LeaveApplications.Where(x=>x.isactive==true)
                                                   join dbltype in _db.LeaveTypeMasters on dbleave.leavetype_id equals dbltype.id
                                                   join mas in _db.AssociateMasters on dbleave.createdby equals mas.id
                                                   from dbapprove in _db.AssociateMasters.Where(x => x.id == dbleave.approvedby).DefaultIfEmpty()
                                                   select new LeaveApplicationModel
                                                   ()
                                                   {
                                                       id = dbleave.id,
                                                       leaveappliedby = dbleave.leaveappliedby,
                                                       employeereason = dbleave.employeereason,
                                                       employerreason = dbleave.employerreason,
                                                       approvedby = dbleave.approvedby,
                                                       status = dbleave.status,
                                                       createdby = dbleave.createdby,
                                                       leavetype_id = dbleave.leavetype_id,
                                                       notificationdate = dbleave.notificationdate,
                                                       startdate = dbleave.startdate,
                                                       enddate = dbleave.enddate,
                                                       startsession = dbleave.startsession,
                                                       endsession = dbleave.endsession,
                                                       requested_no_of_leaves = dbleave.requested_no_of_leaves,
                                                       associatecode=mas.associate_code,
                                                       createdbyname = dbleave.createdbyname,
                                                       updatedbyname = dbleave.updatedbyname,
                                                       createdon = dbleave.createdon,
                                                       updatedon = dbleave.updatedon,
                                                       isactive = mas.isactive,
                                                       approvedbyname = dbapprove.associate_name,
                                                       leave_type_name = dbltype.leave_code
                                                   }).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name),_db);
            }
            return model.LeaveApplicationModelList;
        }

        /// <summary>
        /// Save Leave Data,
        /// Created By : Himanshu
        /// </summary>
        /// <returns></returns>
        public int SaveDetails(LeaveApplicationModel model)
        {

            int Result = 0;
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {

                    try
                    {
                        LeaveApplication leave = new LeaveApplication();
                        {
                            leave.leaveappliedby = model.leaveappliedby;
                            leave.employeereason = model.employeereason;
                            leave.approvedby = model.approvedby;
                            leave.status = model.status;
                            leave.leavetype_id = model.leavetype_id;
                            leave.notificationdate = model.notificationdate;
                            leave.startdate = model.startdate;
                            leave.enddate = model.enddate;
                            leave.startsession = model.startsession;
                            leave.endsession = model.endsession;
                            leave.requested_no_of_leaves = model.requested_no_of_leaves;
                            leave.createdby = model.createdby;
                            leave.createdon = DateTime.Now;
                            leave.createdbyname = model.createdbyname;
                            leave.isactive = true;
                            leave.updatedby = model.updatedby;
                            leave.updatedbyname = model.updatedbyname;
                            leave.updatedon = DateTime.Now;
                            _db.LeaveApplications.Add(leave);
                            _db.SaveChanges();
                            scope.Complete();
                            Result = leave.id;
                        }
                    }
                    catch (Exception ex)
                    {
                        scope.Dispose();
                        CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                        Result = 0;
                    }
                }
            }

            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = 0;
            }
            return Result;

        }

        public bool CancelLeave(LeaveApplicationModel model)
        {

            bool Result=true;
            try
            {
                var data = _db.LeaveApplications.FirstOrDefault(x => x.id == model.id);
                if (data!=null){
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {

                        try
                        {
                            
                            
                                data.leaveappliedby = model.leaveappliedby;
                                data.employeereason = model.employeereason;
                                data.approvedby = model.approvedby;
                                data.status = model.status;
                                data.leavetype_id = model.leavetype_id;
                                data.notificationdate = model.notificationdate;
                                data.startdate = model.startdate;
                                data.enddate = model.enddate;
                                data.startsession = model.startsession;
                                data.endsession = model.endsession;
                                data.requested_no_of_leaves = model.requested_no_of_leaves;
                                data.createdby = model.createdby;
                                data.createdon = DateTime.Now;
                                data.createdbyname = model.createdbyname;
                                data.isactive = false;
                                data.updatedby = model.updatedby;
                                data.updatedbyname = model.updatedbyname;
                                data.updatedon = DateTime.Now;
                                _db.SaveChanges();

                            var leaveApp = _db.LeaveAppMains.FirstOrDefault(x => x.leaveapp_id == data.id);
                            if(leaveApp!= null)
                            {
                                leaveApp.isactive = false;
                            }
                                scope.Complete();
                            model.isactive = false;
                                Result = true;
                            
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = false;
                        }
                    }
                }
                
            }

            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = false;
            }
            return Result;

        }
        /// <summary>
        /// Get Department Data to save applicable leave,
        /// Created By : Himanshu
        /// </summary>
        /// <returns></returns>
        public List<DepartmentModel> GetDepartmentList()
        {
            DepartmentModel department = new DepartmentModel();
            try
            {
                department.DepartmentModelList = _db.Departments.Select(x => new DepartmentModel
                {
                    id = x.id,
                    applicable_leave = x.applicable_leave,
                    createdby = x.createdby,
                    createdon = DateTime.Now,
                    createdbyname = x.createdbyname,
                    isactive = x.isactive,
                    updatedby = x.updatedby,
                    updatedbyname = x.updatedbyname,
                    updatedon = DateTime.Now,
                }).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return department.DepartmentModelList;
        }

        /// <summary>
        /// Save Leave Type,
        /// Created By : Himanshu
        /// </summary>
        /// <returns></returns>
        public string SaveLeaveType(LeaveTypeVM model)
        {

            {
                {
                    string Result = string.Empty;
                    LeaveTypeMaster leave_obj = new LeaveTypeMaster();
                    try
                    {
                        //Check for Duplicate Record
                        bool bool_sameNameExists = _db.LeaveTypeMasters.Any(x => x.leave_code.ToLower().Trim() == model.leave_code.ToLower().Trim());
                        if (bool_sameNameExists)
                        {
                            Result = MessageHelper.Leave_Duplicate;
                        }
                        else
                        {
                            using (var scope = new System.Transactions.TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                            {
                                try
                                {
                                    {
                                        leave_obj.leave_code = model.leave_code;
                                        leave_obj.leavetext = model.leavetext;
                                        leave_obj.min_days = model.min_days;
                                        leave_obj.max_days = model.max_days;
                                        leave_obj.leave_apply = model.leave_apply;
                                        leave_obj.before_min = model.before_min;
                                        leave_obj.leave_type = model.leave_type;
                                        leave_obj.carry_forward = model.carry_forward;
                                        leave_obj.leave_balance_check = model.leave_balance_check;
                                        leave_obj.holiday_inclusive = model.holiday_inclusive;
                                        leave_obj.createdby = model.createdby;
                                        leave_obj.createdon = model.createdon;
                                        leave_obj.updatedby = model.updatedby;
                                        leave_obj.updatedon = model.updatedon;
                                        leave_obj.isactive = model.isactive;
                                        leave_obj.updatedbyname = model.updatedbyname;
                                        leave_obj.createdbyname = model.createdbyname;
                                        leave_obj.is_default = model.is_default;
                                        _db.LeaveTypeMasters.Add(leave_obj);
                                        _db.SaveChanges();
                                        scope.Complete();
                                        Result = MessageHelper.Leave_AddSuccess;
                                    }
                                }

                                catch (Exception ex)
                                {
                                    scope.Dispose();
                                    CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                    Result = MessageHelper.Leave_AddSuccess;
                                }
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                        Result = MessageHelper.Leave_AddDetails;
                    }
                    return Result;
                }

            }
        }
        /// <summary>
        /// Update Leave Type Data,
        /// Created By : Himanshu
        /// </summary>
        /// <returns></returns>
        public string UpdateLeaveType(LeaveTypeVM model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.LeaveTypeMasters.FirstOrDefault(x => x.id == model.id);
                if (data != null)

                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {

                        data.leave_code = model.leave_code;
                        data.leavetext = model.leavetext;
                        data.min_days = model.min_days;
                        data.max_days = model.max_days;
                        data.leave_apply = model.leave_apply;
                        data.before_min = model.before_min;
                        data.leave_type = model.leave_type;
                        data.carry_forward = model.carry_forward;
                        data.leave_balance_check = model.leave_balance_check;
                        data.holiday_inclusive = model.holiday_inclusive;
                        data.createdby = model.createdby;
                        data.createdon = model.createdon;
                        data.updatedby = model.updatedby;
                        data.updatedon = model.updatedon;
                        data.isactive = model.isactive;
                        data.is_default = model.is_default;
                        data.updatedbyname = model.updatedbyname;
                        data.createdbyname = model.createdbyname;
                        _db.SaveChanges();
                        scope.Complete();
                        Result = MessageHelper.Leave_UpdateSuccess;
                    }
                }
            }

            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Leave_UpdateError;
            }
            return Result;
        }

        /// <summary>
        /// Save Opening Leave Data,
        /// Created By : Himanshu
        /// </summary>
        /// <returns></returns>
        public string SaveOpeningLeave(OpeningLeaveModel openingLeave)
        {
            OpeningLeave open_leave = new OpeningLeave();
            string Result = string.Empty;
            try
            {
                //Check for Duplicate Record
                bool bool_sameNameExists = _db.OpeningLeaves.Any(x => x.emp_id == openingLeave.emp_id && x.year == DateTime.Now.Year);
                if (bool_sameNameExists)
                {
                    open_leave = _db.OpeningLeaves.FirstOrDefault(x => x.emp_id == openingLeave.emp_id && x.year == DateTime.Now.Year);
                    open_leave.updatedon = DateTime.Now;
                    open_leave.opening_leave = openingLeave.casual_leave;
                    open_leave.createdby = openingLeave.createdby;
                    open_leave.updatedby = openingLeave.updatedby;
                    open_leave.createdbyname = openingLeave.createdbyname;
                    open_leave.updatedbyname = openingLeave.updatedbyname;
                    using (var scope = new System.Transactions.TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.OpeningLeave_UpdateSuccess;
                        }

                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OpeningLeave_UpdateError;
                        }

                    }
                }
                else
                {
                    List<OpeningLeave> leave = new List<OpeningLeave>();
                    leave.Insert(0, new OpeningLeave
                    {
                        emp_id = openingLeave.emp_id,
                        leave_id = 2,
                        createdon = DateTime.Now,
                        updatedon = DateTime.Now,
                        opening_leave = openingLeave.casual_leave,
                        createdby = openingLeave.createdby,
                        updatedby = openingLeave.updatedby,
                        createdbyname = openingLeave.createdbyname,
                        updatedbyname = openingLeave.updatedbyname,
                        isactive = true,
                        year = DateTime.Now.Year
                    });
                    //leave.Insert(1, new OpeningLeave
                    //{
                    //    emp_id = openingLeave.emp_id,
                    //    leave_id = 1,
                    //    createdon = DateTime.Now,
                    //    updatedon = DateTime.Now,
                    //    opening_leave = openingLeave.earn_leave,
                    //    createdby = openingLeave.createdby,
                    //    updatedby = openingLeave.updatedby,
                    //    createdbyname = openingLeave.createdbyname,
                    //    updatedbyname = openingLeave.updatedbyname,
                    //    isactive = true,
                    //    year = DateTime.Now.Year
                    //});
                    using (var scope = new System.Transactions.TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            {
                                foreach (var item in leave)
                                {
                                    _db.OpeningLeaves.Add(item);
                                    _db.SaveChanges();
                                }
                                scope.Complete();
                                Result = MessageHelper.OpeningLeave_AddSuccess;

                            }
                        }

                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OpeningLeave_UpdateError;
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OpeningLeave_UpdateError;
            }
            return Result;
        }

        /// <summary>
        /// Get Opening Leave Data,
        /// Created By : Himanshu
        /// </summary>
        /// <returns></returns>
        public List<OpeningLeaveModel> GetOpeningLeave()
        {
            OpeningLeaveModel model = new OpeningLeaveModel();
            try
            {
                model.OpeningLeaveModelList = _db.OpeningLeaves.Select(x => new OpeningLeaveModel
                {


                    id = x.id,
                    createdby = x.createdby,
                    createdbyname = x.createdbyname,
                    createdon = x.createdon,
                    emp_id = x.emp_id,
                    leave_id = x.leave_id,
                    isactive = x.isactive,
                    opening_leave = x.opening_leave,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    updatedbyname = x.updatedbyname,
                }).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.OpeningLeaveModelList;
        }

        public List<int> GetReportingData(int associateid)
        {
            SessionModel model = new SessionModel();
            LeaveApplicationModel leave = new LeaveApplicationModel();
            try
            {
                var _data = (from main in _db.AssociateMains.Where(x => x.associate_id == associateid)
                             join mas in _db.AssociateMasters on main.associate_id equals mas.id
                             join usr in _db.Users on mas.associate_code equals usr.username
                             select new SessionModel
                             {
                                 associate_id = main.associate_id,
                                 reporting_person_id = main.reportingperson_id == null ? 0 : main.reportingperson_id.Value,
                                 associate_code = mas.associate_code,
                                 user_id = usr.id,
                                 username = usr.username,
                                 associate_name = mas.associate_name,
                             }).FirstOrDefault();


                var additional = (from main in _db.AssociateMains.Where(x => x.associate_id == associateid)
                                  join addReport in _db.AssociateAdditionalInfos on main.associate_id equals addReport.associate_id
                                  select new SessionModel
                                  {
                                      reporting_person_id = addReport.reporting_person_id,
                                  }).ToList().Distinct();


                List<int> reporting_ids = new List<int>();

                reporting_ids.Add(_data.reporting_person_id);

                foreach (var item in additional)
                {
                    reporting_ids.Add(item.reporting_person_id);
                }

                return reporting_ids.Distinct().ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return null;
        }
        /// <summary>
        /// Getting Users According To Current Associate ID,
        /// Created By : Navodit
        /// </summary>
        /// <returns></returns>
        public List<AssociateMasterModel> GetUsers(int associateid)
        {
            AssociateMasterModel model = new AssociateMasterModel();
            try
            {
                if (associateid<=0)
                {
                    model.AssociateMasterModelList = _db.AssociateMasters.Where(x => x.isactive == true)
                .Select(x => new AssociateMasterModel
                {
                    id = x.id,
                    associate_name = x.associate_name,
                    applicable_to = x.applicable_to,
                    application_date = x.application_date,
                    associate_code = x.associate_code,
                    createdby = x.createdby,
                    createdon = x.createdon,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    isactive = x.isactive
                }).OrderByDescending(m => m.updatedon).ToList();
                }
                else
                {

                    model.AssociateMasterModelList = (from main in _db.AssociateMains.Where(x => x.isactive == true && x.reportingperson_id == associateid)
                                                      join mast in _db.AssociateMasters.Where(x => x.isactive == true) on main.associate_id equals mast.id
                                                      select new AssociateMasterModel
                                                      {
                                                          id = mast.id,
                                                          associate_name = mast.associate_name,
                                                          applicable_to = mast.applicable_to,
                                                          application_date = mast.application_date,
                                                          associate_code = mast.associate_code,
                                                          createdby = mast.createdby,
                                                          createdon = mast.createdon,
                                                          updatedby = mast.updatedby,
                                                          updatedon = mast.updatedon,
                                                          isactive = mast.isactive
                                                      }).OrderByDescending(m => m.updatedon).ToList();
                    List<AssociateMasterModel> Aditionalassociate = new List<AssociateMasterModel>();
                    Aditionalassociate = (from add in _db.AssociateAdditionalInfos.Where(x => x.reporting_person_id == associateid)
                                          join mast in _db.AssociateMasters.Where(x => x.isactive == true) on add.associate_id equals mast.id
                                          select new AssociateMasterModel
                                          {
                                              id = mast.id,
                                              associate_name = mast.associate_name,
                                              applicable_to = mast.applicable_to,
                                              application_date = mast.application_date,
                                              associate_code = mast.associate_code,
                                              createdby = mast.createdby,
                                              createdon = mast.createdon,
                                              updatedby = mast.updatedby,
                                              updatedon = mast.updatedon,
                                              isactive = mast.isactive
                                          }).OrderByDescending(m => m.updatedon).ToList();
                    foreach (var item in Aditionalassociate)
                    {
                        model.AssociateMasterModelList.Add(item);
                    }
                    AssociateMasterModel associate = new AssociateMasterModel();
                    var associatemodel = _db.AssociateMasters.FirstOrDefault(x => x.id == associateid);
                    associate = new AssociateMasterModel()
                    {
                        id = associatemodel.id,
                        associate_name = associatemodel.associate_name,
                        applicable_to = associatemodel.applicable_to,
                        application_date = associatemodel.application_date,
                        associate_code = associatemodel.associate_code,
                        createdby = associatemodel.createdby,
                        createdon = associatemodel.createdon,
                        updatedby = associatemodel.updatedby,
                        updatedon = associatemodel.updatedon,
                        isactive = associatemodel.isactive
                    };
                    model.AssociateMasterModelList.Add(associate);
                }

            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            var DistinctData = model.AssociateMasterModelList.DistinctBy(x => x.id).ToList();
            return DistinctData;
            // return null;
        }
        /// <summary>
        /// Send Leave Application Mail,
        /// Created By : Himanshu
        /// </summary>
        /// <returns></returns>
        public bool SendLeaveReport(LeaveApplicationModel model, HttpContext httpContext, IConfiguration configuration)
        {
            string str = string.Empty;
            bool result;
            var _emailData = _db.EmailConfigurations.FirstOrDefault();
            MailMessage mail = new MailMessage();
            try
            {
                StringBuilder URL = new StringBuilder();
                // Requested modification in url,, need to access the url with 'https'
                //URL.Append(String.Format("{0}://{1}", httpContext.Request.Scheme, httpContext.Request.Host.Host));
                URL.Append(String.Format("https://{0}", httpContext.Request.Host.Host));
                URL.Append(String.Format(":{0}", httpContext.Request.Host.Port));

                URL.Append(String.Format("/{0}/{1}?LeaveID={2}", "Leave", "Login", model.leaveapp_id));


                if (_emailData != null)
                {
                    string FromEmail = _emailData.email_send_from;
                    string Pass = _emailData.password;
                    List<string> mails = new List<string>();
                    SmtpClient SmtpServer = new SmtpClient(_emailData.smtp);
                    mail.From = new MailAddress(FromEmail, model.username);
                    if (!string.IsNullOrWhiteSpace(model.reporting_email))
                    {
                        if (mails != null && !mails.Contains(model.reporting_email))
                        {
                            mails.Add(model.reporting_email);
                            mail.To.Add(model.reporting_email);
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(model.hod_email))
                    {
                        if (mails != null && !mails.Contains(model.hod_email))
                        {
                            mails.Add(model.hod_email);
                            mail.To.Add(model.hod_email);
                        }
                    }
                    //
                    if (mail.To.Count<=0)
                    {
                        mail.To.Add(configuration.GetValue<string>("ConnectionStrings:DefaultReportingPerson"));
                    }
                    string MailToList ="";
                    MailToList = mail.To.ToString();
                    //
                    if (model.additional_reporting_email != null && model.additional_reporting_email.Count != 0)
                    {
                        foreach (var item in model.additional_reporting_email.Distinct())
                        {
                            if (mails != null && !mails.Contains(item))
                            {
                                mails.Add(item);
                                if(!MailToList.Contains(item))
                                    mail.CC.Add(item);
                            }
                            // mail.CC.Add(item);
                        }

                    }
                    string[] emails = configuration.GetValue<string>("ConnectionStrings:LeaveAddition").Split(';');
                    if (emails != null && emails.Count() > 0)
                    {
                        foreach (var email in emails)
                        {
                            if (mails != null && !mails.Contains(email))
                            {
                                mails.Add(email);
                                if (!MailToList.Contains(email))
                                    mail.CC.Add(email);
                            }
                            // mail.CC.Add(email);
                        }
                    }
                    if (model.official_email != null)
                    {
                        if (mails != null && !mails.Contains(model.official_email))
                        { 
                            mails.Add(model.official_email);
                            if (!MailToList.Contains(model.official_email))
                                mail.CC.Add(model.official_email);
                        }
                    }
                    mail.ReplyToList.Add(model.official_email);
                    //mail.ReplyTo.Add(model.official_email);
                    
                    mail.IsBodyHtml = true;
                    if (model.isactive == false)
                    {
                        mail.Subject = "Leave Cancellation";
                        //mail body 
                        mail.Body = string.Format("<b>" + model.associatecode + "</b><br/><b>" + model.username + "</b> has cancelled the leave.<br/><br/>" + "From : " + model.startdate.ToString("dd-MMM-yyyy") + " to " + model.enddate.ToString("dd-MMM-yyyy") + "<br/>Total Days : " + model.requested_no_of_leaves + ".<br/><i>Declaration:If you have already received e-mail for Leave Application then ignore this mail.</i>");
                    }
                    else
                    {
                        mail.Subject = "Leave Application";
                        if (model.requested_no_of_leaves == 0.5)
                        {
                            mail.Body = string.Format("<b>" + model.associatecode + "</b><br/><b>" + model.username + "</b> has applied for leave.<br/><br/>" + "From : " + model.startdate.ToString("dd-MMM-yyyy") + " to " + model.enddate.ToString("dd-MMM-yyyy") + "<br/>Session : " + model.sessionName + "<br/>Total Days : " + model.requested_no_of_leaves + "<br/>Reason : " + model.employeereason + "<br/>Take action using below link.<br/><br/><a href='" + Convert.ToString(URL) + "'>Approve/Disapprove<a/><br/><br/><i>Declaration:If you have already received e-mail for Leave Application then ignore this mail.</i>");
                        }
                        else
                        {
                            mail.Body = string.Format("<b>" + model.associatecode + "</b><br/><b>" + model.username + "</b> has applied for leave.<br/><br/>" + "From : " + model.startdate.ToString("dd-MMM-yyyy") + " to " + model.enddate.ToString("dd-MMM-yyyy") + "<br/>Total Days : " + model.requested_no_of_leaves + "<br/>Reason : " + model.employeereason + "<br/>Take action using below link.<br/><br/><a href='" + Convert.ToString(URL) + "'>Approve/Disapprove<a/><br/><br/><i>Declaration:If you have already received e-mail for Leave Application then ignore this mail.</i>");
                        }
                    }
                    
                    SmtpServer.Host = _emailData.smtp;
                    SmtpServer.EnableSsl = _emailData.ssl;
                    ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate,
                 X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                    SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.Port = _emailData.port;
                    
                    SmtpServer.Credentials = new System.Net.NetworkCredential(FromEmail, Pass);
                  
                    SmtpServer.Send(mail);
                    str = MessageHelper.EmailSent;
                    result = true;
                }
                return true;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                result = false;
            }
            string status = string.Empty;
            if (result)
                status = "Success";
            else
                status = "Failed";
            CommonUtility.WriteMailLogs("From =>" + _emailData.email_send_from + " To=>" + mail.To + "CC=>" + mail.CC + " Date=>" + DateTime.Now.ToShortDateString() + " Body=>" + mail.Body + " Status=>" + status, "SendLeaveReport");
            return result;
        }



        public bool SendLeaveApprovalDisapprovalReport(LeaveApplicationModel model, HttpContext httpContext, IConfiguration configuration)
        {
            string str = string.Empty;
            bool result;
            var _emailData = _db.EmailConfigurations.FirstOrDefault();
            MailMessage mail = new MailMessage();
            try
            {
                if (_emailData != null)
                {
                    string FromEmail = _emailData.email_send_from;
                    string Pass = _emailData.password;

                    SmtpClient SmtpServer = new SmtpClient(_emailData.smtp);
                    mail.From = new MailAddress(model.official_email, model.username);
                    mail.To.Add(model.employer_associate_email);
                    if (model.Mail_to_emails != null && model.Mail_to_emails.Count > 0)
                    {
                        foreach (var item in model.Mail_to_emails)
                        {
                            mail.To.Add(item);
                        }
                        string[] emails = configuration.GetValue<string>("ConnectionStrings:LeaveAddition").Split(';');
                        if (emails != null && emails.Count() > 0)
                        {
                            foreach (var email in emails)
                            {
                                mail.CC.Add(email);
                            }
                        }
                    }
                    mail.IsBodyHtml = true;
                    mail.Subject = "Leave Application Status";

                    if (model.status_text == "Approved")
                    {
                        mail.Body = string.Format("<b>" + model.employer_associate_code + "<b><br/><b>" + model.employer_associate_name + "</b><br/><br/>" + model.leave_name + " from " + model.startdate.ToString("dd-MMM-yyyy") + " to " + model.enddate.ToString("dd-MMM-yyyy") + "<br/>Total Days : " + model.requested_no_of_leaves + " <br/>Reason : " + model.employeereason + " <br/>Remark : " + model.employerreason + "<br/>Apply Date : " + model.notificationdate.ToString("dd-MMM-yyyy") + "<br/>Status : " + model.status_text + "<br/>ApprovedBy : " + model.associatename + "<br/><br/><i>Declaration:If you have already received e-mail for Leave Application then ignore this mail.</i>");
                    }
                    else if (model.status_text == "DisApproved")
                    {
                        mail.Body = string.Format("<b>" + model.employer_associate_code + "<b><br/><b>" + model.employer_associate_email + "</b><br/><br/>" + model.leave_name + " from " + model.startdate.ToString("dd-MMM-yyyy") + " to " + model.enddate.ToString("dd-MMM-yyyy") + "<br/>Total Days : " + model.requested_no_of_leaves + " <br/>Reason : " + model.employeereason + " <br/>Remark : " + model.employerreason + "<br/>Apply Date : " + model.notificationdate.ToString("dd-MMM-yyyy") + "<br/>Status : " + model.status_text + "<br/>DisApprovedBy : " + model.associatename + "<br/><br/><i>Declaration:If you have already received e-mail for Leave Application then ignore this mail.</i>");
                    }
                    else
                    {
                        mail.Body = string.Format("<b>" + model.employer_associate_code + "<b><br/><b>" + model.employer_associate_name + "</b><br/><br/>" + model.leave_name + " from " + model.startdate.ToString("dd-MMM-yyyy") + " to " + model.enddate.ToString("dd-MMM-yyyy") + "<br/>Total Days : " + model.requested_no_of_leaves + " <br/>Reason : " + model.employeereason + " <br/>Remark : " + model.employerreason + "<br/>Apply Date : " + model.notificationdate.ToString("dd-MMM-yyyy") + "<br/>Status : " + model.status_text + "<br/>ApprovedBy : " + model.associatename + "<br/><br/><i>Declaration:If you have already received e-mail for Leave Application then ignore this mail.</i>");
                    }
                    SmtpServer.Host = _emailData.smtp;
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.Port = _emailData.port;
                    SmtpServer.Credentials = new System.Net.NetworkCredential(FromEmail, Pass);
                    SmtpServer.EnableSsl = _emailData.ssl;
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                 System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };
                    SmtpServer.Send(mail);
                    result = true;
                }
                else
                {
                    result = false;
                }

            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                result = false;
            }
            string status = string.Empty;
            if (result)
                status = "Success";
            else
                status = "Failed";
            CommonUtility.WriteMailLogs("From =>" + _emailData.email_send_from + " To=>" + mail.To + " Date=>" + DateTime.Now.ToShortDateString() + " Body=>" + mail.Body + " Status=>" + status, "SendLeaveApprovalDisapprovalReport");
            return result;
        }


        public List<LeaveApplicationModel> GetLeaveData(int leaveid)
        {
            LeaveApplicationModel model = new LeaveApplicationModel();
            try
            {
                var data = (from leave in _db.LeaveApplications.Where(x => x.id == leaveid)
                            select new LeaveApplicationModel
                            {
                                id = leave.id,
                                createdby = leave.createdby,
                                createdbyname = leave.createdbyname,
                                createdon = leave.createdon,
                                leavetype_id = leave.leavetype_id,
                                leaveappliedby = leave.leaveappliedby,
                                startsession=leave.startsession,
                                endsession=leave.endsession,
                                notificationdate = leave.notificationdate,
                                enddate = leave.enddate,
                                startdate = leave.startdate,
                                status = leave.status,
                                employerreason = leave.employerreason,
                                approvedby = leave.approvedby,
                                employeereason = leave.employeereason,
                                requested_no_of_leaves = leave.requested_no_of_leaves,
                                updatedby = leave.updatedby,
                                updatedon = leave.updatedon,
                                updatedbyname = leave.updatedbyname,
                                isactive = leave.isactive

                            }).ToList();
                model.LeaveApplicationModelList = data;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.LeaveApplicationModelList;
        }

        /// <summary>
        /// Save Leave App Mains Data,
        /// Created By : Himanshu
        /// </summary>
        /// <returns></returns>
        public bool SaveLeaveAppMains(LeaveAppMainModel model)
        {
            using (var scope = new System.Transactions.TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    LeaveAppMain leave = new LeaveAppMain();
                    leave.isactive = model.isactive;
                    leave.leaveapp_id = model.leaveapp_id;
                    leave.createdby = model.createdby;
                    leave.updatedby = model.updatedby;
                    leave.updatedbyname = model.updatedbyname;
                    leave.createdbyname = model.createdbyname;
                    leave.createdon = model.createdon;
                    leave.updatedon = model.updatedon;
                    leave.leave_date = model.leave_date;
                    leave.totalleave = model.totalleave;

                    _db.LeaveAppMains.Add(leave);
                    _db.SaveChanges();
                    scope.Complete();

                    return true;
                }
                catch (Exception ex)
                {
                    CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                    scope.Dispose();
                    return false;
                }
            }
        }

        public void getactiveleavetype()
        {
            throw new NotImplementedException();
        }

        public void insertleaveapplication()
        {
            throw new NotImplementedException();
        }

        public void getleavestatus(string username, int associate_code)
        {
            throw new NotImplementedException();
        }

        public void getassociateandreportingemailid(int associate_code)
        {
            throw new NotImplementedException();
        }

        public void gethodemailid(int associate_code)
        {
            throw new NotImplementedException();
        }

        public void checkauthentication(int associate_code, int status)
        {
            throw new NotImplementedException();
        }

        public void getusername(int leaveapp_id)
        {
            throw new NotImplementedException();
        }

        public void reportinglogin(string username, int userpass)
        {
            throw new NotImplementedException();
        }

        public void getleavedetail(int leaveapp_id)
        {
            throw new NotImplementedException();
        }

        public void getleavedetaildirect(int leaveapp_id)
        {
            throw new NotImplementedException();
        }

        public void updateleavestatus(int leaveapp_id)
        {
            throw new NotImplementedException();
        }

        public void getLeaveDetailsExport(string leaveappliedby)
        {
            throw new NotImplementedException();
        }

        public void ExcludeHodId(int associate_code)
        {
            throw new NotImplementedException();
        }

        public void getApprovedLeaveList(string leaveappliedby)
        {
            throw new NotImplementedException();
        }

        public void getLeaveListOnSearchClick(string leaveappliedby, DateTime notificationdate)
        {
            throw new NotImplementedException();
        }

        public void getAllApprovedLeaveList()
        {
            throw new NotImplementedException();
        }

        public void GetLeaveDetailByDate()
        {
            throw new NotImplementedException();
        }

        public void getAllApprovedLeaveListSearch()
        {
            throw new NotImplementedException();
        }

        public void getAuthorizedPersonName(int associate_code)
        {
            throw new NotImplementedException();
        }

        public void LeaveGranted_DateExceed()
        {
            throw new NotImplementedException();
        }

        public void GetLeaveColumnHeader()
        {
            throw new NotImplementedException();
        }

        public void CheckLeave_ValidDateDate()
        {
            throw new NotImplementedException();
        }

        public void getLeavetypeMasterList()
        {
            throw new NotImplementedException();
        }

        public void insertLeavetypeMasterList()
        {
            throw new NotImplementedException();
        }

        public void editLeavetypeMasterList(int leaveid)
        {
            throw new NotImplementedException();
        }

        public void updateLeavetypeMasterList(int leaveid)
        {
            throw new NotImplementedException();
        }

        public void updateLeavetypeStatus(int leaveid)
        {
            throw new NotImplementedException();
        }

        public void getAppliedLeave(int reportingperson_id)
        {
            throw new NotImplementedException();
        }

        public void getAssociateid(int associate_code)
        {
            throw new NotImplementedException();
        }

        public void getLeaveApproveRejectDetail(int leaveapp_id)
        {
            throw new NotImplementedException();
        }
    }
}

