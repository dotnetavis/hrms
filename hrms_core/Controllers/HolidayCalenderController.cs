﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace hrms_core.Controllers
{
    public class HolidayCalenderController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IHolidayServices holidayServices => new HolidayServices(_db);
        public HolidayCalenderController(ApplicationDbContext options) => _db = options;

        [HttpGet]
        public IActionResult HolidayCalUpload()
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
                HolidayCalenderModel holiday_obj = new HolidayCalenderModel();
                holiday_obj.createdby = SesMdl.associate_id;
                holiday_obj.createdbyname = SesMdl.associate_name;
                holiday_obj.updatedby = SesMdl.associate_id;
                holiday_obj.updatedbyname = SesMdl.associate_name;
                holiday_obj.HolidayCalenderModelList = holidayServices.getHolidayCalender();
                return View(holiday_obj);
        }
        [HttpPost]
        public IActionResult HolidayCalUpload(HolidayCalenderModel model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
             if (SesMdl == null)
             {
                   return RedirectToAction("Login", "Account");
             }
                model.createdby = SesMdl.user_id;
                model.updatedby = SesMdl.user_id;
                model.createdbyname = SesMdl.username;
                model.updatedbyname = SesMdl.username;
                model = Upload();
                if(model!=null)
                {
                  TempData["msg"] = holidayServices.SaveEditHolidayCalnderDetails(model);
                }
               return RedirectToAction("HolidayCalUpload","HolidayCalender");
        }
        public HolidayCalenderModel Upload()
        {
            try
            {
                HolidayCalenderModel model = new HolidayCalenderModel();
                DataTable dataSet = new DataTable();

                if (Request.Form.Files["postedFile"] != null)
                {
                    var filename = Path.GetFileName(Request.Form.Files["postedFile"].FileName);
                    string extension = Path.GetExtension(filename).ToLower();
                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload", filename);
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload"));
                    }
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        Request.Form.Files["postedfile"].CopyToAsync(stream);
                    }
                    model.calendername = filename;
                    model.calender_path = path.ToString();
                    return model;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return null;
            }
        }
    }
}