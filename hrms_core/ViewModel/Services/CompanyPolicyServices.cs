﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.ViewModel.Services.Interfaces;
using hrms_core.EF;
using hrms_core.Models;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class CompanyPolicyServices : ICompanyPolicyServices
    {
        public ApplicationDbContext _db;
        public CompanyPolicyServices(ApplicationDbContext _Db)
        {
            _db = _Db;
        }
        public string DisablePolicy(int policy_id)
        {
            string Result = string.Empty;
            var policy = _db.CompanyPolicy.FirstOrDefault(x => x.id == policy_id);
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    policy.isactive = false;
                    _db.SaveChanges();
                    scope.Complete();
                    Result = MessageHelper.Policy_Disabled;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                    Result = MessageHelper.Policy_AddError;
                }
            }
            return Result;
        }
        public string Enablepolicy(int policy_id)
        {
            string Result = string.Empty;
            var policy = _db.CompanyPolicy.FirstOrDefault(x => x.id == policy_id);
            try
            {
                bool bool_sameYearExist = _db.CompanyPolicy.Any(x => x.policy_year == policy.policy_year && x.isactive == true);
                if (bool_sameYearExist)
                {
                    Result = MessageHelper.Policy_Duplicate;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            policy.isactive = true;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Policy_Enable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.Policy_AddError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Policy_AddError;
            }
            return Result;
        }
        public List<CompanyPolicyModel> GetPolicies()
        {
            string Result = string.Empty;
            CompanyPolicyModel CompanyPolicies = new CompanyPolicyModel();
            try
            {
                CompanyPolicies.policies = _db.CompanyPolicy
                .Select(x => new CompanyPolicyModel
                {
                    id = x.id,
                    policy_year = x.policy_year,
                    policy_content = x.policy_content,
                    updatedbyname = x.updatedbyname,
                    createdbyname = x.createdbyname,
                    isactive = x.isactive,
                    createdon = DateTime.Now,
                    updatedon = DateTime.Now,
                    createdby = x.createdby,
                    updatedby = x.updatedby,
                }).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return CompanyPolicies.policies;
        }
        public List<CompanyPolicyModel> GetIsActivePolicies()
        {
            string Result = string.Empty;
            CompanyPolicyModel CompanyPolicies = new CompanyPolicyModel();
            try
            {
                CompanyPolicies.policies = _db.CompanyPolicy
                .Select(x => new CompanyPolicyModel
                {
                    id = x.id,
                    policy_year = x.policy_year,
                    policy_content = x.policy_content,
                    updatedbyname = x.updatedbyname,
                    createdbyname = x.createdbyname,
                    isactive = x.isactive,
                    createdon = DateTime.Now,
                    updatedon = DateTime.Now,
                    createdby = x.createdby,
                    updatedby = x.updatedby,
                }).Where(x=>x.isactive==true).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return CompanyPolicies.policies;

        }
        public string SavePolicy(CompanyPolicyModel policy)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.CompanyPolicy.FirstOrDefault(x => x.isactive == true && x.policy_year == policy.policy_year);
                if (data == null)
                {
                    using (var scope = new System.Transactions.TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            CompanyPolicy companyPolicy = new CompanyPolicy();
                            {
                                companyPolicy.policy_year = policy.policy_year;
                                companyPolicy.policy_content = policy.policy_content;
                                companyPolicy.updatedbyname = policy.updatedbyname;
                                companyPolicy.createdbyname = policy.createdbyname;
                                companyPolicy.isactive = true;
                                companyPolicy.createdon = DateTime.Now;
                                companyPolicy.updatedon = DateTime.Now;
                                companyPolicy.createdby = policy.createdby;
                                companyPolicy.updatedby = policy.updatedby;
                                _db.CompanyPolicy.Add(companyPolicy);
                                _db.SaveChanges();
                                scope.Complete();
                                Result = MessageHelper.Policy_AddSuccess;
                            }

                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.Policy_AddError;
                        }
                    }
                }
                else
                {
                    Result = MessageHelper.Task_Duplicate;
                    return Result;
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Task_AddError;
            }
            return Result;
        }
        public CompanyPolicyModel getPolicyById(int? policy_id)
        {
            CompanyPolicyModel policy = new CompanyPolicyModel();
            policy = _db.CompanyPolicy
                .Select(x => new CompanyPolicyModel
                {
                    id = x.id,
                    policy_year = x.policy_year,
                    policy_content = x.policy_content,
                    updatedbyname = x.updatedbyname,
                    createdbyname = x.createdbyname,
                    isactive = true,
                    createdon = DateTime.Now,
                    updatedon = DateTime.Now,
                    createdby = x.createdby,
                    updatedby = x.updatedby,
                }).Where(x => x.id == policy_id).FirstOrDefault();
            return policy;
        }
        public string UpdatePolicy(CompanyPolicyModel model)
        {
            string Result = string.Empty;
            var data = _db.CompanyPolicy.FirstOrDefault(x => x.id == model.id);
            try
            {
                if (data != null)
                {
                    bool bool_sameNameExists = _db.CompanyPolicy.Any(x => x.policy_year == model.policy_year && x.id != model.id && x.isactive == true);
                    if (bool_sameNameExists)
                    {
                        Result = MessageHelper.Policy_Duplicate;
                    }
                    else
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            try
                            {
                                data.policy_content = model.policy_content;
                                data.policy_year = data.policy_year;
                                data.updatedon = DateTime.Now;
                                data.updatedby = model.updatedby;
                                data.updatedbyname = model.updatedbyname;
                                _db.SaveChanges();
                                scope.Complete();
                                Result = MessageHelper.Policy_Update;
                            }
                            catch (Exception ex)
                            {
                                scope.Dispose();
                                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                Result = MessageHelper.Policy_AddError;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Policy_AddError;
            }
            return Result;
        }
    }
}
