﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace hrms_core.Controllers
{
    public class EditAttendanceController : Controller
    {
        private readonly ApplicationDbContext _db;
        public IConfiguration Configuration { get; }
        private IHRAttendance hrAttendanceService => new HRAttendanceService(_db);
        private IAssociateServices associateServices => new AssociateServices(_db);
        public EditAttendanceController(ApplicationDbContext db, IConfiguration configuration)
        {
            Configuration = configuration;
            _db = db;
        }

        [HttpGet]
        public IActionResult Index()
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                HRAttendanceDataVM model = new HRAttendanceDataVM();

                model.AssociateVMList = (from master in _db.AssociateMasters.Where(x => x.isactive == true && x.is_super_admin == false)
                                         join main in _db.AssociateMains
                                         on master.id equals main.associate_id
                                         select new AssociateVM
                                         {
                                             associate_id = master.id,
                                             associate_code = master.associate_code,
                                             associate_name = master.associate_name,
                                             //designation = _db.Designations.FirstOrDefault(x => x.id == main.designation_id).designationtext,
                                             //department = _db.Departments.FirstOrDefault(x => x.id == main.department_id).departmenttext,

                                             contact = main.mobileno,
                                             DOJ = master.application_date,
                                         }).ToList();
                model.AssociateVMList.Insert(0, new AssociateVM { associate_id = 0, associate_name = "--Select--" });

                return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }
        [HttpPost]
        public IActionResult Index(HRAttendanceDataVM model, string CommandName, DateTime SelectedDate)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);

            if (SesMdl != null)
            {
                var id = SesMdl.role_id;
                model.AssociateVMList = (from master in _db.AssociateMasters.Where(x => x.isactive == true && x.is_super_admin == false)
                                         join main in _db.AssociateMains
                                         on master.id equals main.associate_id
                                         select new AssociateVM
                                         {
                                             associate_id = master.id,
                                             associate_code = master.associate_code,
                                             associate_name = master.associate_name,
                                             //designation = _db.Designations.FirstOrDefault(x => x.id == main.designation_id).designationtext,
                                             //department = _db.Departments.FirstOrDefault(x => x.id == main.department_id).departmenttext,

                                             contact = main.mobileno,
                                             DOJ = master.application_date,
                                         }).ToList();
                model.AssociateVMList.Insert(0, new AssociateVM { associate_id = 0, associate_name = "--Select--" });
                if (CommandName == "Submit")
                {
                        if (model.associateId > 0)
                        {
                            model.ListUserName = hrAttendanceService.GetUsers(model.associateId);
                        }
                        else
                        {
                            model.ListUserName = hrAttendanceService.GetUsers(null);
                        }
                        try
                        {
                            var completeAttendance = (from associate in _db.AssociateMasters.Where(x => x.isactive == true)
                                                      from att in _db.attendancedata.Where(x => x.attendancedate.Date == SelectedDate.Date && x.associate_code == associate.associate_code)
                                                      select new AttendanceDataVm()
                                                      {
                                                          associate_code = att.associate_code,
                                                          associateName = associate.associate_name,
                                                          attendancedate = att.attendancedate,
                                                          id = att.id,
                                                          intime = att.intime,
                                                          outtime = att.outtime,
                                                          logintime = att.logintime
                                                      }).ToList();
                            foreach (var User in model.ListUserName)
                            {
                                 var login = completeAttendance.Where(x => x.associate_code == User.associate_code).FirstOrDefault();
                                if (login != null)
                                {
                                    model.AttendanceDataVmList.Add(login);
                                }
                                else
                                {
                                    AttendanceDataVm Nologin = new AttendanceDataVm();
                                    Nologin.id = 0;
                                    Nologin.intime = "00:00:00";
                                    Nologin.logintime = "00:00:00";
                                    Nologin.outtime = "00:00:00";
                                    Nologin.attendancedate = SelectedDate.Date;
                                    Nologin.associate_code = User.associate_code;
                                    Nologin.associateName = User.associate_name;
                                    model.AttendanceDataVmList.Add(Nologin);
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            TempData["msg"] = "Something went wrong.Try again ";
                            return RedirectToAction("Index", "EditAttendance");
                        }
                    }

                
                //model.associateMasters = new List<AssociateMasterModel>();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }
        public IActionResult changeAttendance(HRAttendanceDataVM model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);

            if (SesMdl != null)
            {
                model.changedBy = SesMdl.associate_name;
                if (model.AttendanceDataVmList != null)
                {
                    bool result;
                    try
                    {
                        result = hrAttendanceService.changeAttendance(model);
                        if (result)
                        {
                            TempData["msg"] = "Changes Saved Successfully";
                        }
                        else
                        {
                            TempData["msg"] = "Something went wrong.Try again";
                        }
                    }
                    catch (Exception ex)
                    {
                        CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                        TempData["msg"] = "Something went wrong.Try again ";
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("Index", "EditAttendance");
        }
        public JsonResult GetAllSelected(bool Selection)
        {
            if (Selection)
            {

            }
            return null;
        }

    }
}