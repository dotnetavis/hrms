﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class DailyServices : IDailyServices
    {
        private readonly ApplicationDbContext _db;
        //  private IConfiguration configuration;
        public DailyServices(ApplicationDbContext options)
        {
            _db = options;
            //configuration = conf;
        }
        /// <summary>
        /// Get All DailyReportDetails
        /// </summary>
        /// <returns></returns>
        public List<DailyActivityReportModel> GetAllDailyReportDetails()
        {
            DailyActivityReportModel obj_dailyreport = new DailyActivityReportModel();
            try
            {

                obj_dailyreport.DailyActivityReportModelList = _db.DailyActivityReports.Where(x => x.isactive == true).Select(obj => new DailyActivityReportModel
                {
                    id = obj.id,
                    associate_id = obj.associate_id,
                    totalhour = obj.totalhour,
                    reportingdate = obj.reportingdate.Date,
                    is_authorized = obj.is_authorized,
                    on_duty = obj.on_duty,
                    createdby = obj.createdby,
                    createdbyname = obj.createdbyname,
                    updatedby = obj.updatedby,
                    updatedbyname = obj.updatedbyname,
                    updatedon = obj.updatedon,
                    isactive = obj.isactive,
                    createdon = obj.createdon,
                }).OrderByDescending(x=>x.reportingdate).ToList();

                return obj_dailyreport.DailyActivityReportModelList;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return null;
            }
        }

        /// <summary>
        /// SaveData   
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int SaveData(DARVM model)
        {
            string Result = string.Empty;
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    DailyActivityReport dailyActivityReport = new DailyActivityReport();
                    {
                        dailyActivityReport.associate_id = model.dailyActivityReportModel.associate_id;
                        dailyActivityReport.reportingdate = model.dailyActivityReportModel.reportingdate.Date;
                        dailyActivityReport.is_authorized = model.dailyActivityReportModel.is_authorized;
                        dailyActivityReport.dailyActivities = model.dailyActivityReportModel.dailyActivities;
                        dailyActivityReport.totalhour = model.dailyActivityReportModel.totalhour;
                        dailyActivityReport.on_duty = model.dailyActivityReportModel.on_duty;
                        dailyActivityReport.isactive = true;
                        dailyActivityReport.createdby = model.dailyReportDetailModel.createdby;
                        dailyActivityReport.updatedby = model.dailyReportDetailModel.updatedby;
                        dailyActivityReport.createdbyname = model.dailyReportDetailModel.createdbyname;
                        dailyActivityReport.updatedbyname = model.dailyReportDetailModel.updatedbyname;
                        dailyActivityReport.createdon = DateTime.Now;
                        dailyActivityReport.updatedon = DateTime.Now;
                    };
                    _db.DailyActivityReports.Add(dailyActivityReport);
                    _db.SaveChanges();
                    foreach (var item in model.dailyReportDetailModel.DailyReportDetailModelList)
                    {
                        if (item != null)
                        {
                            DailyReportDetail detail = new DailyReportDetail
                            {
                                dar_id = dailyActivityReport.id,
                                projectid = item.projectid,
                                taskid = item.taskid,
                                activitydetail = item.activitydetail,
                                hour = item.hour,
                                percentage = item.percentage,
                                status = item.status,
                                taskstatus = "",
                                remark = item.remark ?? item.remark,
                                createdbyname = model.dailyReportDetailModel.createdbyname,
                                updatedbyname = model.dailyReportDetailModel.updatedbyname,
                                createdby = model.dailyReportDetailModel.createdby,
                                updatedby = model.dailyReportDetailModel.updatedby,
                                createdon = DateTime.Now,
                                updatedon = DateTime.Now,
                                isactive = true
                            };
                            _db.DailyReportDetails.Add(detail);
                            _db.SaveChanges();
                        }

                    }
                    scope.Complete();
                    scope.Dispose();
                    Result = MessageHelper.DAR_AddSuccess;
                    return dailyActivityReport.id;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                    Result = MessageHelper.DAR_AddError;
                    return 0;
                }
            }
        }

        /// <summary>
        /// Get ActivityReportById
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public DailyActivityReportModel GetActivityReportById(int ID)
        {
            DailyActivityReportModel obj_activity = new DailyActivityReportModel();
            try
            {
                var Data = _db.DailyActivityReports.FirstOrDefault(x => x.id == ID && x.isactive == true);
                if (Data != null)
                {
                    obj_activity.id = Data.id;
                    obj_activity.associate_id = Data.associate_id;
                    obj_activity.is_authorized = Data.is_authorized;
                    obj_activity.createdon = Data.createdon;
                    obj_activity.updatedon = Data.updatedon;
                    obj_activity.createdbyname = Data.createdbyname;
                    obj_activity.updatedbyname = Data.updatedbyname;
                    obj_activity.createdby = Data.createdby;
                    obj_activity.updatedby = Data.updatedby;
                    obj_activity.reportingdate = Data.reportingdate.Date;
                    obj_activity.totalhour = Data.totalhour;
                    obj_activity.on_duty = Data.on_duty;
                    obj_activity.isactive = Data.isactive;
                }
                return obj_activity;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return null;
            }

        }

        /// <summary>
        /// EditDailyReport
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int EditDailyReport(DARVM model)
        {
            string Result = string.Empty;
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {

                    var dailyActivityReport = _db.DailyActivityReports.FirstOrDefault(x => x.id == model.dailyActivityReportModel.id);
                    if (dailyActivityReport != null)
                    {
                        dailyActivityReport.associate_id = model.dailyActivityReportModel.associate_id;
                        dailyActivityReport.reportingdate = model.dailyActivityReportModel.reportingdate.Date;
                        dailyActivityReport.is_authorized = model.dailyActivityReportModel.is_authorized;
                        dailyActivityReport.dailyActivities = model.dailyActivityReportModel.dailyActivities;
                        dailyActivityReport.totalhour = model.dailyActivityReportModel.totalhour;
                        dailyActivityReport.on_duty = model.dailyActivityReportModel.on_duty;
                        dailyActivityReport.isactive = true;
                        dailyActivityReport.createdby = model.dailyReportDetailModel.createdby;
                        dailyActivityReport.updatedby = model.dailyReportDetailModel.updatedby;
                        dailyActivityReport.updatedbyname = model.dailyReportDetailModel.updatedbyname;
                        dailyActivityReport.createdbyname = model.dailyReportDetailModel.createdbyname;
                        _db.SaveChanges();
                    }
                    var detail = _db.DailyReportDetails.Where(x => x.dar_id == model.dailyActivityReportModel.id);
                    _db.DailyReportDetails.RemoveRange(detail);
                    _db.SaveChanges();
                    foreach (var item in model.dailyReportDetailModel.DailyReportDetailModelList)
                    {
                        if (item != null)
                        {
                            DailyReportDetail _detail = new DailyReportDetail
                            {
                                dar_id = dailyActivityReport.id,
                                projectid = item.projectid,
                                taskid = item.taskid,
                                activitydetail = item.activitydetail,
                                hour = item.hour,
                                percentage = item.percentage,
                                status = item.status,
                                taskstatus = "",
                                remark = item.remark ?? item.remark,
                                createdbyname = model.dailyReportDetailModel.createdbyname,
                                updatedbyname = model.dailyReportDetailModel.updatedbyname,
                                createdby = model.dailyReportDetailModel.createdby,
                                updatedby = model.dailyReportDetailModel.updatedby,
                                createdon = DateTime.Now,
                                updatedon = DateTime.Now,
                                isactive = true
                            };
                            _db.DailyReportDetails.Add(_detail);
                            _db.SaveChanges();
                        }
                    }
                    _db.SaveChanges();
                    scope.Complete();
                    Result = MessageHelper.DAR_AddSuccess;
                    return model.dailyActivityReportModel.id;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                    Result = MessageHelper.DAR_AddError;
                    return 0;
                }
            }
        }

        /// <summary>
        /// GetAllDailyReport
        /// </summary>
        /// <returns></returns>
        public List<DailyReportDetailModel> GetAllDailyReport()
        {
            DailyReportDetailModel detail = new DailyReportDetailModel();
            try
            {
                detail.DailyReportDetailModelList = _db.DailyReportDetails.Where(x => x.isactive == true).Select(obj => new DailyReportDetailModel
                {

                    projectid = obj.projectid,
                    taskid = obj.taskid,
                    activitydetail = obj.activitydetail,
                    hour = obj.hour,
                    percentage = obj.percentage,
                    status = obj.status,
                    taskstatus = obj.taskstatus,
                    remark = obj.remark,
                    isactive = obj.isactive,
                    createdon = obj.createdon,
                    updatedon = obj.updatedon,
                    updatedbyname = obj.updatedbyname,
                    createdbyname = obj.createdbyname,
                    updatedby = obj.updatedby,
                    createdby = obj.createdby
                }).ToList();

                return detail.DailyReportDetailModelList;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return null;
            }
        }

        /// <summary>
        /// GetActivityById
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public DailyReportDetailModel GetActivityById(int ID)
        {
            DailyReportDetailModel obj_activity = new DailyReportDetailModel();
            try
            {
                var Data = _db.DailyReportDetails.FirstOrDefault(x => x.id == ID && x.isactive == true);
                if (Data != null)
                {
                    obj_activity.id = Data.id;
                    obj_activity.projectid = Data.projectid;
                    obj_activity.taskid = Data.taskid;
                    obj_activity.taskstatus = Data.taskstatus;
                    obj_activity.status = Data.status;
                    obj_activity.remark = Data.remark ?? Data.remark;
                    obj_activity.percentage = Data.percentage;
                    obj_activity.activitydetail = Data.activitydetail;
                    obj_activity.dar_id = Data.dar_id;
                    obj_activity.isactive = Data.isactive;
                    obj_activity.createdon = Data.createdon;
                    obj_activity.updatedon = Data.updatedon;
                    obj_activity.updatedbyname = Data.updatedbyname;
                    obj_activity.createdbyname = Data.createdbyname;
                    obj_activity.updatedby = Data.updatedby;
                    obj_activity.createdby = Data.createdby;
                }
                return obj_activity;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return null;
            }

        }

        /// <summary>
        ///  Get Daily Activity Details Data,
        /// Created By : Bilal 
        ///On Date: 09/07/2018
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public List<DailyReportDetailModel> GetDARDetails(int ID)
        {
            DailyReportDetailModel model = new DailyReportDetailModel();
            try
            {
                model.DailyReportDetailModelList = (from dar in _db.DailyReportDetails.Where(x => x.dar_id == ID && x.isactive == true)
                                                    join proj in _db.ProjectMasters on dar.projectid equals proj.id
                                                    join task in _db.TaskMasters on dar.taskid equals task.id
                                                    select new DailyReportDetailModel()
                                                    {
                                                        id = dar.id,
                                                        projectid = dar.projectid,
                                                        taskid = dar.taskid,
                                                        taskstatus = dar.taskstatus,
                                                        status = dar.status,
                                                        hour = dar.hour,
                                                        remark = dar.remark,
                                                        percentage = dar.percentage,
                                                        activitydetail = dar.activitydetail,
                                                        dar_id = dar.dar_id,
                                                        project_name = proj.projecttext,
                                                        task_name = task.tasktext,
                                                        isactive = model.isactive,
                                                        createdon = model.createdon,
                                                        updatedon = model.updatedon,
                                                        updatedbyname = model.updatedbyname,
                                                        createdbyname = model.createdbyname,
                                                        updatedby = model.updatedby,
                                                        createdby = model.createdby,

                                                    }).ToList();

            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.DailyReportDetailModelList;
        }

        /// <summary>
        /// Daily Activity Report Mailing System
        /// </summary>
        /// <param name="model"></param>
        /// <param name="httpContext"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public bool SendDARReport(DARVM model, HttpContext httpContext, IConfiguration configuration)
        {
            string str = string.Empty;
            bool result = false;
            var _emailData = _db.EmailConfigurations.FirstOrDefault();
            MailMessage mail = new MailMessage();
            try
            {

                if (_emailData != null)
                {
                    string FromEmail = _emailData.email_send_from;
                    string Pass = _emailData.password;

                    SmtpClient SmtpServer = new SmtpClient(_emailData.smtp);
                    mail.From = new MailAddress(FromEmail, model.username);
                    if (!string.IsNullOrWhiteSpace(model.reporting_email))
                    {
                        mail.To.Add(model.reporting_email);
                    }
                    if (!string.IsNullOrWhiteSpace(model.hod_email))
                    {
                        mail.To.Add(model.hod_email);
                    }
                    string[] emails = configuration.GetValue<string>("ConnectionStrings:DARADDITIONAL").Split(';');
                    if (emails != null && emails.Count() > 0)
                    {
                        foreach (var email in emails)
                        {
                            if (!string.IsNullOrWhiteSpace(email))
                            {
                                mail.CC.Add(email);
                            }
                        }
                    }
                    if (model.additional_reporting_email != null && model.additional_reporting_email.Count != 0)
                    {
                        foreach (var item in model.additional_reporting_email.Distinct())
                        {
                            mail.CC.Add(item);
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(model.official_email))
                    {
                        mail.CC.Add(model.official_email);
                    }
                    if (!string.IsNullOrWhiteSpace(model.official_email))
                        mail.ReplyToList.Add(model.official_email);

                    mail.Subject = "Daily Activity Report";
                    mail.IsBodyHtml = true;
                    StringBuilder mailtext = new StringBuilder();
                    mailtext.AppendLine(String.Format("Dear Sir/Maam,<br/>"));
                    mailtext.AppendLine(String.Format("Please find enclosed here with the Daily Activity Report as on {0}", model.dailyActivityReportModel.reportingdate == null ? "dd-MMM-yyyy<br/><br/>" : model.dailyActivityReportModel.reportingdate.ToString("dd-MMM-yyyy<br/><br/>")));
                    mailtext.Append(String.Format("<table border='1'><tr><th> PROJECT NAME </th><th> TASK NAME </th><th>ACTIVITY DETAIL </th><th> HOURS </th><th>PERCENTAGE</th><th>STATUS</th><th>REMARK</th></tr>"));
                    foreach (var item in model.lstDailyReportDetailModel)
                    {
                        mailtext.Append(String.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}%</td><td>{5}</td><td>{6}</td></tr>", item.project_name, item.task_name, item.activitydetail, item.hour, item.percentage, item.status, item.remark));
                    }
                    mailtext.Append(String.Format("</table><br/><br/>Regards<br/>{0}<br/><br>{1}<br/><br/><br/>", model.username, model.designation));
                    //    mailtext.Append(String.Format("<br/><br/><i>Declaration:If you have already received e-mail for Leave Application then ignore this mail.</i><br/>"));

                    mail.Body = Convert.ToString(mailtext);
                    SmtpServer.Host = _emailData.smtp;
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.Port = _emailData.port;
                    SmtpServer.Credentials = new System.Net.NetworkCredential(FromEmail, Pass);
                    SmtpServer.EnableSsl = _emailData.ssl;
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                 System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };
                    SmtpServer.Send(mail);
                    str = MessageHelper.EmailSent;
                    result = true;
                }
                else
                    result = false;

            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                result = false;
            }
            string status = string.Empty;
            if (result)
                status = "Success";
            else
                status = "Failed";
            CommonUtility.WriteMailLogs("From =>" + _emailData.email_send_from + " To=>" + mail.To + "CC=>" + mail.CC + " Date=>" + DateTime.Now.ToShortDateString() + " Body=>" + mail.Body + " Status=>" + status, "SendDARReport");
            return result;
        }
    }
}

