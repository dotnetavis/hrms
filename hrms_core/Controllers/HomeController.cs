﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using hrms_core.Models;
using hrms_core.ViewModel;
using hrms_core.EF;
using hrms_core.ViewModel.Services.Interfaces;
using hrms_core.ViewModel.Services;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;

namespace hrms_core.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _db;

        private IConfigurationServices configurationServices => new ConfigurationServices(_db);
        private IAssociateServices associateServices => new AssociateServices(_db);

        public HomeController(ApplicationDbContext db) => _db = db;


        public IActionResult Index()
        {
            //TODO: Need Clarification on it
            string Pass = "84980d51";
            string EncPass = CommonUtility.MD5Convertor(Pass);
            return View();
        }
        [HttpGet]
        public IActionResult ConfigurationSetting()
        {
            SettingEmailVM model = new SettingEmailVM();
            var UserData = _db.Users.ToList();
            if (UserData != null && UserData.Count > 0)
            {
                SessionModel Ses = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
                if (Ses != null)
                {
                    ViewBag.Configuration = "False";
                    ViewBag.EmailConfiguration = "False";
                    model.configurationModel.ConfigurationList = configurationServices.GetConfigurationSettings();
                    if (model.configurationModel.ConfigurationList != null && model.configurationModel.ConfigurationList.Count > 0)
                    {
                        ViewBag.Configuration = "True";
                        var data = model.configurationModel.ConfigurationList.FirstOrDefault();
                        model.configurationModel.id = data.id;
                        model.configurationModel.associate_prefix = data.associate_prefix;
                        model.configurationModel.max_numeric_character = data.max_numeric_character;
                        model.configurationModel.company_name = data.company_name;
                        model.configurationModel.contact_no = data.contact_no;
                        model.configurationModel.email_id = data.email_id;
                        model.configurationModel.address = data.address;
                    }
                    model.emailConfigurationModel.EmailConfigurationModelList = configurationServices.GetConfigurationEmail();
                    if (model.emailConfigurationModel.EmailConfigurationModelList != null && model.emailConfigurationModel.EmailConfigurationModelList.Count > 0)
                    {
                        ViewBag.EmailConfiguration = "True";
                        var data = model.emailConfigurationModel.EmailConfigurationModelList.FirstOrDefault();
                        model.emailConfigurationModel.id = data.id;
                        model.emailConfigurationModel.email_send_from = data.email_send_from;
                        model.emailConfigurationModel.confirm_password = data.password;
                        model.emailConfigurationModel.smtp = data.smtp;
                        model.emailConfigurationModel.port = data.port;
                        model.emailConfigurationModel.ssl = data.ssl;
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            else
            {
                ViewBag.Configuration = "False";
                ViewBag.EmailConfiguration = "False";
                model.configurationModel.ConfigurationList = configurationServices.GetConfigurationSettings();
                if (model.configurationModel.ConfigurationList != null && model.configurationModel.ConfigurationList.Count > 0)
                {
                    ViewBag.Configuration = "True";
                    var data = model.configurationModel.ConfigurationList.FirstOrDefault();
                    model.configurationModel.id = data.id;
                    model.configurationModel.associate_prefix = data.associate_prefix;
                    model.configurationModel.max_numeric_character = data.max_numeric_character;
                    model.configurationModel.company_name = data.company_name;
                    model.configurationModel.contact_no = data.contact_no;
                    model.configurationModel.email_id = data.email_id;
                    model.configurationModel.address = data.address;
                }
                model.emailConfigurationModel.EmailConfigurationModelList = configurationServices.GetConfigurationEmail();
                if (model.emailConfigurationModel.EmailConfigurationModelList != null && model.emailConfigurationModel.EmailConfigurationModelList.Count > 0)
                {
                    ViewBag.EmailConfiguration = "True";
                    var data = model.emailConfigurationModel.EmailConfigurationModelList.FirstOrDefault();
                    model.emailConfigurationModel.id = data.id;
                    model.emailConfigurationModel.email_send_from = data.email_send_from;
                    model.emailConfigurationModel.confirm_password = data.confirm_password;
                    model.emailConfigurationModel.smtp = data.smtp;
                    model.emailConfigurationModel.port = data.port;
                    model.emailConfigurationModel.ssl = data.ssl;
                }
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult ConfigurationSetting(SettingEmailVM model)
        {
            try
            {
                if (model.configurationModel != null)
                {
                    if (model.configurationModel.id > 0)
                    {
                        SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
                        if (SesMdl != null)
                        {
                            model.configurationModel.createdby = SesMdl.user_id;
                            model.configurationModel.updatedby = SesMdl.user_id;
                            model.configurationModel.createdbyname = SesMdl.username;
                            model.configurationModel.updatedbyname = SesMdl.username;
                            TempData["Msg"] = configurationServices.UpdateConfigurationSettings(model.configurationModel);
                        }
                        else
                        {
                            return RedirectToAction("Login", "Account");
                        }
                    }
                    else
                        TempData["Msg"] = configurationServices.SaveConfigurationSettings(model.configurationModel);
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return RedirectToAction("ConfigurationSetting", "Home");
        }
        [HttpPost]
        public IActionResult EmailConfiguration(SettingEmailVM model)
        {
            //TODO: Get SuperAdmin Data to pass CreatedBy and all common properties
            //TODO : Implement the Update Feature for Configuration Section
            try
            {
                if (model.emailConfigurationModel != null)
                {
                    if (model.emailConfigurationModel.id > 0)
                    {
                        SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
                        if (SesMdl != null)
                        {
                            model.emailConfigurationModel.createdby = SesMdl.user_id;
                            model.emailConfigurationModel.updatedby = SesMdl.user_id;
                            model.emailConfigurationModel.createdbyname = SesMdl.username;
                            model.emailConfigurationModel.updatedbyname = SesMdl.username;
                            TempData["Msg"] = configurationServices.UpdateEmailConfiguration(model.emailConfigurationModel);
                        }
                        else
                        {
                            return RedirectToAction("Login", "Account");
                        }
                    }
                    else
                        TempData["Msg"] = configurationServices.SaveEmailConfiguration(model.emailConfigurationModel);
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return RedirectToAction("ConfigurationSetting", "Home");
        }
        [HttpGet]
        public IActionResult LoginHistory()
        {
            LoginHistoryVM model = new LoginHistoryVM();
            try
            {
                model.LoginHistoryList = _db.LoginHistory.OrderByDescending(x=>x.created_on).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return View(model);
        }
        
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult AssociateMaster()
        {
            return View();
        }

        public IActionResult AssociateShift()
        {
            return View();
        }

        public IActionResult AttendanceSheet()
        {
            return View();
        }


        public IActionResult CarryForwardLeave()

        {
            return View();
        }


        public IActionResult ChangePassword()

        {
            return View();
        }


        public IActionResult ConsolidatedDar()

        {
            return View();
        }

        public IActionResult DailyActivityApprovedReport()

        {
            return View();
        }

        public IActionResult DailyActivityAuthenticationReport()

        {
            return View();
        }


        public IActionResult DailyActivityDetailReport()

        {
            return View();
        }


        public IActionResult DailyActivityReport()

        {
            return View();
        }


        public IActionResult DashboardAdmin()

        {
            return View();
        }

        public IActionResult DepartmentMaster()

        {
            return View();
        }


        public IActionResult DesignationMaster()

        {
            return View();
        }

        public IActionResult DownloadPaySlip()

        {
            return View();
        }

        public IActionResult DownloadSalarySlip()

        {
            return View();
        }


        public IActionResult EmployeeDashboard()

        {
            return View();
        }

        public IActionResult FNFSettlement()

        {
            return View();
        }

        public IActionResult ForgotPassword()

        {
            return View();
        }

        public IActionResult HolidayCalendarMaster()

        {
            return View();
        }

        public IActionResult LeaveType()

        {
            return View();
        }

        public IActionResult LeaveApplication()

        {
            return View();
        }

        public IActionResult LeaveApplicationApproveDisapprove()

        {
            return View();
        }


        public IActionResult LeaveApplicationAuthentication()

        {
            return View();
        }

        public IActionResult LeaveApplicationApproveDisapproveDirect()

        {
            return View();
        }


        public IActionResult LeaveGrantReject()

        {
            return View();
        }


        public IActionResult LeaveSummaryHR()

        {
            return View();
        }

        public IActionResult Login()

        {
            return View();
        }

        public IActionResult OpeningLeave()

        {
            return View();
        }

        public IActionResult PayElement()

        {
            return View();
        }

        public IActionResult PaySlip()

        {
            return View();
        }

        public IActionResult PayElementHeader()

        {
            return View();
        }

        public IActionResult Policies()

        {
            return View();
        }

        public IActionResult PrepareSalarySlip()

        {
            return View();
        }

        public IActionResult ProjectMaster()

        {
            return View();
        }

        public IActionResult ProjectTimelineSummary()

        {
            return View();
        }

        public IActionResult QualificationMaster()

        {
            return View();
        }

        public IActionResult ReasonOfLeaving()

        {
            return View();
        }

        public IActionResult ResetPasswordAuthentication()

        {
            return View();
        }

        public IActionResult SalesDailyActivityReport()

        {
            return View();
        }

        public IActionResult ShiftMaster()

        {
            return View();
        }

        public IActionResult SystemConfiguration()

        {
            return View();
        }



    }
}
