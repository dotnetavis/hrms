﻿using hrms_core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IResignationServices
    {
        /// <summary>
        /// Get the Resignation Data by UserId,
        /// Created By : Bilal 
        ///On Date: 02/11/2018
        /// </summary>
        /// <returns></returns>
        List<ResignationVM> GetResignationByUser(int UserId);

        /// <summary>
        /// Get the Resignation All Data,
        /// Created By : Bilal 
        ///On Date: 02/11/2018
        /// </summary>
        /// <returns></returns>
        List<ResignationVM> GetAllResignation();

        /// <summary>
        /// Save Resignation Data,
        /// Created By : Bilal 
        ///On Date: 02/11/2018
        /// </summary>
        /// <returns></returns>
        int SaveResignation(ResignationVM model);

        /// <summary>
        /// Check Your Existing Resignation,
        /// Created By : Bilal 
        ///On Date: 02/11/2018
        /// </summary>
        /// <returns></returns>
        bool CheckResignation(int AssociateId);

        /// <summary>
        /// Send Resignation Mail,
        /// Created By : Bilal 
        ///On Date: 02/11/2018
        /// </summary>
        /// <returns></returns>
        bool SendResignationMail(ResignationVM model, HttpContext httpContext, IConfiguration configuration);

        /// <summary>
        /// Get the Resignation Data by ResignationId,
        /// Created By : Bilal 
        ///On Date: 05/11/2018
        /// </summary>
        /// <returns></returns>
        List<ResignationVM> GetResignationId(int ResignationId);

        /// <summary>
        /// Get the Resignation Data by ResignationId,
        /// Created By : Bilal 
        ///On Date: 13/11/2018
        /// </summary>
        /// <returns></returns>
        ResignationViewModel GetResignationById(int ResignationId);

        /// <summary>
        /// Approve / Reject Resignation Data,
        /// Created By : Bilal 
        ///On Date: 13/11/2018
        /// </summary>
        /// <returns></returns>
        int ApproveRejectResignation(ResignationViewModel model);

        /// <summary>
        /// Send Resignation Approval Rejection Mail,
        /// Created By : Bilal 
        ///On Date: 13/11/2018
        /// </summary>
        /// <returns></returns>
        bool SendResignationApprovalRejectionMail(ResignationViewModel model, HttpContext httpContext, IConfiguration configuration,List<string>CC_EmailId,string ResignAppliedBy_EMail);
    }
}
