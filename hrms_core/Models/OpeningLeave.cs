﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class OpeningLeave:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage = "Select Employee Name")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Select Valid Employee Name")]
        public int emp_id { get; set; }
        public int leave_id { get; set; }
        public double opening_leave { get; set; }
        [Range(2018, int.MaxValue, ErrorMessage = "Select Valid Year")]
        public int year { get; set; }
       
      
    }
}
