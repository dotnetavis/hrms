﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class PaymentSlipVM
    {
        public PayElementModel PayElementModel { get; set; }
        public PayslipModel payslipList { get; set; }
        public List<AssociateMasterModel> AssociateMasterModelList { get; set; }
        public List<PaymentSlipVM> paymentsliplist { get; set; }
        public PaymentSlipVM()
        {
            PayElementModel = new PayElementModel();
            AssociateMasterModelList = new List<AssociateMasterModel>();
            paymentsliplist = new List<PaymentSlipVM>();
            payslipList=new PayslipModel();
        }
    }
}
