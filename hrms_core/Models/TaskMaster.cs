﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class TaskMaster: CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage = "Task Name Required")]

        public string tasktext { get; set; }
        [Required(ErrorMessage = "Date Required")]
        [DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = false, DataFormatString = "dd/MM/yyyy")]
        public DateTime assigneddate { get; set; }
        [Required(ErrorMessage = "Date Required")]
        [DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = false, DataFormatString = "dd/MM/yyyy")]
        public DateTime expectedcompletiondate { get; set; }
        [Required(ErrorMessage = "Date Required")]
        [DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = false, DataFormatString = "dd/MM/yyyy")]
        public DateTime actualcompletiondate { get; set; }
        public int projectid { get; set; }
        public string assignto { get; set; }
        
        
    }
}

