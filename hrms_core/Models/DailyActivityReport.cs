﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class DailyActivityReport : CommonAttributeModel
    {
        [Key]
        public int id { get; set; }    //primary key
        [Required(ErrorMessage = "Select Atleast One Associate")]
        [Range(1, int.MaxValue, ErrorMessage = "Select Atleast One Assoiate")]
        public int associate_id { get; set; }
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Enter Reporting Date")]
        public DateTime reportingdate { get; set; }
        public bool is_authorized { get; set; }
        public string totalhour { get; set; }
        public bool on_duty { get; set; }
        public string activitydetails { get; set; }
        [ForeignKey("dar_id")]
        public virtual ICollection<DailyReportDetail> dailyActivities { get; set; }
    }
}
