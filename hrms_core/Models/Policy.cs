﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class Policy:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public string file_name { get; set; }
        public string file_url { get; set; }

    }
}
