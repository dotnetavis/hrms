﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IMenuServices
    {
        void getPopulatedMenu(int associate_id);
        void getPopulatedSubmenu(int menu_parent_id);
        void Check_Authorized_Menu(int associate_code, String menu_url);

    }
}
