﻿using hrms_core.EF;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class UserServices : IUserServices
    {

        private readonly ApplicationDbContext _db;
        public UserServices(ApplicationDbContext options) => _db = options;
        /// <summary>
        /// Get the GetDepartment Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        public List<UserModel> GetUserDetails(UserModel user)
        {
            UserModel model = new UserModel();
            try
            {
                model.UserModelList = (from use in _db.Users.Where(x => x.isactive == true)
                                       join masters in _db.AssociateMasters.Where(x => x.isactive == true) on use.username equals masters.associate_code
                                       join associate in _db.AssociateMains.Where(x => x.isactive == true) on masters.id equals associate.associate_id
                                       select new UserModel
                                       {
                                           id = use.id,
                                           username = use.username,
                                           userpass = use.userpass,
                                           official_email = associate.official_email,
                                       }).Where(x => x.username == user.username || x.official_email.ToLower() == user.username.ToLower()).ToList();

                //model.UserModelList = _db.Users.Where(x => x.username == user.username).Select(obj => new UserModel
                //{
                //    id = obj.id,
                //    username = obj.username,
                //    userpass = obj.userpass,
                //    guid = obj.guid
                //}).ToList();


            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.UserModelList;
        }

        /// <summary>
        /// Get the GetDepartment Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        public List<UserModel> GetUserDetailsByGUID(string Guid)
        {
            UserModel model = new UserModel();
            try
            {
                model.UserModelList = _db.Users.Where(x => x.guid == Guid).Select(obj => new UserModel
                {
                    id = obj.id,
                    username = obj.username,
                    userpass = obj.userpass,
                    guid = obj.guid
                }).ToList();


            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.UserModelList;
        }
        public List<UserModel> SavePassword(UserModel user)
        {
            UserModel model = new UserModel();

            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    user.userpass = model.confirmnewpassword;
                    user.createdon = DateTime.Now;
                    user.updatedon = DateTime.Now;
                    user.createdby = model.createdby;
                    user.updatedby = model.updatedby;
                    user.updatedbyname = model.updatedbyname;
                    user.createdbyname = model.createdbyname;
                    user.isactive = true;
                    _db.Users.Add(user);
                    _db.SaveChanges();
                    scope.Complete();

                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);

                }
                return model.UserModelList;
            }
        }

    }
}






