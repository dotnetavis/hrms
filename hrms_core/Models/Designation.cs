﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class Designation:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }    //primary key
        [Required(ErrorMessage = "Designation Name Required")]
        public string designationtext { get; set; }
        public string designationdesc { get; set; }
        [ForeignKey("designation_id")]
        public virtual ICollection<AssociateMain> AssociateMainDetails { get; set; }
        [ForeignKey("designation_id")]
        public virtual ICollection<AssociateExperience> AssociateExperienceDetails { get; set; }
    }
}
