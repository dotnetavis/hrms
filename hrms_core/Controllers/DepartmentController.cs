﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace hrms_core.Controllers
{
    public class DepartmentController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IDepartmentServices DepartmentServices => new departmentServices(_db);
        private IAssociateServices AssociateServices => new AssociateServices(_db);
        public DepartmentController(ApplicationDbContext db) => _db = db;

        [HttpGet]
        public IActionResult Index(int? id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            DepartmentModel model = new DepartmentModel
            {
                AssociateMasterModelList = AssociateServices.GetAssociateList_Active()
            };
            model.createdby = SesMdl.user_id;
            model.updatedby = SesMdl.user_id;
            model.createdbyname = SesMdl.username;
            model.updatedbyname = SesMdl.username;
            model.AssociateMasterModelList.Insert(0, new AssociateMasterModel { id = 0, associate_name = "--Select--" });
            model.DepartmentModelList = DepartmentServices.GetDepartmentList();
            if (id > 0 && id != null)
            {
                if (model.DepartmentModelList.Count > 0 && model.DepartmentModelList != null)
                {
                    var data = model.DepartmentModelList.FirstOrDefault(x => x.id == id);
                    if (data != null)
                    {
                        model.id = data.id;
                        model.departmenttext = data.departmenttext;
                        model.applicable_leave = data.applicable_leave;
                        model.isactive = data.isactive;
                    }
                }
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult Index(DepartmentModel model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.user_id;
                model.updatedby = SesMdl.user_id;
                model.createdbyname = SesMdl.username;
                model.updatedbyname = SesMdl.username;
                if (model.id > 0)
                    TempData["msg"] = DepartmentServices.UpdateDepartment(model);
                else
                    TempData["msg"] = DepartmentServices.SaveDepartment(model);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("Index", "Department", new { id = 0 });
        }
        [HttpPost]
        public IActionResult DisableDepartment(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            DepartmentModel model = new DepartmentModel
            {
                id = id,
                updatedby = SesMdl.associate_id
            };
            TempData["msg"] = DepartmentServices.DisableDepartment(model);
            return Json(true);
        }
        [HttpPost]
        public IActionResult EnableDepartment(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            DepartmentModel model = new DepartmentModel
            {
                id = id,
                updatedby = SesMdl.associate_id
            };
            TempData["msg"] = DepartmentServices.EnableDepartment(model);
            return Json(true);
        }

        [HttpGet]
        public IActionResult HODAssign()
        {
            DepartmentHODAssignModel model = new DepartmentHODAssignModel();
            model.DepartmentList = _db.Departments.ToList();
            model.AssociateList = (from master in _db.AssociateMasters
                                   join main in _db.AssociateMains.Where(x=>x.is_hod == true)
                                   on master.id equals main.associate_id
                                   select new AssociateMaster()
                                   {
                                       id = master.id,
                                       associate_name = master.associate_name
                                   }).ToList();
            return View(model);
        }

        [HttpPost]
        public IActionResult HODAssign(DepartmentHODAssignModel model)
        {

            return RedirectToAction("HODAssign", "Department");
        }
    }
}