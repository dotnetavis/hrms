﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class LoginHistoryVM : LoginModel
    {
        public List<LoginHistory> LoginHistoryList { get; set; }
        //public LoginHistory loginHistory { get; set; }
        public LoginHistoryVM()
        {
            LoginHistoryList = new List<LoginHistory>();
            //loginHistory = new LoginHistory();
        }
    }
}
