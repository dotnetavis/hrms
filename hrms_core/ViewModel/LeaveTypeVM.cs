﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class LeaveTypeVM : LeaveTypeMaster
    { 
        public List<LeaveTypeVM> LeaveTypeList { get; set; }
        public List<LeaveTypeMaster> LeaveTypeMasterList { get; set; }
        public int emp_id { get; set; }
        public AssociateMasterModel AssociateMaster { get; set; }
        public List<AssociateMasterModel> AssociateMasterModelList { get; set; }
        public AssociateMainModel associateMain { get; set; }
        public LeaveTypeVM()
        {
            LeaveTypeList = new List<LeaveTypeVM>();
            LeaveTypeMasterList = new List< LeaveTypeMaster>();
            AssociateMasterModelList = new List<AssociateMasterModel>();
            AssociateMaster = new AssociateMasterModel();
            associateMain = new AssociateMainModel();
        }
    }
}
