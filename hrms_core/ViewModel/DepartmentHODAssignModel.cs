﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.Models;

namespace hrms_core.ViewModel
{
    public class DepartmentHODAssignModel : DepartmentHODAssign
    {
       public List<DepartmentHODAssign> DepartmentHodList { get; set; }
       public List<DepartmentHODAssignModel> DepartmentHodModelList { get; set; }
       public List<Department> DepartmentList { get; set; }
        public List<AssociateMaster> AssociateList { get; set; }

        public DepartmentHODAssignModel()
        {
            DepartmentHodList = new List<DepartmentHODAssign>();
            DepartmentHodModelList = new List<DepartmentHODAssignModel>();
            DepartmentList = new List<Department>();
        }

    }
}
