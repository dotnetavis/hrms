﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class ProjectDeptAssign : CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public int project_id { get; set; }
        public int department_id { get; set; }
        public bool is_selected { get; set; }
    }
}
