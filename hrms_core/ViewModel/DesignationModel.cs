﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class DesignationModel: Designation
    {
        public List<DesignationModel> DesignationModelList { get; set; }
        public DesignationModel()
        {
            DesignationModelList = new List<DesignationModel>();
        }
    }
}
