﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class ForgotPasswordModel: ForgotPassword
    {
        public List<ForgotPasswordModel> ForgotPasswordModelList { get; set; }
        public ForgotPasswordModel()
        {
            ForgotPasswordModelList = new List<ForgotPasswordModel>();
        }
    }
}
