﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IConsolidatedDar
    {
        void getDar(int associate_code);
        void departmentid(int associate_code);
        void department_listusername(int associate_code);
        void getProjectTimelineSummery(int projectid, int associat_id);
        void getProjectTimelineSummeryHourMinute(int projectid, int associat_id);
        
    }
}
