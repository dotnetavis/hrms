﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface ICountryServices
    {
        /// <summary>
        /// Get the Country Data,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        List<CountryModel> GetCountry();

        /// <summary>
        /// Get the Country Data Only Active,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        /// 
        List<CountryModel> GetCountry_Active();
        string SaveCountry(CountryModel model);
        string UpdateCountry(CountryModel model);
        string EnableCountry(CountryModel model);
        string DisableCountry(CountryModel model);
    }
}
