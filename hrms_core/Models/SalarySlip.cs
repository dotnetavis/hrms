﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class SalarySlip:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public int pay_element_id { get; set; }
        public virtual PayElement associated { get; set; }
        public double payable_days { get; set; }
        public double carry_home_salary { get; set; }
        [Required(ErrorMessage = "Enter Month Year")]
        public string month_year { get; set; }
    }
}


   
       