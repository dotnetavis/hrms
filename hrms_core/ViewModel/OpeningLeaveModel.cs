﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class OpeningLeaveModel: OpeningLeave
    {
        public List<OpeningLeaveModel> OpeningLeaveModelList { get; set; }
        public AssociateMasterModel AssociateMaster { get; set; }
        public List<AssociateMasterModel> AssociateMasterModelList { get; set; }
        public AssociateMainModel associateMain { get; set; }
        public List<LeaveTypeMaster> LeaveTypeList { get; set; }
        [Range(-76, 76, ErrorMessage = "Invalid Casual Leave Value")]
        public double casual_leave { get; set; }
         [Range(-76,76, ErrorMessage = "Invalid Earn Leave Value")]
        public int earn_leave { get; set; }
        public OpeningLeaveModel()
        {
            OpeningLeaveModelList = new List<OpeningLeaveModel>();
            AssociateMasterModelList = new List<AssociateMasterModel>();
            AssociateMaster = new AssociateMasterModel();
            associateMain = new AssociateMainModel();
            LeaveTypeList = new List<LeaveTypeMaster>();
        }
        
    }
}
