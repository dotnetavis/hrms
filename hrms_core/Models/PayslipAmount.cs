﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class PayslipAmount:CommonAttributeModel
    {   
        [Key]
        public int id { get; set; }                //primary key
        public virtual AssociateMaster associate { get; set; }   //foreign key
        [Required(ErrorMessage = "Enter Month Year")]
        public string monthyear { get; set; }
        public int totalammount { get; set; }
    }
}
