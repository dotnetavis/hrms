﻿ using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using hrms_core.Models;
using hrms_core.ViewModel;
using hrms_core.EF;
using hrms_core.ViewModel.Services.Interfaces;
using hrms_core.ViewModel.Services;
using System.IO;
using System.Data;

namespace hrms_core.Controllers
{
    public class ProfilePageController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IProfilePageServices profileServices => new ProfilePageServices(_db);
        private IAssociateServices associateServices => new AssociateServices(_db);
        private ICountryServices countryServices => new CountryServices(_db);
        private IDesignationServices designationServices => new DesignationServices(_db);
        private IDepartmentServices departmentServices => new departmentServices(_db);
        private IQualificationServices qualificationServices => new QualificationServices(_db);
        private IStateServices stateServices => new StateServices(_db);
        private ICityServices cityServices => new CityServices(_db);
        private IEmployeeDashboardServices employeeServices => new EmployeeDashboardServices(_db);
        public ProfilePageController(ApplicationDbContext options) => _db = options;

        [HttpGet]
        public IActionResult Profile(int? id)

        {
            AssociateVM model = new AssociateVM();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.user_id;
                model.updatedby = SesMdl.user_id;
                model.createdbyname = SesMdl.username;
                model.updatedbyname = SesMdl.username;

                #region Bind Department Dropdown
                model.DepartmentModelList = departmentServices.GetDepartment_Active();
                #endregion


                model.EmployeeDashboardModelList = employeeServices.GetEmployeeDetailsByID(SesMdl.associate_id);


                #region Bind Country Dropdown
                model.PresentCountryModelList = countryServices.GetCountry_Active();
                model.PermanentCountryModelList = countryServices.GetCountry_Active();
                model.PresentCountryModelList.Insert(0, new CountryModel { id = 0, countrytext = "--Select--" });
                model.PermanentCountryModelList.Insert(0, new CountryModel { id = 0, countrytext = "--Select--" });
                #endregion
                #region Bind Qualification Dropdown
                model.qualificationModel.QualificationModelList = qualificationServices.GetQualification_Active();
                #endregion
                model.PresentStateModelList.Insert(0, new StateModel { id = 0, statetext = "--Select--" });
                model.PermanentStateModelList.Insert(0, new StateModel { id = 0, statetext = "--Select--" });
                model.PresentCityModelList.Insert(0, new CityModel { id = 0, citytext = "--Select--" });
                model.PermanentCityModelList.Insert(0, new CityModel { id = 0, citytext = "--Select--" });
                id = SesMdl.associate_id;
                
                if (id != null && id > 0)
                {
                    #region Master
                    model.associateMaster = _db.AssociateMasters.Where(x => x.id == id && x.isactive == true)
                   .Select(x => new AssociateMasterModel
                   {
                       id = x.id,
                       associate_name = x.associate_name,
                       applicable_to = x.applicable_to,
                       application_date = x.application_date,
                       associate_code = x.associate_code,
                       createdby = x.createdby,
                       createdon = x.createdon,
                       updatedby = x.updatedby,
                       updatedon = x.updatedon,
                       isactive = x.isactive,
                       AssociateMasterModelList = associateServices.GetAssociateList_Active()
                   }).FirstOrDefault();
                    #endregion
                    //if (model.associateMaster != null)
                    //{

                    //}
                    #region Main
                    model.associateMain = (from master in _db.AssociateMasters.Where(x => x.id == id && x.isactive == true)
                                           join main in _db.AssociateMains
                                           on master.id equals main.associate_id
                                           select new AssociateMainModel
                                           {
                                               id = main.id,
                                               associate_id = main.associate_id,
                                               designation_id = main.designation_id,
                                               department_id = main.department_id,
                                               department = _db.Departments.FirstOrDefault(x=>x.id==main.department_id).departmenttext,
                                               designation = _db.Designations.FirstOrDefault(x=>x.id==main.designation_id).designationtext,
                                               is_hod = main.is_hod,
                                               extensionno = main.extensionno,
                                               mobileno = main.mobileno,
                                               personal_email = main.personal_email,
                                               official_email = main.official_email,
                                               account_no = main.account_no,
                                               bankname = main.bankname,
                                               ifsc = main.ifsc,
                                               doc = (main.doc == null ? default(DateTime) : main.doc),
                                               qualification_id = main.qualification_id,
                                               qualification = _db.Qualifications.FirstOrDefault(x=>x.id==main.qualification_id).qualificationtext,
                                               shiftid = main.shiftid,
                                               pf_no = main.pf_no,
                                               esi_no = main.esi_no,
                                               applicable_leave = main.applicable_leave,
                                               shift_type = main.shift_type,
                                               role_id = main.role_id,
                                               reportingperson_id = main.reportingperson_id,
                                           }).FirstOrDefault();
                    #endregion
                   

                    #region Personal
                    model.associatePersonal = (from master in _db.AssociateMasters.Where(x => x.id == id && x.isactive == true)
                                               join personal in _db.AssociatePersonals
                                               on master.id equals personal.associate_id
                                               select new AssociatePersonalModel
                                               {
                                                   id = personal.id,
                                                   associate_id = personal.associate_id,
                                                   pan_card = personal.pan_card,
                                                   dob = personal.dob,
                                                   anniversary = personal.anniversary,
                                                   father_husband_name = personal.father_husband_name,
                                                   mothername = personal.mothername,
                                                   present_address = personal.present_address,
                                                   present_city_id = personal.present_city_id,
                                                   present_country_id = personal.present_country_id,
                                                   present_state_id = personal.present_state_id,
                                                   permanent_address = personal.permanent_address,
                                                   permanent_city_id = personal.permanent_city_id,
                                                   permanent_country_id = personal.permanent_country_id,
                                                   permanent_state_id = personal.permanent_state_id,
                                                   bloodgroup = personal.bloodgroup,
                                                   adhar_card_no = personal.adhar_card_no,
                                                   gender = personal.gender,
                                                   maritalstatus = personal.maritalstatus
                                               }).FirstOrDefault();
                    #endregion

                    #region profilePage
                    model.profilepage = (from profile in _db.ProfilePages.Where(x => x.associate_id == id.Value)
                                         select new ProfilePageModel
                                         {
                                             createdby = profile.createdby,
                                             updatedby=profile.updatedby,
                                             createdbyname=profile.createdbyname,
                                             updatedbyname=profile.updatedbyname,
                                             associate_id=profile.associate_id,
                                             image_path=profile.image_path,
                                             isactive=profile.isactive
                                             
                                         }).FirstOrDefault();

                    #endregion
                    #region Qualification





                    model.AssociateQualificationModelList = (from qualification in _db.AssociateQualifications.Where(x => x.associate_id == model.associateMain.associate_id && x.isactive == true)
                                                             select new AssociateQualificationModel
                                                             {
                                                                 id = qualification.id,
                                                                 associate_id = qualification.associate_id,
                                                                 qualification_id = qualification.qualification_id,
                                                                 qualification_year = qualification.qualification_year,
                                                                 institute_name = qualification.institute_name,
                                                                 percentage = qualification.percentage,
                                                                 QualificationModelList = qualificationServices.GetQualification_Active(),
                                                             }).ToList();

                  var associate_qualification = model.AssociateQualificationModelList.FirstOrDefault(x=>x.qualification_id == model.associateMain.qualification_id);
                    if(associate_qualification != null)
                    {
                        model.associateQualification.institute_name = associate_qualification.institute_name;
                    }
                    else
                        model.associateQualification.institute_name = "No Institute Name added";

                    #endregion


                    if (model.associatePersonal != null)
                    {
                        #region Bind State
                        model.PermanentStateModelList = stateServices.GetState_ByCountryID(model.associatePersonal.permanent_country_id);
                        model.PresentStateModelList = stateServices.GetState_ByCountryID(model.associatePersonal.present_country_id.Value);

                        model.PresentStateModelList.Insert(0, new StateModel { id = 0, statetext = "--Select--" });
                        model.PermanentStateModelList.Insert(0, new StateModel { id = 0, statetext = "--Select--" });
                        #endregion

                        #region Bind City
                        model.PresentCityModelList = cityServices.GetCity_ByStateId(model.associatePersonal.present_state_id.Value);
                        model.PermanentCityModelList = cityServices.GetCity_ByStateId(model.associatePersonal.permanent_state_id);
                        model.PresentCityModelList.Insert(0, new CityModel { id = 0, citytext = "--Select--" });
                        model.PermanentCityModelList.Insert(0, new CityModel { id = 0, citytext = "--Select--" });
                        #endregion
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
                return View(model);
            }
        

        [HttpPost]
        public IActionResult Profile (ProfilePageModel model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl== null)
            {
                return RedirectToAction("Profile", "ProfilePage");
            }
            int associate_id = SesMdl.associate_id;
            
                if (Request.Form.Files["userPhoto"]?.Length > 0)
                {
                    var filename = Path.GetFileName(Request.Form.Files["userPhoto"].FileName);
                    var _FileName = Path.GetFileNameWithoutExtension(Request.Form.Files["userPhoto"].FileName);
                    Guid objGuid = Guid.NewGuid();
                    _FileName = _FileName + '_' + Convert.ToString(objGuid);
                    string extension = Path.GetExtension(filename).ToLower();
                    model.image_path = _FileName + extension;
                    string[] validFileTypes = { ".png", ".jpg", ".jpeg", ".gif" };
                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "ProfilePic", model.image_path);
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "ProfilePic"));
                    }
                if (validFileTypes.Contains(extension))
                {
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                   // model.image_path = path;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        Request.Form.Files["userPhoto"].CopyTo(stream);
                    }
                }
                else
                {
                    TempData["msg"] = "Invalid file type";
                    return RedirectToAction("Profile", "ProfilePage");
                }
                
            }
            TempData["msg"] = profileServices.UpdateProfile(model, associate_id);

            //    if (SesMdl != null)
            //    {
            //        model.createdby = SesMdl.user_id;
            //        model.updatedby = SesMdl.user_id;
            //        model.createdbyname = SesMdl.username;
            //        model.updatedbyname = SesMdl.username;
            //        if (model.id > 0)
            //        {
            //            if (Request.Form.Files["userPhoto"].Length > 0)
            //            {
            //                var filename = Path.GetFileName(Request.Form.Files["userPhoto"].FileName);
            //                var _FileName = Path.GetFileNameWithoutExtension(Request.Form.Files["userPhoto"].FileName);
            //                Guid objGuid = Guid.NewGuid();
            //                _FileName = _FileName + '_' + Convert.ToString(objGuid);
            //                string extension = Path.GetExtension(filename).ToLower();
            //                model.imagepath = _FileName + extension;
            //                string[] validFileTypes = { ".png", ".jpg", ".jpeg", ".gif" };
            //                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload", model.imagepath);
            //                if (!Directory.Exists(path))
            //                {
            //                    Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload"));
            //                }
            //                if (validFileTypes.Contains(extension))
            //                {
            //                    if (System.IO.File.Exists(path))
            //                    {
            //                        System.IO.File.Delete(path);
            //                    }
            //                    using (var stream = new FileStream(path, FileMode.Create))
            //                    {
            //                        Request.Form.Files["userPhoto"].CopyTo(stream);
            //                    }
            //                }
            //                else
            //                {
            //                    TempData["msg"] = "Invalid file type";
            //                    return RedirectToAction("Employee", "EmployeeDashboard");
            //                }
            //            }
            //            TempData["msg"] = employeeServices.UpdateEmployeeDeatails(model);
            //        }
            //        else
            //        {
            //            if (Request.Form.Files["userPhoto"].Length > 0)
            //            {
            //                var filename = Path.GetFileName(Request.Form.Files["userPhoto"].FileName);
            //                var _FileName = Path.GetFileNameWithoutExtension(Request.Form.Files["userPhoto"].FileName);
            //                Guid objGuid = Guid.NewGuid();
            //                _FileName = _FileName + '_' + Convert.ToString(objGuid);
            //                string extension = Path.GetExtension(filename).ToLower();
            //                model.imagepath = _FileName + extension;
            //                string[] validFileTypes = { ".png", ".jpg", ".jpeg", ".gif" };
            //                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload", model.imagepath);
            //                if (!Directory.Exists(path))
            //                {
            //                    Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload"));
            //                }
            //                if (validFileTypes.Contains(extension))
            //                {
            //                    if (System.IO.File.Exists(path))
            //                    {
            //                        System.IO.File.Delete(path);

            //                    }
            //                    using (var stream = new FileStream(path, FileMode.Create))
            //                    {
            //                        Request.Form.Files["userPhoto"].CopyTo(stream);
            //                    }
            //                }
            //                else
            //                {
            //                    TempData["msg"] = "Invalid file type";
            //                    return RedirectToAction("Employee", "EmployeeDashboard");
            //                }
            //            }
            //            TempData["msg"] = employeeServices.SaveEmployeeDetails(model);
            //        }
            return RedirectToAction( "Profile", "ProfilePage",new { id= associate_id });
        }
        //public ProfilePageModel Upload()
        //{
        //    try
        //    {
        //        ProfilePageModel model = new ProfilePageModel();
        //        DataTable dataSet = new DataTable();
        //        if (Request.Form.Files["postedFile"] != null)
        //        {
        //            var filename = Path.GetFileName(Request.Form.Files["postedFile"].FileName);
        //            string extension = Path.GetExtension(filename).ToLower();
        //            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload", filename);
        //            if (!Directory.Exists(path))
        //            {
        //                Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload"));
        //            }
        //            if (System.IO.File.Exists(path))
        //            {
        //                System.IO.File.Delete(path);
        //            }
        //            using (var stream = new FileStream(path, FileMode.Create))
        //            {
        //                Request.Form.Files["postedfile"].CopyToAsync(stream);
        //            }

        //            model.image_path = filename;
        //            model.image_url = path.ToString();
        //            return model;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        CommonUtility.WriteErrorLogs(ex, "ProfilePage=>Upload");
        //        return null;

        //    }
        //}

        [HttpPost]
        public IActionResult Index(int CountryId)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            ProfilePageModel model = new ProfilePageModel();
            model.PresentStateModelList = stateServices.GetState_ByCountryID(CountryId);
            return View(model.PresentStateModelList);
        }

        [HttpPost]
        public JsonResult GetStateByCountryId(int CountryId)
        {
            AssociateVM model = new AssociateVM();
            model.PresentStateModelList = stateServices.GetState_ByCountryID(CountryId);
            return Json(model.PresentStateModelList);
        }
        [HttpPost]
        public JsonResult GetCityByStateId(int StateId)
        {
            AssociateVM model = new AssociateVM();
            model.PresentCityModelList = cityServices.GetCity_ByStateId(StateId);
            return Json(model.PresentCityModelList);
        }

    }
}