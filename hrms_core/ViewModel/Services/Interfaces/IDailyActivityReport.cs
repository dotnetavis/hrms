﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IDailyActivityReport
    {
        void getassosiatename(int associate_code);
        void getdarassosiatename(int dar_id);
        void getdepartmentid(int associate_id);
        void getadditionalifficialmail(int associate_code);
        void reportingdetailLogin(string username, string userpass, int associateid);
        void getreportingpersonid(int associate_code);
        void updateondutytable(int dar_id);
        void updateauthorizedailyactivityreport(int dar_id);
        void getReportingHodPersonName(int associate_code);
        void getreportingpersonname(int associate_id);
        void checkinsertvalue(int associat_id, DateTime reportingdate);
        void getprojectname();
        void getofficialmailid(int associate_code);
        void getdepartmenthodmailid(int associate_code);
        void gatdailyreportdetail(int associate_code);
        void gettotalhourdetailforview(int dar_id);
        void getdailyreportdetail(int dar_id);
        void getdailyactivitydetail(int dar_id);
        void gettaskname(int projectid);
        void getdesignation(string username);
        void saveondutydetail();
        void updatedailyactivityreport();                                                     
        void adddailyreportdetail();                                                                                                                                                                                                                                                                                                                                     
        void getusernameForOutDuty();
        void checkauthenticationOutDuty(int associate_code);
        void getLoginAssociateId(int associate_code);
        void loginoutDutyAuthorization(string username, string userpass);
    }  
}
