﻿using hrms_core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IAssociateServices
    {
        /// <summary>
        /// Get the AssociateMaster Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        List<AssociateMasterModel> GetAssociateList();
        List<AssociateAdditionalInfoModel> GetAssociateInfoList_Active();
        List<AssociateMainModel> GetAssociateMainList();
        /// <summary>
        /// Get the AssociateMaster Data,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        List<AssociateMasterModel> GetAssociateList_Active();
        int SaveAssociateData(AssociateVM model);
        string UpdateAssociateData(AssociateVM model);
        /// <summary>
        /// Get the Associate Code,
        /// Created By : Bilal 
        ///On Date: 05/07/2018
        /// </summary>
        /// <returns></returns>
        string GetAssociateCode();

        /// <summary>
        /// Get the Default Password,
        /// Created By : Bilal 
        ///On Date: 06/07/2018
        /// </summary>
        /// <returns></returns>
        string GetDefaultPassword();
        string GetSuperAdminPassword();
        List<AssociatePersonalModel> GetAssociatePersonalList_Active();
        bool SendAssociateDetails(AssociateVM model, HttpContext httpContext, IConfiguration configuration);
        List<AssociateMasterModel> GetAssociateInactiveList();
    }
}
