﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace hrms_core.ViewModel.Services
{
    public class TaskMasterServices : ITaskMasterServices
    {
        private readonly ApplicationDbContext _db;
        public TaskMasterServices(ApplicationDbContext options)
        {
            _db = options;
        }
        /// <summary>
        /// Get Task's Data
        /// Created by:- Yashashwini
        /// Date:- 01/06/2018
        /// </summary>
        /// <returns></returns>

        //for associate
        public List<TaskMasterModel> GetTask(int AssociateId)
        {
            TaskMasterModel model = new TaskMasterModel();
            try
            {
                //model.TaskMasterModelList = (from project in _db.ProjectMasters.Where(x => x.isactive == true)
                //                             join task in _db.TaskMasters.Where(x => x.isactive == true)
                //                             on project.id equals task.projectid
                //                             join assin_task in _db.TaskAssigns.Where(x => x.associate_id == AssociateId && x.isactive == true && x.is_selected == true)
                //                             on task.id equals assin_task.task_id
                //                             select new TaskMasterModel
                //                             {
                //                                 id = task.id,
                //                                 actualcompletiondate = task.actualcompletiondate,
                //                                 assigneddate = task.assigneddate,
                //                                 is_selected = assin_task.is_selected,
                //                                 createdby = task.createdby,
                //                                 project_name = project.projecttext,
                //                                 createdon = task.createdon,
                //                                 expectedcompletiondate = task.expectedcompletiondate,
                //                                 isactive = task.isactive,
                //                                 projectid = task.projectid,
                //                                 tasktext = task.tasktext,
                //                                 updatedby = task.updatedby,
                //                                 updatedon = task.updatedon,
                //                             }).Distinct().OrderBy(x => x.tasktext).ToList();
                model.TaskMasterModelList = (from project in _db.ProjectMasters.Where(x => x.isactive == true)
                                             join projectassign in _db.projectdeptassigns.Where(x=>x.isactive==true && x.is_selected==true)
                                             on project.id equals projectassign.project_id
                                             join main in _db.AssociateMains.Where(x=> x.isactive==true && x.associate_id==AssociateId)
                                             on projectassign.department_id equals main.department_id
                                             join task in _db.TaskMasters.Where(x => x.isactive == true)
                                             on project.id equals task.projectid
                                             select new TaskMasterModel
                                             {
                                                 id = task.id,
                                                 actualcompletiondate = task.actualcompletiondate,
                                                 assigneddate = task.assigneddate,
                                                 createdby = task.createdby,
                                                 project_name = project.projecttext,
                                                 createdon = task.createdon,
                                                 expectedcompletiondate = task.expectedcompletiondate,
                                                 isactive = task.isactive,
                                                 projectid = task.projectid,
                                                 tasktext = task.tasktext,
                                                 updatedby = task.updatedby,
                                                 updatedon = task.updatedon,
                                             }).Distinct().OrderBy(x => x.tasktext).ToList();


            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.TaskMasterModelList;
        }
        //For reporting persons
        public List<TaskMasterModel> GetTask_ByProjectID(int ProjectID)
        {
            TaskMasterModel model = new TaskMasterModel();
            try
            {
                model.TaskMasterModelList = (from task in _db.TaskMasters.Where(x => x.projectid == ProjectID)
                                             join project in _db.ProjectMasters.Where(x => x.isactive == true) on task.projectid equals project.id
                                             select new TaskMasterModel
                                             {
                                                 id = task.id,
                                                 tasktext = task.tasktext,
                                                 projectid = task.projectid,
                                                 isactive = task.isactive,
                                                 assigneddate=task.assigneddate,
                                                 actualcompletiondate=task.actualcompletiondate,
                                                 expectedcompletiondate=task.expectedcompletiondate,
                                                 project_name=project.projecttext,
                                                 createdon = task.createdon,
                                                 createdby = task.createdby,
                                                 updatedon = task.updatedon,
                                                 updatedby = task.updatedby
                                             }).OrderBy(x => x.tasktext).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.TaskMasterModelList;
        }
        /// <summary>
        /// To save task's data
        /// Created by:- Yashashwini
        /// Date:- 01/06/2018
        /// </summary>
        /// <returns></returns>


        public string SaveTask(TaskMasterModel model)
        {
            string Result = string.Empty;
            TaskMaster taskMaster = new TaskMaster();

            try
            {
                //Check for Duplicate Record
                bool bool_sameNameExists = _db.TaskMasters.Any(x => x.tasktext.ToLower().Trim() == model.tasktext.ToLower().Trim() && x.projectid == model.projectid);
                if (bool_sameNameExists)
                {
                    Result = MessageHelper.Task_Duplicate;
                }
                else
                {
                    using (var scope = new System.Transactions.TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            {
                                taskMaster.tasktext = model.tasktext;
                                taskMaster.assigneddate = model.assigneddate;
                                taskMaster.expectedcompletiondate = model.expectedcompletiondate;
                                taskMaster.actualcompletiondate = model.actualcompletiondate;

                                taskMaster.projectid = model.projectid;
                                taskMaster.isactive = true;
                                taskMaster.updatedby = model.updatedby;
                                taskMaster.updatedbyname = model.updatedbyname;
                                taskMaster.updatedon = DateTime.Now;
                                taskMaster.createdby = model.createdby;
                                taskMaster.createdon = DateTime.Now;
                                taskMaster.createdbyname = model.createdbyname;
                                _db.TaskMasters.Add(taskMaster);
                                _db.SaveChanges();
                                foreach (var item in model.AssociateList)
                                {
                                    TaskAssign task = new TaskAssign();
                                    task.task_id = taskMaster.id;
                                    task.associate_id = item.id;
                                    task.is_selected = item.is_selected;
                                    task.isactive = true;
                                    task.updatedby = model.updatedby;
                                    task.updatedbyname = model.updatedbyname;
                                    task.updatedon = DateTime.Now;
                                    task.createdby = model.createdby;
                                    task.createdon = DateTime.Now;
                                    task.createdbyname = model.createdbyname;
                                    _db.TaskAssigns.Add(task);
                                    _db.SaveChanges();
                                }
                                scope.Complete();
                                Result = MessageHelper.Task_AddSuccess;
                            }
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.Designation_AddError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Task_AddError;
            }
            return Result;
        }

        /// <summary>
        /// To Disable a Task
        /// Created by:- Yashashwini
        /// Date:- 01/06/2018
        /// </summary>>
        /// <returns></returns>


        public string DisableTask(TaskMasterModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.TaskMasters.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))

                    {
                        try
                        {
                            data.isactive = false;
                            data.updatedby = model.updatedby;
                            data.updatedbyname = model.updatedbyname;
                            data.updatedon = DateTime.Now;
                            data.createdby = model.createdby;
                            data.createdon = model.createdon;
                            data.createdbyname = model.createdbyname;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Project_Disable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }

        /// <summary>
        /// To Enable a task again
        /// Created by:- Yashashwini
        /// Date:- 01/06/2018
        /// </summary>>
        /// <returns></returns>

        public string EnableTask(TaskMasterModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.TaskMasters.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = true;
                            data.updatedby = model.updatedby;
                            data.updatedbyname = model.updatedbyname;
                            data.updatedon = DateTime.Now;
                            data.createdby = model.createdby;
                            data.createdon = model.createdon;
                            data.createdbyname = model.createdbyname;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Task_Enable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }


        /// <summary>
        /// To Update a Task
        /// Created by:- Yashashwini
        /// Date:- 01/06/2018
        /// </summary>
        /// <returns></returns>
        public string UpdateTask(TaskMasterModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.TaskMasters.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    //Check for Duplicate Record
                    bool bool_sameNameExists = _db.TaskMasters.Any(x => x.tasktext.ToLower().Trim() == model.tasktext.ToLower().Trim() && x.id != model.id && x.projectid== model.projectid);
                    if (bool_sameNameExists)
                    {
                        Result = MessageHelper.Task_Duplicate;
                    }
                    else
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            try
                            {
                                data.tasktext = model.tasktext;
                                data.assigneddate = model.assigneddate;
                                //data.departmentid = model.departmentid;
                                data.expectedcompletiondate = model.expectedcompletiondate;
                                data.actualcompletiondate = model.actualcompletiondate;
                                data.isactive = true;
                                data.updatedby = model.updatedby;
                                data.updatedbyname = model.updatedbyname;
                                data.updatedon = DateTime.Now;
                                data.createdby = model.createdby;
                                data.createdon = model.createdon;
                                data.createdbyname = model.createdbyname;
                                _db.SaveChanges();

                                foreach (var item in model.AssociateList)
                                {
                                    var task_assign = _db.TaskAssigns.FirstOrDefault(x => x.associate_id == item.id && x.task_id == data.id);
                                    if (task_assign != null)
                                    {
                                        task_assign.is_selected = item.is_selected;

                                        task_assign.updatedon = DateTime.Now;
                                        task_assign.updatedby = model.updatedby;
                                        task_assign.updatedbyname = model.updatedbyname;

                                        _db.SaveChanges();
                                    }
                                    else
                                    {
                                        TaskAssign taskAssign = new TaskAssign();
                                        taskAssign.is_selected = item.is_selected;
                                        taskAssign.associate_id = item.id;
                                        taskAssign.task_id = model.id;
                                        taskAssign.updatedon = DateTime.Now;
                                        taskAssign.updatedby = model.updatedby;
                                        taskAssign.updatedbyname = model.updatedbyname;
                                        _db.TaskAssigns.Add(taskAssign);
                                        _db.SaveChanges();
                                    }
                                }

                                scope.Complete();
                                Result = MessageHelper.Task_AddSuccess;
                            }
                            catch (Exception ex)
                            {
                                scope.Dispose();
                                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                Result = MessageHelper.Task_AddError;
                            }
                        }
                    }
                }
            }


            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Task_AddError;
            }
            return Result;
        }

        public List<TaskMasterModel> GetTaskList()
        {

            TaskMasterModel model = new TaskMasterModel();
            try
            {
                model.TaskMasterModelList = (from task in _db.TaskMasters
                                             join project in _db.ProjectMasters
                                             on task.projectid equals project.id
                                             select new TaskMasterModel
                                             {
                                                 id = task.id,
                                                 actualcompletiondate = task.actualcompletiondate,
                                                 assigneddate = task.assigneddate,
                                                 assignto = task.assignto,
                                                 createdby = task.createdby,
                                                 project_name = project.projecttext,
                                                 createdon = task.createdon,
                                                 expectedcompletiondate = task.expectedcompletiondate,
                                                 isactive = task.isactive,
                                                 projectid = task.projectid,
                                                 tasktext = task.tasktext,
                                                 updatedby = task.updatedby,
                                                 updatedon = task.updatedon,


                                             }).OrderByDescending(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.TaskMasterModelList;
        }
    }
}




