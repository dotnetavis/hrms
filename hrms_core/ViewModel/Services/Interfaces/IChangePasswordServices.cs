﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IChangePasswordServices
    {
        //void savechangepassword(int userid);
        //void checkuserpassword(int userid,string userpass);
        //bool AddPassword(UserModel model);
        List<UserModel> CheckUserPassword(UserModel user);
        bool ChangePass(UserModel user);
    }
}
