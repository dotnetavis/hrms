﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class StateModel: State
    {
        public List<StateModel> StateModelList { get; set; }
        public List<CountryModel> CountryModelList { get; set; }
        public string country_name { get; set; }
        public StateModel()
        {
            StateModelList = new List<StateModel>();
            CountryModelList = new List<CountryModel>();
        }
    }
}
