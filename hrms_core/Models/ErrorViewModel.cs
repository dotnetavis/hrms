using System;

namespace hrms_core.Models
{
    public class ErrorViewModel:CommonAttributeModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}