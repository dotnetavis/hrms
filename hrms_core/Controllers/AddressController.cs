﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace hrms_core.Controllers
{
    public class AddressController : Controller
    {
        private readonly ApplicationDbContext _db;
        private ICountryServices countryServices => new CountryServices(_db);
        private ICityServices cityServices => new CityServices(_db);
        private IStateServices stateServices => new StateServices(_db);
        public AddressController(ApplicationDbContext db) => _db = db;

        [HttpGet]
        public IActionResult Country(int? id)
        {
            CountryModel model = new CountryModel();
            SessionModel Ses = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (Ses != null)
            {
                model.createdby = Ses.user_id;
                model.updatedby = Ses.user_id;
                model.createdbyname = Ses.username;
                model.updatedbyname = Ses.username;
                model.CountryModelList = countryServices.GetCountry();
                if (id > 0 && id != null)
                {
                    if (model.CountryModelList.Count > 0 && model.CountryModelList != null)
                    {
                        var data = model.CountryModelList.FirstOrDefault(x => x.id == id);
                        if (data != null)
                        {
                            model.countrytext = data.countrytext;
                            model.isactive = data.isactive;
                        }
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult Country(CountryModel model)
        {
            SessionModel Ses = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (Ses != null)
            {
                model.createdby = Ses.user_id;
                model.updatedby = Ses.user_id;
                model.createdbyname = Ses.username;
                model.updatedbyname = Ses.username;
                if (model.id>0)
                TempData["msg"] = countryServices.UpdateCountry(model);
                else
                TempData["msg"] = countryServices.SaveCountry(model);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("Country", "Address", new { id = "" });
        }
        [HttpPost]
        public JsonResult DisableCountry(int id)
        {
            CountryModel model = new CountryModel();
            model.id = id;
            model.updatedby = 1;
            TempData["msg"] = countryServices.DisableCountry(model);
            return Json(true);
        }
        [HttpPost]
        public JsonResult EnableCountry(int id)
        {
            CountryModel model = new CountryModel();
            model.id = id;
            model.updatedby = 1;
            TempData["msg"] = countryServices.EnableCountry(model);
            return Json(true);
        }

        [HttpGet]
        public  IActionResult State(int?id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            StateModel model = new StateModel();
            model.createdby = SesMdl.user_id;
            model.updatedby = SesMdl.user_id;
            model.createdbyname = SesMdl.username;
            model.updatedbyname = SesMdl.username;
            model.CountryModelList = countryServices.GetCountry_Active();
            model.StateModelList = stateServices.GetState();
            if(id>0&&id!=null)
            {
                if(model.StateModelList.Count>0&&model.StateModelList!=null)
                {
                    var data = _db.States.FirstOrDefault(x => x.id == id);
                    if(data!=null)
                    {
                        model.country_id = data.country_id;
                        model.statetext = data.statetext;
                        model.isactive = data.isactive;
                    }
                }
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult State(StateModel model)
        {
            SessionModel Ses = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (Ses != null)
            {
                model.createdby = Ses.user_id;
                model.updatedby = Ses.user_id;
                model.createdbyname = Ses.username;
                model.updatedbyname = Ses.username;
                if (model.id > 0)
                TempData["msg"] = stateServices.UpdateState(model);
               else
                TempData["msg"] = stateServices.SaveState(model);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("State", "Address", new { id = "" });
        }

        [HttpPost]
        public JsonResult DisableState(int id)
        {
            StateModel model = new StateModel();
            model.id = id;
            model.updatedby = 1;
            TempData["msg"] = stateServices.DisableState(model);
            return Json(true);
        }
        [HttpPost]
        public JsonResult EnableState(int id)
        {
            StateModel model = new StateModel();
            model.id = id;
            model.updatedby = 1;
            TempData["msg"] = stateServices.EnableState(model);
            return Json(true);
        }

        [HttpGet]
        public IActionResult City(int?id)
        {
            CityModel model = new CityModel();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            model.createdby = SesMdl.user_id;
            model.updatedby = SesMdl.user_id;
            model.createdbyname = SesMdl.username;
            model.updatedbyname = SesMdl.username;
            model.StateModelList = stateServices.GetState_Active();
            model.CityModelList = cityServices.GetCity();
            if(id>0&&id!=null)
            {
                if(model.CityModelList.Count>0&&model.CityModelList!=null)
                {
                    var data = _db.Citys.FirstOrDefault(x => x.id == id);
                    if(data!=null)
                    {
                        model.state_id = data.state_id;
                        model.citytext = data.citytext;
                        model.isactive = data.isactive;
                    }
                }
            }

            return View(model);
        }

        [HttpPost]
        public IActionResult City(CityModel model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            model.createdby = SesMdl.user_id;
            model.updatedby = SesMdl.user_id;
            model.createdbyname = SesMdl.username;
            model.updatedbyname = SesMdl.username;
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (model.id > 0)
                TempData["msg"] = cityServices.UpdateCity(model);
            else
                TempData["msg"] = cityServices.SaveCity(model);
            return RedirectToAction("City", "Address", new { id = "" });
        }

        [HttpPost]
        public JsonResult DisableCity(int id)
        {
            CityModel model = new CityModel();
            model.id = id;
            model.updatedby = 1;
            TempData["msg"] = cityServices.DisableCity(model);
            return Json(true);
        }

        [HttpPost]
        public JsonResult EnableCity(int id)
        {
            CityModel model = new CityModel();
            model.id = id;
            model.updatedby = 1;
            TempData["msg"] = cityServices.EnableCity(model);
            return Json(true);
        }
    }
}