﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace hrms_core.Models
{
    public class EmployeeDashboard:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }                  //primary key
       
        [Required(ErrorMessage ="Enter Award Name")]
        public string awardname { get; set; }
       // [Range(1,Int32.MaxValue,ErrorMessage ="Department Required")]
        public int department_id { get; set; }
       // [Range(1, Int32.MaxValue, ErrorMessage = "Employee Name Required")]
        public int employee_id { get; set; }
        [Required(ErrorMessage = "Enter Designation")]
        public string designation_text { get; set; }
        [Required(ErrorMessage = "Enter month")]
        public string month_name { get; set; }
        public string remark { get; set; }
        public string imagepath { get; set; }
        
    }
}
