﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class EmployeeDashboardServices : IEmployeeDashboardServices
    {
        private readonly ApplicationDbContext _db;
        public EmployeeDashboardServices(ApplicationDbContext options)
        {
            _db = options;
        }
        public List<EmployeeDashboardModel> GetEmployeeDetails()
        {
            EmployeeDashboardModel model = new EmployeeDashboardModel();
            try
            {
                model.EmployeeDashboardModelList = (from empDash in _db.EmployeeDashboards
                                                    join master in _db.AssociateMasters on empDash.employee_id equals master.id
                                                    join depart in _db.Departments on empDash.department_id equals depart.id
                                                    select new EmployeeDashboardModel
                                                    {
                                                        id = empDash.id,
                                                        awardname = empDash.awardname,
                                                        department_id = empDash.department_id,
                                                        employee_id = empDash.employee_id,
                                                        designation_text = empDash.designation_text,
                                                        month_name = empDash.month_name,
                                                        remark = empDash.remark,
                                                        imagepath = empDash.imagepath,
                                                        isactive = empDash.isactive,
                                                        createdby = empDash.createdby,
                                                        updatedon = empDash.updatedon,
                                                        updatedby = empDash.updatedby,
                                                        updatedbyname=empDash.updatedbyname,
                                                        createdbyname=empDash.createdbyname,
                                                        employee_name = master.associate_name,
                                                        department_name = depart.departmenttext,

                                                    }).OrderByDescending(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.EmployeeDashboardModelList;
        }

        public List<EmployeeDashboardModel> GetEmployeeDetailsByID(int associateid)
        {
            EmployeeDashboardModel model = new EmployeeDashboardModel();
            try
            {
                model.EmployeeDashboardModelList = (from empDash in _db.EmployeeDashboards.Where(x=>x.id==associateid)
                                                    join master in _db.AssociateMasters on empDash.employee_id equals master.id
                                                    join depart in _db.Departments on empDash.department_id equals depart.id
                                                    select new EmployeeDashboardModel
                                                    {
                                                        id = empDash.id,
                                                        awardname = empDash.awardname,
                                                        department_id = empDash.department_id,
                                                        employee_id = empDash.employee_id,
                                                        designation_text = empDash.designation_text,
                                                        month_name = empDash.month_name,
                                                        remark = empDash.remark,
                                                        imagepath = empDash.imagepath,
                                                        isactive = empDash.isactive,
                                                        createdby = empDash.createdby,
                                                        updatedon = empDash.updatedon,
                                                        updatedby = empDash.updatedby,
                                                        updatedbyname = empDash.updatedbyname,
                                                        createdbyname = empDash.createdbyname,
                                                        employee_name = master.associate_name,
                                                        department_name = depart.departmenttext,

                                                    }).OrderByDescending(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.EmployeeDashboardModelList;
        }

        public string SaveEmployeeDetails(EmployeeDashboardModel model)
        {
            string Result = string.Empty;
            EmployeeDashboard employeeDashboard = new EmployeeDashboard();
            try
            {
                //Check for Duplicacy in Records
                bool bool_sameNameExists = _db.EmployeeDashboards.Any(x => x.awardname.ToLower().Trim() == model.awardname.ToLower().Trim() && x.month_name == model.month_name && x.department_id == model.department_id && x.awardname == model.awardname);
                if (bool_sameNameExists)
                {
                    Result = MessageHelper.Employee_Duplicate;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            var list = (from user in _db.Users
                                        join asso in _db.AssociateMains on user.id equals asso.associate_id
                                        where user.id == asso.associate_id
                                        select new UserModel
                                        {
                                         //   userfullname = user.userfullname
                                        }).ToList();
                          
                            employeeDashboard.awardname = model.awardname;
                            employeeDashboard.department_id = model.department_id;
                            employeeDashboard.employee_id = model.employee_id;
                            employeeDashboard.designation_text = model.designation_text;
                            employeeDashboard.month_name = model.month_name;
                            employeeDashboard.remark = model.remark;
                            employeeDashboard.imagepath = model.imagepath;
                            employeeDashboard.isactive = true;
                            employeeDashboard.updatedon = DateTime.Now;
                            employeeDashboard.createdby = 1;
                            _db.EmployeeDashboards.Add(employeeDashboard);
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Employee_AddSuccess;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.Employee_AddError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Employee_AddError;
            }
            return Result;
        }

        public string UpdateEmployeeDeatails(EmployeeDashboardModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.EmployeeDashboards.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    //Check for Duplicate Record
                    bool bool_sameNameExists = _db.EmployeeDashboards.Any(x => x.awardname.ToLower().Trim() == model.awardname.ToLower().Trim() && x.id != model.id);
                    if (bool_sameNameExists)
                    {
                        Result = MessageHelper.qualification_duplicate;
                    }
                    else
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            try
                            {
                                data.awardname = model.awardname;
                                data.awardname = model.awardname;
                                data.department_id = model.department_id;
                                data.employee_id = model.employee_id;
                                data.designation_text = model.designation_text;
                                data.month_name = model.month_name;
                                data.remark = model.remark;
                                data.imagepath = model.imagepath;
                                data.isactive =true;
                                data.updatedon = DateTime.Now;
                                data.createdon = model.createdon;
                                data.updatedby = model.updatedby;
                                data.createdby = model.createdby;
                                data.updatedbyname = model.updatedbyname;
                                data.createdbyname = model.createdbyname;
                                _db.SaveChanges();
                                scope.Complete();
                                Result = MessageHelper.qualification_addsuccess;
                            }
                            catch (Exception ex)
                            {
                                scope.Dispose();
                                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                Result = MessageHelper.qualification_adderror;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.qualification_adderror;
            }
            return Result;
        }
        public string DisableEmployee(EmployeeDashboardModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.EmployeeDashboards.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            string filename = data.imagepath;
                            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload", filename);
                            if (File.Exists(path))
                            {
                                File.Delete(path);
                            }
                            _db.EmployeeDashboards.Remove(data);
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Project_Disable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }
        public string EnableEmployee(EmployeeDashboardModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.EmployeeDashboards.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = true;
                            data.updatedon = DateTime.Now;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Employee_Enable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }
        public List<AssociateMasterModel> GetEmployeeByDepartmentId(int DepartmentId)
        {
            EmployeeDashboardModel model = new EmployeeDashboardModel();
            try
            {
                model.AssociateMasterModelList = (from main in _db.AssociateMains.Where(x => x.department_id == DepartmentId)
                                                  join master in _db.AssociateMasters on main.associate_id equals master.id
                                                  select new AssociateMasterModel
                                                  {
                                                      associate_name = master.associate_name,
                                                      id = master.id,
                                                      department_id = main.department_id
                                                  }).ToList();
                return model.AssociateMasterModelList;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return model.AssociateMasterModelList;
            }
        }
        public string GetDesignationByAssociateId(int AssociateId)
        {
            EmployeeDashboardModel model = new EmployeeDashboardModel();
            string DesignationName = string.Empty;
            try
            {
                model.AssociateMasterModelList = (from main in _db.AssociateMains.Where(x => x.associate_id == AssociateId)
                                                  join Desig in _db.Designations on main.associate_id equals Desig.id
                                                  select new AssociateMasterModel
                                                  {
                                                      DesignationText = Desig.designationtext
                                                  }).ToList();
                if (model.AssociateMasterModelList != null)
                    DesignationName = model.AssociateMasterModelList.FirstOrDefault().DesignationText;
                return DesignationName;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return DesignationName;
            }
        }
    }
}
