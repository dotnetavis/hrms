﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class EmailConfiguration:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage ="Email Send From Required")]
        public string email_send_from { get; set; }
        [Required(ErrorMessage = "Password Required")]
        [MinLength(5,ErrorMessage ="Password length must be greater than 5")]
        public string password { get; set; }
        [Required(ErrorMessage = "SMTP Required")]
        public string smtp { get; set; }
        [Required(ErrorMessage = "Port Required")]
        [Range(100,999,ErrorMessage ="Invalid Port")]
        public int port { get; set; }
        [Required(ErrorMessage = "SSL Required")]
        public bool ssl { get; set; }
        
    }
}
