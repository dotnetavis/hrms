﻿using hrms_core.EF;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;
using NUglify.Helpers;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.EntityFrameworkCore.Internal;

namespace hrms_core.ViewModel.Services
{
    public class AttendanceService : IAttendance
    {
        private readonly ApplicationDbContext _db;
        public AttendanceService(ApplicationDbContext options) => _db = options;
        public List<AssociateMasterModel> GetUsers(int? associateid)
        {
            AssociateMasterModel model = new AssociateMasterModel();
            try
            {
                if (!associateid.HasValue)
                {
                    model.AssociateMasterModelList = _db.AssociateMasters.Where(x => x.isactive == true)
                .Select(x => new AssociateMasterModel
                {
                    id = x.id,
                    associate_name = x.associate_name,
                    applicable_to = x.applicable_to,
                    application_date = x.application_date,
                    associate_code = x.associate_code,
                    createdby = x.createdby,
                    createdon = x.createdon,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    isactive = x.isactive
                }).OrderByDescending(m => m.updatedon).ToList();
                }
                else
                {

                    model.AssociateMasterModelList = (from main in _db.AssociateMains.Where(x => x.isactive == true && x.reportingperson_id == associateid)
                                                      join mast in _db.AssociateMasters.Where(x => x.isactive == true) on main.associate_id equals mast.id
                                                      select new AssociateMasterModel
                                                      {
                                                          id = mast.id,
                                                          associate_name = mast.associate_name,
                                                          applicable_to = mast.applicable_to,
                                                          application_date = mast.application_date,
                                                          associate_code = mast.associate_code,
                                                          createdby = mast.createdby,
                                                          createdon = mast.createdon,
                                                          updatedby = mast.updatedby,
                                                          updatedon = mast.updatedon,
                                                          isactive = mast.isactive
                                                      }).OrderByDescending(m => m.updatedon).ToList();
                    List<AssociateMasterModel> Aditionalassociate = new List<AssociateMasterModel>();
                    Aditionalassociate=(from add in _db.AssociateAdditionalInfos.Where(x=> x.reporting_person_id==associateid)
                                       join mast in _db.AssociateMasters.Where(x=> x.isactive==true) on add.associate_id equals mast.id
                                       select new AssociateMasterModel
                                       {
                                           id = mast.id,
                                           associate_name = mast.associate_name,
                                           applicable_to = mast.applicable_to,
                                           application_date = mast.application_date,
                                           associate_code = mast.associate_code,
                                           createdby = mast.createdby,
                                           createdon = mast.createdon,
                                           updatedby = mast.updatedby,
                                           updatedon = mast.updatedon,
                                           isactive = mast.isactive
                                       }).OrderByDescending(m => m.updatedon).ToList();
                    foreach(var item in Aditionalassociate)
                    {
                        model.AssociateMasterModelList.Add(item);
                    }
                    AssociateMasterModel associate = new AssociateMasterModel();
                    var associatemodel = _db.AssociateMasters.FirstOrDefault(x => x.id == associateid);
                    associate = new AssociateMasterModel()
                    {
                        id = associatemodel.id,
                        associate_name = associatemodel.associate_name,
                        applicable_to = associatemodel.applicable_to,
                        application_date = associatemodel.application_date,
                        associate_code = associatemodel.associate_code,
                        createdby = associatemodel.createdby,
                        createdon = associatemodel.createdon,
                        updatedby = associatemodel.updatedby,
                        updatedon = associatemodel.updatedon,
                        isactive = associatemodel.isactive
                    };
                    model.AssociateMasterModelList.Add(associate);
                }

            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            var DistinctData = model.AssociateMasterModelList.DistinctBy(x => x.id).ToList();
            return DistinctData;
           // return null;
        }
        public List<AttendanceDataVm> GetAttendanceDatas(int associate_code, DateTime FromDate, DateTime TODate)
        {
            AttendanceDataVm model = new AttendanceDataVm();
            try
            {
                model.AttendanceDataVmList = (from att in _db.attendancedata.Where(x => x.attendancedate.Date >= FromDate.Date && x.attendancedate.Date <= TODate.Date)
                                              join asso in _db.AssociateMasters on associate_code equals asso.id
                                              select new AttendanceDataVm()
                                              {
                                                  associate_code = asso.associate_code,
                                                  username = asso.associate_name,
                                                  intime = att.intime.ToString(),
                                                  attendancedate = att.attendancedate.Date,
                                                  outtime = att.outtime.ToString(),
                                                  logintime = att.logintime
                                              }).ToList();
                return model.AttendanceDataVmList;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return null;
            }
        }
        public List<DateTime> GetDates(DateTime FromDate, DateTime TODate)
        {
            AttendanceDataVm model = new AttendanceDataVm();
            var dates = new List<DateTime>();

            for (var dt = FromDate; dt <= TODate; dt = dt.AddDays(1))
            {
                dates.Add(dt);
            }
            return dates;
        }
        public List<String> GetLoginTimes(int associate_code, DateTime FromDate, DateTime TODate)
        {
            try
            {
                AttendanceDataVm model = new AttendanceDataVm
                {
                    AttendanceDataVmList = GetAttendanceDatas(associate_code, FromDate, TODate)
                };
                for (DateTime i = FromDate; i <= TODate;)
                {
                    var login = _db.attendancedata.Where(x => x.attendancedate == i.Date && x.id == associate_code).FirstOrDefault().logintime;

                    model.LoginTimes.Add(login.ToString());
                }
                return model.LoginTimes;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return null;
            }

        }
    }
}
