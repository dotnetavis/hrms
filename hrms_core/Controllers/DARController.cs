﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Transactions;
using Microsoft.Extensions.Configuration;

namespace hrms_core.Controllers
{
    public class DARController : Controller
    {
        private readonly ApplicationDbContext _db;
        public IConfiguration Configuration { get; }
        private IDailyActivityReport dailyReportServices => new DailyActivityReportServices(_db);
        private IAttendance attendanceService => new AttendanceService(_db);
        private IProjectServices projectServices => new ProjectServices(_db);
        private ITaskMasterServices taskMasterServices => new TaskMasterServices(_db);
        private IDailyServices dailyServices => new DailyServices(_db);
        public int modelPartialCount = 0;

        public DARController(ApplicationDbContext options, IConfiguration configuration)
        {
            _db = options;
            Configuration = configuration;
        }

        [HttpGet]
        public IActionResult CreateDAR(int? id)
        {
            DARVM model = new DARVM();
            List<int> _ListAssociate = new List<int>();
            int ProjectId = 0;
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.dailyActivityReportModel.associate_id = SesMdl.associate_id;
                model.associate_name = SesMdl.associate_name;
                model.dailyReportDetailModel.createdby = SesMdl.user_id;
                model.dailyReportDetailModel.updatedby = SesMdl.user_id;
                model.dailyReportDetailModel.createdbyname = SesMdl.username;
                model.dailyReportDetailModel.updatedbyname = SesMdl.username;
                model.dailyActivityReportModel.reportingdate = DateTime.Now.Date;
                model.dailyActivityReportModel.dailyReportDetaillist = dailyServices.GetAllDailyReport();
                model.ProjectMasterModelList = projectServices.GetProject(SesMdl.associate_id);
                model.ProjectMasterModelList.Insert(0, new ProjectMasterModel { id = 0, projecttext = "--Select--" });
                model.TaskMasterModelList = taskMasterServices.GetTask(SesMdl.associate_id);
                model.TaskMasterModelList.Insert(0, new TaskMasterModel { id = 0, tasktext = "--Select--" });

                if (id != null && id > 0)
                {
                    model.dailyActivityReportModel = dailyServices.GetActivityReportById(id.Value);
                    model.dailyReportDetailModel.DailyReportDetailModelList = dailyServices.GetDARDetails(id.Value);
                    if (model.dailyReportDetailModel.DailyReportDetailModelList != null && model.dailyReportDetailModel.DailyReportDetailModelList.Count > 0)
                    {
                        model.dailyReportDetailModel.DailyReportDetailModelList.FirstOrDefault().ProjectMasterModelList = projectServices.GetProject(SesMdl.associate_id);
                    }
                    else
                    {
                        model.dailyReportDetailModel.DailyReportDetailModelList.Add(new DailyReportDetailModel());
                        model.dailyReportDetailModel.DailyReportDetailModelList.FirstOrDefault().ProjectMasterModelList = projectServices.GetProject(SesMdl.associate_id);
                    }
                    model.dailyReportDetailModel.DailyReportDetailModelList.FirstOrDefault().ProjectMasterModelList.Insert(0, new ProjectMasterModel { id = 0, projecttext = "--Select--" });
                    model.dailyReportDetailModel.id = id.Value;
                    var _data = dailyServices.GetActivityById(id.Value);
                    if (_data != null)
                    {
                        int FirstTableId = _db.DailyActivityReports.OrderBy(x=>x.id).Select(x => x.id).LastOrDefault();
                        foreach (var item in model.dailyReportDetailModel.DailyReportDetailModelList)
                        {
                            if (item != null)
                            {
                                DailyReportDetail detail = new DailyReportDetail
                                {
                                    id = item.id,
                                    dar_id = FirstTableId,
                                    projectid = item.projectid
                                };
                                ProjectId = item.projectid;
                                detail.taskid = item.taskid;
                                detail.activitydetail = item.activitydetail;
                                detail.hour = item.hour;
                                detail.percentage = item.percentage;
                                detail.status = item.status;
                                detail.taskstatus = item.taskstatus;
                                detail.remark = item.remark ?? item.remark;
                                item.ProjectMasterModelList = projectServices.GetProject(SesMdl.associate_id);
                                item.ProjectMasterModelList.Insert(0, new ProjectMasterModel { id = 0, projecttext = "--Select--" });
                                item.TaskMasterModelList = taskMasterServices.GetTask(SesMdl.associate_id).ToList();
                                item.TaskMasterModelList.Insert(0, new TaskMasterModel { id = 0, tasktext = "--Select--" });
                            }
                        }
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult CreateDAR(DARVM model, string CommandName)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            int acceptedActivity = 0;
            if (SesMdl != null)
            {
                model.dailyActivityReportModel.associate_id = SesMdl.associate_id;
                model.associate_name = SesMdl.associate_name;
                model.dailyReportDetailModel.createdby = SesMdl.user_id;
                model.dailyReportDetailModel.updatedby = SesMdl.user_id;
                model.dailyReportDetailModel.createdbyname = SesMdl.username;
                model.dailyReportDetailModel.updatedbyname = SesMdl.username;
                var TotalHours = 0;
                var TotalMins = 0;

                var darlist = _db.DailyActivityReports.Where(x => x.associate_id == SesMdl.associate_id && x.reportingdate == model.dailyActivityReportModel.reportingdate).ToList();

                if (darlist != null && darlist.Count > 0 && darlist.FirstOrDefault().is_authorized == true)
                {
                    TempData["msg"] = "Daily activity report for " + model.dailyActivityReportModel.reportingdate.ToString("dd-MMM-yyyy") + " already exist.";
                    return RedirectToAction("CreateDAR", "DAR");
                }
                if (model.dailyReportDetailModel.DailyReportDetailModelList == null || model.dailyReportDetailModel.DailyReportDetailModelList.Count == 0)
                {
                    TempData["msg"] = "Please Enter Atleast One Daily Activity Details";
                    return View(model);
                }
                if (model.dailyReportDetailModel.DailyReportDetailModelList != null && model.dailyReportDetailModel.DailyReportDetailModelList.Count > 0)
                {
                    foreach (var item in model.dailyReportDetailModel.DailyReportDetailModelList)
                    {
                        if (item != null && item.hour != null)
                        {
                            var CalculatedHours = item.hour.Split(':');
                            int _hours = Convert.ToInt32(CalculatedHours[0]);
                            int _minutes = Convert.ToInt32(CalculatedHours[1]);
                            TotalHours += _hours;
                            TotalMins += _minutes;
                        }

                    }
                }
                int resultantHour = TotalMins / 60 + TotalHours;
                int resultantMin = TotalMins % 60;
                model.dailyActivityReportModel.totalhour = resultantHour.ToString() + ":" + resultantMin.ToString();
                bool isCompleteDetailExist = true;
                if (model.dailyReportDetailModel.DailyReportDetailModelList != null && model.dailyReportDetailModel.DailyReportDetailModelList.Count > 0)
                {
                    foreach (var item in model.dailyReportDetailModel.DailyReportDetailModelList)
                    {
                        if (item != null)
                        {
                            acceptedActivity += 1;
                            item.ProjectMasterModelList = projectServices.GetProject(SesMdl.associate_id);
                            item.ProjectMasterModelList.Insert(0, new ProjectMasterModel { id = 0, projecttext = "--Select--" });
                            item.TaskMasterModelList = taskMasterServices.GetTask(SesMdl.associate_id);
                            item.TaskMasterModelList.Insert(0, new TaskMasterModel { id = 0, tasktext = "--Select--" });
                            if (item.projectid == 0 || item.taskid == 0 || item.activitydetail == null || item.hour == null || item.status == null || item.percentage == 0)
                            {
                                if (isCompleteDetailExist == true)
                                    isCompleteDetailExist = false;
                            }
                        }
                        //else
                        //{
                        //    isCompleteDetailExist = false;
                        //}

                    }

                }
                if (acceptedActivity == 0)
                {
                    TempData["msg"] = "Please Enter Atleast One Daily Activity Details";
                    return View(model);
                }
                else
                {
                    if (!isCompleteDetailExist)
                    {
                        TempData["msg"] = "Enter Complete Daily Activity Details";
                        return View(model);
                    }
                }
                if (model.dailyReportDetailModel.DailyReportDetailModelList != null && model.dailyReportDetailModel.DailyReportDetailModelList.Count > 0)
                {
                    foreach (var item in model.dailyReportDetailModel.DailyReportDetailModelList)
                    {
                        if (item != null)
                        {
                            if (item.hour != null)
                            {
                                if (resultantHour > 24)
                                {

                                    item.ProjectMasterModelList = projectServices.GetProject(SesMdl.associate_id);
                                    item.ProjectMasterModelList.Insert(0, new ProjectMasterModel { id = 0, projecttext = "--Select--" });
                                    item.TaskMasterModelList = taskMasterServices.GetTask(SesMdl.associate_id).Where(x => x.projectid == item.projectid).ToList();
                                    item.TaskMasterModelList.Insert(0, new TaskMasterModel { id = 0, tasktext = "--Select--" });
                                    TempData["msg"] = "Please Enter less than or equal to 24 hours";
                                    return View(model);
                                }
                            }
                        }

                    }
                }
                if (CommandName == "Save & Authorized")
                {
                    model.dailyActivityReportModel.is_authorized = true;
                }
                else
                {
                    model.dailyActivityReportModel.is_authorized = false;
                }
                if (model != null)
                {
                    int result = 0;
                    bool _result;
                    if (model.dailyActivityReportModel.id > 0)
                    {
                        result = dailyServices.EditDailyReport(model);

                    }
                    else
                    {
                        result = dailyServices.SaveData(model);
                    }
                    if (model.dailyActivityReportModel.is_authorized == true && result > 0)
                    {
                        model.official_email = SesMdl.official_email;
                        model.hod_email = SesMdl.head_of_department_email;
                        model.username = _db.AssociateMasters.FirstOrDefault(x => x.associate_code == SesMdl.username).associate_name;
                        model.reporting_email = SesMdl.reporting_person_email;
                        model.additional_reporting_email = SesMdl.addition_reporting;
                        model.designation = SesMdl.designation_text;
                        model.lstDailyReportDetailModel = dailyServices.GetDARDetails(result);

                        _result = dailyServices.SendDARReport(model, HttpContext, Configuration);
                        if (_result == true)
                        {
                            TempData["msg"] = "DAR Report is saved and authorized";
                        }
                    }
                    if (result > 0)
                        TempData["msg"] = "Daily Activity Report Submitted Successfully.";
                    else
                        TempData["msg"] = "Daily Activity Report not Submitted.";
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("DARReport", "DAR");
        }

        [HttpPost]
        public JsonResult GetTaskByProjectId(int ProjectID)
        {
            DARVM model = new DARVM
            {
                TaskMasterModelList = taskMasterServices.GetTask_ByProjectID(ProjectID)
            };
            return Json(model.TaskMasterModelList);
        }
        public PartialViewResult DailyActivityReportPartial()
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            DailyReportDetailModel model = new DailyReportDetailModel();
            if (SesMdl != null)
            {

                modelPartialCount++;
                model.ProjectMasterModelList = projectServices.GetProject(SesMdl.associate_id);
                model.ProjectMasterModelList.Insert(0, new ProjectMasterModel { id = 0, projecttext = "--Select--" });
                model.TaskMasterModelList.Insert(0, new TaskMasterModel { id = 0, tasktext = "--Select--" });
                model.PartialCount = modelPartialCount;
                return PartialView("_DailyActivityDetail", model);
            }
            else
            {
                TempData["msg"] = "Session Has Been Expired";
                return PartialView("_DailyActivityDetail", model);
            }

        }
        public IActionResult GetTaskList(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            TaskMasterModel model = new TaskMasterModel();
            if (SesMdl != null)
            {
                List<int> Associate_List = new List<int>();
                var data = _db.AssociateMains.Where(x => x.reportingperson_id == SesMdl.associate_id && x.isactive == true).ToList();
                if (data != null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        Associate_List.Add(item.associate_id);
                    }
                    model.TaskMasterModelList = taskMasterServices.GetTask_ByProjectID(id).Where(x => x.projectid == id).ToList();
                }
                else
                {
                    model.TaskMasterModelList = taskMasterServices.GetTask(SesMdl.associate_id).Where(x => x.projectid == id).ToList();
                }
                return Json(model.TaskMasterModelList);
            }
            else
            {
                TempData["msg"] = "Session Has Been Expired";
                return RedirectToAction("Login", "Account");
            }
        }
        [HttpGet]
        public IActionResult DARReport()
        {
            DailyActivityReportModel model = new DailyActivityReportModel();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.user_id;
                model.updatedby = SesMdl.user_id;
                model.createdbyname = SesMdl.username;
                model.updatedbyname = SesMdl.username;
                model.DailyActivityReportModelList = dailyServices.GetAllDailyReportDetails().Where(x => x.associate_id == SesMdl.associate_id).ToList();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult DARReport(DailyActivityReportModel model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.user_id;
                model.updatedby = SesMdl.user_id;
                model.createdbyname = SesMdl.username;
                model.updatedbyname = SesMdl.username;
                var data = dailyServices.GetAllDailyReportDetails().Where(x => x.associate_id == model.associate_id).ToList();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

            return RedirectToAction("DARReport", "DAR");
        }

        [HttpGet]
        public IActionResult AssociateReportDetails(int DarId)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                DailyActivityReportModel model = new DailyActivityReportModel
                {
                    DailyActivityReportModelList = (from daily in _db.DailyActivityReports.Where(x => x.id == DarId)
                                                    join master in _db.AssociateMasters
                                                    on daily.associate_id equals master.id
                                                    select new DailyActivityReportModel
                                                    {
                                                        createdbyname = daily.createdbyname,
                                                        createdon = daily.createdon,
                                                        reportingdate = daily.reportingdate,
                                                        is_authorized = daily.is_authorized,
                                                        isactive = daily.isactive,
                                                        totalhour = daily.totalhour,
                                                        on_duty = daily.on_duty,
                                                        associate_id = daily.associate_id,
                                                        associate_name = master.associate_name
                                                    }).ToList(),
                    dailyReportDetaillist = _db.DailyReportDetails.Where(x => x.dar_id == DarId).Select(obj => new DailyReportDetailModel
                    {
                        dar = obj.dar,
                        dar_id = obj.dar_id,
                        activitydetail = obj.activitydetail,
                        hour = obj.hour,
                        percentage = obj.percentage,
                        remark = obj.remark == null ? "" : obj.remark,
                        status = obj.status,
                        taskstatus = obj.taskstatus,
                        task_name = _db.TaskMasters.FirstOrDefault(x => x.id == obj.taskid).tasktext,
                        project_name = _db.ProjectMasters.FirstOrDefault(x => x.id == obj.projectid).projecttext
                    }).ToList()
                };
                return Json(model);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpGet]
        public IActionResult AdminDARBucket()
        {
            DailyActivityReportModel model = new DailyActivityReportModel();
            SessionModel session = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (session == null)
            {
                return RedirectToAction("Login", "Account");
            }
            model.AssociateVMList = (from master in _db.AssociateMasters.Where(x => x.isactive == true && x.is_super_admin == false)
                                     join main in _db.AssociateMains
                                     on master.id equals main.associate_id
                                     select new AssociateVM
                                     {
                                         associate_id = master.id,
                                         associate_code = master.associate_code,
                                         associate_name = master.associate_name,
                                         designation = _db.Designations.FirstOrDefault(x => x.id == main.designation_id).designationtext,
                                         department = _db.Departments.FirstOrDefault(x => x.id == main.department_id).departmenttext,

                                         contact = main.mobileno,
                                         DOJ = master.application_date,
                                     }).OrderBy(x => x.associate_name).ToList();
            return View(model);
        }
        [HttpPost]
        public IActionResult AdminDARBucket(DailyActivityReportModel model)
        {
            string AssociateName = String.Empty;
            SessionModel session = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            List<DailyActivityReportModel> dailyActivityReportModels = new List<DailyActivityReportModel>();
            if (session == null)
            {
                return RedirectToAction("Login", "Account");
            }
            model.AssociateVMList = (from master in _db.AssociateMasters.Where(x => x.isactive == true && x.is_super_admin == false)
                                     join main in _db.AssociateMains
                                     on master.id equals main.associate_id
                                     select new AssociateVM
                                     {
                                         associate_id = master.id,
                                         associate_code = master.associate_code,
                                         associate_name = master.associate_name,
                                         designation = _db.Designations.FirstOrDefault(x => x.id == main.designation_id).designationtext,
                                         department = _db.Departments.FirstOrDefault(x => x.id == main.department_id).departmenttext,

                                         contact = main.mobileno,
                                         DOJ = master.application_date,
                                     }).OrderBy(x => x.associate_name).ToList();
            
            if (model != null)
            {
                if (model.SelectedDate != DateTime.MinValue)
                {
                    if (model.SelectedDate > DateTime.Now)
                    {
                        TempData["msg"] = "Wrong Input. SelectedDate is exciding the limits ";
                        return RedirectToAction("AdminDARBucket", "DAR");
                    }
                    dailyActivityReportModels = dailyServices.GetAllDailyReportDetails().Where(x => x.reportingdate == model.SelectedDate && x.is_authorized == true && x.isactive == true).ToList();
                    foreach (AssociateVM CurrentAssocaite in model.AssociateVMList)
                    {
                        DailyActivityReportModel CurrentDarModel = new DailyActivityReportModel();
                        DailyActivityReportModel DAR = dailyActivityReportModels.FirstOrDefault(x => x.associate_id == CurrentAssocaite.associate_id);
                        CurrentDarModel.reportingdate = model.SelectedDate;
                        CurrentDarModel.associate_name = CurrentAssocaite.associate_name;
                        if (DAR != null)
                        {
                            CurrentDarModel.id = DAR.id;
                            CurrentDarModel.totalhour = DAR.totalhour;
                            CurrentDarModel.is_authorized = DAR.is_authorized;
                        }
                        else
                        {
                            CurrentDarModel.id = 0;
                            CurrentDarModel.totalhour = "0";
                            CurrentDarModel.is_authorized = false;
                        }
                        model.DailyActivityReportModelList.Add(CurrentDarModel);
                    }
                    model.DailyActivityReportModelList.OrderBy(x => x.reportingdate);
                }
                else
                {
                    AssociateName = model.AssociateVMList.FirstOrDefault(x => x.associate_id == model.associate_id).associate_name;
                    model.Dates = attendanceService.GetDates(model.StartDate, model.EndDate);
                    if (model.Dates.Count > 31)
                    {
                        TempData["msg"] = "Wrong Input. Selected days count shouldn't be more than 31. ";
                        return RedirectToAction("AdminDARBucket", "DAR");
                    }
                    if (model.EndDate > DateTime.Now)
                    {
                        TempData["msg"] = "Wrong Input. ToDate is exciding the limits ";
                        return RedirectToAction("AdminDARBucket", "DAR");
                    }
                    if (model.EndDate < model.StartDate)
                    {
                        TempData["msg"] = "Wrong Input.ToDate Can't be smaller than FromDate";
                        return RedirectToAction("AdminDARBucket", "DAR");
                    }
                    if (model.associate_id <= 0)
                    {
                        TempData["msg"] = "Select Atleast One Associate";
                        return RedirectToAction("AdminDARBucket", "DAR");
                    }
                    dailyActivityReportModels = dailyServices.GetAllDailyReportDetails().Where(x => x.associate_id == model.associate_id && x.reportingdate >= model.StartDate && x.reportingdate <= model.EndDate && x.is_authorized == true && x.isactive == true).ToList();
                    if (model.Dates != null)
                    {
                        foreach (DateTime Current in model.Dates)
                        {
                            DailyActivityReportModel CurrentDarModel = new DailyActivityReportModel();
                            DailyActivityReportModel DAR = dailyActivityReportModels.FirstOrDefault(x => x.reportingdate == Current);
                            CurrentDarModel.reportingdate = Current;
                            CurrentDarModel.associate_name = AssociateName;
                            if (DAR != null)
                            {
                                CurrentDarModel.id = DAR.id;
                                CurrentDarModel.totalhour = DAR.totalhour;
                                CurrentDarModel.is_authorized = DAR.is_authorized;
                            }
                            else
                            {
                                CurrentDarModel.id = 0;
                                CurrentDarModel.totalhour = "0";
                                CurrentDarModel.is_authorized = false;
                            }
                            model.DailyActivityReportModelList.Add(CurrentDarModel);
                        }
                        model.DailyActivityReportModelList.OrderBy(x => x.reportingdate);
                    }
                }
                
            }
            return View(model);
        }

        [HttpPut]
        public IActionResult AdminDARBucket(DateTime StartDate)
        {
            DailyActivityReportModel model = new DailyActivityReportModel();
            SessionModel session = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            List<DailyActivityReportModel> dailyActivityReportModels = new List<DailyActivityReportModel>();
            if (session == null)
            {
                return RedirectToAction("Login", "Account");
            }
            model.StartDate = StartDate;
            model.AssociateVMList = (from master in _db.AssociateMasters.Where(x => x.isactive == true && x.is_super_admin == false)
                                     join main in _db.AssociateMains
                                     on master.id equals main.associate_id
                                     select new AssociateVM
                                     {
                                         associate_id = master.id,
                                         associate_code = master.associate_code,
                                         associate_name = master.associate_name,
                                         designation = _db.Designations.FirstOrDefault(x => x.id == main.designation_id).designationtext,
                                         department = _db.Departments.FirstOrDefault(x => x.id == main.department_id).departmenttext,

                                         contact = main.mobileno,
                                         DOJ = master.application_date,
                                     }).OrderBy(x => x.associate_name).ToList();
            if (StartDate != null)
            {
                dailyActivityReportModels = dailyServices.GetAllDailyReportDetails().Where(x => x.reportingdate == StartDate && x.is_authorized == true && x.isactive == true).ToList();
                foreach (AssociateVM CurrentAssocaite in model.AssociateVMList)
                {
                    DailyActivityReportModel CurrentDarModel = new DailyActivityReportModel();
                    DailyActivityReportModel DAR = dailyActivityReportModels.FirstOrDefault(x => x.associate_id == CurrentAssocaite.associate_id);
                    CurrentDarModel.reportingdate =StartDate;
                    CurrentDarModel.createdbyname = CurrentAssocaite.associate_name;
                    if (DAR != null)
                    {
                        CurrentDarModel.id = DAR.id;
                        CurrentDarModel.totalhour = DAR.totalhour;
                        CurrentDarModel.is_authorized = DAR.is_authorized;
                    }
                    else
                    {
                        CurrentDarModel.id = 0;
                        CurrentDarModel.totalhour = "0";
                        CurrentDarModel.is_authorized = false;
                    }
                    model.DailyActivityReportModelList.Add(CurrentDarModel);
                }
                model.DailyActivityReportModelList.OrderBy(x => x.reportingdate);
            }

            return View(model);
        }
    }
}