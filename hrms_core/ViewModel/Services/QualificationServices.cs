﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class QualificationServices: IQualificationServices
    {
        private readonly ApplicationDbContext _db;
        public QualificationServices(ApplicationDbContext options) => _db = options;
        /// <summary>
        /// Get the Qualification Data,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public List<QualificationModel> GetQualification()
        {
            QualificationModel model = new QualificationModel();
            try
            {
                model.QualificationModelList = _db.Qualifications
                    .Select(x => new QualificationModel
                    {
                        id = x.id,
                        qualificationtext=x.qualificationtext,
                        isactive = x.isactive,
                        createdon = x.createdon,
                        createdby = x.createdby,
                        updatedon = x.updatedon,
                        updatedby = x.updatedby,
                        createdbyname=x.createdbyname,
                        updatedbyname=x.updatedbyname,
                    }).OrderByDescending(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.QualificationModelList;
        }

        /// <summary>
        /// Get the Qualification Data Only Active,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public List<QualificationModel> GetQualification_Active()
        {
            QualificationModel model = new QualificationModel();
            try
            {
                model.QualificationModelList = _db.Qualifications.Where(x=>x.isactive==true)
                    .Select(x => new QualificationModel
                    {
                        id = x.id,
                        qualificationtext = x.qualificationtext,
                        isactive = x.isactive,
                        createdon = x.createdon,
                        createdby = x.createdby,
                        updatedon = x.updatedon,
                        updatedby = x.updatedby,
                        createdbyname = x.createdbyname,
                        updatedbyname = x.updatedbyname,
                    }).OrderBy(x => x.qualificationtext).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.QualificationModelList;
        }

        public string SaveQualification(QualificationModel model)
        {
            Qualification qualification = new Qualification();
            string Result = string.Empty;
            try
            {
                //Check for Duplicate Record
                bool bool_sameNameExists = _db.Qualifications.Any(x => x.qualificationtext.ToLower().Trim() == model.qualificationtext.ToLower().Trim());
                if (bool_sameNameExists)
                {
                    Result = MessageHelper.qualification_duplicate;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            qualification.qualificationtext = model.qualificationtext;
                            qualification.isactive = true;
                            qualification.createdon = DateTime.Now;
                            qualification.updatedon = DateTime.Now;
                            qualification.createdby = model.createdby;
                            qualification.updatedby = model.updatedby;
                            qualification.createdbyname = model.createdbyname;
                            qualification.updatedbyname = model.updatedbyname;
                            _db.Qualifications.Add(qualification);
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.qualification_addsuccess;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.qualification_adderror;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.qualification_adderror;
            }
            return Result;
        }

        public string UpdateQualification(QualificationModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Qualifications.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    //Check for Duplicate Record
                    bool bool_sameNameExists = _db.Qualifications.Any(x => x.qualificationtext.ToLower().Trim() == model.qualificationtext.ToLower().Trim() && x.id != model.id);
                    if (bool_sameNameExists)
                    {
                        Result = MessageHelper.qualification_duplicate;
                    }
                    else
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            try
                            {
                                data.qualificationtext = model.qualificationtext;
                                data.isactive = true;
                                data.updatedon = DateTime.Now;
                                data.createdon = model.createdon;
                                data.updatedby = model.updatedby;
                                data.createdby = model.createdby;
                                data.createdbyname = model.createdbyname;
                                data.updatedbyname = model.updatedbyname;
                                _db.SaveChanges();
                                scope.Complete();
                                Result = MessageHelper.qualification_addsuccess;
                            }
                            catch (Exception ex)
                            {
                                scope.Dispose();
                                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                Result = MessageHelper.qualification_adderror;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.qualification_adderror;
            }
            return Result;
        }

        public string DisableQualification(QualificationModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Qualifications.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = false;
                            data.updatedon = DateTime.Now;
                            data.createdby = model.createdby;
                            data.updatedby = model.updatedby;
                            data.createdbyname = model.createdbyname;
                            data.updatedbyname = model.updatedbyname;
                            data.createdon = model.createdon;
                            data.updatedon = model.updatedon;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.qualification_Disable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }

       
        public string EnableQualification(QualificationModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Qualifications.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = true;
                            data.updatedon = DateTime.Now;
                            data.updatedby = 1;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.qualification_Enable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }
        public QualificationModel GetQualificationById(int ID)
        {
            QualificationModel obj_qualification = new QualificationModel();
            try
            {
                var Data = _db.Qualifications.FirstOrDefault(x => x.id == ID);
                if (Data != null)
                {
                    obj_qualification.id = Data.id;
                    obj_qualification.qualificationtext = Data.qualificationtext;
                    obj_qualification.isactive = true;
                    obj_qualification.createdby = Data.createdby;
                    obj_qualification.updatedby = Data.updatedby;
                    obj_qualification.createdbyname = Data.createdbyname;
                    obj_qualification.updatedbyname = Data.updatedbyname;
                    obj_qualification.createdon = Data.createdon;
                    obj_qualification.updatedon = Data.updatedon;
                }
                return obj_qualification;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return null;
            }

        }
    }
}
