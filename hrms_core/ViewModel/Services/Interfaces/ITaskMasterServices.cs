﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    interface ITaskMasterServices
    {
        /// <summary>
        /// To get a Task's data
        /// Created by:- Yashashwini
        /// Date:- 01/06/2018
        /// </summary>
        /// <returns></returns>
        List<TaskMasterModel> GetTask(int AssociateId);
        List<TaskMasterModel> GetTask_ByProjectID(int ProjectID);

        /// <summary>
        /// To Update a task
        /// Created by:- Yashashwini
        /// Date:- 01/06/2018
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        string UpdateTask(TaskMasterModel model);

        /// <summary>
        /// To save a task
        /// Created by:- Yashashwini
        /// Date:- 01/06/2018
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        string SaveTask(TaskMasterModel model);

        /// <summary>
        ///  To Disable a task 
        /// Created by:- Yashashwini
        /// Date:- 01/06/2018
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        string DisableTask(TaskMasterModel model);

        /// <summary>
        /// To Enable a task again
        /// Created by:- Yashashwini
        /// Date:- 01/06/2018
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        string EnableTask(TaskMasterModel model);
    }
   

} 
