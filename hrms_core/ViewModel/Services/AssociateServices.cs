﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class AssociateServices : IAssociateServices
    {
        private readonly ApplicationDbContext _db;
        private IConfigurationServices configurationServices => new ConfigurationServices(_db);
        private ILoginServices loginServices => new LoginServices(_db);
        public AssociateServices(ApplicationDbContext options) => _db = options;

        /// <summary>
        /// Get the AssociateMaster Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        public List<AssociateMasterModel> GetAssociateList()
        {
            AssociateMasterModel model = new AssociateMasterModel();
            try
            {
                model.AssociateMasterModelList = _db.AssociateMasters.Where(x => x.isactive == true)
                .Select(x => new AssociateMasterModel
                {
                    id = x.id,
                    associate_name = x.associate_name,
                    applicable_to = x.applicable_to,
                    application_date = x.application_date,
                    associate_code = x.associate_code,
                    createdby = x.createdby,
                    createdon = x.createdon,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    isactive = x.isactive
                }).OrderByDescending(m => m.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.AssociateMasterModelList;
        }

        public List<AssociateMasterModel> GetAssociateInactiveList()
        {
            AssociateMasterModel model = new AssociateMasterModel();
            try
            {
                model.AssociateMasterModelList = _db.AssociateMasters.Where(x => x.isactive == false)
                .Select(x => new AssociateMasterModel
                {
                    id = x.id,
                    associate_name = x.associate_name,
                    applicable_to = x.applicable_to,
                    application_date = x.application_date,
                    associate_code = x.associate_code,
                    createdby = x.createdby,
                    createdon = x.createdon,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    isactive = x.isactive
                }).OrderByDescending(m => m.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.AssociateMasterModelList;
        }

        public List<AssociateMainModel> GetAssociateMainList()
        {
            AssociateMainModel model = new AssociateMainModel();
            try
            {
                model.AssociateMainModelList = _db.AssociateMains.Where(x => x.isactive == true)
                .Select(x => new AssociateMainModel
                {
                    id = x.id,
                    associate_id = x.associate_id,
                    reportingperson_id = x.reportingperson_id,
                    mobileno = x.mobileno,
                    account_no = x.account_no,
                    applicable_leave = x.applicable_leave,
                    bankname = x.bankname,
                    createdbyname = x.createdbyname,
                    department_id = x.department_id,
                    designation_id = x.designation_id,
                    doc = x.doc == null ? default(DateTime) : x.doc,
                    esi_no = x.esi_no,
                    extensionno = x.extensionno,
                    is_hod = x.is_hod,
                    ifsc = x.ifsc,
                    official_email = x.official_email,
                    shiftid = x.shiftid,
                    shift_type = x.shift_type,
                    updatedbyname = x.updatedbyname,
                    qualification_id = x.qualification_id,
                    role_id = x.role_id,
                    personal_email = x.personal_email,
                    createdby = x.createdby,
                    createdon = x.createdon,
                    pf_no = x.pf_no,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    isactive = x.isactive
                }).OrderByDescending(m => m.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.AssociateMainModelList;
        }

        /// <summary>
        /// Get the AssociateMaster Data,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public List<AssociateMasterModel> GetAssociateList_Active()
        {
            AssociateMasterModel model = new AssociateMasterModel();
            try
            {
                model.AssociateMasterModelList = _db.AssociateMasters.Where(x => x.isactive == true)
                .Select(x => new AssociateMasterModel
                {
                    id = x.id,
                    associate_name = x.associate_name,
                    applicable_to = x.applicable_to,
                    application_date = x.application_date,
                    associate_code = x.associate_code,
                    createdby = x.createdby,
                    createdon = x.createdon,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    isactive = x.isactive
                }).OrderBy(m => m.associate_name).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.AssociateMasterModelList;
        }
        public List<AssociatePersonalModel> GetAssociatePersonalList_Active()
        {
            AssociatePersonalModel model = new AssociatePersonalModel();
            try
            {
                model.AssociatePersonalModelList = _db.AssociatePersonals.Where(x => x.isactive == true)
                .Select(x => new AssociatePersonalModel
                {
                    id = x.id,
                    associate_id = x.associate_id,
                    pan_card = x.pan_card,
                    anniversary = x.anniversary,
                    bloodgroup = x.bloodgroup,
                    dob = x.dob,
                    father_husband_name = x.father_husband_name,
                    mothername = x.mothername,
                    permanent_address = x.permanent_address,
                    permanent_city_id = x.permanent_city_id,
                    permanent_country_id = x.permanent_country_id,
                    permanent_state_id = x.permanent_state_id,
                    present_address = x.present_address,
                    present_city_id = x.present_city_id,
                    present_state_id = x.present_state_id,
                    present_country_id = x.present_country_id,
                    maritalstatus = x.maritalstatus,
                    adhar_card_no = x.adhar_card_no,
                    gender = x.gender,
                    createdby = x.createdby,
                    createdon = x.createdon,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    isactive = x.isactive
                }).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.AssociatePersonalModelList;
        }

        public List<AssociateAdditionalInfoModel> GetAssociateInfoList_Active()
        {
            AssociateAdditionalInfoModel model = new AssociateAdditionalInfoModel();
            try
            {
                model.AssociateAdditionalInfoModelList = _db.AssociateAdditionalInfos.ToList().Where(x => x.isactive == true)
                .Select(x => new AssociateAdditionalInfoModel
                {
                    id = x.id,
                    mail_to_hod = x.mail_to_hod,
                    reporting_person_id = x.reporting_person_id,
                    remark = x.remark,
                    createdon = x.createdon,
                    updatedbyname = x.updatedbyname,
                    createdby = x.createdby,
                    createdbyname = x.createdbyname,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    isactive = x.isactive
                }).OrderBy(m => m.associate_id).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.AssociateAdditionalInfoModelList;
        }

        /// <summary>
        /// Save the Associate Data,
        /// Created By : Bilal
        ///  Update By : Kritika
        ///On Date: 12/06/2018
        /// </summary>
        /// <returns></returns>
        public int SaveAssociateData(AssociateVM model)
        {
            string Result = string.Empty;
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    #region Master 

                    AssociateMaster associateMaster = new AssociateMaster();
                    {
                        associateMaster.associate_name = model.associateMaster.associate_name;
                        associateMaster.associate_code = GetAssociateCode();
                        associateMaster.application_date = model.associateMaster.application_date;
                        associateMaster.applicable_to = model.associateMaster.applicable_to;
                        associateMaster.isactive = true;
                        associateMaster.createdon = DateTime.Now;
                        associateMaster.updatedon = DateTime.Now;
                        associateMaster.createdby = model.createdby;
                        associateMaster.updatedby = model.updatedby;
                        associateMaster.createdbyname = model.createdbyname;
                        associateMaster.updatedbyname = model.updatedbyname;
                    }
                    _db.AssociateMasters.Add(associateMaster);
                    _db.SaveChanges();
                    #endregion
                    int associate_id = associateMaster.id;

                    #region Personal
                    AssociatePersonal associatePersonal = new AssociatePersonal();
                    {
                        associatePersonal.associate_id = associate_id;
                        associatePersonal.pan_card = model.associatePersonal.pan_card;
                        associatePersonal.anniversary = model.associatePersonal.anniversary;
                        associatePersonal.bloodgroup = model.associatePersonal.bloodgroup;
                        associatePersonal.adhar_card_no = model.associatePersonal.adhar_card_no;
                        associatePersonal.dob = model.associatePersonal.dob;
                        associatePersonal.father_husband_name = model.associatePersonal.father_husband_name;
                        associatePersonal.mothername = model.associatePersonal.mothername;
                        associatePersonal.permanent_address = model.associatePersonal.permanent_address;
                        associatePersonal.permanent_city_id = model.associatePersonal.permanent_city_id;
                        associatePersonal.permanent_country_id = model.associatePersonal.permanent_country_id;
                        associatePersonal.permanent_state_id = model.associatePersonal.permanent_state_id;
                        associatePersonal.present_address = model.associatePersonal.present_address;
                        associatePersonal.present_city_id = model.associatePersonal.present_city_id;
                        associatePersonal.present_state_id = model.associatePersonal.present_state_id;
                        associatePersonal.present_country_id = model.associatePersonal.present_country_id;
                        associatePersonal.maritalstatus = model.associatePersonal.maritalstatus;
                        associatePersonal.gender = model.associatePersonal.gender;
                        associatePersonal.isactive = true;
                        associatePersonal.createdby = model.createdby;
                        associatePersonal.updatedby = model.updatedby;
                        associatePersonal.createdbyname = model.createdbyname;
                        associatePersonal.updatedbyname = model.updatedbyname;
                    }
                    _db.AssociatePersonals.Add(associatePersonal);
                    _db.SaveChanges();
                    #endregion

                    #region Main
                    AssociateMain associateMain = new AssociateMain();
                    associateMain.associate_id = associate_id;
                    associateMain.designation_id = model.associateMain.designation_id;
                    associateMain.department_id = model.associateMain.department_id;
                    if (model.associateMain.reportingperson_id == 0)
                    {
                        associateMain.reportingperson_id = associate_id;
                    }
                    else
                    {
                        associateMain.reportingperson_id = model.associateMain.reportingperson_id;
                    }
                    associateMain.applicable_leave = model.associateMain.applicable_leave;
                    associateMain.is_hod = model.associateMain.is_hod;
                    associateMain.reportingperson_id = model.associateMain.reportingperson_id;
                    associateMain.role_id = model.associateMain.role_id;
                    associateMain.extensionno = model.associateMain.extensionno;
                    associateMain.mobileno = model.associateMain.mobileno;
                    associateMain.official_email = model.associateMain.official_email;
                    associateMain.personal_email = model.associateMain.personal_email;
                    associateMain.bankname = model.associateMain.bankname;
                    associateMain.account_no = model.associateMain.account_no;
                    associateMain.ifsc = model.associateMain.ifsc;
                    associateMain.pf_no = model.associateMain.pf_no;
                    associateMain.doc = model.associateMain.doc;
                    associateMain.esi_no = model.associateMain.esi_no;
                    associateMain.qualification_id = model.associateMain.qualification_id;
                    associateMain.createdon = DateTime.Now;
                    associateMain.updatedon = DateTime.Now;
                    associateMain.createdby = model.createdby;
                    associateMain.updatedby = model.updatedby;
                    associateMain.createdbyname = model.createdbyname;
                    associateMain.updatedbyname = model.updatedbyname;
                    associateMain.isactive = true;
                    _db.AssociateMains.Add(associateMain);
                    _db.SaveChanges();
                    #endregion

                    #region AssociateQualification
                    if (model.AssociateQualificationModelList != null && model.AssociateQualificationModelList.Count() != 0)
                    {
                        foreach (var item in model.AssociateQualificationModelList)
                        {
                            if (item != null)
                            {
                                AssociateQualification associateQualification = new AssociateQualification();
                                associateQualification.associate_id = associate_id;
                                associateQualification.qualification_id = item.qualification_id;
                                associateQualification.qualification_year = item.qualification_year;
                                associateQualification.institute_name = item.institute_name;
                                associateQualification.percentage = item.percentage;
                                associateQualification.isactive = true;
                                associateQualification.createdon = DateTime.Now;
                                associateQualification.updatedon = DateTime.Now;
                                associateQualification.createdby = model.createdby;
                                associateQualification.updatedby = model.updatedby;
                                associateQualification.createdbyname = model.createdbyname;
                                associateQualification.updatedbyname = model.updatedbyname;
                                _db.AssociateQualifications.Add(associateQualification);
                                _db.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    #region AssociateSkillMaster
                    if (model.AssociateSkillModelList != null && model.AssociateSkillModelList.Count() != 0)
                    {
                        foreach (var item in model.AssociateSkillModelList)
                        {
                            if (item != null)
                            {
                                AssociateSkillModel associateSkill = new AssociateSkillModel();
                                {
                                    associateSkill.associate_id = associate_id;
                                    associateSkill.skillcode = item.skillcode;
                                    associateSkill.skill_description = item.skill_description;
                                    associateSkill.remarks = item.remarks;
                                    associateSkill.isactive = true;
                                    associateSkill.createdon = DateTime.Now;
                                    associateSkill.updatedon = DateTime.Now;
                                    associateSkill.createdby = model.createdby;
                                    associateSkill.updatedby = model.updatedby;
                                    associateSkill.createdbyname = model.createdbyname;
                                    associateSkill.updatedbyname = model.updatedbyname;
                                    _db.AssociateSkills.Add(associateSkill);
                                    _db.SaveChanges();

                                }
                            }
                        }
                    }
                    #endregion

                    #region AssociateExperienceMaster
                    if (model.AssociateExperienceModelList != null && model.AssociateExperienceModelList.Count() != 0)
                    {
                        foreach (var item in model.AssociateExperienceModelList)
                        {
                            if (item != null)
                            {
                                AssociateExperienceModel associateExperience = new AssociateExperienceModel();
                                {
                                    associateExperience.associate_id = associate_id;
                                    associateExperience.previous_company = item.previous_company;
                                    associateExperience.gross_salary = item.gross_salary;
                                    associateExperience.designation_id = item.designation_id;
                                    associateExperience.gross_salary = item.gross_salary;
                                    associateExperience.carryhome_salary = item.carryhome_salary;
                                    associateExperience.ctc = item.ctc;
                                    associateExperience.extra_allowance = item.extra_allowance;
                                    associateExperience.to_date = item.to_date;
                                    associateExperience.from_date = item.from_date;
                                    associateExperience.isactive = true;
                                    associateExperience.createdon = DateTime.Now;
                                    associateExperience.updatedon = DateTime.Now;
                                    associateExperience.createdby = model.createdby;
                                    associateExperience.updatedby = model.updatedby;
                                    associateExperience.createdbyname = model.createdbyname;
                                    associateExperience.updatedbyname = model.updatedbyname;
                                    _db.AssociateExperiences.Add(associateExperience);
                                    _db.SaveChanges();
                                }
                            }
                        }
                    }
                    #endregion


                    #region AssociateAdditionalInfo
                    if (model.AssociateAdditionalInfoModelList != null && model.AssociateAdditionalInfoModelList.Count() != 0)
                    {
                        foreach (var item in model.AssociateAdditionalInfoModelList)
                        {
                            if (item != null)
                            {
                                AssociateAdditionalInfoModel associateAdditionalInfo = new AssociateAdditionalInfoModel();
                                {

                                    associateAdditionalInfo.associate_id = associate_id;
                                    associateAdditionalInfo.remark = model.Remarks;
                                    if (model.mail_Hod == 1)
                                    {
                                        associateAdditionalInfo.mail_to_hod = true;
                                    }
                                    else
                                    {
                                        associateAdditionalInfo.mail_to_hod = false;
                                    }
                                    //associateAdditionalInfo.remark = item.remark;
                                    associateAdditionalInfo.reporting_person_id = item.reporting_person_id;
                                    associateAdditionalInfo.isactive = true;
                                    associateAdditionalInfo.createdon = DateTime.Now;
                                    associateAdditionalInfo.updatedon = DateTime.Now;
                                    associateAdditionalInfo.createdby = model.createdby;
                                    associateAdditionalInfo.updatedby = model.updatedby;
                                    associateAdditionalInfo.createdbyname = model.createdbyname;
                                    associateAdditionalInfo.updatedbyname = model.updatedbyname;

                                    _db.AssociateAdditionalInfos.Add(associateAdditionalInfo);
                                    _db.SaveChanges();
                                }

                            }
                        }
                    }
                    #endregion
                    #region User Save Data
                    UserModel user = new UserModel();
                    user.username = associateMaster.associate_code;
                    user.userpass = GetDefaultPassword();
                    user.createdby = model.createdby;
                    user.createdbyname = model.createdbyname;
                    user.createdon = DateTime.Now;
                    user.updatedby = model.updatedby;
                    user.updatedbyname = model.updatedbyname;
                    user.updatedon = DateTime.Now;
                    user.isactive = true;
                    _db.Users.Add(user);
                    _db.SaveChanges();
                    #endregion
                    scope.Complete();
                    Result = MessageHelper.Associate_AddSuccess;
                    return associate_id;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                    Result = MessageHelper.Associate_AddError;
                    return 0;
                }
            }
        }

        /// <summary>
        /// Save the Associate Data,
        /// Created By : Kritika
        ///On Date: 12/06/2018
        /// </summary>
        /// <returns></returns>
        public string UpdateAssociateData(AssociateVM model)
        {
            string Result = string.Empty;
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    try
                    {
                        #region Master 
                        var master_data = _db.AssociateMasters.FirstOrDefault(x => x.id == model.associateMaster.id);
                        if (master_data != null)
                        {
                            master_data.associate_name = model.associateMaster.associate_name;
                            master_data.id = model.associateMaster.id;
                            master_data.application_date = model.associateMaster.application_date;
                            master_data.applicable_to = model.associateMaster.applicable_to;
                            master_data.isactive = true;
                            master_data.createdon = DateTime.Now;
                            master_data.updatedon = DateTime.Now;
                            master_data.createdby = model.createdby;
                            master_data.updatedby = model.updatedby;
                            master_data.createdbyname = model.createdbyname;
                            master_data.updatedbyname = model.updatedbyname;
                            master_data.is_super_admin = false;
                            _db.SaveChanges();
                        }

                        #endregion

                        #region Personal
                        var personal_data = _db.AssociatePersonals.FirstOrDefault(x => x.associate_id == master_data.id);
                        if (personal_data != null)
                        {
                            personal_data.associate_id = model.associateMaster.id;
                            personal_data.pan_card = model.associatePersonal.pan_card;
                            personal_data.anniversary = model.associatePersonal.anniversary;
                            personal_data.adhar_card_no = model.associatePersonal.adhar_card_no;
                            personal_data.bloodgroup = model.associatePersonal.bloodgroup;
                            personal_data.dob = model.associatePersonal.dob;
                            personal_data.father_husband_name = model.associatePersonal.father_husband_name;
                            personal_data.mothername = model.associatePersonal.mothername;
                            personal_data.permanent_address = model.associatePersonal.permanent_address;
                            personal_data.permanent_city_id = model.associatePersonal.permanent_city_id;
                            personal_data.permanent_country_id = model.associatePersonal.permanent_country_id;
                            personal_data.permanent_state_id = model.associatePersonal.permanent_state_id;
                            personal_data.present_address = model.associatePersonal.present_address;
                            personal_data.present_city_id = model.associatePersonal.present_city_id;
                            personal_data.present_state_id = model.associatePersonal.present_state_id;
                            personal_data.present_country_id = model.associatePersonal.present_country_id;
                            personal_data.maritalstatus = model.associatePersonal.maritalstatus;
                            personal_data.gender = model.associatePersonal.gender;
                            personal_data.isactive = true;
                            personal_data.createdon = DateTime.Now;
                            personal_data.updatedon = DateTime.Now;
                            personal_data.createdby = model.createdby;
                            personal_data.updatedby = model.updatedby;
                            personal_data.createdbyname = model.createdbyname;
                            personal_data.updatedbyname = model.updatedbyname;
                            _db.SaveChanges();
                        }
                        else
                        {
                            AssociatePersonal associatePersonal = new AssociatePersonal();
                            associatePersonal.pan_card = model.associatePersonal.pan_card;
                            associatePersonal.anniversary = model.associatePersonal.anniversary;
                            associatePersonal.adhar_card_no = model.associatePersonal.adhar_card_no;
                            associatePersonal.bloodgroup = model.associatePersonal.bloodgroup;
                            associatePersonal.associate_id = model.associateMaster.id;
                            associatePersonal.dob = model.associatePersonal.dob;
                            associatePersonal.father_husband_name = model.associatePersonal.father_husband_name;
                            associatePersonal.mothername = model.associatePersonal.mothername;
                            associatePersonal.permanent_address = model.associatePersonal.permanent_address;
                            associatePersonal.permanent_city_id = model.associatePersonal.permanent_city_id;
                            associatePersonal.permanent_country_id = model.associatePersonal.permanent_country_id;
                            associatePersonal.permanent_state_id = model.associatePersonal.permanent_state_id;
                            associatePersonal.present_address = model.associatePersonal.present_address;
                            associatePersonal.present_city_id = model.associatePersonal.present_city_id;
                            associatePersonal.present_state_id = model.associatePersonal.present_state_id;
                            associatePersonal.present_country_id = model.associatePersonal.present_country_id;
                            associatePersonal.maritalstatus = model.associatePersonal.maritalstatus;
                            associatePersonal.gender = model.associatePersonal.gender;
                            associatePersonal.isactive = true;
                            associatePersonal.createdon = DateTime.Now;
                            associatePersonal.updatedon = DateTime.Now;
                            associatePersonal.createdby = model.createdby;
                            associatePersonal.updatedby = model.updatedby;
                            associatePersonal.createdbyname = model.createdbyname;
                            associatePersonal.updatedbyname = model.updatedbyname;
                            _db.AssociatePersonals.Add(associatePersonal);
                            _db.SaveChanges();
                        }
                        #endregion

                        #region Main
                        var main_data = _db.AssociateMains.FirstOrDefault(x => x.associate_id == master_data.id);
                        if (main_data != null)
                        {
                            main_data.associate_id = model.associateMaster.id;
                            main_data.designation_id = model.associateMain.designation_id;
                            main_data.department_id = model.associateMain.department_id;
                            main_data.reportingperson_id = model.associateMain.reportingperson_id;
                            main_data.applicable_leave = model.associateMain.applicable_leave;
                            main_data.is_hod = model.associateMain.is_hod;
                            main_data.role_id = model.associateMain.role_id;
                            main_data.extensionno = model.associateMain.extensionno;
                            main_data.mobileno = model.associateMain.mobileno;
                            main_data.official_email = model.associateMain.official_email;
                            main_data.personal_email = model.associateMain.personal_email;
                            main_data.bankname = model.associateMain.bankname;
                            main_data.account_no = model.associateMain.account_no;
                            main_data.ifsc = model.associateMain.ifsc;
                            main_data.pf_no = model.associateMain.pf_no;
                            main_data.esi_no = model.associateMain.esi_no;
                            main_data.qualification_id = model.associateMain.qualification_id;
                            main_data.isactive = true;
                            main_data.doc = model.associateMain.doc;
                            main_data.createdon = DateTime.Now;
                            main_data.updatedon = DateTime.Now;
                            main_data.createdby = model.createdby;
                            main_data.updatedby = model.updatedby;
                            main_data.createdbyname = model.createdbyname;
                            main_data.updatedbyname = model.updatedbyname;
                            _db.SaveChanges();
                        }
                        else
                        {
                            AssociateMain associateMain = new AssociateMain();
                            associateMain.associate_id = model.associateMaster.id;
                            associateMain.designation_id = model.associateMain.designation_id;
                            associateMain.department_id = model.associateMain.department_id;
                            associateMain.reportingperson_id = model.associateMain.reportingperson_id;
                            associateMain.applicable_leave = model.associateMain.applicable_leave;
                            associateMain.is_hod = model.associateMain.is_hod;
                            associateMain.role_id = model.associateMain.role_id;
                            associateMain.extensionno = model.associateMain.extensionno;
                            associateMain.mobileno = model.associateMain.mobileno;
                            associateMain.official_email = model.associateMain.official_email;
                            associateMain.personal_email = model.associateMain.personal_email;
                            associateMain.bankname = model.associateMain.bankname;
                            associateMain.account_no = model.associateMain.account_no;
                            associateMain.ifsc = model.associateMain.ifsc;
                            associateMain.doc = model.associateMain.doc;
                            associateMain.pf_no = model.associateMain.pf_no;
                            associateMain.esi_no = model.associateMain.esi_no;
                            associateMain.qualification_id = model.associateMain.qualification_id;
                            associateMain.isactive = true;
                            associateMain.createdon = DateTime.Now;
                            associateMain.updatedon = DateTime.Now;
                            associateMain.createdby = model.createdby;
                            associateMain.updatedby = model.updatedby;
                            associateMain.createdbyname = model.createdbyname;
                            associateMain.updatedbyname = model.updatedbyname;
                            _db.AssociateMains.Add(associateMain);
                            _db.SaveChanges();

                        }
                        #endregion

                        #region AssociateQualification
                        //if (model.AssociateQualificationModelList != null && model.AssociateQualificationModelList.Count != 0)
                        //{
                        var qualification_data = _db.AssociateQualifications.Where(x => x.associate_id == master_data.id);
                        _db.AssociateQualifications.RemoveRange(qualification_data);
                        _db.SaveChanges();
                        foreach (var item in model.AssociateQualificationModelList)
                        {
                            if (item != null)
                            {
                                AssociateQualification associateQualification = new AssociateQualification();
                                associateQualification.associate_id = master_data.id;
                                associateQualification.qualification_id = item.qualification_id;
                                associateQualification.qualification_year = item.qualification_year;
                                associateQualification.institute_name = item.institute_name;
                                associateQualification.percentage = item.percentage;
                                associateQualification.isactive = true;
                                associateQualification.createdon = DateTime.Now;
                                associateQualification.updatedon = DateTime.Now;
                                associateQualification.createdby = model.createdby;
                                associateQualification.updatedby = model.updatedby;
                                associateQualification.createdbyname = model.createdbyname;
                                associateQualification.updatedbyname = model.updatedbyname;
                                _db.AssociateQualifications.Add(associateQualification);
                                _db.SaveChanges();
                            }
                        }
                        //}
                        #endregion

                        #region AssociateSkillMaster
                        //if (model.AssociateSkillModelList != null && model.AssociateSkillModelList.Count != 0)
                        //{
                        var skill_data = _db.AssociateSkills.Where(x => x.associate_id == master_data.id);
                        _db.AssociateSkills.RemoveRange(skill_data);
                        _db.SaveChanges();
                        foreach (var item in model.AssociateSkillModelList)
                        {
                            if (item != null)
                            {
                                AssociateSkill associateSkill = new AssociateSkill();
                                associateSkill.associate_id = master_data.id;
                                associateSkill.skillcode = item.skillcode;
                                associateSkill.skill_description = item.skill_description;
                                associateSkill.remarks = item.remarks;
                                associateSkill.isactive = true;
                                associateSkill.createdon = DateTime.Now;
                                associateSkill.updatedon = DateTime.Now;
                                associateSkill.createdby = model.createdby;
                                associateSkill.updatedby = model.updatedby;
                                associateSkill.createdbyname = model.createdbyname;
                                associateSkill.updatedbyname = model.updatedbyname;
                                _db.AssociateSkills.Add(associateSkill);
                                _db.SaveChanges();
                            }
                        }
                        //}
                        #endregion

                        #region AssociateExperienceMaster
                        //if (model.AssociateExperienceModelList != null && model.AssociateExperienceModelList.Count != 0)
                        //  {

                        var experience_data = _db.AssociateExperiences.Where(x => x.associate_id == master_data.id);
                        _db.AssociateExperiences.RemoveRange(experience_data);
                        _db.SaveChanges();
                        foreach (var item in model.AssociateExperienceModelList)
                        {
                            if (item != null)
                            {
                                AssociateExperience associateExperience = new AssociateExperience();
                                associateExperience.associate_id = master_data.id;
                                associateExperience.previous_company = item.previous_company;
                                associateExperience.gross_salary = item.gross_salary;
                                associateExperience.designation_id = item.designation_id;
                                associateExperience.carryhome_salary = item.carryhome_salary;
                                associateExperience.ctc = item.ctc;
                                associateExperience.extra_allowance = item.extra_allowance;
                                associateExperience.to_date = item.to_date;
                                associateExperience.from_date = item.from_date;
                                associateExperience.isactive = true;
                                associateExperience.createdon = DateTime.Now;
                                associateExperience.updatedon = DateTime.Now;
                                associateExperience.createdby = model.createdby;
                                associateExperience.updatedby = model.updatedby;
                                associateExperience.createdbyname = model.createdbyname;
                                associateExperience.updatedbyname = model.updatedbyname;
                                _db.AssociateExperiences.Add(associateExperience);
                                _db.SaveChanges();
                            }
                        }
                        //}
                        #endregion

                        //#region AssociateAdditionalInfo
                        //if (model.AssociateAdditionalInfoModelList != null && model.AssociateAdditionalInfoModelList.Count != 0)
                        //{
                        var additional_data = _db.AssociateAdditionalInfos.Where(x => x.associate_id == master_data.id);
                        _db.AssociateAdditionalInfos.RemoveRange(additional_data);
                        _db.SaveChanges();
                        foreach (var item in model.AssociateAdditionalInfoModelList)
                        {
                            if (item != null)
                            {
                                AssociateAdditionalInfo associateAdditionalInfo =
                                 new AssociateAdditionalInfo();
                                associateAdditionalInfo.associate_id = master_data.id;
                                if (model.mail_Hod == 1)
                                {
                                    associateAdditionalInfo.mail_to_hod = true;
                                }
                                else
                                {
                                    associateAdditionalInfo.mail_to_hod = false;
                                }
                                associateAdditionalInfo.remark = model.Remarks;
                                associateAdditionalInfo.reporting_person_id = item.reporting_person_id;
                                associateAdditionalInfo.isactive = true;
                                associateAdditionalInfo.createdon = DateTime.Now;
                                associateAdditionalInfo.updatedon = DateTime.Now;
                                associateAdditionalInfo.createdby = model.createdby;
                                associateAdditionalInfo.updatedby = model.updatedby;
                                associateAdditionalInfo.createdbyname = model.createdbyname;
                                associateAdditionalInfo.updatedbyname = model.updatedbyname;
                                _db.AssociateAdditionalInfos.Add(associateAdditionalInfo);
                                _db.SaveChanges();
                            }
                        }
                        //}
                        _db.SaveChanges();
                        scope.Complete();
                        scope.Dispose();
                        Result = "Associate Changes Updated Successfully!";
                    }
                    catch (Exception ex)
                    {
                        scope.Dispose();
                        CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                        Result = "Error";
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = "Error";
            }
            return Result;
        }

        /// <summary>
        /// Get the Associate Code,
        /// Created By : Bilal 
        ///On Date: 05/07/2018
        /// </summary>
        /// <returns></returns>
        public string GetAssociateCode()
        {
            string Code = string.Empty;
            try
            {
                var Data = configurationServices.GetConfigurationSettings();
                if (Data != null && Data.Count > 0)
                {
                    string Prefix = string.Empty;
                    int MaxDigit = 0;
                    Prefix = Data.FirstOrDefault().associate_prefix;
                    MaxDigit = Data.FirstOrDefault().max_numeric_character;
                    var LoginData = loginServices.GetUserData();
                    if (LoginData != null && LoginData.Count > 0)
                    {
                        var LastCode = LoginData.OrderByDescending(x => x.username).Take(1).FirstOrDefault().username;
                        string OnlyLastValue = LastCode.Substring(LastCode.Length - MaxDigit, MaxDigit);
                        int UpdatedCodeNumber = Convert.ToInt32(OnlyLastValue) + 1;
                        string Suffex = string.Empty;
                        int length = Data.FirstOrDefault().max_numeric_character - Convert.ToInt32(Convert.ToString(UpdatedCodeNumber).Length);
                        for (int i = 0; i < length; i++)
                        {
                            Suffex += '0';
                        }
                        Code = Prefix + Suffex + UpdatedCodeNumber;
                    }
                    else
                    {
                        string Suffex = string.Empty;
                        for (int i = 0; i < Data.FirstOrDefault().max_numeric_character; i++)
                        {
                            Suffex += '0';
                        }
                        Code = Prefix + Suffex;
                    }
                    CommonUtility.WriteMsgLogs("Associate Code : " + Code + "", "AssociateServices=>GetAssociateCode");
                }
                else
                {
                    CommonUtility.WriteMsgLogs("Please Configured the ConfigurationSetting Page", "AssociateServices=>GetAssociateCode");
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return Code;
        }

        /// <summary>
        /// Get the Default Password,
        /// Created By : Bilal 
        ///On Date: 06/07/2018
        /// </summary>
        /// <returns></returns>
        public string GetDefaultPassword()
        {
            string PassCode = string.Empty;
            try
            {
                Guid guid = Guid.NewGuid();
                if (guid != null)
                {
                    var _data = Convert.ToString(guid).Split('-');
                    if (_data != null)
                    {
                        PassCode = Convert.ToString(_data[0]);
                        CommonUtility.WriteMsgLogs("Default Password : " + PassCode + "", "AssociateServices=>GetDefaultPassword");
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                PassCode = string.Empty;
            }
            return CommonUtility.MD5Convertor(PassCode);
        }
        public string GetSuperAdminPassword()
        {
            string PassCode = string.Empty;
            try
            {
                Guid guid = Guid.NewGuid();
                if (guid != null)
                {
                    var _data = Convert.ToString(guid).Split('-');
                    if (_data != null)
                    {
                        PassCode = Convert.ToString(_data[0]);
                        CommonUtility.WriteMsgLogs("Default Password : " + PassCode + "", "AssociateServices=>GetDefaultPassword");
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return PassCode;
        }
        public AssociateMasterModel GetAssociateMaster(int id)
        {
            AssociateMasterModel model = new AssociateMasterModel();
            try
            {
                model = _db.AssociateMasters.Where(x => x.id == id && x.isactive == true)
                .Select(x => new AssociateMasterModel
                {
                    id = x.id,
                    associate_name = x.associate_name,
                    applicable_to = x.applicable_to,
                    application_date = x.application_date,
                    associate_code = x.associate_code,

                    createdby = x.createdby,
                    createdon = x.createdon,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    isactive = x.isactive
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model;
        }
        public AssociateMainModel GetAssociateMain(int id)
        {
            AssociateMainModel model = new AssociateMainModel();
            try
            {
                model = _db.AssociateMains.Where(x => x.associate_id == id && x.isactive == true)
                .Select(x => new AssociateMainModel
                {
                    id = x.id,
                    associate_id = x.associate_id,
                    designation_id = x.designation_id,
                    department_id = x.department_id,
                    reportingperson_id = x.reportingperson_id,
                    is_hod = x.is_hod,
                    role_id = x.role_id,
                    extensionno = x.extensionno,
                    mobileno = x.mobileno,
                    official_email = x.official_email,
                    personal_email = x.personal_email,
                    bankname = x.bankname,
                    account_no = x.account_no,
                    applicable_leave = x.applicable_leave,
                    ifsc = x.ifsc,
                    pf_no = x.pf_no,
                    esi_no = x.esi_no,
                    qualification_id = x.qualification_id,
                    createdby = x.createdby,
                    createdon = x.createdon,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    isactive = x.isactive
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model;
        }
        public AssociatePersonalModel GetAssociatePersonal(int id)
        {
            AssociatePersonalModel model = new AssociatePersonalModel();
            try
            {
                model = _db.AssociatePersonals.Where(x => x.id == id && x.isactive == true)
                .Select(x => new AssociatePersonalModel
                {
                    id = x.id,
                    associate_id = x.associate_id,
                    pan_card = x.pan_card,
                    anniversary = x.anniversary,
                    bloodgroup = x.bloodgroup,
                    dob = x.dob,
                    father_husband_name = x.father_husband_name,
                    mothername = x.mothername,
                    permanent_address = x.permanent_address,
                    permanent_city_id = x.permanent_city_id,
                    permanent_country_id = x.permanent_country_id,
                    permanent_state_id = x.permanent_state_id,
                    present_address = x.present_address,
                    present_city_id = x.present_city_id,
                    present_state_id = x.present_state_id,
                    present_country_id = x.present_country_id,
                    maritalstatus = x.maritalstatus,
                    adhar_card_no = x.adhar_card_no,
                    gender = x.gender,
                    createdby = x.createdby,
                    createdon = x.createdon,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    isactive = x.isactive
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model;
        }
        /// <summary>
        /// New Associate Mailing System 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="httpContext"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public bool SendAssociateDetails(AssociateVM model, HttpContext httpContext, IConfiguration configuration)
        {
            string str = string.Empty;
            bool result;
            var userpass = CommonUtility.GetDecryptMD5(_db.Users.FirstOrDefault(x => x.username == model.associate_code).userpass);
            model.associateMaster.associate_name = _db.AssociateMasters.FirstOrDefault(x => x.associate_code == model.associate_code).associate_name;
            var _emailData = _db.EmailConfigurations.FirstOrDefault();
            MailMessage mail = new MailMessage();
            try
            {

                if (_emailData != null)
                {
                    string FromEmail = _emailData.email_send_from;
                    string Pass = _emailData.password;

                    SmtpClient SmtpServer = new SmtpClient(_emailData.smtp);
                    mail.From = new MailAddress(FromEmail, "HR");
                    if (model.associateMain.personal_email != null)
                    {
                        mail.To.Add(model.associateMain.personal_email);
                    }
                    mail.Subject = "HRMS Credentials";
                    mail.IsBodyHtml = true;
                    mail.Body = string.Format("Hi, <b>" + model.associateMaster.associate_name + "</b><br/>Your HRMS Credentials: <br/><br/>Username: " + model.associate_code + "<br>Password: " + userpass + "");
                    SmtpServer.Host = _emailData.smtp;
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.Port = _emailData.port;
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                 System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };
                    SmtpServer.Credentials = new System.Net.NetworkCredential(FromEmail, Pass);
                    SmtpServer.EnableSsl = _emailData.ssl;
                    SmtpServer.Send(mail);
                    str = MessageHelper.EmailSent;
                    result = true;
                }
                result = false;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                result = false;
            }
            string status = string.Empty;
            if (result)
                status = "Success";
            else
                status = "Failed";
            CommonUtility.WriteMailLogs("From =>" + _emailData.email_send_from + " To=>" + mail.To + " Date=>" + DateTime.Now.ToShortDateString() + " Body=>" + mail.Body + " Status=>" + status, "SendAssociateDetails");
            return result;
        }

    }
}