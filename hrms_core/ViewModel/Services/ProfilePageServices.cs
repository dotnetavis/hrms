﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class ProfilePageServices : IProfilePageServices
    {
        private readonly ApplicationDbContext _db;
        public ProfilePageServices(ApplicationDbContext options)
        {
            _db = options;
        }

        public List<AssociateMasterModel> GetAssociate(int associate)
        {
            AssociateMasterModel model = new AssociateMasterModel();
            try
            {
                model.AssociateMasterModelList = _db.AssociateMasters.Where(x => x.id == associate)
                .Select(x => new AssociateMasterModel
                {
                    id = x.id,
                    associate_name = x.associate_name,
                    applicable_to = x.applicable_to,
                    application_date = x.application_date,
                    associate_code = x.associate_code,
                    createdby = x.createdby,
                    createdon = x.createdon,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    isactive = x.isactive
                }).OrderByDescending(m => m.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.AssociateMasterModelList;
        }

        public List<AssociateMainModel> GetAssociateMainList()
        {
            AssociateMainModel model = new AssociateMainModel();
            try
            {
                model.AssociateMainModelList = _db.AssociateMains.Where(x => x.isactive == true)
                .Select(x => new AssociateMainModel
                {
                    id = x.id,
                    associate_id = x.associate_id,
                    reportingperson_id = x.reportingperson_id,
                    mobileno = x.mobileno,
                    account_no = x.account_no,
                    applicable_leave = x.applicable_leave,
                    bankname = x.bankname,
                    createdbyname = x.createdbyname,
                    department_id = x.department_id,
                    designation_id = x.designation_id,
                    doc = x.doc == null ? default(DateTime) : x.doc,
                    esi_no = x.esi_no,
                    extensionno = x.extensionno,
                    is_hod = x.is_hod,
                    ifsc = x.ifsc,
                    official_email = x.official_email,
                    shiftid = x.shiftid,
                    shift_type = x.shift_type,
                    updatedbyname = x.updatedbyname,
                    qualification_id = x.qualification_id,
                    role_id = x.role_id,
                    personal_email = x.personal_email,
                    createdby = x.createdby,
                    createdon = x.createdon,
                    pf_no = x.pf_no,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    isactive = x.isactive
                }).OrderByDescending(m => m.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.AssociateMainModelList;
        }

        public List<AssociatePersonalModel> GetAssociatePersonalList_Active()
        {
            AssociatePersonalModel model = new AssociatePersonalModel();
            try
            {
                model.AssociatePersonalModelList = _db.AssociatePersonals.Where(x => x.isactive == true)
                .Select(x => new AssociatePersonalModel
                {
                    id = x.id,
                    associate_id = x.associate_id,
                    pan_card = x.pan_card,
                    anniversary = x.anniversary,
                    bloodgroup = x.bloodgroup,
                    dob = x.dob,
                    father_husband_name = x.father_husband_name,
                    mothername = x.mothername,
                    permanent_address = x.permanent_address,
                    permanent_city_id = x.permanent_city_id,
                    permanent_country_id = x.permanent_country_id,
                    permanent_state_id = x.permanent_state_id,
                    present_address = x.present_address,
                    present_city_id = x.present_city_id,
                    present_state_id = x.present_state_id,
                    present_country_id = x.present_country_id,
                    maritalstatus = x.maritalstatus,
                    adhar_card_no = x.adhar_card_no,
                    gender = x.gender,
                    createdby = x.createdby,
                    createdon = x.createdon,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    isactive = x.isactive
                }).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.AssociatePersonalModelList;
        }

        public string UpdateProfile(ProfilePageModel model, int associate_id)
        {
            string Result = string.Empty;
            model.associate_id = associate_id;
            //AssociateMainModel associateMain = new AssociateMainModel();
            //AssociateMasterModel associateMaster = new AssociateMasterModel();
            //AssociatePersonalModel associatePersonal = new AssociatePersonalModel();

            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    try
                    {
                        var associatePersonal = _db.AssociatePersonals.FirstOrDefault(x => x.associate_id == associate_id);
                        if (associatePersonal != null)
                        {
                            #region Personal 
                            associatePersonal.pan_card = model.associatePersonal.pan_card;
                            associatePersonal.anniversary = model.associatePersonal.anniversary;
                            associatePersonal.bloodgroup = model.associatePersonal.bloodgroup;
                            associatePersonal.dob = model.associatePersonal.dob;
                            associatePersonal.father_husband_name = model.associatePersonal.father_husband_name;
                            associatePersonal.mothername = model.associatePersonal.mothername;
                            associatePersonal.permanent_address = model.associatePersonal.permanent_address;
                            associatePersonal.permanent_city_id = model.associatePersonal.permanent_city_id;
                            associatePersonal.permanent_country_id = model.associatePersonal.permanent_country_id;
                            associatePersonal.permanent_state_id = model.associatePersonal.permanent_state_id;
                            associatePersonal.present_address = model.associatePersonal.present_address;
                            associatePersonal.present_city_id = model.associatePersonal.present_city_id;
                            associatePersonal.present_state_id = model.associatePersonal.present_state_id;
                            associatePersonal.present_country_id = model.associatePersonal.present_country_id;
                            associatePersonal.maritalstatus = model.associatePersonal.maritalstatus;
                            associatePersonal.gender = model.associatePersonal.gender;
                            associatePersonal.isactive = true;
                            associatePersonal.createdby = model.associateMain.createdby;
                            associatePersonal.updatedby = model.associateMain.updatedby;
                            associatePersonal.createdbyname = model.associateMain.createdbyname;
                            associatePersonal.createdon = DateTime.Now;
                            associatePersonal.updatedon = DateTime.Now;
                            associatePersonal.updatedbyname = model.associateMain.updatedbyname;
                            _db.SaveChanges();
                            #endregion
                        }
                        else
                        {
                            associatePersonal.pan_card = model.associatePersonal.pan_card;
                            associatePersonal.anniversary = model.associatePersonal.anniversary;
                            associatePersonal.bloodgroup = model.associatePersonal.bloodgroup;
                            associatePersonal.dob = model.associatePersonal.dob;
                            associatePersonal.father_husband_name = model.associatePersonal.father_husband_name;
                            associatePersonal.mothername = model.associatePersonal.mothername;
                            associatePersonal.permanent_address = model.associatePersonal.permanent_address;
                            associatePersonal.permanent_city_id = model.associatePersonal.permanent_city_id;
                            associatePersonal.permanent_country_id = model.associatePersonal.permanent_country_id;
                            associatePersonal.permanent_state_id = model.associatePersonal.permanent_state_id;
                            associatePersonal.present_address = model.associatePersonal.present_address;
                            associatePersonal.present_city_id = model.associatePersonal.present_city_id;
                            associatePersonal.present_state_id = model.associatePersonal.present_state_id;
                            associatePersonal.present_country_id = model.associatePersonal.present_country_id;
                            associatePersonal.maritalstatus = model.associatePersonal.maritalstatus;
                            associatePersonal.gender = model.associatePersonal.gender;
                            associatePersonal.isactive = true;
                            associatePersonal.createdby = model.associateMain.createdby;
                            associatePersonal.updatedby = model.associateMain.updatedby;
                            associatePersonal.createdbyname = model.associateMain.createdbyname;
                            associatePersonal.createdon = DateTime.Now;
                            associatePersonal.updatedon = DateTime.Now;
                            associatePersonal.updatedbyname = model.associateMain.updatedbyname;
                            _db.AssociatePersonals.Add(associatePersonal);
                            _db.SaveChanges();
                        }
                        var associateMain = _db.AssociateMains.FirstOrDefault(x => x.associate_id == associate_id);
                        if (associateMain != null)
                        {
                            #region Main
                            associateMain.associate_id = associate_id;
                            associateMain.mobileno = model.associateMain.mobileno;
                            associateMain.bankname = model.associateMain.bankname;
                            associateMain.account_no = model.associateMain.account_no;
                            associateMain.ifsc = model.associateMain.ifsc;
                            associateMain.isactive = true;
                            associateMain.createdby = model.associateMain.createdby;
                            associateMain.updatedby = model.associateMain.updatedby;
                            associateMain.createdbyname = model.associateMain.createdbyname;
                            associateMain.createdon = DateTime.Now;
                            associateMain.updatedon = DateTime.Now;
                            associateMain.updatedbyname = model.associateMain.updatedbyname;
                            _db.SaveChanges();
                            #endregion
                        }
                        #region
                        if (model.id > 0)
                        {
                            var profile_data = _db.ProfilePages.FirstOrDefault(x => x.associate_id == model.id);
                            if (profile_data != null)
                            {
                                profile_data.associate_id = model.associate_id;
                                if (!string.IsNullOrEmpty(model.image_path))
                                    profile_data.image_path = model.image_path;
                                //profile_data.image_url = model.image_url;
                                profile_data.updatedbyname = model.updatedbyname;
                                profile_data.createdbyname = model.createdbyname;
                                profile_data.isactive = true;
                                profile_data.createdon = DateTime.Now;
                                profile_data.updatedon = DateTime.Now;
                                profile_data.createdby = model.createdby;
                                profile_data.updatedby = model.updatedby;
                                _db.SaveChanges();
                            }
                            else
                            {
                                ProfilePage profile = new ProfilePage();
                                {
                                    profile.image_path = model.image_path;
                                    //profile.image_url = model.image_url;
                                    profile.associate_id = model.associate_id;
                                    profile.updatedbyname = model.updatedbyname;
                                    profile.createdbyname = model.createdbyname;
                                    profile.isactive = true;
                                    profile.createdon = DateTime.Now;
                                    profile.updatedon = DateTime.Now;
                                    profile.createdby = model.createdby;
                                    profile.updatedby = model.updatedby;
                                    _db.ProfilePages.Add(profile);
                                    _db.SaveChanges();
                                }
                            }
                            scope.Complete();
                            Result = MessageHelper.ProfilePage_AddSuccess;
                        }

                    }
                    catch (Exception ex)
                    {
                        scope.Dispose();
                        CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                        Result = MessageHelper.ProfilePage_AddError;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.ProfilePage_AddError;
            }
            return Result;
        }
    }
}
#endregion