﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface ILoginServices
    {
        List<LoginModel> doLogin(LoginModel model);

        /// <summary>
        /// Get the User Data,
        /// Created By : Bilal 
        ///On Date: 05/07/2018
        /// </summary>
        /// <returns></returns>
        List<LoginModel> GetUserData();

        /// <summary>
        /// Get the All User Data,
        /// Created By : Bilal 
        ///On Date: 10/07/2018
        /// </summary>
        /// <returns></returns>
        List<SessionModel> GetAllSessionData(string UserName, bool IsSuperAdmin);

        ///// <summary>
        ///// Save the Login Details,
        ///// Created By : Bilal 
        /////On Date: 20/08/2018
        ///// </summary>
        ///// <returns></returns>
        //void SaveLoginDetails(LoginHistoryVM loginHistory);
    }
}
