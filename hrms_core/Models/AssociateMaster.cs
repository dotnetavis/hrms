﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class AssociateMaster : CommonAttributeModel
    {
        [Key]
        public int id { get; set; }   //primary key
        public bool is_super_admin { get; set; }
        public string associate_code { get; set; }
        [Required(ErrorMessage = "Enter Associate Name")]
        public string associate_name { get; set; }
        public DateTime application_date { get; set; }
        public DateTime? applicable_to { get; set; }
        [ForeignKey("associate_id")]
        public virtual ICollection<AssociateQualification> AssociateQualificationDetails { get; set; }
        [ForeignKey("associate_id")]
        public virtual ICollection<AssociateAdditionalInfo> AssociateAdditionalInfoDetails { get; set; }
        [ForeignKey("associate_id")]
        public virtual ICollection<HeaderAssign> HeaderAssignDetails { get; set; }
        [ForeignKey("associate_id")]
        public virtual ICollection<AssociateMain> AssociateMains { get; set; }
        [ForeignKey("associate_id")]
        public virtual ICollection<AssociatePersonal> AssociatePersonals { get; set; }
        [ForeignKey("associate_id")]
        public virtual ICollection<AssociateShift> AssociateShifts { get; set; }
        [ForeignKey("associate_id")]
        public virtual ICollection<AssociateExperience> AssociateExperiences { get; set; }
        [ForeignKey("associate_id")]
        public virtual ICollection<AssociateAttendance> AssociateAttendances { get; set; }
        [ForeignKey("associate_id")]
        public virtual ICollection<AssociateAttachedInfo> AssociateAttachedInfos { get; set; }
        [ForeignKey("associate_id")]
        public virtual ICollection<AssociateSkill> AssociateSkills { get; set; }
        [ForeignKey("associate_id")] 
        public virtual ICollection<ProfilePage> ProfilePages { get; set; }
        [ForeignKey("associate_id")]
        public virtual ICollection<ConferenceRoom> ConferenceRooms { get; set; }
        [ForeignKey("associate_id")]
        public virtual ICollection<Resignation> ResignationDetails { get; set; }
        [ForeignKey("associate_id")]
        public virtual ICollection<NoticePeriod> NoticePeriodDetails { get; set; }
    }
}
