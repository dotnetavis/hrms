﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class RoleServices : IRoleServices
    {
        private readonly ApplicationDbContext _db;
        public RoleServices(ApplicationDbContext options) => _db = options;

        /// <summary>
        /// Get the PageMenu Data,
        /// Created By : Bilal 
        ///On Date: 25/05/2018
        /// </summary>
        /// <returns></returns>
        public List<PageMenuModel> GetMenuList()
        {
            PageMenuModel model = new PageMenuModel();
            try
            {
                model.PageMenuModelList = _db.PageMenus.Select(x => new PageMenuModel
                {
                    id = x.id,
                    menu_name = x.menu_name,
                    menu_url = x.menu_url,
                    controller_name = x.controller_name,
                    action_name = x.action_name,
                    module_name = x.module_name,
                    isactive=x.isactive,
                    createdby = x.createdby,
                    updatedby = x.updatedby,
                    createdbyname = x.createdbyname,
                    updatedbyname = x.updatedbyname,
                    createdon = x.createdon,
                    updatedon = x.updatedon
                }).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.PageMenuModelList;
        }

        /// <summary>
        /// Get the PageMenu Data,
        /// Created By : Bilal 
        ///On Date: 25/05/2018
        /// </summary>
        /// <returns></returns>
        public List<PageMenuModel> GetMenuList(int Id)
        {
            PageMenuModel model = new PageMenuModel();
            try
            {
                model.PageMenuModelList = (from pm in _db.PageMenus
                                           from rd in _db.role_menu_details.Where(x => x.page_menu_id == pm.id && x.role_id == Id).DefaultIfEmpty()
                                           select new PageMenuModel
                                           {
                                               id = pm.id,
                                               menu_name = pm.menu_name,
                                               menu_url = pm.menu_url,
                                               controller_name = pm.controller_name,
                                               action_name = pm.action_name,
                                               module_name = pm.module_name,
                                               isactive = model.isactive,
                                               createdby = model.createdby,
                                               updatedby = model.updatedby,
                                               createdbyname = model.createdbyname,
                                               updatedbyname = model.updatedbyname,
                                               createdon = model.createdon,
                                               updatedon = model.updatedon,
                                               is_selected = rd != null ? rd.is_selected : false
                                           }).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.PageMenuModelList;
        }

        /// <summary>
        /// Get the Role Data,
        /// Created By : Bilal 
        ///On Date: 25/05/2018
        /// </summary>
        /// <returns></returns>
        public List<RoleMasterModel> GetRoleList()
        {
            RoleMasterModel model = new RoleMasterModel();
            try
            {
                model.RoleMasterModelList = _db.role_master.Select(x => new RoleMasterModel
                {
                    id = x.id,
                    role_name = x.role_name,
                    role_description = x.role_description,
                    isactive = x.isactive,
                    createdby = x.createdby,
                    updatedby = x.updatedby,
                    createdon = x.createdon,
                    updatedon = x.updatedon,
                    createdbyname = x.createdbyname,
                    updatedbyname = x.updatedbyname,
                    PageMenu = _db.PageMenus.FirstOrDefault(m => m.id == x.id)
                }).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.RoleMasterModelList;
        }

        /// <summary>
        /// Get the RoleDetails Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        public List<RoleMenuDetailModel> GetRoleDetails(int Id)
        {
            RoleMenuDetailModel model = new RoleMenuDetailModel();
            try
            {
                model.RoleMenuDetailModelList = _db.role_menu_details.Select(x => new RoleMenuDetailModel
                {
                    id = x.id,
                    is_selected = x.is_selected,
                    page_menu_id = x.page_menu_id,
                    role_id = x.role_id,
                    isactive = x.isactive,
                    createdby = x.createdby,
                    updatedby = x.updatedby,
                    createdon = x.createdon,
                    updatedon = x.updatedon,
                    createdbyname = x.createdbyname,
                    updatedbyname = x.updatedbyname,
                    RoleMasters = _db.role_master.FirstOrDefault(m => m.id == Id)
                }).ToList();

            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.RoleMenuDetailModelList;
        }

        /// <summary>
        /// Save the Role Data,
        /// Created By : Bilal 
        ///On Date: 25/05/2018
        /// </summary>
        /// <returns></returns>
        public string SaveRole(RoleMasterModel model)
        {
            string Result = string.Empty;

            RoleMenuDetails roleMenuDetail;
            try
            {
                bool bool_sameNameExists = _db.role_master.Any(x => x.role_name.ToLower().Trim() == model.role_name.ToLower().Trim());
                if (bool_sameNameExists)
                {
                    Result = MessageHelper.Role_Duplicate;
                }
                else
                {
                    try
                    {
                        RoleMaster roleMaster = new RoleMaster();
                        roleMaster.role_name = model.role_name;
                        roleMaster.role_description = model.role_description;
                        roleMaster.isactive = true;
                        roleMaster.createdon = DateTime.Now;
                        roleMaster.updatedon = DateTime.Now;
                        roleMaster.createdby = model.createdby;
                        roleMaster.createdbyname = model.createdbyname;
                        roleMaster.updatedby = model.updatedby;
                        roleMaster.updatedbyname = model.updatedbyname;
                        _db.role_master.Add(roleMaster);
                        _db.SaveChanges();
                        if (roleMaster.id > 0)
                        {
                            foreach (var item in model.PageMenuList)
                            {
                                roleMenuDetail = new RoleMenuDetails();
                                roleMenuDetail.page_menu_id = item.id;
                                roleMenuDetail.role_id = roleMaster.id;
                                roleMenuDetail.is_selected = item.is_selected;
                                roleMenuDetail.isactive = true;
                                roleMenuDetail.createdon = DateTime.Now;
                                roleMenuDetail.updatedon = DateTime.Now;
                                roleMenuDetail.createdby = item.createdby;
                                roleMenuDetail.createdbyname = item.createdbyname;
                                roleMenuDetail.updatedby = item.updatedby;
                                roleMenuDetail.updatedbyname = item.updatedbyname;
                                _db.role_menu_details.Add(roleMenuDetail);
                                _db.SaveChanges();
                            }
                        }
                        Result = MessageHelper.Role_AddSuccess;
                    }
                    catch (Exception ex)
                    {
                        CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                        Result = MessageHelper.Role_AddError;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Role_AddError;
            }
            return Result;
        }

        /// <summary>
        /// Update the Role Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        public string UpdateRole(RoleMasterModel model)
        {
            string Result = string.Empty;
            RoleMenuDetails roleMenuDetail;
            try
            {
                var data = _db.role_master.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    //Check for Duplicate Record
                    bool bool_sameNameExists = _db.role_master.Any(x => x.role_name.ToLower().Trim() == model.role_name.ToLower().Trim() && x.id != model.id);
                    if (bool_sameNameExists)
                    {
                        Result = MessageHelper.Role_Duplicate;
                    }
                    else
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            try
                            {
                                data.role_name = model.role_name;
                                data.role_description = model.role_description;
                                data.isactive = true;
                                data.updatedon = DateTime.Now;
                                data.createdon = model.createdon;
                                data.createdby = model.createdby;
                                data.createdbyname = model.createdbyname;
                                data.updatedby = model.updatedby;
                                data.updatedbyname = model.updatedbyname;
                                _db.SaveChanges();

                                if (data.id > 0)
                                {
                                    foreach (var item in model.PageMenuList)
                                    {

                                        var MenuPage = _db.role_menu_details.FirstOrDefault(x => x.role_id == data.id && x.page_menu_id == item.id);
                                        if (MenuPage != null)
                                        {
                                            MenuPage.is_selected = item.is_selected;
                                            _db.SaveChanges();
                                        }
                                        else
                                        {
                                            roleMenuDetail = new RoleMenuDetails();
                                            roleMenuDetail.page_menu_id = item.id;
                                            roleMenuDetail.role_id = data.id;
                                            roleMenuDetail.is_selected = item.is_selected;
                                            roleMenuDetail.isactive = true;
                                            roleMenuDetail.updatedon = DateTime.Now;
                                            roleMenuDetail.createdon = model.createdon;
                                            roleMenuDetail.createdby = model.createdby;
                                            roleMenuDetail.createdbyname = model.createdbyname;
                                            roleMenuDetail.updatedby = model.updatedby;
                                            roleMenuDetail.updatedbyname = model.updatedbyname;
                                            _db.role_menu_details.Add(roleMenuDetail);
                                            _db.SaveChanges();
                                        }

                                    }
                                }
                                scope.Complete();

                                Result = MessageHelper.Designation_AddSuccess;
                            }
                            catch (Exception ex)
                            {
                                scope.Dispose();
                                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                Result = MessageHelper.Designation_AddError;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Role_AddError;
            }
            return Result;
        }

        /// <summary>
        /// Disable the Role Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        public string DisableRole(RoleMasterModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.role_master.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = false;
                            data.updatedon = DateTime.Now;
                            data.createdon = DateTime.Now;
                            data.createdby = model.createdby;
                            data.createdbyname = model.createdbyname;
                            data.updatedby = model.updatedby;
                            data.updatedbyname = model.updatedbyname;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Role_Disable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }

        /// <summary>
        /// Enable the Role Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        public string EnableRole(RoleMasterModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.role_master.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = true;
                            data.updatedon = DateTime.Now;
                            data.createdon = DateTime.Now;
                            data.createdby = model.createdby;
                            data.createdbyname = model.createdbyname;
                            data.updatedby = model.updatedby;
                            data.updatedbyname = model.updatedbyname;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Role_Enable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }
    }
}
