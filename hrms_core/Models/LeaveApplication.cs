﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class LeaveApplication:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }      //priamry key
        public int leaveappliedby { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = "Select Leave Type")]
        [ForeignKey("leavetype_id")]
        public int leavetype_id { get; set; }
        public DateTime notificationdate { get; set; }
        public DateTime startdate { get; set; }
        public DateTime enddate { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = "Select Start Session")]
        public string startsession { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = "Select End Session")]
        public string endsession { get; set; }
        [Required(ErrorMessage = "Enter Employee Reason")]
        public string employeereason { get; set; }
        //[Required(ErrorMessage = "Enter Employer Reason")]
        public string employerreason { get; set; }
        public int approvedby { get; set; }
        public int status { get; set; }
        public double requested_no_of_leaves { get; set; }
       


    }
}





       