﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class PageAssign:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public int designationid { get; set; }
        public virtual PageMenu menu { get; set; }
        
    }
}

        