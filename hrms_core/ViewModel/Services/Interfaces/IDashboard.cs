﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IDashboard
    {
        void getHighestLeaveTaken();
        void getDepartmentStrength();
        void GetEmployee_Award();
        void Get_Birthday();
        void Get_Anniversary();
        void Get_Marriage_Anniversary();
        void getQualificationRatio();

    }
}
