﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class State:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        [ForeignKey("country_id")]
        public int country_id { get; set; }
        [Required(ErrorMessage = "Enter State")]
        public string statetext { get; set; }
        [ForeignKey("state_id")]
        public virtual ICollection<City> CityDetails { get; set; }
        [ForeignKey("permanent_state_id")]
        public virtual ICollection<AssociatePersonal> AssociatePersonal_Permanent { get; set; }
    }
}

 
     