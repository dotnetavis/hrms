﻿using System;
using System.Collections.Generic;
using System.Linq;
using hrms_core.EF;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace hrms_core.Controllers
{
    public class LeaveController : Controller
    {
        private readonly ApplicationDbContext _db;
        public IConfiguration Configuration { get; }
        private IDepartmentServices departmentservices => new departmentServices(_db);
        private IAssociateServices associateServices => new AssociateServices(_db);
        private ILeaveServices leaveservices => new LeaveServices(_db);
        private ILoginServices loginServices => new LoginServices(_db);
        public static string sessionName = string.Empty;
        public LeaveController(ApplicationDbContext options, IConfiguration configuration)
        {
            _db = options;
            Configuration = configuration;
        }


        [HttpGet]
        public IActionResult CreateLeave(int? id)
        {
            LeaveApplicationModel model = new LeaveApplicationModel();
            double leaves_Count = 0;
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.notificationdate = DateTime.Now;
                model.createdby = SesMdl.user_id;
                model.updatedby = SesMdl.user_id;
                model.createdbyname = SesMdl.username;
                model.updatedbyname = SesMdl.username;
                model.LeaveApplicationModelList = leaveservices.GetLeave().Where(x => x.createdby == SesMdl.associate_id && x.createdon.Year == DateTime.Now.Year).ToList();
                var openinglist = _db.OpeningLeaves.Where(x => x.emp_id == SesMdl.associate_id && x.year == DateTime.Now.Year).ToList();
                model.notificationdate = DateTime.Now.Date;
                model.startdate = DateTime.Now.Date;
                model.enddate = DateTime.Now.Date;
                model.search_startdate = DateTime.Now.Date;
                model.search_enddate = DateTime.Now.Date;


                model.LeaveList = (from dbleavetype in _db.LeaveTypeMasters
                                   from dbapplicable in _db.Departments.Where(a => a.id == SesMdl.department_id).DefaultIfEmpty()
                                   select new LeaveTypeMasterModel()
                                   {
                                       leave_code = dbleavetype.leave_code,
                                       leavetext = dbleavetype.leavetext,
                                       id = dbleavetype.id,
                                       applicable_leave = dbleavetype.is_default == true ? dbapplicable.applicable_leave : 0,
                                   }).ToList();

                if (model != null && model.LeaveList != null && model.LeaveList.Count > 0)
                {
                    foreach (var item in model.LeaveList)
                    {
                        leaves_Count = 0;
                        var LeaveData = model.LeaveApplicationModelList.Where(x => x.leavetype_id == item.id && x.createdby == SesMdl.associate_id && x.status == 1).ToList();
                        //var LeaveData = _db.LeaveApplications.Where(x => x.leavetype_id == item.id && x.createdby == SesMdl.associate_id && x.status == 1).ToList();
                        if (LeaveData != null && LeaveData.Count() > 0)
                        {
                            foreach (var LeaveDataitem in LeaveData)
                            {
                                leaves_Count += Convert.ToDouble(LeaveDataitem.requested_no_of_leaves);
                            }
                        }
                        if (openinglist != null)
                        {
                            var OpeningLeave = openinglist.FirstOrDefault();
                            if (OpeningLeave != null)
                            {
                                item.opening_leave = Convert.ToDouble(OpeningLeave.opening_leave);

                            }
                            else
                            {
                                item.opening_leave = 0;
                            }
                        }
                        else
                        {
                            item.opening_leave = 0;
                        }

                        item.availed_leaves = leaves_Count;
                        item.balance_leave = (item.applicable_leave + item.opening_leave) - (item.availed_leaves);
                    }


                }

                //var LeaveData_CL = _db.LeaveApplications.Where(x => x.leavetype_id == 2 && x.createdby == SesMdl.associate_id && x.status == 1).ToList();
                //if (LeaveData_CL != null && LeaveData_CL.Count() > 0)
                //{
                //    foreach (var item in LeaveData_CL)
                //    {
                //        cl_leaves += Convert.ToDouble(item.requested_no_of_leaves);
                //    }
                //}
                //var LeaveData_SL = _db.LeaveApplications.Where(x => x.leavetype_id == 3 && x.createdby == SesMdl.associate_id && x.status == 1).ToList();
                //if (LeaveData_SL != null && LeaveData_SL.Count() > 0)
                //{
                //    foreach (var item in LeaveData_EL)
                //    {
                //        sl_leaves += Convert.ToDouble(item.requested_no_of_leaves);
                //    }
                //}


                //foreach (var item in model.LeaveApplicationModelList)
                //{
                //    var Leave_code = _db.LeaveTypeMasters.Where(x => x.id == item.leavetype_id).FirstOrDefault();
                //    if (Leave_code != null)
                //        item.leave_type_name = Leave_code.leave_code;

                //    if (item.approvedby > 0)
                //    {
                //        var Approver = _db.AssociateMasters.FirstOrDefault(x => x.id == item.approvedby);
                //        if (Approver != null)
                //            item.approvedbyname = Approver.associate_name;
                //    }
                //}

                //session

                //if (openinglist.Count() != 0)
                //{
                //    foreach (var item in model.LeaveList)
                //    {
                //        if (item.id == 1)
                //        {
                //            item.opening_leave = Convert.ToInt32(openinglist.FirstOrDefault(x => x.leave_id == 1).opening_leave);
                //            item.availed_leaves = el_leaves;
                //            item.balance_leave = (item.applicable_leave + item.opening_leave) - (item.availed_leaves);
                //        }
                //        else if (item.id == 2)
                //        {
                //            item.opening_leave = Convert.ToInt32(openinglist.FirstOrDefault(x => x.leave_id == 2).opening_leave);
                //            item.availed_leaves = cl_leaves;
                //            item.balance_leave = (item.applicable_leave + item.opening_leave) - (item.availed_leaves);
                //        }
                //        else
                //        {
                //            item.opening_leave = Convert.ToInt32(openinglist.FirstOrDefault(x => x.leave_id == 3).opening_leave);
                //            item.availed_leaves = sl_leaves;
                //            item.balance_leave = (item.applicable_leave + item.opening_leave) - (item.availed_leaves);
                //        }
                //    }
                //}
                //else
                //{
                //    foreach (var item in model.LeaveList)
                //    {
                //        if (item.id == 1)
                //        {
                //            item.opening_leave = 0;
                //            item.availed_leaves = el_leaves;
                //            item.balance_leave = (item.applicable_leave + item.opening_leave) - item.availed_leaves;
                //        }
                //        else
                //        {
                //            item.opening_leave = 0;
                //            item.availed_leaves = cl_leaves;
                //            item.balance_leave = (item.applicable_leave + item.opening_leave) - item.availed_leaves;
                //        }
                //    }
                //}
                if (id > 0 && id != null)
                {
                    if (model.LeaveApplicationModelList.Count > 0 && model.LeaveApplicationModelList != null)
                    {
                        var data = model.LeaveApplicationModelList.FirstOrDefault(x => x.id == id);
                        if (data != null)
                        {
                            model.id = data.id;
                            model.notificationdate = data.notificationdate;
                            model.startdate = data.startdate;
                            model.startsession = data.startsession;
                            model.enddate = data.enddate;
                            model.endsession = data.endsession;
                            model.employeereason = data.employeereason;
                            model.employerreason = data.employerreason;
                            model.leavetype_id = data.leavetype_id;
                            model.approvedbyname = _db.AssociateMasters.FirstOrDefault(x => x.id == data.approvedby).associate_name;
                            model.status = data.status;
                            model.leaveappliedby = data.leaveappliedby;
                        }
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult CreateLeave(LeaveApplicationModel model, string CommandName)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            int result;
            bool _result;
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            model.leaveappliedby = SesMdl.associate_id;
            if (CommandName == "Apply")
            {
                bool leaveExist = false;
                var LeaveData = _db.LeaveApplications.Where(x => x.createdby == SesMdl.associate_id && (x.status == 1 || x.status == 0) && x.isactive == true).ToList();
                for (DateTime i = model.startdate; i <= model.enddate; i = i.AddDays(1))
                {
                    leaveExist = LeaveData.Any(x => x.startdate <= i && x.enddate >= i);
                    if (leaveExist)
                    {
                        TempData["msg"] = "You have already applied a leave for the same Day";
                        return RedirectToAction("CreateLeave", "Leave");
                    }
                }

                result = leaveservices.SaveDetails(model);
                if (result > 0)
                {
                    model.leaveapp_id = CommonUtility.MD5Convertor(result.ToString());
                    model.official_email = SesMdl.official_email;
                    model.hod_email = SesMdl.head_of_department_email;
                    model.username = _db.AssociateMasters.FirstOrDefault(x => x.associate_code == SesMdl.username).associate_name;
                    model.reporting_email = SesMdl.reporting_person_email;
                    model.additional_reporting_email = SesMdl.addition_reporting;
                    model.designation = SesMdl.designation_text;
                    model.associatecode = SesMdl.associate_code;
                    model.associatename = SesMdl.associate_name;
                    if (model.requested_no_of_leaves == 0.5)
                    {
                        model.sessionName = sessionName;
                    }
                    _result = leaveservices.SendLeaveReport(model, HttpContext, Configuration);
                    if (_result)
                    {
                        TempData["msg"] = "Leave has been applied successfully";
                    }
                    else
                    {
                        TempData["msg"] = "Something went wrong";
                    }

                }

            }
            //else if (CommandName == "Search")
            //{
            //    model.LeaveApplicationModelList = leaveservices.GetLeave().Where(x => x.startdate.Date >= model.search_startdate.Date && x.startdate.Date <= model.search_enddate.Date && x.createdby == SesMdl.associate_id).ToList();
            //    //model.LeaveApplicationModelList = leaveservices.GetLeave().Where(x => x.startdate.Date>=model.search_startdate.Date && x.enddate.Date<=model.search_enddate.Date && x.createdby == SesMdl.associate_id).ToList();
            //    foreach (var item in model.LeaveApplicationModelList)
            //    {
            //        item.leave_type_name = _db.LeaveTypeMasters.Where(x => x.id == item.leavetype_id).FirstOrDefault().leave_code;
            //        if (item.approvedby > 0)
            //            item.approvedbyname = _db.AssociateMasters.FirstOrDefault(x => x.id == item.approvedby).associate_name;
            //    }
            //    model.LeaveList = _db.LeaveTypeMasters.Select(x => new LeaveTypeMasterModel
            //    {
            //        leavetext = x.leavetext,
            //        id = x.id,
            //        applicable_leave = _db.Departments.Where(a => a.id == SesMdl.department_id).Select(a => a.applicable_leave).FirstOrDefault(),

            //    }).ToList();
            //    return View(model);
            //}

            return RedirectToAction("CreateLeave", "Leave");
        }


        [HttpGet]
        public IActionResult LeaveType(int? id)
        {
            LeaveTypeVM model = new LeaveTypeVM();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                if (id != null && id != 0)
                {
                    ViewBag.Button = "Update";
                    var data = _db.LeaveTypeMasters.Where(x => x.id == id).FirstOrDefault();
                    model.leave_code = data.leave_code;
                    model.max_days = data.max_days;
                    model.min_days = data.min_days;
                    model.before_min = data.before_min;
                    model.leave_type = data.leave_type;
                    model.leave_balance_check = data.leave_balance_check;
                    model.holiday_inclusive = data.holiday_inclusive;
                    model.carry_forward = data.carry_forward;
                    model.leave_apply = data.leave_apply;
                    model.leavetext = data.leavetext;
                    model.is_default = data.is_default;
                    model.LeaveTypeMasterList = _db.LeaveTypeMasters.ToList();
                    return View(model);
                }
                ViewBag.Button = "Save";
                model.LeaveTypeMasterList = _db.LeaveTypeMasters.ToList();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult LeaveType(LeaveTypeVM model, string CommandName)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                if (CommandName == "Save")
                {
                    TempData["msg"] = leaveservices.SaveLeaveType(model);
                    return RedirectToAction("LeaveType", "Leave");
                }
                else
                {
                    TempData["msg"] = leaveservices.UpdateLeaveType(model);
                    return RedirectToAction("LeaveType", "Leave", new { id = 0 });
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpPost]
        public IActionResult SerachLeave(DateTime FromDate, DateTime ToDate)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            LeaveApplicationModel leaveApplicationModel = new LeaveApplicationModel();
            leaveApplicationModel.LeaveApplicationModelList = leaveservices.GetLeave().Where(x => x.startdate.Date >= FromDate.Date && x.startdate.Date <= ToDate.Date && x.createdby == SesMdl.associate_id).ToList();
            //model.LeaveApplicationModelList = leaveservices.GetLeave().Where(x => x.startdate.Date>=model.search_startdate.Date && x.enddate.Date<=model.search_enddate.Date && x.createdby == SesMdl.associate_id).ToList();
            foreach (var item in leaveApplicationModel.LeaveApplicationModelList)
            {
                item.leave_type_name = _db.LeaveTypeMasters.Where(x => x.id == item.leavetype_id).FirstOrDefault().leave_code;
                if (item.approvedby > 0)
                    item.approvedbyname = _db.AssociateMasters.FirstOrDefault(x => x.id == item.approvedby).associate_name;
            }

            return View("CreateLeave", leaveApplicationModel);
        }

        [HttpGet]
        public IActionResult OpeningLeave()
        {
            OpeningLeaveModel model = new OpeningLeaveModel();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                model.OpeningLeaveModelList = leaveservices.GetOpeningLeave();
                if (model.OpeningLeaveModelList.Count == 0)
                {
                    TempData["msg"] = "No Employee has been added";
                }
                model.AssociateMasterModelList = associateServices.GetAssociateList_Active();
                model.AssociateMasterModelList.Insert(0, new AssociateMasterModel { id = 0, associate_name = "--Select--" });
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult OpeningLeave(OpeningLeaveModel model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            TempData["msg"] = leaveservices.SaveOpeningLeave(model);
            return RedirectToAction("OpeningLeave", "Leave");
        }

        [HttpPost]
        public JsonResult GetRequestedLeaves(DateTime? dayFrom, DateTime? dayTo, int StartSession, int EndSession)
        {
            double days = 0;
            if (StartSession == 3)
            {

                days = 0.25;
                return Json(days);
            }
            else
            {

                if (dayFrom != null && dayTo != null)
                {
                    TimeSpan difference = dayTo.Value.Date - dayFrom.Value.Date;
                    int day = (int)difference.TotalDays + 1;
                    if (StartSession == 1 && EndSession == 1)
                    {
                        days = day;
                    }
                    else if ((StartSession == 2 && EndSession == 1) || (EndSession == 2 && StartSession == 1))

                    {
                        days = day - 0.5;
                    }
                    else if (EndSession == 2 && StartSession == 2 && day != 1)
                    {
                        days = day - 1;
                    }
                    else if (EndSession == 2 && StartSession == 2 && day == 1)
                    {
                        days = 0.5;
                    }
                    else
                    {
                        days = 0;
                    }
                }
                else
                {
                    days = 0;
                }
                var data = days;
                if (data == 0.5)
                {
                    if (StartSession == 2)
                    {
                        sessionName = "Second Half";
                    }
                    if (EndSession == 2)
                    {
                        sessionName = "First Half";
                    }
                }
                return Json(data);
            }

        }

        [HttpGet]
        public IActionResult Login(string LeaveID)
        {
            LeaveApplicationModel model = new LeaveApplicationModel();
            int leave_id = 0;
            if (!String.IsNullOrWhiteSpace(LeaveID))
            {
                leave_id = Convert.ToInt32(CommonUtility.GetDecryptMD5(LeaveID));
            }

            model.id = leave_id;

            return View(model);
        }

        [HttpPost]
        public IActionResult Login(LeaveApplicationModel model)
        {
            string EncryptrdLeaveId = string.Empty;
            if (model.id > 0)
            {
                EncryptrdLeaveId = CommonUtility.MD5Convertor(Convert.ToString(model.id));
                SessionModel sessionModel = new SessionModel();
                List<int> reporting_ids = new List<int>();
                string password = CommonUtility.MD5Convertor(model.userpass);
                int associate_id = _db.LeaveApplications.FirstOrDefault(x => x.id == model.id).createdby;
                reporting_ids = leaveservices.GetReportingData(associate_id);
                var associate_codes = (from master in _db.AssociateMasters.Where(x => reporting_ids.Contains(x.id))
                                       join main in _db.AssociateMains on master.id equals main.associate_id
                                       join user in _db.Users on master.associate_code equals user.username

                                       select new SessionModel()
                                       {
                                           associate_id = master.id,
                                           associate_code = master.associate_code,
                                           password = user.userpass,
                                           official_email = main.official_email,
                                           is_super_admin = master.is_super_admin

                                       }
                                       ).ToList();
                if (associate_codes != null && associate_codes.Count != 0)
                {
                    bool Count = false;
                    foreach (var item in associate_codes)
                    {
                        if ((item.associate_code == model.username || item.official_email == model.username) && item.password == password)
                        {
                            sessionModel = loginServices.GetAllSessionData(item.associate_code, item.is_super_admin).FirstOrDefault();
                            HttpContext.Session.SetObjectAsJson(SessionVariables.SessionData, sessionModel);
                            return RedirectToAction("LeaveApproveDisapprove", "Leave", new { id = model.id });
                        }
                        else
                        {
                            Count = true;
                        }
                    }
                    if (Count)
                    {
                        TempData["msg"] = "Unauthorized User";
                        return RedirectToAction("Login", "Leave", new { LeaveID = EncryptrdLeaveId });
                    }
                }
            }
            TempData["msg"] = "Unauthorized User";
            return RedirectToAction("Login", "Leave", new { LeaveID = EncryptrdLeaveId });


        }

        [HttpGet]
        public IActionResult LeaveApproveDisapprove(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);

            if (SesMdl != null)
            {
                LeaveApplicationModel model = new LeaveApplicationModel();
                double count = 0;
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                var data = leaveservices.GetLeaveData(id);
                model.LeaveApplicationModelList = leaveservices.GetLeave().Where(x => x.createdby == data.FirstOrDefault().createdby && x.createdon.Year == DateTime.Now.Year).ToList();
                foreach (var item in model.LeaveApplicationModelList)
                {
                    if (item.status == 1)
                    {
                        count += item.requested_no_of_leaves;
                    }
                }


                var applicable_leave = _db.AssociateMains.FirstOrDefault(x => x.associate_id == data.FirstOrDefault().createdby).applicable_leave;
                var openinglist = _db.OpeningLeaves.Where(x => x.emp_id == data.FirstOrDefault().createdby).ToList();
                var LeaveType = _db.LeaveTypeMasters.Where(x => x.isactive == true).ToList();

                if (openinglist.Count() != 0)
                {
                    foreach (var item in LeaveType)
                    {
                        var opening_leave1 = Convert.ToInt32(openinglist.FirstOrDefault(x => x.leave_id == item.id).opening_leave);
                        applicable_leave += opening_leave1;
                    }
                }
                if (data != null && data.Count != 0)
                {
                    foreach (var item in data)
                    {
                        model.id = id;
                        model.notificationdate = item.notificationdate;
                        model.startdate = item.startdate;
                        model.enddate = item.enddate;
                        model.associatecode = _db.AssociateMasters.FirstOrDefault(x => x.id == item.createdby).associate_code;
                        model.associatename = item.createdbyname;
                        model.employeereason = item.employeereason;
                        model.employerreason = item.employerreason;
                        model.requested_no_of_leaves = item.requested_no_of_leaves;
                        model.leavetype_id = item.leavetype_id;
                        model.leave_name = _db.LeaveTypeMasters.FirstOrDefault(x => x.id == item.leavetype_id).leavetext;
                        model.status = item.status;
                        model.isactive = item.isactive;
                        model.approvedby = item.approvedby;
                        model.balance_leave = applicable_leave - count;

                    }
                    if (model.isactive == false)
                    {
                        TempData["msg"] = "Leave is Already Deactivated";
                    }

                }
                return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpPost]
        public IActionResult LeaveApproveDisapprove(LeaveApplicationModel model, string CommandName)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            bool mail_result;
            if (SesMdl != null)
            {

                var data = _db.LeaveApplications.FirstOrDefault(x => x.id == model.id);
                if (CommandName == "Approve")
                {

                    if (data != null)
                    {

                        data.status = 1;
                        data.approvedby = SesMdl.associate_id;
                        data.employerreason = model.employerreason;
                        _db.SaveChanges();

                        //int UserId = data.createdby;
                        //var _UserData = _db.AssociateMains.FirstOrDefault(x => x.id == UserId);
                        //if (_UserData != null)
                        //{
                        //    _UserData.applicable_leave= Convert.ToInt32(model.balance_leave-data.requested_no_of_leaves);
                        //    _db.SaveChanges();
                        //}
                    }
                }
                else
                {
                    if (data != null)
                    {
                        data.status = 2;
                        data.approvedby = SesMdl.associate_id;
                        data.employerreason = model.employerreason;
                        _db.SaveChanges();
                    }
                }
                string status = string.Empty;
                if (data.status == 1)
                    model.status_text = "Approved";
                else
                    model.status_text = "DisApproved";

                LeaveAppMainModel leave = new LeaveAppMainModel();
                leave.isactive = model.isactive;
                leave.leaveapp_id = model.id;
                leave.createdby = SesMdl.associate_id;
                leave.updatedby = SesMdl.associate_id;
                leave.updatedbyname = SesMdl.associate_name;
                leave.createdbyname = SesMdl.associate_name;
                leave.createdon = DateTime.Now;
                leave.updatedon = DateTime.Now;
                leave.leave_date = model.notificationdate.Date;
                leave.totalleave = model.requested_no_of_leaves;
                leave.isactive = true;

                bool result = leaveservices.SaveLeaveAppMains(leave);

                List<int> reporting_ids = new List<int>();
                int associate_id = _db.LeaveApplications.FirstOrDefault(x => x.id == model.id).createdby;
                reporting_ids = leaveservices.GetReportingData(associate_id);
                var associate_email = _db.AssociateMains.FirstOrDefault(x => x.associate_id == associate_id);
                if (associate_email != null)
                {
                    model.employer_associate_email = associate_email.official_email;
                }
                var associate = _db.AssociateMasters.FirstOrDefault(x => x.id == associate_id);
                if (associate != null)
                {
                    model.employer_associate_name = associate.associate_name;
                    model.employer_associate_code = associate.associate_code;
                }

                List<string> reporting_emails = new List<string>();
                reporting_emails = _db.AssociateMains.Where(x => reporting_ids.Contains(x.associate_id)).Select(x => x.official_email).ToList();
                if (reporting_emails != null && reporting_emails.Count > 0)
                {
                    model.Mail_to_emails = reporting_emails;
                }
                model.official_email = SesMdl.official_email;
                model.hod_email = SesMdl.head_of_department_email;
                var associate_data = _db.AssociateMasters.FirstOrDefault(x => x.associate_code == SesMdl.username);
                if (associate_data != null)
                {
                    model.username = associate_data.associate_name;
                }
                model.reporting_email = SesMdl.reporting_person_email;
                model.additional_reporting_email = SesMdl.addition_reporting;
                model.designation = SesMdl.designation_text;
                model.associatecode = SesMdl.associate_code;
                model.associatename = SesMdl.associate_name;
                string str = string.Empty;
                mail_result = leaveservices.SendLeaveApprovalDisapprovalReport(model, HttpContext, Configuration);

                TempData["msg"] = CommandName+"d Successfully";
                return RedirectToAction("Login", "Account");

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [HttpGet]
        public IActionResult LeaveApprovedDisApprovedBucket()
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            LeaveApplicationModel model = new LeaveApplicationModel();
            model.createdby = SesMdl.user_id;
            model.updatedby = SesMdl.user_id;
            model.createdbyname = SesMdl.username;
            model.updatedbyname = SesMdl.username;
            if (SesMdl.role_id == 3 || SesMdl.role_id == 5 || SesMdl.role_id == 7 || SesMdl.role_id == 1)
            {
                model.PendingleaveApplicationslist = (from leave in _db.LeaveApplications.Where(x => x.isactive == true && x.status == 0)
                                                      join master in _db.AssociateMasters
                                                      on leave.createdby equals master.id
                                                      select new LeaveApplicationModel
                                                      {
                                                          id = leave.id,
                                                          associatecode = master.associate_code,
                                                          associatename = master.associate_name,
                                                          status = leave.status,
                                                          requested_no_of_leaves = leave.requested_no_of_leaves,
                                                          employeereason = leave.employeereason,
                                                          leavestartdate = leave.startdate.ToShortDateString(),
                                                          leaveenddate = leave.enddate.ToShortDateString(),
                                                          createdon = leave.createdon,
                                                      }).ToList();

                model.ApprovedleaveApplicationslist = (from leave in _db.LeaveApplications.Where(x => x.isactive == true && x.status == 1)
                                                       join master in _db.AssociateMasters on leave.createdby equals master.id
                                                       select new LeaveApplicationModel
                                                       {
                                                           id = leave.id,
                                                           associatecode = master.associate_code,
                                                           associatename = master.associate_name,
                                                           status = leave.status,
                                                           requested_no_of_leaves = leave.requested_no_of_leaves,
                                                           employeereason = leave.employeereason,
                                                           leavestartdate = leave.startdate.ToShortDateString(),
                                                           leaveenddate = leave.enddate.ToShortDateString(),
                                                           createdon = leave.createdon,
                                                       }).ToList();

                model.DisApprovedleaveApplicationslist = (from leave in _db.LeaveApplications.Where(x => x.isactive == true && x.status == 2)
                                                          join master in _db.AssociateMasters
                                                          on leave.createdby equals master.id
                                                          select new LeaveApplicationModel
                                                          {
                                                              id = leave.id,
                                                              associatecode = master.associate_code,
                                                              associatename = master.associate_name,
                                                              status = leave.status,
                                                              requested_no_of_leaves = leave.requested_no_of_leaves,
                                                              employeereason = leave.employeereason,
                                                              leavestartdate = leave.startdate.ToShortDateString(),
                                                              leaveenddate = leave.enddate.ToShortDateString(),
                                                              createdon = leave.createdon,
                                                          }).ToList();
            }
            else
            {
                List<int> DirectReportingAssociatesId = _db.AssociateMains.Where(c => c.reportingperson_id == SesMdl.associate_id && c.isactive == true).Select(c => c.associate_id).ToList();
                List<int> AdditionalReportingAssociatesID = _db.AssociateAdditionalInfos.Where(c => c.reporting_person_id == SesMdl.associate_id && c.isactive == true).Select(c => c.associate_id).ToList();
                
                if (DirectReportingAssociatesId!= null && DirectReportingAssociatesId.Count > 0)
                {
                    model.PendingleaveApplicationslist = (from master in _db.AssociateMasters.Where(c => c.isactive == true)
                                                          join main in _db.AssociateMains.Where(c => c.reportingperson_id == SesMdl.associate_id && c.isactive == true)
                                                          on master.id equals main.associate_id
                                                          join leave in _db.LeaveApplications.Where(x => x.isactive == true && x.status == 0)
                                                           on master.id equals leave.createdby
                                                          select new LeaveApplicationModel
                                                          {
                                                              id = leave.id,
                                                              associatecode = master.associate_code,
                                                              associatename = master.associate_name,
                                                              status = leave.status,
                                                              requested_no_of_leaves = leave.requested_no_of_leaves,
                                                              employeereason = leave.employeereason,
                                                              leavestartdate = leave.startdate.ToShortDateString(),
                                                              leaveenddate = leave.enddate.ToShortDateString(),
                                                              createdon = leave.createdon,
                                                          }).ToList();
                    model.ApprovedleaveApplicationslist = (from master in _db.AssociateMasters.Where(c => c.isactive == true)
                                                           join main in _db.AssociateMains.Where(c => c.reportingperson_id == SesMdl.associate_id && c.isactive == true)
                                                           on master.id equals main.associate_id
                                                           join leave in _db.LeaveApplications.Where(x => x.isactive == true && x.status == 1)
                                                            on master.id equals leave.createdby
                                                           select new LeaveApplicationModel
                                                           {
                                                               id = leave.id,
                                                               associatecode = master.associate_code,
                                                               associatename = master.associate_name,
                                                               status = leave.status,
                                                               requested_no_of_leaves = leave.requested_no_of_leaves,
                                                               employeereason = leave.employeereason,
                                                               leavestartdate = leave.startdate.ToShortDateString(),
                                                               leaveenddate = leave.enddate.ToShortDateString(),
                                                               createdon = leave.createdon,
                                                           }).ToList();
                    model.DisApprovedleaveApplicationslist = (from master in _db.AssociateMasters.Where(c => c.isactive == true)
                                                              join main in _db.AssociateMains.Where(c => c.reportingperson_id == SesMdl.associate_id && c.isactive == true)
                                                              on master.id equals main.associate_id
                                                              join leave in _db.LeaveApplications.Where(x => x.isactive == true && x.status == 2)
                                                               on master.id equals leave.createdby
                                                              select new LeaveApplicationModel
                                                              {
                                                                  id = leave.id,
                                                                  associatecode = master.associate_code,
                                                                  associatename = master.associate_name,
                                                                  status = leave.status,
                                                                  requested_no_of_leaves = leave.requested_no_of_leaves,
                                                                  employeereason = leave.employeereason,
                                                                  leavestartdate = leave.startdate.ToShortDateString(),
                                                                  leaveenddate = leave.enddate.ToShortDateString(),
                                                                  createdon = leave.createdon,
                                                              }).ToList();

                }
                
                    if(AdditionalReportingAssociatesID != null && AdditionalReportingAssociatesID.Count > 0)
                    {
                       List<LeaveApplicationModel> PendingleaveApplicationslist = (from master in _db.AssociateMasters.Where(c => c.isactive == true)
                                                              join add in _db.AssociateAdditionalInfos.Where(c => c.reporting_person_id == SesMdl.associate_id && c.isactive == true)
                                                              on master.id equals add.associate_id
                                                              join leave in _db.LeaveApplications.Where(x => x.isactive == true && x.status == 0)
                                                               on master.id equals leave.createdby
                                                              select new LeaveApplicationModel
                                                              {
                                                                  id = leave.id,
                                                                  associatecode = master.associate_code,
                                                                  associatename = master.associate_name,
                                                                  status = leave.status,
                                                                  requested_no_of_leaves = leave.requested_no_of_leaves,
                                                                  employeereason = leave.employeereason,
                                                                  leavestartdate = leave.startdate.ToShortDateString(),
                                                                  leaveenddate = leave.enddate.ToShortDateString(),
                                                                  createdon = leave.createdon,
                                                              }).ToList();
                        List<LeaveApplicationModel>  ApprovedleaveApplicationslist = (from master in _db.AssociateMasters.Where(c => c.isactive == true)
                                                               join add in _db.AssociateAdditionalInfos.Where(c => c.reporting_person_id == SesMdl.associate_id && c.isactive == true)
                                                               on master.id equals add.associate_id
                                                               join leave in _db.LeaveApplications.Where(x => x.isactive == true && x.status == 1)
                                                                on master.id equals leave.createdby
                                                               select new LeaveApplicationModel
                                                               {
                                                                   id = leave.id,
                                                                   associatecode = master.associate_code,
                                                                   associatename = master.associate_name,
                                                                   status = leave.status,
                                                                   requested_no_of_leaves = leave.requested_no_of_leaves,
                                                                   employeereason = leave.employeereason,
                                                                   leavestartdate = leave.startdate.ToShortDateString(),
                                                                   leaveenddate = leave.enddate.ToShortDateString(),
                                                                   createdon = leave.createdon,
                                                               }).ToList();
                        List<LeaveApplicationModel> DisApprovedleaveApplicationslist = (from master in _db.AssociateMasters.Where(c => c.isactive == true)
                                                                  join add in _db.AssociateAdditionalInfos.Where(c => c.reporting_person_id == SesMdl.associate_id && c.isactive == true)
                                                                  on master.id equals add.associate_id
                                                                  join leave in _db.LeaveApplications.Where(x => x.isactive == true && x.status == 2)
                                                                   on master.id equals leave.createdby
                                                                  select new LeaveApplicationModel
                                                                  {
                                                                      id = leave.id,
                                                                      associatecode = master.associate_code,
                                                                      associatename = master.associate_name,
                                                                      status = leave.status,
                                                                      requested_no_of_leaves = leave.requested_no_of_leaves,
                                                                      employeereason = leave.employeereason,
                                                                      leavestartdate = leave.startdate.ToShortDateString(),
                                                                      leaveenddate = leave.enddate.ToShortDateString(),
                                                                      createdon = leave.createdon,
                                                                  }).ToList();
                        if (PendingleaveApplicationslist != null && PendingleaveApplicationslist.Count > 0)
                            model.PendingleaveApplicationslist.AddRange(PendingleaveApplicationslist);
                        if (ApprovedleaveApplicationslist != null && ApprovedleaveApplicationslist.Count > 0)
                            model.ApprovedleaveApplicationslist.AddRange(ApprovedleaveApplicationslist);
                        if (DisApprovedleaveApplicationslist != null && DisApprovedleaveApplicationslist.Count > 0)
                            model.DisApprovedleaveApplicationslist.AddRange(DisApprovedleaveApplicationslist);
                    }
                model.PendingleaveApplicationslist= model.PendingleaveApplicationslist.Distinct().ToList();
                model.ApprovedleaveApplicationslist = model.ApprovedleaveApplicationslist.Distinct().ToList();
                model.DisApprovedleaveApplicationslist = model.DisApprovedleaveApplicationslist.Distinct().ToList();

            }

            return View(model);
        }

        [HttpGet]
        public IActionResult LeaveApprovedDisApprovedExternel(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);

            if (SesMdl != null)
            {
                LeaveApplicationModel model = new LeaveApplicationModel();
                double count = 0;
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                var data = leaveservices.GetLeaveData(id);
                model.LeaveApplicationModelList = leaveservices.GetLeave().Where(x => x.createdby == data.FirstOrDefault().createdby && x.createdon.Year == DateTime.Now.Year).ToList();
                foreach (var item in model.LeaveApplicationModelList)
                {
                    if (item.status == 1)
                    {
                        count += item.requested_no_of_leaves;
                    }
                }


                var applicable_leave = _db.AssociateMains.FirstOrDefault(x => x.associate_id == data.FirstOrDefault().createdby).applicable_leave;
                var openinglist = _db.OpeningLeaves.Where(x => x.emp_id == data.FirstOrDefault().createdby).ToList();
                var LeaveType = _db.LeaveTypeMasters.Where(x => x.isactive == true).ToList();

                if (openinglist.Count() != 0)
                {
                    foreach (var item in LeaveType)
                    {
                        var opening_leave1 = Convert.ToInt32(openinglist.FirstOrDefault(x => x.leave_id == item.id).opening_leave);
                        applicable_leave += opening_leave1;
                    }
                }
                if (data != null && data.Count != 0)
                {
                    foreach (var item in data)
                    {
                        model.id = id;
                        model.notificationdate = item.notificationdate;
                        model.startdate = item.startdate;
                        model.enddate = item.enddate;
                        model.associatecode = _db.AssociateMasters.FirstOrDefault(x => x.id == item.createdby).associate_code;
                        model.associatename = item.createdbyname;
                        model.employeereason = item.employeereason;
                        model.employerreason = item.employerreason;
                        model.requested_no_of_leaves = item.requested_no_of_leaves;
                        model.leavetype_id = item.leavetype_id;
                        model.leave_name = _db.LeaveTypeMasters.FirstOrDefault(x => x.id == item.leavetype_id).leavetext;
                        model.status = item.status;
                        model.isactive = item.isactive;
                        model.approvedby = item.approvedby;
                        model.balance_leave = applicable_leave - count;

                    }
                    if (model.isactive == false)
                    {
                        TempData["msg"] = "Leave is Already Deactivated";
                    }

                }
                return View(model);

                //LeaveApplicationModel model = new LeaveApplicationModel();
                //double count = 0.0;
                //model.createdby = SesMdl.associate_id;
                //model.createdbyname = SesMdl.associate_name;
                //model.updatedby = SesMdl.associate_id;
                //model.updatedbyname = SesMdl.associate_name;
                //var data = leaveservices.GetLeaveData(id);
                //model.LeaveApplicationModelList = leaveservices.GetLeave().Where(x => x.createdby == data.FirstOrDefault().createdby && x.createdon.Year == DateTime.Now.Year).ToList();
                //foreach (var item in model.LeaveApplicationModelList)
                //{
                //    if (item.status == 1)
                //    {

                //        count += item.requested_no_of_leaves;
                //    }
                //}
                //var applicable_leave = _db.AssociateMains.FirstOrDefault(x => x.associate_id == data.FirstOrDefault().createdby).applicable_leave;

                //if (data != null && data.Count != 0)
                //{
                //    foreach (var item in data)
                //    {
                //        model.id = id;
                //        model.notificationdate = item.notificationdate;
                //        model.startdate = item.startdate;
                //        model.enddate = item.enddate;
                //        model.associatecode = _db.AssociateMasters.FirstOrDefault(x => x.id == item.createdby).associate_code;
                //        model.associatename = item.createdbyname;
                //        model.employeereason = item.employeereason;
                //        model.employerreason = item.employerreason;
                //        model.requested_no_of_leaves = item.requested_no_of_leaves;
                //        model.leavetype_id = item.leavetype_id;
                //        model.leave_name = _db.LeaveTypeMasters.FirstOrDefault(x => x.id == item.leavetype_id).leavetext;
                //        model.status = item.status;
                //        model.approvedby = item.approvedby;
                //        model.balance_leave = applicable_leave - count;
                //    }
                //}
                //return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [HttpPost]
        public IActionResult LeaveApprovedDisApprovedExternel(LeaveApplicationModel model, string CommandName)
        {

            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            bool mail_result;
            if (SesMdl != null)
            {
                var data = _db.LeaveApplications.FirstOrDefault(x => x.id == model.id);
                if (CommandName == "Approve")
                {

                    if (data != null)
                    {

                        data.status = 1;
                        data.approvedby = SesMdl.associate_id;
                        data.employerreason = model.employerreason;
                        _db.SaveChanges();

                        //int UserId = data.createdby;
                        //var _UserData = _db.AssociateMains.FirstOrDefault(x => x.id == UserId);
                        //if (_UserData != null)
                        //{
                        //    _UserData.applicable_leave= Convert.ToInt32(model.balance_leave-data.requested_no_of_leaves);
                        //    _db.SaveChanges();
                        //}
                    }
                }
                else
                {
                    if (data != null)
                    {
                        data.status = 2;
                        data.approvedby = SesMdl.associate_id;
                        data.employerreason = model.employerreason;
                        _db.SaveChanges();
                    }
                }
                string status = string.Empty;
                if (data.status == 1)
                    model.status_text = "Approved";
                else
                    model.status_text = "DisApproved";

                LeaveAppMainModel leave = new LeaveAppMainModel();
                leave.isactive = model.isactive;
                leave.leaveapp_id = model.id;
                leave.createdby = SesMdl.associate_id;
                leave.updatedby = SesMdl.associate_id;
                leave.updatedbyname = SesMdl.associate_name;
                leave.createdbyname = SesMdl.associate_name;
                leave.createdon = DateTime.Now;
                leave.updatedon = DateTime.Now;
                leave.leave_date = model.notificationdate.Date;
                leave.totalleave = model.requested_no_of_leaves;
                leave.isactive = true;

                bool result = leaveservices.SaveLeaveAppMains(leave);

                List<int> reporting_ids = new List<int>();
                int associate_id = _db.LeaveApplications.FirstOrDefault(x => x.id == model.id).createdby;
                reporting_ids = leaveservices.GetReportingData(associate_id);
                var associate_email = _db.AssociateMains.FirstOrDefault(x => x.associate_id == associate_id);
                if (associate_email != null)
                {
                    model.employer_associate_email = associate_email.official_email;
                }
                var associate = _db.AssociateMasters.FirstOrDefault(x => x.id == associate_id);
                if (associate != null)
                {
                    model.employer_associate_name = associate.associate_name;
                    model.employer_associate_code = associate.associate_code;
                }

                List<string> reporting_emails = new List<string>();
                reporting_emails = _db.AssociateMains.Where(x => reporting_ids.Contains(x.associate_id)).Select(x => x.official_email).ToList();
                if (reporting_emails != null && reporting_emails.Count > 0)
                {
                    model.Mail_to_emails = reporting_emails;
                }
                model.official_email = SesMdl.official_email;
                model.hod_email = SesMdl.head_of_department_email;
                var associate_data = _db.AssociateMasters.FirstOrDefault(x => x.associate_code == SesMdl.username);
                if (associate_data != null)
                {
                    model.username = associate_data.associate_name;
                }
                model.reporting_email = SesMdl.reporting_person_email;
                model.additional_reporting_email = SesMdl.addition_reporting;
                model.designation = SesMdl.designation_text;
                model.associatecode = SesMdl.associate_code;
                model.associatename = SesMdl.associate_name;
                string str = string.Empty;
                mail_result = leaveservices.SendLeaveApprovalDisapprovalReport(model, HttpContext, Configuration);
                //var data = _db.LeaveApplications.FirstOrDefault(x => x.id == model.id);
                //if (CommandName == "Approve")
                //{

                //    if (data != null)
                //    {
                //        data.status = 1;
                //        data.approvedby = SesMdl.associate_id;
                //        data.employerreason = model.employerreason;
                //        _db.SaveChanges();
                //        int update = _db.AssociateMains.FirstOrDefault(x => x.id == model.id).applicable_leave;
                //        update = Convert.ToInt32(model.balance_leave);
                //        _db.SaveChanges();
                //    }
                //}
                //else
                //{
                //    if (data != null)
                //    {
                //        data.status = 2;
                //        data.approvedby = SesMdl.associate_id;
                //        data.employerreason = model.employerreason;
                //        _db.SaveChanges();
                //    }
                //}
                //string status = string.Empty;
                //if (data.status == 1)
                //    model.status_text = "Approved";
                //else
                //    model.status_text = "DisApproved";

                //LeaveAppMainModel leave = new LeaveAppMainModel();
                //leave.isactive = model.isactive;
                //leave.leaveapp_id = model.id;
                //leave.createdby = SesMdl.associate_id;
                //leave.updatedby = SesMdl.associate_id;
                //leave.updatedbyname = SesMdl.associate_name;
                //leave.createdbyname = SesMdl.associate_name;
                //leave.createdon = DateTime.Now;
                //leave.updatedon = DateTime.Now;
                //leave.leave_date = model.notificationdate.Date;
                //leave.totalleave = model.requested_no_of_leaves;
                //leave.isactive = true;

                //bool result = leaveservices.SaveLeaveAppMains(leave);

                //List<int> reporting_ids = new List<int>();
                //int associate_id = _db.LeaveApplications.FirstOrDefault(x => x.id == model.id).createdby;
                //reporting_ids = leaveservices.GetReportingData(associate_id);
                //var associate_email = _db.AssociateMains.FirstOrDefault(x => x.associate_id == associate_id);
                //if (associate_email != null)
                //{
                //    model.employer_associate_email = associate_email.official_email;
                //}
                //var associate = _db.AssociateMasters.FirstOrDefault(x => x.id == associate_id);
                //if (associate != null)
                //{
                //    model.employer_associate_name = associate.associate_name;
                //    model.employer_associate_code = associate.associate_code;
                //}

                //List<string> reporting_emails = new List<string>();
                //reporting_emails = _db.AssociateMains.Where(x => reporting_ids.Contains(x.associate_id)).Select(x => x.official_email).ToList();
                //if (reporting_emails != null && reporting_emails.Count > 0)
                //{
                //    model.Mail_to_emails = reporting_emails;
                //}
                //model.official_email = SesMdl.official_email;
                //model.hod_email = SesMdl.head_of_department_email;
                //var associate_data = _db.AssociateMasters.FirstOrDefault(x => x.associate_code == SesMdl.username);
                //if (associate_data != null)
                //{
                //    model.username = associate_data.associate_name;
                //}
                //model.reporting_email = SesMdl.reporting_person_email;
                //model.additional_reporting_email = SesMdl.addition_reporting;
                //model.designation = SesMdl.designation_text;
                //model.associatecode = SesMdl.associate_code;
                //model.associatename = SesMdl.associate_name;
                //string str = string.Empty;
                //mail_result = leaveservices.SendLeaveApprovalDisapprovalReport(model, HttpContext, Configuration);

                TempData["msg"] = CommandName + "d Successfully";
                return RedirectToAction("LeaveApprovedDisApprovedBucket", "Leave");

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [HttpGet]
        public IActionResult LeaveSummary()
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            //int teamLeadRoleId = _db.role_master.FirstOrDefault(x => x.role_name.ToLower().Trim().Contains("team lead")).id;
            //int managerRoleID= _db.role_master.FirstOrDefault(x => x.role_name.ToLower().Trim().Contains("manager")).id;
            //if (SesMdl.role_id==teamLeadRoleId || SesMdl.role_id == teamLeadRoleId)
            //{
            LeaveApplicationModel model = new LeaveApplicationModel();
            model.associateId = SesMdl.associate_id;
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            //model.LeaveApplicationModelList = leaveservices.GetLeave().Where(x => x.createdby == SesMdl.associate_id && x.createdon.Year == DateTime.Now.Year).ToList();
            // var Associates = leaveservices.GetUsers(int model.associateId)
            model.associateMasterModelList = leaveservices.GetUsers(model.associateId);
            if (model.associateMasterModelList != null && model.associateMasterModelList.Count > 0)
            {
                List<LeaveApplicationModel> leaves = new List<LeaveApplicationModel>();
                foreach (var item in model.associateMasterModelList)
                {
                    leaves = leaveservices.GetLeave().Where(x => x.createdby == item.id && x.createdon.Year == DateTime.Now.Year).ToList();
                    model.LeaveApplicationModelList = model.LeaveApplicationModelList.Union(leaves).ToList();
                }
            }
            return View(model);
            //}
            //else{
            //    TempData["msg"] = "Unauthorised To Access";
            //    return RedirectToAction("Index", "Home");
            //}
        }
        [HttpGet]
        public IActionResult AdminLeaveSummary()
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            int HRRoleID = _db.role_master.FirstOrDefault(x => x.role_name.ToLower().Trim().Contains("hr")).id;
            int adminRoleID = _db.role_master.FirstOrDefault(x => x.role_name.ToLower().Trim().Contains("admin")).id;
            int directorRoleID = _db.role_master.FirstOrDefault(x => x.role_name.ToLower().Trim().Contains("director")).id;
            int presidentRoleID = _db.role_master.FirstOrDefault(x => x.role_name.ToLower().Trim().Contains("president")).id;
            if (SesMdl.role_id == HRRoleID || SesMdl.role_id == adminRoleID || SesMdl.role_id == directorRoleID || SesMdl.role_id == presidentRoleID)
            {
                LeaveApplicationModel model = new LeaveApplicationModel();
                model.associateId = SesMdl.associate_id;
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                model.selectedYear = DateTime.Now.Year;
                model.LeaveApplicationModelList = leaveservices.GetLeave().Where(x => x.createdon.Year == model.selectedYear).OrderBy(x => x.associatename).OrderByDescending(x => x.startdate).ToList();
                if (model.LeaveApplicationModelList == null)
                {
                    model.LeaveApplicationModelList.Add(new LeaveApplicationModel
                    {
                    });
                }
                return View(model);
            }
            else
            {
                TempData["msg"] = "Unauthorised To Access";
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        public IActionResult AdminLeaveSummary(LeaveApplicationModel leaveApplicationModel)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            int HRRoleID = _db.role_master.FirstOrDefault(x => x.role_name.ToLower().Trim().Contains("hr")).id;
            int adminRoleID = _db.role_master.FirstOrDefault(x => x.role_name.ToLower().Trim().Contains("admin")).id;
            int directorRoleID = _db.role_master.FirstOrDefault(x => x.role_name.ToLower().Trim().Contains("director")).id;
            int presidentRoleID = _db.role_master.FirstOrDefault(x => x.role_name.ToLower().Trim().Contains("president")).id;
            if (SesMdl.role_id == HRRoleID || SesMdl.role_id == adminRoleID || SesMdl.role_id == directorRoleID || SesMdl.role_id == presidentRoleID)
            {
                LeaveApplicationModel model = new LeaveApplicationModel();
                model.associateId = SesMdl.associate_id;
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                model.LeaveApplicationModelList = leaveservices.GetLeave().Where(x => x.createdon.Year == leaveApplicationModel.selectedYear).OrderBy(x => x.associatename).OrderByDescending(x => x.startdate).ToList();
                model.selectedYear = leaveApplicationModel.selectedYear;
                if (model.LeaveApplicationModelList == null)
                {
                    model.LeaveApplicationModelList.Add(new LeaveApplicationModel
                    {
                    });
                }
                return View(model);
            }
            else
            {
                TempData["msg"] = "Unauthorised To Access";
                return RedirectToAction("Index", "Home");
            }
        }

        public JsonResult CancelLeave(int id)
        {
            bool result, _result;
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                LeaveApplicationModel model = new LeaveApplicationModel();
                model = leaveservices.GetLeaveData(id).FirstOrDefault();
                result = leaveservices.CancelLeave(model);
                if (result)
                {
                    model.leaveapp_id = CommonUtility.MD5Convertor(result.ToString());
                    model.official_email = SesMdl.official_email;
                    model.hod_email = SesMdl.head_of_department_email;
                    model.username = _db.AssociateMasters.FirstOrDefault(x => x.associate_code == SesMdl.username).associate_name;
                    model.reporting_email = SesMdl.reporting_person_email;
                    model.additional_reporting_email = SesMdl.addition_reporting;
                    model.designation = SesMdl.designation_text;
                    model.associatecode = SesMdl.associate_code;
                    model.associatename = SesMdl.associate_name;
                    if (model.requested_no_of_leaves == 0.5)
                    {
                        model.sessionName = sessionName;
                    }
                    _result = leaveservices.SendLeaveReport(model, HttpContext, Configuration);
                    if (_result)
                    {
                        TempData["msg"] = "Leave has been cancelled successfully";
                    }
                    else
                    {
                        TempData["msg"] = "Something went wrong";
                    }

                }
                else
                {
                    TempData["msg"] = "Leave Cancelled Failed";
                }
                return Json(result);
            }
            else
            {
                TempData["msg"] = "Your session has been expired,Relogin again!";
                return null;
            }

        }
        public IActionResult LeaveReport()
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            //int HRRoleID = _db.role_master.FirstOrDefault(x => x.role_name.ToLower().Trim().Contains("hr")).id;
            //int adminRoleID = _db.role_master.FirstOrDefault(x => x.role_name.ToLower().Trim().Contains("admin")).id;
            //int directorRoleID = _db.role_master.FirstOrDefault(x => x.role_name.ToLower().Trim().Contains("director")).id;
            //int presidentRoleID = _db.role_master.FirstOrDefault(x => x.role_name.ToLower().Trim().Contains("president")).id;
            //if (SesMdl.role_id == HRRoleID || SesMdl.role_id == adminRoleID || SesMdl.role_id == directorRoleID || SesMdl.role_id == presidentRoleID)
            //{
            LeaveApplicationModel model = new LeaveApplicationModel();
            model.associateId = SesMdl.associate_id;
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            model.LeaveApplicationModelList = leaveservices.GetLeave().Where(x => x.createdon.Year == DateTime.Now.Year).OrderBy(x => x.associatename).OrderByDescending(x => x.startdate).ToList();
            if (model.LeaveApplicationModelList == null)
            {
                model.LeaveApplicationModelList.Add(new LeaveApplicationModel
                {
                });
            }
            return View(model);
            //}
            //else
            //{
            //    TempData["msg"] = "Unauthorised To Access";
            //    return RedirectToAction("Index", "Home");
            //}
        }
    }
}

