﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class HolidayCalenderDates:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }      //primary key
        public DateTime holiday_date { get; set; }
        public string occasion { get; set; }
        public int duration { get; set; }
        public virtual HolidayCalender calender { get; set; }
    }
}
