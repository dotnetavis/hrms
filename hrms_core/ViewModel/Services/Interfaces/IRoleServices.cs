﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IRoleServices
    {
        /// <summary>
        /// Get the PageMenu Data,
        /// Created By : Bilal 
        ///On Date: 25/05/2018
        /// </summary>
        /// <returns></returns>
        List<PageMenuModel> GetMenuList();

        /// <summary>
        /// Get the PageMenu with RoleMenuDetails Data,
        /// Created By : Bilal 
        ///On Date: 25/05/2018
        /// </summary>
        /// <returns></returns>
        List<PageMenuModel> GetMenuList(int Id);

        /// <summary>
        /// Get the Role Data,
        /// Created By : Bilal 
        ///On Date: 25/05/2018
        /// </summary>
        /// <returns></returns>
        List<RoleMasterModel> GetRoleList();

        /// <summary>
        /// Save the Role Data,
        /// Created By : Bilal 
        ///On Date: 25/05/2018
        /// </summary>
        /// <returns></returns>
        string SaveRole(RoleMasterModel model);

        /// <summary>
        /// Update the Role Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        string UpdateRole(RoleMasterModel model);

        /// <summary>
        /// Disable the Role Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        string DisableRole(RoleMasterModel model);

        /// <summary>
        /// Enable the Role Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        string EnableRole(RoleMasterModel model);

        /// <summary>
        /// Get the RoleDetails Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        List<RoleMenuDetailModel> GetRoleDetails(int Id);
    }
}
