﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class AssociateAttachedInfoModel: AssociateAttachedInfo
    {
        public List<AssociateAttachedInfoModel> AssociateAttachedInfoModelList { get; set; }
        public AssociateAttachedInfoModel()
        {
            AssociateAttachedInfoModelList = new List<AssociateAttachedInfoModel>();
        }
    }
}
