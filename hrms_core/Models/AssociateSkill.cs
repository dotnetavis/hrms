﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class AssociateSkill:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }        //primary key
        [ForeignKey("associate_id")]
        public int associate_id { get; set; }
        [Required(ErrorMessage = "Skill Code Required")]
        public string skillcode { get; set; }
        [Required(ErrorMessage = "Skill Description Required")]
        public string skill_description { get; set; }
        public string remarks { get; set; }
   }
}
