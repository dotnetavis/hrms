﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class ConfigurationModel:Configuration
    {
        public List<ConfigurationModel> ConfigurationList { get; set; }
        public ConfigurationModel()
        {
            ConfigurationList = new List<ConfigurationModel>();
        }
    }
}
