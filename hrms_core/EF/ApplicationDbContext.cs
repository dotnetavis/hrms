﻿using hrms_core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.EF
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            //Database.Migrate();
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // TODO: This is messy, but needed for migrations.
            optionsBuilder.EnableSensitiveDataLogging(true);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ///Default Value Set Here

            #region Configuration
            modelBuilder.Entity<Configuration>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Configuration>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Configuration>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<Configuration>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<Configuration>().Property(b => b.isactive).HasDefaultValueSql("true");

            #endregion

            #region EmailConfiguration
            modelBuilder.Entity<EmailConfiguration>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<EmailConfiguration>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<EmailConfiguration>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<EmailConfiguration>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<EmailConfiguration>().Property(b => b.isactive).HasDefaultValueSql("true");

            #endregion

            #region AssociateAdditionalInfo
            modelBuilder.Entity<AssociateAdditionalInfo>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateAdditionalInfo>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateAdditionalInfo>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociateAdditionalInfo>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociateAdditionalInfo>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region AssociateExperience
            modelBuilder.Entity<AssociateExperience>().Property(b => b.from_date).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateExperience>().Property(b => b.to_date).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateExperience>().Property(b => b.gross_salary).HasDefaultValueSql("0.00");
            modelBuilder.Entity<AssociateExperience>().Property(b => b.carryhome_salary).HasDefaultValueSql("0.00");
            modelBuilder.Entity<AssociateExperience>().Property(b => b.ctc).HasDefaultValueSql("0.00");
            modelBuilder.Entity<AssociateExperience>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateExperience>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateExperience>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociateExperience>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociateExperience>().Property(b => b.isactive).HasDefaultValueSql("true");

            #endregion

            #region AssociateMain
            modelBuilder.Entity<AssociateMain>().Property(b => b.doc).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateMain>().Property(b => b.shift_type).HasDefaultValueSql("2");
            modelBuilder.Entity<AssociateMain>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateMain>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateMain>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociateMain>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociateMain>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region AssociateMaster
            modelBuilder.Entity<AssociateMaster>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateMaster>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateMaster>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociateMaster>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociateMaster>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region AssociatePersonal
            modelBuilder.Entity<AssociatePersonal>().Property(b => b.dob).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociatePersonal>().Property(b => b.anniversary).HasDefaultValueSql("now()");
            //modelBuilder.Entity<AssociatePersonal>().Property(b => b.dob).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociatePersonal>().Property(b => b.permanent_address).HasDefaultValueSql("home");
            modelBuilder.Entity<AssociatePersonal>().Property(b => b.gender).HasDefaultValueSql("Male");
            modelBuilder.Entity<AssociatePersonal>().Property(b => b.maritalstatus).HasDefaultValueSql("Single");
            modelBuilder.Entity<AssociatePersonal>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociatePersonal>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociatePersonal>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociatePersonal>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociatePersonal>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region AssociateRelieve
            modelBuilder.Entity<AssociateRelieve>().Property(b => b.noticedate).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateRelieve>().Property(b => b.date_of_leaving).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateRelieve>().Property(b => b.relieving_letter_issue_date).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateRelieve>().Property(b => b.experience_letter_issue_date).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateRelieve>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateRelieve>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateRelieve>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociateRelieve>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociateRelieve>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region AssociateShift
            modelBuilder.Entity<AssociateShift>().Property(b => b.shiftdate).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateShift>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateShift>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateShift>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociateShift>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociateShift>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region AssociateAttendance
            modelBuilder.Entity<AssociateAttendance>().Property(b => b.attendancedate).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateAttendance>().Property(b => b.ispresent).HasDefaultValueSql("false");
            modelBuilder.Entity<AssociateAttendance>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateAttendance>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<AssociateAttendance>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociateAttendance>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<AssociateAttendance>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region City
            modelBuilder.Entity<City>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<City>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<City>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<City>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<City>().Property(b => b.isactive).HasDefaultValueSql("true");
            modelBuilder.Entity<City>().Property(b => b.citytext).HasDefaultValueSql("Noida");
            #endregion

            #region Country
            modelBuilder.Entity<Country>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Country>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<Country>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<Country>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Country>().Property(b => b.isactive).HasDefaultValueSql("true");
            modelBuilder.Entity<Country>().Property(b => b.countrytext).HasDefaultValueSql("India");
            #endregion

            #region DailyReportDetail
            modelBuilder.Entity<DailyReportDetail>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<DailyReportDetail>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<DailyReportDetail>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<DailyReportDetail>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<DailyReportDetail>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region State
            modelBuilder.Entity<State>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<State>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<State>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<State>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<State>().Property(b => b.isactive).HasDefaultValueSql("true");
            modelBuilder.Entity<State>().Property(b => b.statetext).HasDefaultValueSql("UtterPradesh");
            #endregion

            #region DailyActivityReport
            //modelBuilder.Entity<DailyActivityReport>().Property(b => b.reportingdate).HasDefaultValueSql("now()");
            modelBuilder.Entity<DailyActivityReport>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<DailyActivityReport>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<DailyActivityReport>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<DailyActivityReport>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<DailyActivityReport>().Property(b => b.isactive).HasDefaultValueSql("true");

            #endregion

            #region Department
            modelBuilder.Entity<Department>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Department>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Department>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<Department>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<Department>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region Designation
            modelBuilder.Entity<Designation>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Designation>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Designation>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<Designation>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<Designation>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region EmployeeDashboard
            modelBuilder.Entity<EmployeeDashboard>().Property(b => b.isactive).HasDefaultValueSql("true");
            modelBuilder.Entity<EmployeeDashboard>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<EmployeeDashboard>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<EmployeeDashboard>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<EmployeeDashboard>().Property(b => b.updatedby).HasDefaultValueSql("0");


            #endregion

            #region Fnf
            modelBuilder.Entity<Fnf>().Property(b => b.working_day_month).HasDefaultValueSql("0.00");
            modelBuilder.Entity<Fnf>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Fnf>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Fnf>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<Fnf>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<Fnf>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region Header
            modelBuilder.Entity<Header>().Property(b => b.isactive).HasDefaultValueSql("true");
            modelBuilder.Entity<Header>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Header>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Header>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<Header>().Property(b => b.updatedby).HasDefaultValueSql("0");


            #endregion

            #region HeaderAssign
            modelBuilder.Entity<HeaderAssign>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<HeaderAssign>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<HeaderAssign>().Property(b => b.isactive).HasDefaultValueSql("true");
            modelBuilder.Entity<HeaderAssign>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<HeaderAssign>().Property(b => b.updatedby).HasDefaultValueSql("0");

            #endregion

            #region HolidayCalender
            modelBuilder.Entity<HolidayCalender>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<HolidayCalender>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<HolidayCalender>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<HolidayCalender>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<HolidayCalender>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region HolidayCalenderDates
            modelBuilder.Entity<HolidayCalenderDates>().Property(b => b.holiday_date).HasDefaultValueSql("now()");
            modelBuilder.Entity<HolidayCalenderDates>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<HolidayCalenderDates>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<HolidayCalenderDates>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<HolidayCalenderDates>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<HolidayCalenderDates>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region LeaveApplication
            modelBuilder.Entity<LeaveApplication>().Property(b => b.notificationdate).HasDefaultValueSql("now()");
            modelBuilder.Entity<LeaveApplication>().Property(b => b.startdate).HasDefaultValueSql("now()");
            modelBuilder.Entity<LeaveApplication>().Property(b => b.enddate).HasDefaultValueSql("now()");
            modelBuilder.Entity<LeaveApplication>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<LeaveApplication>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<LeaveApplication>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<LeaveApplication>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<LeaveApplication>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region LeaveTypeMaster
            modelBuilder.Entity<LeaveTypeMaster>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<LeaveTypeMaster>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<LeaveTypeMaster>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<LeaveTypeMaster>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<LeaveTypeMaster>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region NoticePeriod
            modelBuilder.Entity<NoticePeriod>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<NoticePeriod>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<NoticePeriod>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<NoticePeriod>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<NoticePeriod>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region OdDetail
            modelBuilder.Entity<OdDetail>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<OdDetail>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<OdDetail>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<OdDetail>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<OdDetail>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region OpeningLeave
            modelBuilder.Entity<OpeningLeave>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<OpeningLeave>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<OpeningLeave>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<OpeningLeave>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<OpeningLeave>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region PayElement
            modelBuilder.Entity<PayElement>().Property(b => b.basic).HasDefaultValueSql("0.00");
            modelBuilder.Entity<PayElement>().Property(b => b.hra).HasDefaultValueSql("0.00");
            modelBuilder.Entity<PayElement>().Property(b => b.special_skill_allowance).HasDefaultValueSql("0.00");
            modelBuilder.Entity<PayElement>().Property(b => b.mobile_allowance).HasDefaultValueSql("0.00");
            modelBuilder.Entity<PayElement>().Property(b => b.conveyance_allowance).HasDefaultValueSql("0.00");
            modelBuilder.Entity<PayElement>().Property(b => b.management_allowance).HasDefaultValueSql("0.00");
            modelBuilder.Entity<PayElement>().Property(b => b.book_periodicals).HasDefaultValueSql("0.00");
            modelBuilder.Entity<PayElement>().Property(b => b.driver_allowance).HasDefaultValueSql("0.00");
            modelBuilder.Entity<PayElement>().Property(b => b.medical_allowance).HasDefaultValueSql("0.00");
            modelBuilder.Entity<PayElement>().Property(b => b.gross_salary).HasDefaultValueSql("0.00");
            modelBuilder.Entity<PayElement>().Property(b => b.pf).HasDefaultValueSql("0.00");
            modelBuilder.Entity<PayElement>().Property(b => b.esi).HasDefaultValueSql("0.00");
            modelBuilder.Entity<PayElement>().Property(b => b.total_deduction).HasDefaultValueSql("0.00");
            modelBuilder.Entity<PayElement>().Property(b => b.carry_home_salary).HasDefaultValueSql("0.00");
            modelBuilder.Entity<PayElement>().Property(b => b.ctc).HasDefaultValueSql("0.00");
            modelBuilder.Entity<PayElement>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<PayElement>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<PayElement>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<PayElement>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<PayElement>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region PaySlipAmount
            modelBuilder.Entity<PayslipAmount>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<PayslipAmount>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<PayslipAmount>().Property(b => b.isactive).HasDefaultValueSql("true");
            modelBuilder.Entity<PayslipAmount>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<PayslipAmount>().Property(b => b.updatedby).HasDefaultValueSql("0");
            #endregion

            #region PageAssign
            modelBuilder.Entity<PageAssign>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<PageAssign>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<PageAssign>().Property(b => b.isactive).HasDefaultValueSql("true");
            modelBuilder.Entity<PageAssign>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<PageAssign>().Property(b => b.updatedby).HasDefaultValueSql("0");
            #endregion

            #region PageMenu
            modelBuilder.Entity<PageMenu>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<PageMenu>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<PageMenu>().Property(b => b.isactive).HasDefaultValueSql("true");
            modelBuilder.Entity<PageMenu>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<PageMenu>().Property(b => b.updatedby).HasDefaultValueSql("0");
            #endregion

            #region Policy
            modelBuilder.Entity<Policy>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Policy>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Policy>().Property(b => b.isactive).HasDefaultValueSql("true");
            modelBuilder.Entity<Policy>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<Policy>().Property(b => b.updatedby).HasDefaultValueSql("0");
            #endregion
            #region ProjectMaster
            modelBuilder.Entity<ProjectMaster>().Property(b => b.datestart).HasDefaultValueSql("now()");
            modelBuilder.Entity<ProjectMaster>().Property(b => b.expectedcompletiondate).HasDefaultValueSql("now()");
            modelBuilder.Entity<ProjectMaster>().Property(b => b.completiondate).HasDefaultValueSql("now()");
            modelBuilder.Entity<ProjectMaster>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<ProjectMaster>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<ProjectMaster>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<ProjectMaster>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<ProjectMaster>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion
            #region profilepage
            modelBuilder.Entity<ProfilePage>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<ProfilePage>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<ProfilePage>().Property(b => b.isactive).HasDefaultValueSql("true");
            modelBuilder.Entity<ProfilePage>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<ProfilePage>().Property(b => b.updatedby).HasDefaultValueSql("0");
            #endregion

            #region Qualification
            modelBuilder.Entity<Qualification>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Qualification>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Qualification>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<Qualification>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<Qualification>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region ReasonOfLeaving
            modelBuilder.Entity<ReasonOfLeaving>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<ReasonOfLeaving>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<ReasonOfLeaving>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<ReasonOfLeaving>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<ReasonOfLeaving>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region SalarySlip
            modelBuilder.Entity<SalarySlip>().Property(b => b.payable_days).HasDefaultValueSql("0.00");
            modelBuilder.Entity<SalarySlip>().Property(b => b.carry_home_salary).HasDefaultValueSql("0.00");
            modelBuilder.Entity<SalarySlip>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<SalarySlip>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<SalarySlip>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<SalarySlip>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<SalarySlip>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region Shift
            modelBuilder.Entity<Shift>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Shift>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Shift>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<Shift>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<Shift>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region TaskAssign
            modelBuilder.Entity<TaskAssign>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<TaskAssign>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<TaskAssign>().Property(b => b.isactive).HasDefaultValueSql("true");
            modelBuilder.Entity<TaskAssign>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<TaskAssign>().Property(b => b.updatedby).HasDefaultValueSql("0");
            #endregion
            #region TaskMaster
            modelBuilder.Entity<TaskMaster>().Property(b => b.assigneddate).HasDefaultValueSql("now()");
            modelBuilder.Entity<TaskMaster>().Property(b => b.expectedcompletiondate).HasDefaultValueSql("now()");
            modelBuilder.Entity<TaskMaster>().Property(b => b.actualcompletiondate).HasDefaultValueSql("now()");
            modelBuilder.Entity<TaskMaster>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<TaskMaster>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<TaskMaster>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<TaskMaster>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<TaskMaster>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region User
            modelBuilder.Entity<User>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<User>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<User>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<User>().Property(b => b.updatedby).HasDefaultValueSql("0");
            modelBuilder.Entity<User>().Property(b => b.isactive).HasDefaultValueSql("true");
            #endregion

            #region RoleMaster
            modelBuilder.Entity<RoleMaster>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<RoleMaster>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<RoleMaster>().Property(b => b.isactive).HasDefaultValueSql("true");
            modelBuilder.Entity<RoleMaster>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<RoleMaster>().Property(b => b.updatedby).HasDefaultValueSql("0");

            #endregion

            #region Resignation
            modelBuilder.Entity<Resignation>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Resignation>().Property(b => b.updatedon).HasDefaultValueSql("now()");
            modelBuilder.Entity<Resignation>().Property(b => b.isactive).HasDefaultValueSql("true");
            modelBuilder.Entity<Resignation>().Property(b => b.createdby).HasDefaultValueSql("0");
            modelBuilder.Entity<Resignation>().Property(b => b.updatedby).HasDefaultValueSql("0");

            #endregion

        }
        public DbSet<AttendanceChanges> attendancechanges { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        public DbSet<EmailConfiguration> EmailConfigurations { get; set; }
        public DbSet<AssociateAdditionalInfo> AssociateAdditionalInfos { get; set; }
        public DbSet<AssociateAttachedInfo> AssociateAttachedInfos { get; set; }
        //public dbset<associateattendance> associateattendances { get; set; }
        public DbSet<AssociateExperience> AssociateExperiences { get; set; }
        public DbSet<AssociateMain> AssociateMains { get; set; }
        public DbSet<AssociateMaster> AssociateMasters { get; set; }
        public DbSet<AssociatePersonal> AssociatePersonals { get; set; }
        public DbSet<CompanyPolicy> CompanyPolicy { get; set; }
        public DbSet<AssociateQualification> AssociateQualifications { get; set; }
        public DbSet<AssociateRelieve> AssociateRelieves { get; set; }
        public DbSet<AssociateShift> AssociateShifts { get; set; }
        public DbSet<AssociateSkill> AssociateSkills { get; set; }
        public DbSet<City> Citys { get; set; }
        public DbSet<Country> Countrys { get; set; }
        public DbSet<DailyActivityReport> DailyActivityReports { get; set; }
        public DbSet<DailyReportDetail> DailyReportDetails { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Designation> Designations { get; set; }
        public DbSet<EmployeeDashboard> EmployeeDashboards { get; set; }
        public DbSet<Fnf> Fnfs { get; set; }
        public DbSet<Header> Headers { get; set; }
        public DbSet<HeaderAssign> HeaderAssigns { get; set; }
        public DbSet<HolidayCalender> HolidayCalenders { get; set; }
        public DbSet<HolidayCalenderDates> HolidayCalenderDates { get; set; }
        public DbSet<LeaveApplication> LeaveApplications { get; set; }
        public DbSet<LeaveAppMain> LeaveAppMains { get; set; }
        public DbSet<LeaveTypeMaster> LeaveTypeMasters { get; set; }
        public DbSet<NoticePeriod> NoticePeriods { get; set; }
        public DbSet<OdDetail> OdDetails { get; set; }
        public DbSet<OpeningLeave> OpeningLeaves { get; set; }
        public DbSet<PageAssign> PageAssigns { get; set; }
        public DbSet<PageMenu> PageMenus { get; set; }
        public DbSet<PayElement> PayElements { get; set; }
        public DbSet<Payslip> Payslips { get; set; }
        public DbSet<PayslipAmount> PayslipAmounts { get; set; }
        public DbSet<Policy> Policy { get; set; }
        public DbSet<ProfilePage> ProfilePages { get; set; }
        public DbSet<ProjectMaster> Policys { get; set; }
        public DbSet<ProjectMaster> ProjectMasters { get; set; }
        public DbSet<Qualification> Qualifications { get; set; }
        public DbSet<ReasonOfLeaving> ReasonOfLeavings { get; set; }
        public DbSet<SalarySlip> SalarySlips { get; set; }
        public DbSet<Shift> Shifts { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<TaskMaster> TaskMasters { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<RoleMaster> role_master { get; set; }
        public DbSet<RoleMenuDetails> role_menu_details { get; set; }
        public DbSet<TaskAssign> TaskAssigns { get; set; }
        public DbSet<DepartmentHODAssign> departmentHODAssigns { get; set; }
        public DbSet<ProjectDeptAssign> projectdeptassigns { get; set; }
        public DbSet<LoginHistory> LoginHistory { get; set; }
        public DbSet<Attendancedata> attendancedata { get; set; }
        public DbSet<Runninginfo> runninginfo { get; set; }
        public DbSet<ConferenceRoom> conferenceroom { get; set; }
        public DbSet<Resignation> resignations { get; set; }
    }
    public class SessionVariables
    {
        public const string SessionData = "SessionData";
    }


}
