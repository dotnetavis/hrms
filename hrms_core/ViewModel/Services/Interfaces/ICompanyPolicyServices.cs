﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.ViewModel;
namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface ICompanyPolicyServices
    {
        List<CompanyPolicyModel> GetPolicies();
        List<CompanyPolicyModel> GetIsActivePolicies();
        string SavePolicy(CompanyPolicyModel policy);
        string UpdatePolicy(CompanyPolicyModel policy);
        string DisablePolicy(int policy_id);
        string Enablepolicy(int policy_id);
        CompanyPolicyModel getPolicyById(int? id);
    }
}
