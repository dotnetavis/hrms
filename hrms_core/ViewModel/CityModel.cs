﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class CityModel: City
    {
        public List<CityModel> CityModelList { get; set; }
        public List<StateModel> StateModelList { get; set; }
        public string state_text { get; set; }
        public CityModel()
        {
            StateModelList = new List<StateModel>();
            CityModelList = new List<CityModel>();
        }
    }
}
