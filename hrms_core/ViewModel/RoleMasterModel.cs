﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class RoleMasterModel : RoleMaster
    {
        public List<RoleMasterModel> RoleMasterModelList { get; set; }
        public List<RoleMenuDetailModel> RoleMenuDetailModelList { get; set; }
        public virtual PageMenu PageMenu { get; set; }
        public List<PageMenuModel> PageMenuList { get; set; }
        public bool is_selected { get; set; }
        public PageMenuModel[] pageMenuModels { get; set; }
        public RoleMasterModel()
        {
            RoleMasterModelList = new List<RoleMasterModel>();
            PageMenuList = new List<PageMenuModel>();
        }
    }
}
