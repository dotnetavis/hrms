﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace hrms_core.Migrations
{
    public partial class policy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           
            migrationBuilder.CreateTable(
                name: "CompanyPolicy",
                columns: table => new
                {
                    createdon = table.Column<DateTime>(nullable: false),
                    updatedon = table.Column<DateTime>(nullable: false),
                    createdby = table.Column<int>(nullable: false),
                    updatedby = table.Column<int>(nullable: false),
                    createdbyname = table.Column<string>(nullable: true),
                    updatedbyname = table.Column<string>(nullable: true),
                    isactive = table.Column<bool>(nullable: true),
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    policy_content = table.Column<string>(nullable: true),
                    policy_year = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyPolicy", x => x.id);
                });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
            migrationBuilder.DropTable(
                name: "CompanyPolicy");

        }
    }
}
