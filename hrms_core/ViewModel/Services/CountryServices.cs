﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class CountryServices : ICountryServices
    {
        private readonly ApplicationDbContext _db;
        public CountryServices(ApplicationDbContext options) => _db = options;
        /// <summary>
        /// Get the Country Data,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public List<CountryModel> GetCountry()
        {
            CountryModel model = new CountryModel();
            try
            {
                model.CountryModelList = _db.Countrys.Where(x=>x.isactive == true)
                                       .Select(x => new CountryModel
                                       {
                                           id = x.id,
                                           countrytext = x.countrytext,
                                           isactive = x.isactive,
                                           createdon = x.createdon,
                                           createdby = x.createdby,
                                           updatedon = x.updatedon,
                                           updatedby = x.updatedby
                                       }).OrderByDescending(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.CountryModelList;
        }

        public string SaveCountry(CountryModel model)
        {
            string Result = string.Empty;
            Country country = new Country();

            try
            {
                bool sameName = _db.Countrys.Any(x => x.countrytext.ToLower().Trim() == model.countrytext.ToLower().Trim());
                if (sameName)
                {
                    Result = MessageHelper.Country_Duplicate;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            country.countrytext = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(model.countrytext.ToLower()); ;
                            country.createdby = model.createdby;
                            country.createdbyname = model.createdbyname;
                            country.createdon = DateTime.Now;
                            country.updatedby = model.updatedby;
                            country.updatedbyname = model.updatedbyname;
                            country.updatedon = DateTime.Now;
                            country.isactive = true;
                            _db.Countrys.Add(country);
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Country_AddSuccess;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.Country_AddError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Country_AddError;
            }
            return Result;
        }

        public string UpdateCountry(CountryModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Countrys.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    bool same_name = _db.Countrys.Any(x => x.countrytext.ToLower().Trim() == model.countrytext.ToLower().Trim() &&  x.id!=model.id);
                    if (same_name)
                    {
                        Result = MessageHelper.Country_Duplicate;
                    }
                    else
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            try
                            {
                                data.countrytext = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(model.countrytext.ToLower()); ;
                                data.createdby = model.createdby;
                                data.createdbyname = model.createdbyname;
                                data.createdon = DateTime.Now;
                                data.updatedby = model.updatedby;
                                data.updatedbyname = model.updatedbyname;
                                data.updatedon = DateTime.Now;
                                data.isactive = true;
                                _db.SaveChanges();
                                scope.Complete();
                                Result = MessageHelper.Country_AddSuccess;
                            }
                            catch (Exception ex)
                            {
                                scope.Dispose();
                                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                Result = MessageHelper.Country_AddError;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Country_AddError;
            }
            return Result;
        }
        /// <summary>
        /// Get the Country Data Only Active,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public List<CountryModel> GetCountry_Active()
        {
            CountryModel model = new CountryModel();
            try
            {
                model.CountryModelList = _db.Countrys.Where(x => x.isactive == true)
                                       .Select(x => new CountryModel
                                       {
                                           id = x.id,
                                           countrytext = x.countrytext,
                                           isactive = x.isactive,
                                           createdon = x.createdon,
                                           createdby = x.createdby,
                                           updatedon = x.updatedon,
                                           updatedby = x.updatedby
                                       }).OrderBy(x => x.countrytext).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.CountryModelList;
        }
        public string EnableCountry(CountryModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Countrys.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = true;
                            data.updatedon = DateTime.Now;
                            data.updatedby = model.updatedby;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Country_Enable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }

        public string DisableCountry(CountryModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Countrys.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = false;
                            data.updatedby = model.updatedby;
                            data.updatedon = DateTime.Now;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Country_Disable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }
        public CountryModel GetActivityById(int ID)
        {
            CountryModel obj_country = new CountryModel();
            try
            {
                var Data = _db.Countrys.FirstOrDefault(x => x.id == ID);
                if (Data != null)
                {
                    obj_country.id = Data.id;
                    obj_country.countrytext = Data.countrytext;
                    obj_country.createdby = Data.createdby;
                    obj_country.createdbyname = Data.createdbyname;
                    obj_country.updatedby = Data.updatedby;
                    obj_country.updatedbyname = Data.updatedbyname;
                    obj_country.updatedon = Data.updatedon;
                    obj_country.createdon = Data.createdon;
                    obj_country.isactive = true;
                }
                return obj_country;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return null;
            }

        }

    }
}