﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class PageMenuModel: PageMenu
    {
        public List<PageMenuModel> PageMenuModelList { get; set; }
        public bool is_selected { get; set; }
        //public int role_id { get; set; }
        public PageMenuModel()
        {
            PageMenuModelList = new List<PageMenuModel>();
        }
    }
}
