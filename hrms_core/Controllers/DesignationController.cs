﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace hrms_core.Controllers
{
    public class DesignationController : Controller
    {
        private readonly ApplicationDbContext _db;

        private IDesignationServices DesignationServices => new DesignationServices(_db);

        public DesignationController(ApplicationDbContext db) => _db = db;
        [HttpGet]
        public IActionResult Index(int? id)
        {
            DesignationModel model = new DesignationModel();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.user_id;
                model.updatedby = SesMdl.user_id;
                model.createdbyname = SesMdl.username;
                model.updatedbyname = SesMdl.username;
                model.DesignationModelList = DesignationServices.GetDesignation();
               if (id > 0 && id != null)
               {
                 if (model.DesignationModelList.Count > 0 && model.DesignationModelList != null)
                 {
                    var data = model.DesignationModelList.FirstOrDefault(x => x.id == id);
                    if (data != null)
                    {
                        model.id = data.id;
                        model.designationtext = data.designationtext;
                        model.designationdesc = data.designationdesc;
                        model.isactive = data.isactive;
                    }
                 }
               }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult Index(DesignationModel model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.user_id;
                model.updatedby = SesMdl.user_id;
                model.createdbyname = SesMdl.username;
                model.updatedbyname = SesMdl.username;
                if (model.id > 0)
                {
                    TempData["msg"] = DesignationServices.UpdateDesignation(model);
                }
                else
                {
                    TempData["msg"] = DesignationServices.SaveDesignation(model);
                }
              
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("Index", "Designation", new { id = 0 });
        }
        [HttpPost]
        public IActionResult DisableDesignation(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            DesignationModel model = new DesignationModel
            {
                id = id,
                updatedby = 1
            };
            TempData["msg"] = DesignationServices.DisableDesignation(model);
            return Json(true);
        }
        [HttpPost]
        public IActionResult EnableDesignation(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            DesignationModel model = new DesignationModel
            {
                id = id,
                updatedby = 1
            };
            TempData["msg"] = DesignationServices.EnableDesignation(model);
            return Json(true);
        }
    }
}