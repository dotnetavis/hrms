﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class RoleMasterVM
    {
        public RoleMasterModel RoleMasterModel { get; set; }
        public RoleMenuDetailModel RoleMenuDetailModel { get; set; }
        public List<RoleMasterVM> RoleMasterVMList { get; set; }
        public RoleMasterVM()
        {
            RoleMasterModel = new RoleMasterModel();
            RoleMenuDetailModel = new RoleMenuDetailModel();
            RoleMasterVMList = new List<RoleMasterVM>();
        }
    }
}
