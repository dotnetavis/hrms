﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.Models;

namespace hrms_core.ViewModel
{
    public class ConferenceRoomModel:ConferenceRoom
    {
        public string TakenBy { get; set; }
        public List<ConferenceRoom> conferenceEvents { get; set; }
        public ConferenceRoomModel()
        {
            conferenceEvents = new List<ConferenceRoom>();
        }
    }
}
