﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class PayslipModel: Payslip
    {
        public List<AssociateMasterModel> AssociateMasterModelList { get; set; }
        public List<PayslipModel> PayslipModelList { get; set; }
        public PayslipModel()
        {
            AssociateMasterModelList = new List<AssociateMasterModel>();
            PayslipModelList = new List<PayslipModel>();
        }

    }
}
