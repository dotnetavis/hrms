﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.Models;
namespace hrms_core.ViewModel
{
    public class AttendanceDataVm : Attendancedata
    {
        public List<AttendanceDataVm> AttendanceDataVmList { get; set; }
        public string username { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string department { get; set; }
        public string associateName { get; set; }
        public bool is_selected { get; set; }
        //public int associate_id { get; set; }
        public List<AssociateMasterModel> ListUserName { get; set; }
        public List<DateTime> Dates { get; set; }
        public List<String> LoginTimes { get; set; }
        public List<AssociateMasterModel> associateMasters { get; set; }
        public AttendanceDataVm()
        {
            ListUserName = new List<AssociateMasterModel>();
            LoginTimes = new List<string>();
            Dates = new List<DateTime>();
            AttendanceDataVmList = new List<AttendanceDataVm>();
            associateMasters = new List<AssociateMasterModel>();
        }
    }
}
