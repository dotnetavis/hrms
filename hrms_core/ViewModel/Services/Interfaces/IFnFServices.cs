﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    interface IFnFServices
    {
       void getListForFNF();
       void updateResume(int associate_id,string username,int associate_code);
       void insertFullnFinal(int associate_id,string username, int associate_code);
    }
}
