﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class LeaveTypeMasterModel : LeaveTypeMaster
    {
        public List<LeaveTypeMasterModel> LeaveTypeMasterModelList { get; set; }
        public int applicable_leave { get; set; }
        public double opening_leave { get; set; }
        public double availed_leaves { get; set; }
        public double balance_leave { get; set; }
        public LeaveTypeMasterModel()
        {
            LeaveTypeMasterModelList = new List<LeaveTypeMasterModel>();
        }
    }
}
