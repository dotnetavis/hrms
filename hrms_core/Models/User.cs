﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class User:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage = "Enter Username")]
        public string username { get; set; }
        [Required(ErrorMessage = "Enter Password")]
        public string userpass { get; set; }
        public string guid { get; set; }
    }
}

    
        