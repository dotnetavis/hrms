﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class NoticePeriod:CommonAttributeModel
    {
        [Key]
        public int id {get; set; }      
        public DateTime notice_start_date { get; set; }
        public DateTime notice_end_date { get; set; }
        public bool isrenew{ get; set; }
        public bool isfnf { get; set; }
        [ForeignKey("associate_id")]
        public int associate_id { get; set; }
    }
}

   