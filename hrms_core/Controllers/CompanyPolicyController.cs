﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using hrms_core.EF;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using hrms_core.ViewModel;
namespace hrms_core.Controllers
{
    public class CompanyPolicyController : Controller
    {
        public ICompanyPolicyServices companyPolicyServices => new CompanyPolicyServices(_db);
        public ApplicationDbContext _db;
        public CompanyPolicyController(ApplicationDbContext _Db)
        {
            _db = _Db;
        }
        [HttpGet]
        public IActionResult Index(int? id)
       {
            CompanyPolicyModel model = new CompanyPolicyModel();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                if (id != null && id!=0 )
                {
                    model = companyPolicyServices.getPolicyById(id);
                }
                if (model != null)
                {
                    model.policies = companyPolicyServices.GetPolicies();
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

            return View(model);
        }
        [HttpPost]
        public IActionResult Index(CompanyPolicyModel model, string CommandName)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);

            if (SesMdl != null)
            {

                if (CommandName == "Save")
                {
                    model.createdby = SesMdl.user_id;
                    model.createdbyname = SesMdl.username;
                    model.updatedby = SesMdl.user_id;
                    model.updatedbyname = SesMdl.username;
                    model.updatedon = DateTime.Now;
                    model.createdon = DateTime.Now;
                    TempData["msg"] = companyPolicyServices.SavePolicy(model);
                }
                else if (CommandName == "Update")
                {
                    model.updatedby = SesMdl.user_id;
                    model.updatedbyname = SesMdl.username;
                    model.updatedon = DateTime.Now;
                    TempData["msg"] = companyPolicyServices.UpdatePolicy(model);

                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("Index", "CompanyPolicy", new { id = 0 });
        }
        public IActionResult EnablePolicy(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                TempData["msg"] = companyPolicyServices.Enablepolicy(id);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return Json(true);
        }

        public IActionResult DisablePolicy(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                TempData["msg"] = companyPolicyServices.DisablePolicy(id);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return Json(true);
        }
        public IActionResult AssociateView(int? id)
        {
            CompanyPolicyModel model = new CompanyPolicyModel();
            if (id!=null && id != 0)
            {
                model = companyPolicyServices.getPolicyById(id);
            }
                model.policies = companyPolicyServices.GetIsActivePolicies();
            return View(model);
        }
        [HttpPost]
        public IActionResult GetPolicyById(int PolicyId)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            var data = companyPolicyServices.getPolicyById(PolicyId);
            return Json(data);
        }
    }
}