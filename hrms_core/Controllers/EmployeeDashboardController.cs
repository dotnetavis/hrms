﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.IO;
namespace hrms_core.Controllers
{
    public class EmployeeDashboardController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IEmployeeDashboardServices employeeServices => new EmployeeDashboardServices(_db);
        private IDepartmentServices departmentServices => new departmentServices(_db);
        private IAssociateServices associateServices => new AssociateServices(_db);
        public EmployeeDashboardController(ApplicationDbContext options) => _db = options;

        [HttpGet]
        public IActionResult Employee(int? id)
        {
            EmployeeDashboardModel model = new EmployeeDashboardModel();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.user_id;
                model.updatedby = SesMdl.user_id;
                model.createdbyname = SesMdl.username;
                model.updatedbyname = SesMdl.username;
                model.DepartmentModelList = departmentServices.GetDepartment_Active();
                model.DepartmentModelList.Insert(0, new DepartmentModel { id = 0, departmenttext = "--Select--" });
                model.AssociateMasterModelList = associateServices.GetAssociateList_Active();
                model.AssociateMasterModelList.Insert(0, new AssociateMasterModel { id = 0, associate_name = "--Select--" });
                model.EmployeeDashboardModelList = employeeServices.GetEmployeeDetails();
                if (id > 0 && id != null)
                {
                    if (model.EmployeeDashboardModelList.Count > 0 && model.EmployeeDashboardModelList != null)
                    {
                        var data = model.EmployeeDashboardModelList.FirstOrDefault(x => x.id == id);
                        if (data != null)
                        {
                            model.id = data.id;
                            model.awardname = data.awardname;
                            model.employee_id = data.employee_id;
                            model.designation_text = data.designation_text;
                            model.department_id = data.department_id;
                            model.remark = data.remark;
                            model.imagepath = data.imagepath;
                            model.isactive = data.isactive;
                            model.createdby = data.createdby;
                            model.updatedby = model.updatedby;
                            model.updatedon = model.updatedon;
                        }
                    }
                }
                foreach (var item in model.EmployeeDashboardModelList)
                {
                    item.employee_name = _db.AssociateMasters.Where(x => x.id == item.employee_id).FirstOrDefault().associate_name;
                    item.department_name = _db.Departments.Where(x => x.id == item.department_id).FirstOrDefault().departmenttext;
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult Employee(EmployeeDashboardModel model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.user_id;
                model.updatedby = SesMdl.user_id;
                model.createdbyname = SesMdl.username;
                model.updatedbyname = SesMdl.username;
                if (model.id > 0)
                {
                    if (Request.Form.Files["userPhoto"].Length > 0)
                    {
                        var filename = Path.GetFileName(Request.Form.Files["userPhoto"].FileName);
                        var _FileName = Path.GetFileNameWithoutExtension(Request.Form.Files["userPhoto"].FileName);
                        Guid objGuid = Guid.NewGuid();
                        _FileName = _FileName + '_' + Convert.ToString(objGuid);
                        string extension = Path.GetExtension(filename).ToLower();
                        model.imagepath = _FileName + extension;
                        string[] validFileTypes = { ".png", ".jpg", ".jpeg", ".gif" };
                        var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload", model.imagepath);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload"));
                        }
                        if (validFileTypes.Contains(extension))
                        {
                            if (System.IO.File.Exists(path))
                            {
                                System.IO.File.Delete(path);
                            }
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                Request.Form.Files["userPhoto"].CopyTo(stream);
                            }
                        }
                        else
                        {
                            TempData["msg"] = "Invalid file type";
                            return RedirectToAction("Employee", "EmployeeDashboard");
                        }
                    }
                    TempData["msg"] = employeeServices.UpdateEmployeeDeatails(model);
                }
                else
                {
                    if (Request.Form.Files["userPhoto"].Length > 0)
                    {
                        var filename = Path.GetFileName(Request.Form.Files["userPhoto"].FileName);
                        var _FileName = Path.GetFileNameWithoutExtension(Request.Form.Files["userPhoto"].FileName);
                        Guid objGuid = Guid.NewGuid();
                        _FileName = _FileName + '_' + Convert.ToString(objGuid);
                        string extension = Path.GetExtension(filename).ToLower();
                        model.imagepath = _FileName + extension;
                        string[] validFileTypes = { ".png", ".jpg", ".jpeg", ".gif" };
                        var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload", model.imagepath);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload"));
                        }
                        if (validFileTypes.Contains(extension))
                        {
                            if (System.IO.File.Exists(path))
                            {
                                System.IO.File.Delete(path);

                            }
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                Request.Form.Files["userPhoto"].CopyTo(stream);
                            }
                        }
                        else
                        {
                            TempData["msg"] = "Invalid file type";
                            return RedirectToAction("Employee", "EmployeeDashboard");
                        }
                    }
                    TempData["msg"] = employeeServices.SaveEmployeeDetails(model);
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("Employee", "EmployeeDashboard", new { id = 0 });
        }
        [HttpPost]
        public IActionResult GetEmployeeByDepartmentId(int DepartmentId)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            EmployeeDashboardModel model = new EmployeeDashboardModel();
            model.AssociateMasterModelList = employeeServices.GetEmployeeByDepartmentId(DepartmentId);
            return Json(model.AssociateMasterModelList);
        }
        [HttpPost]
        public IActionResult GetDesignationByAssociateId(int AssociateId)
        {
            
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            EmployeeDashboardModel model = new EmployeeDashboardModel();
            var Data = employeeServices.GetDesignationByAssociateId(AssociateId);
            return Json(Data);
        }
        [HttpPost]
        public IActionResult DeleteAward(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            EmployeeDashboardModel model = new EmployeeDashboardModel();
            model.id = id;
            TempData["msg"] = employeeServices.DisableEmployee(model);
            return Json(true);
        }
        [HttpPost]
        public IActionResult GetEmployeeDashboardDataById(int EmpDashboardId)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            EmployeeDashboardModel model = new EmployeeDashboardModel();
            model.id = EmpDashboardId;
            var data = employeeServices.GetEmployeeDetails();
            if (data != null && data.Count > 0)
            {
                model.EmployeeDashboardModelList = data.Where(x => x.id == EmpDashboardId).ToList();
            }
            return Json(model.EmployeeDashboardModelList);
        }
    }
}