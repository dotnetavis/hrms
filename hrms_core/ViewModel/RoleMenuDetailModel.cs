﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class RoleMenuDetailModel: RoleMenuDetails
    {
        public List<RoleMenuDetailModel> RoleMenuDetailModelList { get; set; }
        public virtual RoleMaster RoleMasters { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string ModuleName { get; set; }
        public string MenuName { get; set; }
        //public bool is_selected { get; set; }

        public RoleMenuDetailModel()
        {
            RoleMenuDetailModelList = new List<RoleMenuDetailModel>();
        }
    }
}
