﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Transactions;

namespace hrms_core.Controllers
{
    public class PaymentController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IHeaderServices headerservices => new HeaderServices(_db);
        public IAssociateServices associateServices => new AssociateServices(_db);
        public PaymentController(ApplicationDbContext db) => _db = db;


        [HttpGet]
        public IActionResult Index(int? id)
        {
            HeaderModel model = new HeaderModel();
           
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                model.HeaderModelList = headerservices.GetHeader();
                model.AssociateList = headerservices.GetAllAssociates();
                model.AssociateList.Insert(0, new AssociateMasterModel { id = 0, associate_name = "--Select--" });


                model.PayElementList = headerservices.GetPayElementList();
                if (id > 0 && id != null)
                {
                    model.HeaderModelList = headerservices.GetHeader();

                    model.AssociateList = headerservices.GetAllAssociates();
                    model.AssociateList.Insert(0, new AssociateMasterModel { id = 0, associate_name = "--Select--" });
                    if (model.HeaderModelList.Count > 0 && model.HeaderModelList != null)
                    {
                        var data = model.HeaderModelList.FirstOrDefault(x => x.id == id);
                        if (data != null)
                        {
                            model.id = data.id;
                            model.headername = data.headername;
                            model.isdeduction = data.isdeduction;
                            model.isactive = data.isactive;
                        }
                        model.PayElementList = new List<PayElementModel>();
                        model.PayElementList = headerservices.GetPayElementList();
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult Index(HeaderModel model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (model.id > 0)
                TempData["msg"] = headerservices.UpdateHeader(model);
            else
                TempData["msg"] = headerservices.SaveHeader(model);
            return RedirectToAction("Index", "Payment", new { id = 0 });
        }
       
        [HttpPost]
        public IActionResult EnableHeader(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            HeaderModel model = new HeaderModel
            {
                id = id,
                updatedby = 1
            };
            TempData["msg"] = headerservices.EnableHeader(model);
            return Json(true);
        }
        [HttpPost]
        public IActionResult DisableHeader(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            HeaderModel model = new HeaderModel
            {
                id = id,
                updatedby = 1
            };
            TempData["msg"] = headerservices.DisableHeader(model);
            return Json(true);
        }

        [HttpGet]
        public IActionResult HeaderAssign(int AssociateId)
        {
            HeaderAssignModel model = new HeaderAssignModel();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            model.HeaderModelList = headerservices.GetHeader();
            if (AssociateId > 0)
            {
                var data = _db.HeaderAssigns.Where(x => x.associate_id == AssociateId).ToList();
                if (data != null)
                {
                    foreach (var item in model.HeaderModelList)
                    {
                        item.is_selected = data.FirstOrDefault(x => x.header_id == item.id).is_selected;
                    }
                }

            }
            var associate = _db.AssociateMasters.FirstOrDefault(x => x.id == AssociateId);
            if (associate != null)
            {
                model.associate_name = associate.associate_name;
                model.associate_id = associate.id;
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult HeaderAssign(HeaderAssignModel model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            bool result = _db.HeaderAssigns.Any(x => x.associate_id == model.associate_id);
            if (result)
            {
                TempData["msg"] = headerservices.Update_HeaderAssign(model);
            }
            else
            {
                TempData["msg"] = headerservices.Save_HeaderAssign(model);
            }
            return RedirectToAction("AssociateBucket", "Associate");
        }

        [HttpGet]
        public IActionResult PaySlip(int? id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            PayslipModel model = new PayslipModel();
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            model.AssociateMasterModelList = associateServices.GetAssociateList();
            return View(model);
        }

        [HttpPost]
        public IActionResult PaySlip(PayslipModel model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("PaySlip", "Payment");
        }

        public PartialViewResult PartialPaySlip()
        {
            //TODO : Check the Code

            PayslipModel model = new PayslipModel();
            HeaderModel header = new HeaderModel();
            header.HeaderModelList = headerservices.GetHeaderList();

            return PartialView("_PaySlip", model);
        }
        public IActionResult Payment()
        {
            return View();
        }
    }
}
