﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class ShiftModel: Shift
    {
        public List<ShiftModel> ShiftModelList { get; set; }
        public ShiftModel()
        {
            ShiftModelList = new List<ShiftModel>();
        }
    }
}
