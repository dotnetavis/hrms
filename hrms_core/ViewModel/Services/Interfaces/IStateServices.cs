﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IStateServices
    {
        /// <summary>
        /// Get the State Data,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        List<StateModel> GetState();

        /// <summary>
        /// Get the State Data Only Active,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        List<StateModel> GetState_Active();

        /// <summary>
        /// Get the State Data By CountryId Only Active,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <param name="CountryId"></param>
        /// <returns></returns>
        List<StateModel> GetState_ByCountryID(int CountryId);
        string SaveState(StateModel model);
        string UpdateState(StateModel model);
        string EnableState(StateModel model);
        string DisableState(StateModel model);
    }
}
