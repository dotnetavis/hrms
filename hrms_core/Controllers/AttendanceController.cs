﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using hrms_core.EF;
using hrms_core.Models;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.IO;
using Microsoft.Extensions.Logging;
using System.Data;

namespace hrms_core.Controllers
{
    public class AttendanceController : Controller
    {
        private readonly ApplicationDbContext _db;
        public IConfiguration Configuration { get; }
        private IAttendance attendanceService => new AttendanceService(_db);
        private IAssociateServices associateServices => new AssociateServices(_db);

        public AttendanceController(ApplicationDbContext db, IConfiguration configuration)
        {
            Configuration = configuration;
            _db = db;
        }

        [HttpGet]
        public IActionResult Index()
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                AttendanceDataVm model = new AttendanceDataVm();
                return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

            
        }
        [HttpPost]
        public IActionResult Index(AttendanceDataVm model, string CommandName, DateTime FromDate, DateTime TODate)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);

            if (SesMdl != null)
            {
                model.Dates = attendanceService.GetDates(FromDate, TODate);
                if (model.Dates.Count > 31)
                {
                    TempData["msg"] = "Wrong Input. Selected days count shouldn't be more than 31. ";
                    return RedirectToAction("Index", "Attendance");
                }
                if (TODate > DateTime.Now)
                {
                    TempData["msg"] = "Wrong Input. ToDate is exciding the limits ";
                    return RedirectToAction("Index", "Attendance");
                }
                if (TODate < FromDate)
                {
                    TempData["msg"] = "Wrong Input.ToDate Can't be smaller than FromDate";
                    return RedirectToAction("Index", "Attendance");
                }

                if (CommandName == "Submit")
                {
                    //model.FromDate = FromDate;
                    //model.ToDate = TODate;
                    if (SesMdl.is_super_admin == true)
                    {
                        model.ListUserName = attendanceService.GetUsers(SesMdl.associate_id);
                        try
                        {
                            var Dates = attendanceService.GetDates(FromDate, TODate);
                            //var completeAttendance = _db.attendancedata.Where(x => x.attendancedate.Date >= Dates.FirstOrDefault().Date && x.attendancedate.Date <= Dates.LastOrDefault().Date).ToList();
                            var completeAttendance = (from associate in _db.AssociateMasters.Where(x => x.isactive == true)
                                                      from attendance in _db.attendancedata.Where(x => x.associate_code == associate.associate_code).DefaultIfEmpty()
                                                      select new Attendancedata()
                                                      {
                                                          associate_code = associate.associate_code,
                                                          attendancedate = attendance.attendancedate,
                                                          id = attendance.id,
                                                          intime = attendance.intime,
                                                          outtime = attendance.outtime,
                                                          logintime = attendance.logintime
                                                      }).OrderBy(x=>x.associate_code).Where(x => x.attendancedate.Date >= Dates.FirstOrDefault().Date && x.attendancedate.Date <= Dates.LastOrDefault().Date).ToList();
                            foreach (var User in model.ListUserName)
                            {
                                User.Dates = Dates;
                                for (int i = 0; i < model.Dates.Count; i++)
                                {
                                    var login = completeAttendance.Where(x => x.attendancedate == model.Dates[i] && x.associate_code == User.associate_code).FirstOrDefault();
                                    if (login != null)
                                    {
                                        User.attendanceData.Add(login);
                                        // User.LoginTimes.Add(login.logintime);
                                    }
                                    else
                                    {
                                        Attendancedata Nologin = new Attendancedata();
                                        Nologin.id = 0;
                                        Nologin.intime = "00:00:00";
                                        Nologin.logintime = "00:00:00";
                                        Nologin.outtime = "00:00:00";
                                        Nologin.attendancedate = model.Dates[i];
                                        Nologin.associate_code = User.associate_code;
                                        User.attendanceData.Add(Nologin);
                                        //User.LoginTimes.Add("00:00:00");
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            TempData["msg"] = "Something went wrong.Try again ";
                            return RedirectToAction("Index", "Attendance");
                        }
                    }
                    else{
                        model.ListUserName = attendanceService.GetUsers(SesMdl.associate_id);
                        try
                        {
                            var Dates = attendanceService.GetDates(FromDate, TODate);
                            //var completeAttendance = _db.attendancedata.Where(x => x.attendancedate.Date >= Dates.FirstOrDefault().Date && x.attendancedate.Date <= Dates.LastOrDefault().Date).ToList();
                            var completeAttendance = (from associate in _db.AssociateMasters.Where(x => x.isactive == true)
                                                      from attendance in _db.attendancedata.Where(x => x.associate_code == associate.associate_code).DefaultIfEmpty()
                                                      select new Attendancedata()
                                                      {
                                                          associate_code = associate.associate_code,
                                                          attendancedate = attendance.attendancedate,
                                                          id = attendance.id,
                                                          intime = attendance.intime,
                                                          outtime = attendance.outtime,
                                                          logintime = attendance.logintime
                                                      }).OrderBy(x=>x.associate_code).Where(x => x.attendancedate.Date >= Dates.FirstOrDefault().Date && x.attendancedate.Date <= Dates.LastOrDefault().Date).ToList();

                            foreach (var User in model.ListUserName)
                            {
                                User.Dates = Dates;
                                for (int i = 0; i < model.Dates.Count; i++)
                                {
                                    var login = completeAttendance.Where(x => x.attendancedate == model.Dates[i] && x.associate_code == User.associate_code).FirstOrDefault();
                                    if (login != null)
                                    {
                                        // User.LoginTimes.Add(login.logintime);
                                        User.attendanceData.Add(login);
                                    }
                                    else
                                    {
                                        //User.LoginTimes.Add("00:00:00");
                                        Attendancedata Nologin = new Attendancedata();
                                        Nologin.id = 0;
                                        Nologin.intime = "00:00:00";
                                        Nologin.logintime = "00:00:00";
                                        Nologin.outtime = "00:00:00";
                                        Nologin.attendancedate = model.Dates[i];
                                        Nologin.associate_code = User.associate_code;
                                        User.attendanceData.Add(Nologin);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            TempData["msg"] = "Something went wrong.Try again ";
                            return RedirectToAction("Index", "Attendance");
                        }
                    }
                    
                }
                model.associateMasters = new List<AssociateMasterModel>();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }
    }
}