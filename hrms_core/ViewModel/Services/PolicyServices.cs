﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Transactions;


namespace hrms_core.ViewModel.Services
{
    public class PolicyServices : IPolicyServices

    {
        private readonly ApplicationDbContext _db;
        //private  object fileUpload;

        public PolicyServices(ApplicationDbContext options)
        {
            _db = options;
        }



        public List<PolicyVM> GetPolicyList()
        {
            {
                PolicyVM model = new PolicyVM();
                try
                {
                    model.PolicyList = _db.Policy
                          .Select(x => new PolicyVM
                          {
                              id = x.id,
                                file_name = x.file_name,
                              file_url = x.file_url,
                              updatedbyname = model.updatedbyname,
                              createdbyname = model.createdbyname,
                              isactive = true,
                              createdon = DateTime.Now,
                              updatedon = DateTime.Now,
                              createdby = model.createdby,
                              updatedby = model.updatedby,
                          }).ToList();

                }
                catch (Exception ex)
                {
                    CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                }
                return model.PolicyList;
            }
        }



        public string SavePolicy(PolicyVM model)
        {
         
                string Result = string.Empty;
                try
                {
                
                    using (var scope = new System.Transactions.TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                         try
                        {
                        Policy pol_obj = new Policy();
                        {
                                pol_obj.file_name = model.file_name;
                                pol_obj.file_url = model.file_url;
                                pol_obj.updatedbyname = model.updatedbyname;
                                pol_obj.createdbyname = model.createdbyname;
                                pol_obj.isactive = true;
                                pol_obj.createdon = DateTime.Now;
                                pol_obj.updatedon = DateTime.Now;
                                pol_obj.createdby = model.createdby;
                                pol_obj.updatedby = model.updatedby;
                                _db.Policy.Add(pol_obj);
                                _db.SaveChanges();
                                scope.Complete();
                                Result = MessageHelper.Policy_AddSuccess;
                            }
                        }

                        catch (Exception ex)
                        {
                            scope.Dispose();
                        CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                        Result = MessageHelper.Policy_AddError;
                        }
                    }

                }
                catch (Exception ex)
                {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Task_AddError;
                }
                return Result;
          }

     }
    
}
       

        




        


   



      
      
            
        




      
       

       

        

