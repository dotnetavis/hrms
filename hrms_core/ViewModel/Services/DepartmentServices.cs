﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class departmentServices : IDepartmentServices
    {
        private readonly ApplicationDbContext _db;
        public departmentServices(ApplicationDbContext options)
        {
            _db = options;
        }

        /// <summary>
        /// Get the GetDepartment Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        public List<DepartmentModel> GetDepartmentList()
        {
            DepartmentModel model = new DepartmentModel();
            try
            {
                model.DepartmentModelList = _db.Departments.Select(x => new DepartmentModel
                {
                    id = x.id,
                    applicable_leave = x.applicable_leave,
                    createdby = x.createdby,
                    createdon = x.createdon,
                    createdbyname = x.createdbyname,
                    updatedbyname = x.updatedbyname,
                    departmenttext = x.departmenttext,
                    isactive = x.isactive,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                }).OrderByDescending(m => m.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.DepartmentModelList;
        }

        /// <summary>
        /// Save the Department Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        public string SaveDepartment(DepartmentModel model)
        {
            string Result = string.Empty;
            Department department = new Department();
            try
            {
                //Check for Duplicate Record
                bool bool_sameNameExists = _db.Departments.Any(x => x.departmenttext.ToLower().Trim() == model.departmenttext.ToLower().Trim());
                if (bool_sameNameExists)
                {
                    Result = MessageHelper.Department_Duplicate;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            department.departmenttext = model.departmenttext;
                            department.applicable_leave = model.applicable_leave;
                            department.isactive = true;
                            department.createdon = DateTime.Now;
                            department.updatedon = DateTime.Now;
                            department.createdby = model.createdby;
                            department.updatedby = model.updatedby;
                            department.createdbyname = model.createdbyname;
                            department.updatedbyname = model.updatedbyname;
                            _db.Departments.Add(department);
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Department_AddSuccess;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.Department_AddError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Department_AddError;
            }
            return Result;
        }

        /// <summary>
        /// Update the Department Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        public string UpdateDepartment(DepartmentModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Departments.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    //Check for Duplicate Record
                    bool bool_sameNameExists = _db.Departments.Any(x => x.departmenttext.ToLower().Trim() == model.departmenttext.ToLower().Trim() && x.id != model.id);
                    if (bool_sameNameExists)
                    {
                        Result = MessageHelper.Department_Duplicate;
                    }
                    else
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            try
                            {
                                data.departmenttext = model.departmenttext;
                                data.applicable_leave = model.applicable_leave;
                                data.isactive = true;
                                data.createdon = DateTime.Now;
                                data.updatedon = DateTime.Now;
                                data.createdby = model.createdby;
                                data.updatedby = model.updatedby;
                                data.createdbyname = model.createdbyname;
                                data.updatedbyname = model.updatedbyname;
                                _db.SaveChanges();
                                scope.Complete();
                                Result = MessageHelper.Department_AddSuccess;
                            }
                            catch (Exception ex)
                            {
                                scope.Dispose();
                                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                Result = MessageHelper.Department_AddError;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Department_AddError;
            }
            return Result;
        }

        /// <summary>
        /// Disable the Department Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        public string DisableDepartment(DepartmentModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Departments.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = false;
                            data.updatedon = DateTime.Now;
                            data.updatedby = 1;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Department_Disable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }

        /// <summary>
        /// Enable the Department Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        public string EnableDepartment(DepartmentModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Departments.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = true;
                            data.updatedon = DateTime.Now;
                            data.updatedby = 1;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Department_Enable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }

        public List<DepartmentModel> GetDepartment_Active()
        {
            DepartmentModel model = new DepartmentModel();
            try
            {
                model.DepartmentModelList = _db.Departments.Where(m => m.isactive == true)
                    .Select(x => new DepartmentModel
                    {
                        id = x.id,
                        applicable_leave = x.applicable_leave,
                        createdby = x.createdby,
                        createdon = x.createdon,
                        departmenttext = x.departmenttext,
                        isactive = x.isactive,
                        updatedby = x.updatedby,
                        updatedon = x.updatedon,
                        updatedbyname = x.updatedbyname,
                        createdbyname = x.createdbyname
                    }).OrderBy(m => m.departmenttext).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.DepartmentModelList;
        }

        //public bool SaveHODAssigns
    }
}
