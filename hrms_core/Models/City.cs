﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class City:CommonAttributeModel
    {
        [Key]
        public  int  id { get; set; }       //priamry key
        [Required(ErrorMessage = "Enter City")]
        public string citytext { get; set; }
        [ForeignKey("state_id")]
        public int state_id { get; set; }
        [ForeignKey("permanent_city_id")]
        public virtual ICollection<AssociatePersonal> AssociatePersonal_Permanent { get; set; }

    }
}
