﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class AssociatePersonal:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }         //primary key
        [ForeignKey("associate_id")]
        public int associate_id { get; set; }
        public string pan_card { get; set; }
        public string adhar_card_no { get; set; }
        [Required(ErrorMessage ="Date of Birth Required")]
        public DateTime dob { get; set; }
        public DateTime? anniversary { get; set; }
        public string father_husband_name { get; set; }
        public string mothername { get; set; }
        public string present_address { get; set; }
        public int? present_country_id { get; set; }
        public int? present_state_id { get; set; }
        public int? present_city_id { get; set; }
        [Required(ErrorMessage = "Permanent Address is Required")]
        public  string permanent_address { get; set; }
        [Required(ErrorMessage = "Permanent Country is Required")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Select Country Name")]
        [ForeignKey("permanent_country_id")]
        public int permanent_country_id { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = "Select state Name")]
        [ForeignKey("permanent_state_id")]
        [Required(ErrorMessage = "Permanent State is Required")]
        public int permanent_state_id { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = "Select city Name")]
        [ForeignKey("permanent_city_id")]
        [Required(ErrorMessage = "Permanent City is Required")]
        public int permanent_city_id { get; set; }
        public string bloodgroup { get; set; }
        [Required(ErrorMessage ="Gender Required")]
        public string gender { get; set; }
        [Required(ErrorMessage = "Maritial Status Required")]
        public string maritalstatus { get; set; }
       

    }
}
