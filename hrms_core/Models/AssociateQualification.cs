﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class AssociateQualification:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }            //primary key
        [ForeignKey("associate_id")]
        public int associate_id { get; set; }     //foreign key
        [ForeignKey("qualification_id")]
        public int qualification_id { get; set; }      //foreign key
        [Required(ErrorMessage = "Qualification Year Required")]
        public string qualification_year { get; set; }
        [Required(ErrorMessage = "Institute Name Required")]
        public string institute_name  { get; set; }
        public int percentage { get; set; }
    }
}
