﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class DesignationServices : IDesignationServices
    {
        private readonly ApplicationDbContext _db;
        public DesignationServices(ApplicationDbContext options) => _db = options;

        /// <summary>
        /// Get the Designation Data,
        /// Created By : Bilal 
        ///On Date: 22/05/2018
        /// </summary>
        /// <returns></returns>
        public List<DesignationModel> GetDesignation()
        {
            DesignationModel model = new DesignationModel();
            try
            {
                model.DesignationModelList = _db.Designations
                    .Select(x => new DesignationModel
                    {
                        id = x.id,
                        designationtext = x.designationtext,
                        designationdesc = x.designationdesc,
                        isactive = x.isactive,
                        createdon = x.createdon,
                        createdby = x.createdby,
                        updatedon = x.updatedon,
                        updatedby = x.updatedby,
                        createdbyname=x.createdbyname,
                        updatedbyname=x.updatedbyname
                    }).OrderByDescending(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.DesignationModelList;
        }

        /// <summary>
        /// Save the Designation Data,
        /// Created By : Bilal 
        ///On Date: 22/05/2018
        /// </summary>
        /// <returns></returns>
        public string SaveDesignation(DesignationModel model)
        {
            string Result = string.Empty;
            Designation designation = new Designation();
            try
            {
                //Check for Duplicate Record
                bool bool_sameNameExists = _db.Designations.Any(x => x.designationtext.ToLower().Trim() == model.designationtext.ToLower().Trim());
                if (bool_sameNameExists)
                {
                    Result = MessageHelper.Designation_Duplicate;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            designation.designationtext = model.designationtext;
                            designation.designationdesc = model.designationdesc;
                            designation.isactive = true;
                            designation.createdon = DateTime.Now;
                            designation.updatedon = DateTime.Now;
                            designation.createdby = model.createdby;
                            designation.updatedby = model.updatedby;
                            designation.updatedbyname = model.updatedbyname;
                            designation.createdbyname = model.createdbyname ;
                            _db.Designations.Add(designation);
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Designation_AddSuccess;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.Designation_AddError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Designation_AddError;
            }
            return Result;
        }

        /// <summary>
        /// Update the Designation Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        public string UpdateDesignation(DesignationModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Designations.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    //Check for Duplicate Record
                    bool bool_sameNameExists = _db.Designations.Any(x => x.designationtext.ToLower().Trim() == model.designationtext.ToLower().Trim() && x.id != model.id);
                    if (bool_sameNameExists)
                    {
                        Result = MessageHelper.Designation_Duplicate;
                    }
                    else
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            try
                            {
                                data.designationtext = model.designationtext;
                                data.designationdesc = model.designationdesc;
                                data.isactive = true;
                                data.updatedon = DateTime.Now;
                                data.updatedby = model.updatedby;
                                data.createdby = model.createdby;
                                data.createdon = model.createdon;
                                data.updatedbyname = model.updatedbyname;
                                data.createdbyname = model.createdbyname;
                                _db.SaveChanges();
                                scope.Complete();
                                Result = MessageHelper.Designation_AddSuccess;
                            }
                            catch (Exception ex)
                            {
                                scope.Dispose();
                                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                Result = MessageHelper.Designation_AddError;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Designation_AddError;
            }
            return Result;
        }

        /// <summary>
        /// Disable the Designation Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        public string DisableDesignation(DesignationModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Designations.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = false;
                            data.updatedon = DateTime.Now;
                            data.updatedby = model.updatedby;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Designation_Disable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }

        /// <summary>
        /// Enable the Designation Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        public string EnableDesignation(DesignationModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Designations.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = true;
                            data.updatedon = DateTime.Now;
                            data.updatedby = model.updatedby;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Designation_Enable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }

        /// <summary>
        /// Get the Designation Data,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public List<DesignationModel> GetDesignation_Active()
        {
            DesignationModel model = new DesignationModel();
            try
            {
                model.DesignationModelList = _db.Designations.Where(x=>x.isactive==true)
                    .Select(x => new DesignationModel
                    {
                        id = x.id,
                        designationtext = x.designationtext,
                        designationdesc = x.designationdesc,
                        isactive = x.isactive,
                        createdon = x.createdon,
                        createdby = x.createdby,
                        updatedon = x.updatedon,
                        updatedby = x.updatedby,
                        createdbyname=x.createdbyname,
                        updatedbyname=x.updatedbyname
                    }).OrderBy(x => x.designationtext).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.DesignationModelList;
        }
    }
}
