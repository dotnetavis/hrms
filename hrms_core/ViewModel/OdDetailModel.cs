﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class OdDetailModel: OdDetail
    {
        public List<OdDetailModel> OdDetailModelList { get; set; }
        public OdDetailModel()
        {
            OdDetailModelList = new List<OdDetailModel>();
        }
    }
}
