﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class OdDetail:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public DateTime od_date { get; set; }
        public int applied_by { get; set; }
        public DateTime applied_on { get; set; }
     
       
        public string comments { get; set; }
        public int status { get; set; }
        public int dar_id { get; set; }
    }
}
