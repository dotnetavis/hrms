﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class ShiftServices : IShiftServices
    {
        private readonly ApplicationDbContext _db;
        public ShiftServices(ApplicationDbContext options)
        {
            _db = options;
        }
        public List<ShiftModel> GetShift()
        {
            ShiftModel model = new ShiftModel();
            try
            {
                model.ShiftModelList = _db.Shifts
                    .Select(x => new ShiftModel
                    {
                        id = x.id,
                        shifttext = x.shifttext,
                        starttime = x.starttime,
                        endtime = x.endtime,
                        totaltime = x.totaltime,
                        breaktime = x.breaktime,
                        networkingtime = x.networkingtime,
                        tolerance = x.tolerance,
                        nightshift = x.nightshift,
                        isactive = x.isactive,
                        createdon = x.createdon,
                        createdby = x.createdby,
                        updatedon = x.updatedon,
                        updatedby = x.updatedby,
                        createdbyname=x.createdbyname,
                        updatedbyname=x.updatedbyname
                    }).OrderByDescending(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.ShiftModelList;
        }

        public void editShiftMasterList(int shift_id)
        {
            throw new NotImplementedException();
        }

        public void getShiftMasterList()
        {
            throw new NotImplementedException();
        }

        public void insertShiftMasterList()
        {

            ShiftModel model = new ShiftModel();
            Shift shift = new Shift();
            string Result = string.Empty;
            try
            {
                //Check for Duplicate Record
                bool bool_sameNameExists = _db.Shifts.Any(x => x.shifttext.ToLower().Trim() == model.shifttext.ToLower().Trim());
                if (bool_sameNameExists)
                {
                    Result = MessageHelper.shift_duplicate;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            {
                                shift.shifttext = model.shifttext;
                                shift.starttime = model.starttime;
                                shift.endtime = model.endtime;
                                shift.totaltime = model.totaltime;
                                shift.breaktime = model.breaktime;
                                shift.networkingtime = model.networkingtime;
                                shift.tolerance = model.tolerance;
                                shift.nightshift = model.nightshift;
                                shift.isactive = model.isactive;
                                shift.createdon = model.createdon;
                                shift.createdby = model.createdby;
                                shift.updatedon = model.updatedon;
                                shift.updatedby = model.updatedby;
                                shift.createdbyname = model.createdbyname;
                                shift.updatedbyname = model.updatedbyname;
                                _db.Shifts.Add(shift);
                                _db.SaveChanges();
                                scope.Complete();
                                Result = MessageHelper.shift_addsuccess;
                            }
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.shift_adderror;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.shift_adderror;
            }
        }

        public void updateShiftMasterList()
        {
            string Result = string.Empty;
            ShiftModel model = new ShiftModel();
            try
            {
                var data = _db.Shifts.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    //Check for Duplicate Record
                    bool bool_sameNameExists = _db.Shifts.Any(x => x.shifttext.ToLower().Trim() == model.shifttext.ToLower().Trim() && x.id != model.id);
                    if (bool_sameNameExists)
                    {
                        Result = MessageHelper.shift_update;
                    }
                    else
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            try
                            {
                                data.shifttext = model.shifttext;
                                data.starttime = model.starttime;
                                data.starttime = model.starttime;
                                data.endtime = model.endtime;
                                data.breaktime = model.breaktime;
                                data.totaltime = model.totaltime;
                                data.networkingtime = model.networkingtime;
                                data.tolerance = model.tolerance;
                                model.nightshift = true;
                                data.isactive = true;
                                data.createdon = model.createdon;
                                data.createdby = model.createdby;
                                data.updatedon = DateTime.Now;
                                data.updatedby = model.updatedby;
                                data.createdbyname = model.createdbyname;
                                data.updatedbyname = model.updatedbyname;
                                _db.SaveChanges();
                                scope.Complete();
                                Result = MessageHelper.shift_addsuccess;
                            }
                            catch (Exception ex)
                            {
                                scope.Dispose();
                                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                Result = MessageHelper.shift_adderror;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.shift_adderror;
            }

        }

        public void updateShiftStatus()
        {
            throw new NotImplementedException();
        }
    }
}
