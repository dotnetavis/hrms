﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    interface IProjectServices
    {
        /// <summary>
        /// Get the Project Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        List<ProjectMasterModel> GetProject(int department);

        /// <summary>
        /// Save the Project Data,
        /// Created By : Bilal 
        ///On Date: 22/05/2018
        /// </summary>
        /// <returns></returns>
        string SaveProject(ProjectMasterModel model);

        /// <summary>
        /// Update the Project Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        string UpdateProject(ProjectMasterModel model);

        /// <summary>
        /// Disable the Designation Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        string DisableProject(ProjectMasterModel model);

        /// <summary>
        /// Enable the Designation Data,
        /// Created By : Bilal 
        ///On Date: 29/05/2018
        /// </summary>
        /// <returns></returns>
        string EnableProject(ProjectMasterModel model);
        List<ProjectMasterModel> GetProjectList();

    }
}
