﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    interface IHRAttendance
    {
        List<AssociateMasterModel> GetUsers(int? associateId);
        List<AttendanceDataVm> GetAttendanceDatas(int associate_code, DateTime FromDate, DateTime TODate);
        List<DateTime> GetDates(DateTime FromDate, DateTime TODate);
        List<String> GetLoginTimes(int associate_code, DateTime FromDate, DateTime TODate);
        bool changeAttendance(HRAttendanceDataVM model);
    }
}
