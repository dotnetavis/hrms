﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class AssociateAttendance:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }   //primary key
        [ForeignKey("associate_id")]
        public int associate_id { get; set; }
        public DateTime attendancedate { get; set; }
        public bool ispresent { get; set; }
        public string cardno { get; set; }
        public string p_day { get; set; }
        public string is_manual { get; set; }
        public string reasoncode { get; set; }
        public string mc_no { get; set; }
        public string in_out { get; set; }
        public string paycode { get; set; }
        public DateTime officepunch { get; set; }

    }
}
