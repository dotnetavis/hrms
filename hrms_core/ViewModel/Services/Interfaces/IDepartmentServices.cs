﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IDepartmentServices
    {
        /// <summary>
        /// Get the GetDepartment Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        List<DepartmentModel> GetDepartmentList();
        

        /// <summary>
        /// Get the GetDepartment Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        string SaveDepartment(DepartmentModel model);

        /// <summary>
        /// Update the Department Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        string UpdateDepartment(DepartmentModel model);
       

        /// <summary>
        /// Disable the Department Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        string DisableDepartment(DepartmentModel model);

        /// <summary>
        /// Enable the Department Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        string EnableDepartment(DepartmentModel model);
        List<DepartmentModel> GetDepartment_Active();
    }
}
