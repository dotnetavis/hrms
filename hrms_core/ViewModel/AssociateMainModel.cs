﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class AssociateMainModel: AssociateMain
    {
        public List<AssociateMainModel> AssociateMainModelList { get; set; }
        public string userfullname { get; set; }
        public string username { get; set; }
        public string userpass { get; set; }
        public string department { get; set; }
        public string designation { get; set; }
        public string HOD_Name { get; set; }
        public string qualification { get; set; }
        public string university { get; set; }
        public AssociateMainModel()
        {
            AssociateMainModelList = new List<AssociateMainModel>();
        }
    }
}
