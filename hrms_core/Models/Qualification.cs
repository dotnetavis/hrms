﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class Qualification:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage ="Enter Qualification")]
        public string qualificationtext { get; set; }
        [ForeignKey("qualification_id")]
        public virtual ICollection<AssociateMain> AssociateMainDetails { get; set; }
        [ForeignKey("qualification_id")]
        public virtual ICollection<AssociateQualification> AssociateQualificationDetails { get; set; }
    }
}
