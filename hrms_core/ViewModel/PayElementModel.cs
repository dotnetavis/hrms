﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class PayElementModel: PayElement
    {
        public List<PayElementModel> PayElementModelList { get; set; }
        public string associatename { get; set; }
        public PayElementModel()
        {
            PayElementModelList = new List<PayElementModel>();
        }
    }
}
