﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class ChangePasswordServices : IChangePasswordServices
    {
        private readonly ApplicationDbContext _db;
        public ChangePasswordServices(ApplicationDbContext options) => _db = options;

        public List<UserModel> CheckUserPassword( UserModel user)
        {
            UserModel model = new UserModel();
            try
            {
                model.UserModelList = _db.Users.Where(x=>x.username == user.username && x.isactive == true)
                    .Select(x => new UserModel
                    {
                        id = x.id,
                       username = x.username,
                       userpass = x.userpass
                    }).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.UserModelList;
        }

        public bool ChangePass(UserModel user)
        {
            var data = _db.Users.FirstOrDefault(x => x.username == user.username && x.isactive == true);
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    data.userpass = CommonUtility.MD5Convertor(user.confirmnewpassword);
                    _db.SaveChanges();
                    scope.Complete();
                    return true;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                    CommonUtility.WriteMsgLogs("UserName : "+user.username+" Password : " + user.confirmnewpassword + "", "AssociateServices=>GetDefaultPassword");
                    return false;
                }
            }
        }
    }
}