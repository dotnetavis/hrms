﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.Models;
namespace hrms_core.ViewModel
{
    public class CompanyPolicyModel: CompanyPolicy
    {
        public List<CompanyPolicyModel> policies { get; set; }
        public DateTime policy_time { get; set; }
        public CompanyPolicyModel()
        {
            policies = new List<CompanyPolicyModel>();
        }
    }
}
