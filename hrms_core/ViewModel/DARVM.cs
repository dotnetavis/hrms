﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class DARVM
    {
        public DailyActivityReportModel dailyActivityReportModel { get; set; }
        public DailyReportDetailModel dailyReportDetailModel { get; set; }
        public List<ProjectMasterModel> ProjectMasterModelList { get; set; }
        public List<TaskMasterModel> TaskMasterModelList { get; set; }
        public string associate_name { get; set; }
        public string department { get; set; }
        public List<DARVM> DARVMList { get; set; }
        public string username { get; set; }
        public string userpass { get; set; }
        public string dar_content { get; set; }
        public string reporting_email { get; set; }
        public string hod_email { get; set; }
        public List<string> additional_reporting_email { get; set; }
        public string designation { get; set; }
        public string official_email { get; set; }
        public List<DailyReportDetailModel> lstDailyReportDetailModel { get; set; }
        public DARVM()
        {
            dailyActivityReportModel = new DailyActivityReportModel();
            dailyReportDetailModel = new DailyReportDetailModel();
            ProjectMasterModelList = new List<ProjectMasterModel>();
            TaskMasterModelList = new List<TaskMasterModel>();
            DARVMList = new List<DARVM>();
            lstDailyReportDetailModel = new List<DailyReportDetailModel>();
        }
    }
}
