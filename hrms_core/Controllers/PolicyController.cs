﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace hrms_core.Controllers
{
    public class PolicyController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IPolicyServices policyServices => new PolicyServices(_db);
        // private IDepartmentServices departmentServices => new departmentServices(_db);
        public PolicyController(ApplicationDbContext options) => _db = options;

        [HttpGet]
        public IActionResult Index(int? id)
        {
            PolicyVM model = new PolicyVM();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                model.PolicyList = policyServices.GetPolicyList();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }

        [HttpPost]

        public IActionResult Index(PolicyVM model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);

            if (SesMdl != null)
            {
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                model = Upload();
                if (model != null)
                {
                    TempData["msg"] = policyServices.SavePolicy(model);
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("Index", "Policy");
        }
        public IActionResult DownloadPolicy(string filename)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload") + "\\" + filename;
            byte[] fileBytes = System.IO.File.ReadAllBytes(path);

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
        }
        public PolicyVM Upload()
        {
            try
            {
                PolicyVM model = new PolicyVM();
                DataTable dataSet = new DataTable();

                if (Request.Form.Files["postedFile"] != null)
                {
                    var filename = Path.GetFileName(Request.Form.Files["postedFile"].FileName);
                    string extension = Path.GetExtension(filename).ToLower();
                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload", filename);
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload"));
                    }
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        Request.Form.Files["postedfile"].CopyToAsync(stream);
                    }

                    model.file_name = filename;
                    model.file_url = path.ToString();
                    return model;

                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return null;

            }
        }
    }
}

