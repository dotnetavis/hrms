﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class HolidayCalenderModel: HolidayCalender
    {
        public List<HolidayCalenderModel> HolidayCalenderModelList { get; set; }
        public HolidayCalenderModel()
        {
            HolidayCalenderModelList = new List<HolidayCalenderModel>();
        }
    }
}
