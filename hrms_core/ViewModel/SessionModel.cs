﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.Models;

namespace hrms_core.ViewModel
{
    public class SessionModel
    {
        public int associate_id { get; set; }
        public int user_id { get; set; }
        public string associate_code { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string associate_name { get; set; }
        public int designation_id { get; set; }
        public int department_id { get; set; }
        public string designation_text { get; set; }
        public string department_text { get; set; }
        public int reporting_person_id { get; set; }
        public string reporting_person { get; set; }
        public string reporting_person_email { get; set; }
        public string head_of_department_email { get; set; }
        public string personal_email { get; set; }
        public string official_email { get; set; }
        public List<string> addition_reporting { get; set; }
    //    public bool mailto_hod { get; set; }
        public int role_id { get; set; }
        public string role_name { get; set; }
        public bool is_super_admin { get; set; }
        public DateTime associate_since { get; set; }
        public List<RoleMenuDetailModel> PageMenuModelList { get; set; }
        public List<SessionModel> SessionModelList { get; set; }
        public string ProfilePic { get; set; }
        public SessionModel()
        {
            PageMenuModelList = new List<RoleMenuDetailModel>();
            SessionModelList = new List<SessionModel>();
            addition_reporting = new List<string>();
        }
    }
}
