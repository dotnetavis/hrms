﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class DailyActivityReportModel: DailyActivityReport
    {
        public List<DailyActivityReportModel> DailyActivityReportModelList { get; set; }
        public List<ProjectMasterModel> ProjectMasterModelList { get; set; }
        public List<TaskMasterModel> TaskMasterModelList { get; set; }
        public TaskMasterModel TaskMaster { get; set; }
        public ProjectMasterModel ProjectMaster { get; set; }
        public List<DailyReportDetailModel> dailyReportDetaillist { get; set; }
        public string associate_name { get; set; }
        public List<AssociateVM> AssociateVMList { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime SelectedDate { get; set; }
        public List<DateTime> Dates { get; set; }
        public DailyActivityReportModel()
        {
            Dates = new List<DateTime>();
            AssociateVMList = new List<AssociateVM>();
            DailyActivityReportModelList = new List<DailyActivityReportModel>();
            ProjectMasterModelList = new List<ProjectMasterModel>();
            ProjectMaster = new ProjectMasterModel();
            TaskMaster = new TaskMasterModel();
            dailyReportDetaillist = new List<DailyReportDetailModel>();
            TaskMasterModelList = new List<TaskMasterModel>();
        }
    }
}
