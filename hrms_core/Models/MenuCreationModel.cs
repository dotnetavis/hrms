﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class MenuCreationModel:CommonAttributeModel
    {
        [Key]
        public int menu_id { get; set; }
        public string menu_name { get; set; }
        public string controller_name { get; set; }
        public string action_name { get; set; }
        public string module_name { get; set; }

    }
}
