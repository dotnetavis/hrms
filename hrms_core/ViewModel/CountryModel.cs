﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class CountryModel: Country
    {
        public List<CountryModel> CountryModelList { get; set; }
        public CountryModel()
        {
            CountryModelList = new List<CountryModel>();
        }
    }
}
