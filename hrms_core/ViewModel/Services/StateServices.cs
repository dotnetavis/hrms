﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class StateServices : IStateServices
    {
        private readonly ApplicationDbContext _db;
        public StateServices(ApplicationDbContext options) => _db = options;
        /// <summary>
        /// Get the State Data,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public List<StateModel> GetState()
        {
            StateModel model = new StateModel();
            try
            {
                model.StateModelList = (from state in _db.States
                                        join country in _db.Countrys on state.country_id equals country.id
                                        select new StateModel
                                        {
                                            id = state.id,
                                            statetext = state.statetext,
                                            country_id = state.country_id,
                                            country_name = country.countrytext,
                                            isactive = state.isactive,
                                            createdon = state.createdon,
                                            createdby = state.createdby,
                                            updatedon = state.updatedon,
                                            updatedby = state.updatedby,
                                            updatedbyname = state.updatedbyname,
                                            createdbyname = state.createdbyname
                                        }).OrderByDescending(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.StateModelList;
        }
        /// <summary>
        /// Get the State Data By CountryId Only Active,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <param name="CountryId"></param>
        /// <returns></returns>
        public List<StateModel> GetState_ByCountryID(int CountryId)
        {
            StateModel model = new StateModel();
            try
            {
                model.StateModelList = (from state in _db.States.Where(x => x.isactive == true && x.country_id == CountryId)
                                        join country in _db.Countrys.Where(x => x.isactive == true) on state.country_id equals country.id
                                        select new StateModel
                                        {
                                            id = state.id,
                                            statetext = state.statetext,
                                            country_id = state.country_id,
                                            country_name = country.countrytext,
                                            isactive = true,
                                            createdon = state.createdon,
                                            createdby = state.createdby,
                                            updatedon = state.updatedon,
                                            updatedby = state.updatedby,
                                            updatedbyname = state.updatedbyname,
                                            createdbyname = state.createdbyname
                                        }).OrderBy(x => x.statetext).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.StateModelList;
        }
        /// <summary>
        /// Get the State Data Only Active,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public List<StateModel> GetState_Active()
        {
            StateModel model = new StateModel();
            try
            {
                model.StateModelList = (from state in _db.States.Where(x => x.isactive == true)
                                        join country in _db.Countrys.Where(x => x.isactive == true) on state.country_id equals country.id
                                        select new StateModel
                                        {
                                            id = state.id,
                                            statetext = state.statetext,
                                            country_id = state.country_id,
                                            country_name = country.countrytext,
                                            isactive = state.isactive,
                                            createdon = state.createdon,
                                            createdby = state.createdby,
                                            updatedon = state.updatedon,
                                            updatedby = state.updatedby,
                                            updatedbyname = state.updatedbyname,
                                            createdbyname = state.createdbyname
                                        }).OrderBy(x => x.statetext).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.StateModelList;
        }


        public string SaveState(StateModel model)
        {
            State state = new State();
            string Result = string.Empty;
            try
            {
                bool Same_Name = _db.States.Any(x => x.statetext.ToLower().Trim() == model.statetext.ToLower().Trim());
                if (Same_Name)
                {
                    Result = MessageHelper.State_Duplicate;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            state.country_id = model.country_id;
                            state.statetext = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(model.statetext.ToLower());//model.statetext;
                            state.isactive = true;
                            state.updatedby = model.updatedby;
                            state.updatedbyname = model.updatedbyname;
                            state.updatedon = DateTime.Now;
                            state.createdby = model.createdby;
                            state.createdon = DateTime.Now;
                            state.createdbyname = model.createdbyname;
                            _db.States.Add(state);
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.State_AddSuccess;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.State_AddError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.State_AddError;
            }
            return Result;
        }

        public string UpdateState(StateModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.States.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    bool Same_Name = _db.States.Any(x => x.statetext.ToLower().Trim() == model.statetext.ToLower().Trim() && x.id != model.id);
                    if (Same_Name)
                    {
                        Result = MessageHelper.State_Duplicate;
                    }
                    else
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            try
                            {
                                data.statetext = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(model.statetext.ToLower());
                                data.country_id = model.country_id;
                                data.isactive = true;
                                data.updatedby = model.updatedby;
                                data.updatedbyname = model.updatedbyname;
                                data.updatedon = DateTime.Now;
                                data.createdby = model.createdby;
                                data.createdon = model.createdon;
                                data.createdbyname = model.createdbyname;
                                _db.SaveChanges();
                                scope.Complete();
                                Result = MessageHelper.Country_AddSuccess;
                            }
                            catch (Exception ex)
                            {
                                scope.Dispose();
                                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                Result = MessageHelper.Country_AddError;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.Country_AddError;
            }
            return Result;
        }

        public string DisableState(StateModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.States.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = false;
                            data.updatedby = model.updatedby;
                            data.updatedbyname = model.updatedbyname;
                            data.updatedon = DateTime.Now;
                            data.createdby = model.createdby;
                            data.createdon = model.createdon;
                            data.createdbyname = model.createdbyname;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.State_Disable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }

        public string EnableState(StateModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.States.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = true;
                            data.updatedby = model.updatedby;
                            data.updatedbyname = model.updatedbyname;
                            data.updatedon = DateTime.Now;
                            data.createdby = model.createdby;
                            data.createdon = model.createdon;
                            data.createdbyname = model.createdbyname;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.State_Enable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }

    }
}
