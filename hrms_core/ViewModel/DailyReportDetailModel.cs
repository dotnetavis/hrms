﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class DailyReportDetailModel: DailyReportDetail
    {
        public List<DailyReportDetailModel> DailyReportDetailModelList { get; set; }
        public List<ProjectMasterModel> ProjectMasterModelList { get; set; }
        public List<TaskMasterModel> TaskMasterModelList { get; set; }
        public TaskMasterModel TaskMaster { get; set; }
        public ProjectMasterModel ProjectMaster { get; set; }
        public string task_name { get; set; }
        public string project_name { get; set; }
        public string department { get; set; }
        public int PartialCount { get; set; }
        public DailyReportDetailModel()
        {
            DailyReportDetailModelList = new List<DailyReportDetailModel>();
            ProjectMasterModelList = new List<ProjectMasterModel>();
            ProjectMaster = new ProjectMasterModel();
            TaskMaster = new TaskMasterModel();
            TaskMasterModelList = new List<TaskMasterModel>();
        }
    }
}
