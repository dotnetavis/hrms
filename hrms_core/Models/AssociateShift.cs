﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class AssociateShift :CommonAttributeModel
    { 
        [Key]
        public int id { get; set; }             //primary key
        [ForeignKey("shift_id")]
        public int shift_id { get; set; }    //foreign key
        [ForeignKey("associate_id")]
        public int associate_id { get; set; }       //foreign key
        public DateTime shiftdate { get; set; }



    }
}
