﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class PageMenu:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public string menu_name { get; set; }
        public string menu_url { get; set; }
        public string controller_name { get; set; }
        public string action_name { get; set; }
        public string module_name { get; set; }
        [ForeignKey("page_menu_id")]
        public virtual ICollection<RoleMenuDetails> RoleMenuDetails { get; set; }
    }
}
