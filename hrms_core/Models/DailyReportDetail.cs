﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class DailyReportDetail:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }   //primary key
        public virtual  DailyActivityReport dar{ get; set; }  //foreign key
        [Required(ErrorMessage = "Select Project Name")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Select Project Name")]
        public int projectid { get; set; }
        [Required(ErrorMessage = "Select Task Name")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Select Task Name")]
        public int taskid { get; set; }
        [Required(ErrorMessage = "Activity Detail Required")]
        public string activitydetail { get; set; }
        [Range(1,100,ErrorMessage = "Percentage cannot be more than 100 or less than 0")]
        public double percentage { get; set; }
        [Required(ErrorMessage = "Task Status Required")]
        public string taskstatus { get; set; }
        public string remark { get; set; }
        [Required(ErrorMessage = "Enter hours")]
        [RegularExpression(@"^(?:[01][0-9]|2[0-3]):[0-5][0-9]$",ErrorMessage ="Enter time in 24 hour format")]
        public string hour { get; set; }
        [Required(ErrorMessage = "Select Status")]
        public string status { get; set; }
        [ForeignKey("dar_id")]
        public int dar_id { get; set; }
    }
}
