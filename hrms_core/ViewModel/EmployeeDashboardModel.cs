﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class EmployeeDashboardModel: EmployeeDashboard
    {
        public List<EmployeeDashboardModel> EmployeeDashboardModelList { get; set; }
        public List<DepartmentModel> DepartmentModelList { get; set; }
        public AssociateMainModel associateMain { get; set; }
        public string employee_name { get; set; }
        public string department_name { get; set; }
        public AssociateMasterModel AssociateMaster { get; set; }
        public List<AssociateMasterModel> AssociateMasterModelList { get; set; }
        public QualificationModel qualificationModel { get; set; }
        public EmployeeDashboardModel()
        {
            EmployeeDashboardModelList = new List<EmployeeDashboardModel>();
            DepartmentModelList = new List<DepartmentModel>();
            AssociateMasterModelList = new List<AssociateMasterModel>();
            associateMain = new AssociateMainModel();
            qualificationModel = new QualificationModel();
            AssociateMaster= new AssociateMasterModel();
        }
    }
}
