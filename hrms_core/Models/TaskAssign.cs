﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class TaskAssign:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }                               
        [ForeignKey("associate_id")]
        public int associate_id { get; set; }   
        [ForeignKey("task_id")]
        public int task_id { get; set; }  
        public bool isdeduction { get; set; }
        public bool is_selected { get; set; }
    }
}
