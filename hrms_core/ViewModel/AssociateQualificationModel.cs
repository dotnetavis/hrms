﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class AssociateQualificationModel: AssociateQualification
    {
        public List<QualificationModel> QualificationModelList { get; set; }
        public List<AssociateQualificationModel> AssociateQualificationModelList { get; set; }
        public AssociateQualificationModel()
        {
            QualificationModelList = new List<QualificationModel>();
            AssociateQualificationModelList = new List<AssociateQualificationModel>();
        }
    }
}
