﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class NoticePeriodModel: NoticePeriod
    {
        public List<NoticePeriodModel> NoticePeriodModelList { get; set; }
        public NoticePeriodModel()
        {
            NoticePeriodModelList = new List<NoticePeriodModel>();

        }
    }
}
