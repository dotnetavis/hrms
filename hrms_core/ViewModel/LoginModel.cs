﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class LoginModel : User
    {
        public List<LoginModel> LoginModelList { get; set; }
        public string official_email { get; set; }
        public bool is_super_admin { get; set; }
        public LoginModel()
        {
            LoginModelList = new List<LoginModel>();
        }
    }
}