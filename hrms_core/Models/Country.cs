﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class Country:CommonAttributeModel
    {
        [Key]
        public int id{ get; set; }             //primary key
        [Required(ErrorMessage = "Enter Country")]
        public string countrytext { get; set; }
        [ForeignKey("country_id")]
        public virtual ICollection<State> CountryDetails { get; set; }
        [ForeignKey("permanent_country_id")]
        public virtual ICollection<AssociatePersonal> AssociatePersonal_Permanent { get; set; }

    }
}
