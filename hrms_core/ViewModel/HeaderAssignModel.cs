﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class HeaderAssignModel: HeaderAssign
    {
        public List<HeaderAssignModel> HeaderAssignModelList { get; set; }
        public List<AssociateMainModel> AssociateList { get; set; }
        public string header_name { get; set; }
        public string associate_name { get; set; }
        public List<HeaderModel> HeaderModelList { get; set; }
        public HeaderAssignModel()
        {
            HeaderAssignModelList = new List<HeaderAssignModel>();
            HeaderModelList = new List<HeaderModel>();
        }
    }
}
