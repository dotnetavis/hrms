﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class HeaderServices : IHeaderServices
    {
        private readonly ApplicationDbContext _db;
        public HeaderServices(ApplicationDbContext options)
        {
            _db = options;
        }
        public List<HeaderModel> GetHeaderList()
        {
            HeaderModel header = new HeaderModel();
            try
            {
                header.HeaderAssignModelList = _db.HeaderAssigns.Select(x => new HeaderAssignModel
                {
                    header_id = x.header_id,
                    associate_id = x.associate_id,
                    is_selected = x.is_selected,
                    createdby=x.createdby,
                    createdbyname=x.createdbyname,
                    createdon=x.createdon,
                    updatedby=x.updatedby,
                    updatedbyname=x.updatedbyname,
                    updatedon=x.updatedon,
                    isactive=x.isactive
                }).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return header.HeaderModelList;
        }
        public List<PayElementModel> GetPayElementList()
        {
            PayElementModel model = new PayElementModel();
            try
            {
                model.PayElementModelList = _db.PayElements.Select(x => new PayElementModel
                {

                    id = x.id,
                    associate = x.associate,
                    basic = x.basic,
                    hra = x.hra,
                    esi = x.esi,
                    special_skill_allowance = x.special_skill_allowance,
                    mobile_allowance = x.mobile_allowance,
                    conveyance_allowance = x.conveyance_allowance,
                    management_allowance = x.management_allowance,
                    book_periodicals = x.book_periodicals,
                    driver_allowance = x.driver_allowance,
                    medical_allowance = x.medical_allowance,
                    gross_salary = x.gross_salary,
                    pf = x.pf,
                    total_deduction = x.total_deduction,
                    carry_home_salary = x.carry_home_salary,
                    ctc = x.ctc,
                    createdon = x.createdon,
                    createdby = x.createdby,
                    createdbyname = x.createdbyname,
                    updatedon = x.updatedon,
                    updatedby = x.updatedby,
                    updatedbyname = x.updatedbyname,
                    isactive=x.isactive,
                    is_current = x.is_current,
                }).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.PayElementModelList;
        }

        public List<PayElementModel> GetPayElementList(int Id)
        {
            PayElementModel model = new PayElementModel();
            try
            {
                model.PayElementModelList = (from AssociateMaster in _db.AssociateMasters
                                             join PayElement in _db.PayElements on AssociateMaster.id equals PayElement.id
                                             select new PayElementModel
                                             {
                                                 associatename = AssociateMaster.associate_name,
                                                 associate = PayElement.associate,
                                                 basic = PayElement.basic,
                                                 hra = PayElement.hra,
                                                 esi = PayElement.esi,
                                                 special_skill_allowance = PayElement.special_skill_allowance,
                                                 mobile_allowance = PayElement.mobile_allowance,
                                                 conveyance_allowance = PayElement.conveyance_allowance,
                                                 management_allowance = PayElement.management_allowance,
                                                 book_periodicals = PayElement.book_periodicals,
                                                 driver_allowance = PayElement.driver_allowance,
                                                 medical_allowance = PayElement.medical_allowance,
                                                 gross_salary = PayElement.gross_salary,
                                                 pf = PayElement.pf,
                                                 total_deduction = PayElement.total_deduction,
                                                 carry_home_salary = PayElement.carry_home_salary,
                                                 ctc = PayElement.ctc,
                                                 createdon = PayElement.createdon,
                                                 createdby = PayElement.createdby,
                                                 createdbyname = PayElement.createdbyname,
                                                 updatedon = PayElement.updatedon,
                                                 updatedby = PayElement.updatedby,
                                                 updatedbyname = PayElement.updatedbyname,
                                                 is_current = PayElement.is_current,
                                                 isactive=PayElement.isactive
                                             }).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.PayElementModelList;
        }

        public List<AssociateMasterModel> GetAllAssociates()
        {
            HeaderModel header = new HeaderModel();
            try
            {
                header.AssociateList = _db.AssociateMasters.Select(x => new AssociateMasterModel
                {
                    id = x.id,
                    associate_name = x.associate_name
                }).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return header.AssociateList;
        }

        public List<HeaderModel> GetHeader()
        {
            HeaderModel model = new HeaderModel();
            try
            {
                model.HeaderModelList = (from hd in _db.Headers
                                         select new HeaderModel
                                         {
                                             id = hd.id,
                                             headername = hd.headername,
                                             isdeduction = hd.isdeduction,
                                             isactive = hd.isactive,
                                             createdon = hd.createdon,
                                             createdby = hd.createdby,
                                             updatedon = hd.updatedon,
                                             updatedby = hd.updatedby,
                                             createdbyname=hd.createdbyname,
                                             updatedbyname=hd.updatedbyname
                                         }).OrderByDescending(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.HeaderModelList;
        }

        public List<HeaderModel> GetHeaderByAssociateId(int AssociateId)
        {
            HeaderModel model = new HeaderModel();
            try
            {
                model.HeaderModelList = (from hd in _db.Headers.Where(x => x.isactive == true)
                                         from hda in _db.HeaderAssigns.Where(x => x.header_id == hd.id && x.associate_id == AssociateId).DefaultIfEmpty()
                                         select new HeaderModel
                                         {
                                             id = hd.id,
                                             associate = hda.associate_id,
                                             is_selected = hda.is_selected,
                                             headername = hd.headername,
                                             isdeduction = hd.isdeduction,
                                             isactive = hd.isactive,
                                             createdon = hd.createdon,
                                             createdby = hd.createdby,
                                             updatedon = hd.updatedon,
                                             updatedby = hd.updatedby,
                                             createdbyname = hd.createdbyname,
                                             updatedbyname = hd.updatedbyname
                                         }).OrderByDescending(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.HeaderModelList;
        }

        /// <summary>
        /// Get the Header Data,
        /// Created By : Himanshu
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>

      
        public string UpdateHeader(HeaderModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Headers.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    //Check for Duplicate Record
                    bool bool_sameNameExists = _db.Headers.Any(x => x.headername.ToLower().Trim() == model.headername.ToLower().Trim() && x.id != model.id);
                    if (bool_sameNameExists)
                    {
                        Result = MessageHelper.header_update;
                    }
                    else
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            try
                            {
                                data.headername = model.headername;
                                data.isdeduction = model.isdeduction;
                                data.isactive = true;
                                data.updatedon = DateTime.Now;
                                data.createdon = model.createdon;
                                data.updatedby = model.updatedby;
                                data.createdby = model.createdby;
                                data.createdbyname = model.createdbyname;
                                data.updatedbyname = model.updatedbyname;
                                _db.SaveChanges();
                                scope.Complete();
                                Result = MessageHelper.header_addsuccess;
                            }
                            catch (Exception ex)
                            {
                                scope.Dispose();
                                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                Result = MessageHelper.header_adderror;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.header_adderror;
            }
            return Result;
        }
        /// <summary>
        /// Update the Header Data,
        /// Created By : Himanshu
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public string SaveHeader(HeaderModel model)
        {
            Header header = new Header();
            string Result = string.Empty;
            try
            {
                //Check for Duplicate Record
                bool bool_sameNameExists = _db.Headers.Any(x => x.headername.ToLower().Trim() == model.headername.ToLower().Trim());
                if (bool_sameNameExists)
                {
                    Result = MessageHelper.header_duplicate;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            header.headername = model.headername;
                            header.isdeduction = model.isdeduction;
                            header.isactive = true;
                            header.createdon = DateTime.Now;
                            header.updatedon = DateTime.Now;
                            header.updatedby = model.updatedby;
                            header.createdby = model.createdby;
                            header.createdbyname = model.createdbyname;
                            header.updatedbyname = model.updatedbyname;
                            _db.Headers.Add(header);
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.header_addsuccess;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.header_adderror;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.header_adderror;
            }
            return Result;
        }
        /// <summary>
        /// Save the Header Data,
        /// Created By : Himanshu
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public void BindPaySlip()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Disable the Header Data,
        /// Created By : Himanshu 
        ///On Date: 18/06/2018
        /// </summary>
        /// <returns></returns>
        public string DisableHeader(HeaderModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Headers.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = false;
                            data.createdon = DateTime.Now;
                            data.updatedon = DateTime.Now;
                            data.updatedby = model.updatedby;
                            data.createdby = model.createdby;
                            data.createdbyname = model.createdbyname;
                            data.updatedbyname = model.updatedbyname;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Header_Disable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }

        /// <summary>
        /// Enable the Header Data,
        /// Created By : Himanshu 
        ///On Date: 18/06/2018
        /// </summary>
        /// <returns></returns>
        public string EnableHeader(HeaderModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Headers.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = true;
                            data.updatedon = DateTime.Now;
                            data.updatedby = model.updatedby;
                            data.createdby = model.createdby;
                            data.createdbyname = model.createdbyname;
                            data.updatedbyname = model.updatedbyname;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.Header_Enable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }

        public void BindPaySlipWithFilter(DateTime monthyear)
        {
            throw new NotImplementedException();
        }

        public void CheakCurrentMonth_PaySlip(DateTime monthyear, int associate_id)
        {
            throw new NotImplementedException();
        }

        public void DecryptPaySlip_Id(int payslip_id)
        {
            throw new NotImplementedException();
        }

        public void DeleteHeader(int headerid)
        {
            throw new NotImplementedException();
        }

        public void DeletePaySlip(int payslip_id)
        {
            throw new NotImplementedException();
        }

        public void Delete_HeaderAssign(int associate_id)
        {
            throw new NotImplementedException();
        }

        public void GetAbsent(int payslip_id, DateTime date_part)
        {
            throw new NotImplementedException();
        }
        public void GetHeaders()
        {
            throw new NotImplementedException();
        }

        public List<HeaderAssignModel> GetHeader_Assign(int associate_id)
        {

            HeaderAssignModel model = new HeaderAssignModel();
            try
            {
                model.HeaderAssignModelList = _db.HeaderAssigns.Select(
                    x => new HeaderAssignModel
                    {
                        header_id = x.header_id,
                        associate_id = x.associate_id,
                        is_selected = x.is_selected,
                        isactive = x.isactive,
                        createdby = x.createdby,
                        createdbyname = x.createdbyname,
                        createdon = x.createdon,
                        updatedby = x.updatedby,
                        updatedbyname = x.updatedbyname,
                        updatedon = x.updatedon
                    }
                ).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            
            return model.HeaderAssignModelList;
        }

        public void GetHeader_AssignByDeduction(int payslip_id)
        {
            throw new NotImplementedException();
        }

        public void GetLastPaySlpi_Id()
        {
            throw new NotImplementedException();
        }

        public void GetLeaveTaken(int payslip_id)
        {
            throw new NotImplementedException();
        }

        public void SaveSalarySlip()
        {
            throw new NotImplementedException();
        }

        public void SaveSalarySlip_ammount()
        {
            throw new NotImplementedException();
        }

        public string Save_HeaderAssign(HeaderAssignModel model)
        {

            string Result = string.Empty;
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    foreach (var item in model.HeaderModelList)
                    {
                        HeaderAssign header = new HeaderAssign();
                        header.header_id = item.id;
                        header.associate_id = model.associate_id;
                        header.is_selected = item.is_selected;
                        header.isactive = true;
                        header.createdon = DateTime.Now;
                        header.updatedon = DateTime.Now;
                        header.updatedby = model.updatedby;
                        header.createdby = model.createdby;
                        header.createdbyname = model.createdbyname;
                        header.updatedbyname = model.updatedbyname;
                        _db.HeaderAssigns.Add(header);
                        _db.SaveChanges();

                    }

                    scope.Complete();
                    Result = "Header Assigned Successfully";

                }

            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = "Error";
            }
            return Result;
        }

        public string Update_HeaderAssign(HeaderAssignModel model)
        {

            string Result = string.Empty;
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {

                    foreach (var item in model.HeaderModelList)
                    {
                        var header = _db.HeaderAssigns.FirstOrDefault(x => x.header_id == item.id && x.associate_id == model.associate_id);
                        if (header != null)
                        {
                           
                            header.is_selected = item.is_selected;
                            header.isactive =true;
                            header.createdon = model.updatedon;
                            header.updatedon = DateTime.Now;
                            header.updatedby = model.updatedby;
                            header.createdby = model.createdby;
                            header.createdbyname = model.createdbyname;
                            header.updatedbyname = model.updatedbyname;
                            _db.SaveChanges();
                        }

                    }

                    scope.Complete();
                    Result = "Header Updated Successfully";

                }

            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = "Error";
            }
            return Result;
        }

        public HeaderModel GetHearderById(int Id)
        {
            HeaderModel model = new HeaderModel();
            try
            {

                model.HeaderModelList = _db.Headers.Where(x => x.id == Id)
                    .Select(x => new HeaderModel
                    {
                        id = model.id,
                        headername = x.headername,
                        HeaderDetails = x.HeaderDetails,
                        isdeduction = x.isdeduction,
                        isactive = model.isactive,
                        createdon = model.updatedon,
                        updatedon = DateTime.Now,
                        updatedby = model.updatedby,
                        createdby = model.createdby,
                        createdbyname = model.createdbyname,
                        updatedbyname = model.updatedbyname
                    }).ToList();
                model = model.HeaderModelList.FirstOrDefault();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model;
        }
    }
}

