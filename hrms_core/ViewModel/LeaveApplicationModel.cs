﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class LeaveApplicationModel : LeaveApplication
    {
        public List<LeaveApplicationModel> LeaveApplicationModelList { get; set; }
        public List<OpeningLeaveModel> OpeningLeaveModelList { get; set; }
        public List<AssociateMasterModel> associateMasterModelList { get; set; }
        public OpeningLeaveModel openingLeaveModel { get; set; }
        public List<LeaveTypeMasterModel> LeaveList { get; set; }
        public string username { get; set; }
        public string userpass { get; set; }
        public string leavestartdate { get; set; }
        public string leaveenddate { get; set; }
        public string leave_content { get; set; }
        public int associateId { get; set; }
        public string associatecode { get; set; }
        public string associatename { get; set; }
        public string reporting_email { get; set; }
        public string hod_email { get; set; }
        public List<string> additional_reporting_email { get; set; }
        public string designation { get; set; }
        public string official_email { get; set; }
        public string leave_name { get; set; }
        public string sessionName { get; set; }
        public string leave_type_name { get; set; }
        public DateTime search_enddate { get; set; }
        public DateTime search_startdate { get; set; }
        public string approvedbyname { get; set; }
        public string leaveapp_id { get; set; }
        public double balance_leave { get; set; }
        public List<LeaveApplication> leavedetailslist { get; set; }
        public List<Department> DepartmentModelList { get; set; }
        public List<LeaveApplicationModel> PendingleaveApplicationslist { get; set; }
        public List<LeaveApplicationModel> ApprovedleaveApplicationslist { get; set; }
        public List<LeaveApplicationModel> DisApprovedleaveApplicationslist { get; set; }
        public List<string> Mail_to_emails { get; set; }
        public string status_text { get; set; }
        public string employer_associate_name { get; set; }
        public string reporting_person { get; set; }
        public string employer_associate_code { get; set; }
        public string employer_associate_email { get; set; }
        public List<LeaveTypeMaster> leavetypelist { get; set; }
        public List<LeaveApplicationModel> LeaveSummaryList { get; set; }
        public List<int> currentMonthList { get; set; }
        public int selectedYear { get; set; }
        public LeaveApplicationModel()
        {
            openingLeaveModel = new OpeningLeaveModel();
            OpeningLeaveModelList = new List<OpeningLeaveModel>();
            LeaveApplicationModelList = new List<LeaveApplicationModel>();
            LeaveList = new List<LeaveTypeMasterModel>();
            DepartmentModelList = new List<Department>();
            PendingleaveApplicationslist = new List<LeaveApplicationModel>();
            ApprovedleaveApplicationslist = new List<LeaveApplicationModel>();
            DisApprovedleaveApplicationslist = new List<LeaveApplicationModel>();
            leavedetailslist = new List<LeaveApplication>();
            leavetypelist = new List<LeaveTypeMaster>();
            Mail_to_emails = new List<string>();
            LeaveSummaryList = new List<LeaveApplicationModel>();
            currentMonthList = new List<int>();
            associateMasterModelList = new List<AssociateMasterModel>();
        }
    }
}
