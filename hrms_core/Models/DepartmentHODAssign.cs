﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class DepartmentHODAssign : CommonAttributeModel

    {
        [Key]
        public int id { get; set; }
        public int Associate_id { get; set; }
        public int Department_id { get; set; }
    }
}
