﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class DailyActivityReportServices: IDailyActivityReport
    {
        private readonly ApplicationDbContext _db;
        public DailyActivityReportServices(ApplicationDbContext options)
        {
            _db = options;
        }

        public void adddailyreportdetail()
        {
            throw new NotImplementedException();
        }

        public void checkauthenticationOutDuty(int associate_code)
        {
            throw new NotImplementedException();
        }

        public void checkinsertvalue(int associat_id, DateTime reportingdate)
        {
            throw new NotImplementedException();
        }

        public void gatdailyreportdetail(int associate_code)
        {
            throw new NotImplementedException();
        }

        public void getadditionalifficialmail(int associate_code)
        {
            throw new NotImplementedException();
        }

        public void getassosiatename(int associate_code)
        {
            throw new NotImplementedException();
        }

        public void getdailyactivitydetail(int dar_id)
        {
            throw new NotImplementedException();
        }

        public void getdailyreportdetail(int dar_id)
        {
            throw new NotImplementedException();
        }

        public void getdarassosiatename(int dar_id)
        {
            throw new NotImplementedException();
        }

        public void getdepartmenthodmailid(int associate_code)
        {
            throw new NotImplementedException();
        }

        public void getdepartmentid(int associate_id)
        {
            throw new NotImplementedException();
        }

        public void getdesignation(string username)
        {
            throw new NotImplementedException();
        }

        public void getLoginAssociateId(int associate_code)
        {
            throw new NotImplementedException();
        }

        public void getofficialmailid(int associate_code)
        {
            throw new NotImplementedException();
        }

        public void getprojectname()
        {
            throw new NotImplementedException();
        }

        public void getReportingHodPersonName(int associate_code)
        {
            throw new NotImplementedException();
        }

        public void getreportingpersonid(int associate_code)
        {
            throw new NotImplementedException();
        }

        public void getreportingpersonname(int associate_id)
        {
            throw new NotImplementedException();
        }

        public void gettaskname(int projectid)
        {
            throw new NotImplementedException();
        }

        public void gettotalhourdetailforview(int dar_id)
        {
            throw new NotImplementedException();
        }

        public void getusernameForOutDuty()
        {
            throw new NotImplementedException();
        }

        public void loginoutDutyAuthorization(string username, string userpass)
        {
            throw new NotImplementedException();
        }

        public void reportingdetailLogin(string username, string userpass, int associateid)
        {
            throw new NotImplementedException();
        }

        public void saveondutydetail()
        {
            throw new NotImplementedException();
        }

        public void updateauthorizedailyactivityreport(int dar_id)
        {
            throw new NotImplementedException();
        }

        public void updatedailyactivityreport()
        {
            throw new NotImplementedException();
        }

        public void updateondutytable(int dar_id)
        {
            throw new NotImplementedException();
        }
    }
}
