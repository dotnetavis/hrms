﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
   public interface IAttendance
    {
        List<AssociateMasterModel> GetUsers(int? departmentid);
        List<AttendanceDataVm> GetAttendanceDatas(int associate_code,DateTime FromDate, DateTime TODate);
        List<DateTime> GetDates(DateTime FromDate, DateTime TODate);
        List<String> GetLoginTimes(int associate_code, DateTime FromDate, DateTime TODate);
    }
}