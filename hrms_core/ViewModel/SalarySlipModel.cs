﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class SalarySlipModel: SalarySlip
    {
        public List<SalarySlipModel> SalarySlipModelList { get; set; }
        public SalarySlipModel()
        {
            SalarySlipModelList = new List<SalarySlipModel>();
        }
    }
}
