﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    interface ICarryForwardLeaveServices
    {
        void getCarryForwardList();
        void getCarryForwardlistOnSearchClick(string userfullname);
    }
}
