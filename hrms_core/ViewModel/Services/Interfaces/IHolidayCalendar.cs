﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    interface IHolidayServices
    {
        string SaveEditHolidayCalnderDetails(HolidayCalenderModel model);
        List<HolidayCalenderModel> getHolidayCalender();
    }
}
