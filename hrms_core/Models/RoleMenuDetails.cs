﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class RoleMenuDetails:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage = "Please Select Page")]
        [ForeignKey("page_menu_id")]
        public int page_menu_id { get; set; }
        [ForeignKey("role_id")]
        public int role_id { get; set; }
        public bool is_selected { get; set; }
    }
}
