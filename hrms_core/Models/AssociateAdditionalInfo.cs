﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class AssociateAdditionalInfo:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }          //primary key
        public bool mail_to_hod { get; set; }
        [ForeignKey("associate_id")]
        public int associate_id { get; set; }
        public string remark { get; set; }
        public int reporting_person_id { get; set; }
    }
}
