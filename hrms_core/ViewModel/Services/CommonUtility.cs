﻿using hrms_core.EF;
using hrms_core.Models;
using Serilog;
using System;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Threading;

namespace hrms_core.ViewModel.Services
{
    public class CommonUtility
    {
        private static string keyString = "27SEP83M-02FE-B201-2BOB-26NOV1983BYE";

        /// <summary>
        ///  Write Error Log by passing message and function
        /// </summary>
        /// <param name="message"></param> 
        /// <param name="Function"></param> 
        /// <returns></returns>
        /// <remarks></remarks>
        public static void WriteMsgLogs(string message, String Function)
        {
            Log.Information($"({Function}) | {message}");
        }

        /// <summary>
        ///  Write Mail Log by passing message and function
        /// </summary>
        /// <param name="message"></param> 
        /// <param name="Function"></param> 
        /// <returns></returns>
        /// <remarks></remarks>
        public static void WriteMailLogs(string message, String Function)
        {
            Log.Warning($"({Function}) | {message}");
        }

        /// <summary>
        ///  Write Device and IPAddress Log by passing message and function
        /// </summary>
        /// <param name="message"></param> 
        /// <param name="Function"></param> 
        /// <returns></returns>
        /// <remarks></remarks>
        public static void WriteDeviceAndIPAddressLogs(string message, String Function)
        {
            Log.Information($"({Function}) | {message}");
        }

        /// <summary>
        ///  Write Error Log by passing message and function
        /// </summary>
        /// <param name="message"></param> 
        /// <param name="Function"></param> 
        /// <returns></returns>
        /// <remarks></remarks>
        public static void WriteErrorLogs(Exception ex, String Function,ApplicationDbContext _db=null)
        {
            new Thread(() => SendException($"({Function}) | {ex}", _db)).Start();
            Log.Fatal($"({Function}) | {ex}");
        }

        /// <summary>
        ///  Write Error Log by passing message and function
        /// </summary>
        /// <param name="message"></param> 
        /// <param name="QueryText"></param> 
        /// <returns></returns>
        /// <remarks></remarks>
        public static void WriteQueryLogs(String QueryText)
        {
            Log.Debug($"(Query) | {QueryText}");
        }


        #region MD5 Encryption
        /// <summary>
        /// Get Encrpted Value of Passed value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string MD5Convertor(string value)
        {
            return EncryptMD5(keyString, value);
        }

        /// <summary>
        /// Encrypt value
        /// </summary>
        /// <param name="Passphrase"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string EncryptMD5(string Passphrase, string Message)
        {
            try
            {
                byte[] Results;
                System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

                // Step 1. We hash the passphrase using MD5
                // We use the MD5 hash generator as the result is a 128 bit byte array
                // which is a valid length for the TripleDES encoder we use below

                MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
                byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));

                // Step 2. Create a new TripleDESCryptoServiceProvider object
                TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider
                {

                    // Step 3. Setup the encoder
                    Key = TDESKey,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                };

                // Step 4. Convert the input string to a byte[]
                byte[] DataToEncrypt = UTF8.GetBytes(Message);

                // Step 5. Attempt to encrypt the string
                try
                {
                    ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                    Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
                }
                finally
                {
                    // Clear the TripleDes and Hashprovider services of any sensitive information
                    TDESAlgorithm.Clear();
                    HashProvider.Clear();
                }

                // Step 6. Return the encrypted string as a base64 encoded string
                return Convert.ToBase64String(Results);
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", "CommonUtility", System.Reflection.MethodBase.GetCurrentMethod().Name));
                return "";
            }
        }

        #endregion

        #region MD5 Decryption           

        /// <summary>
        /// Get Decrypted value of passed encrypted string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string GetDecryptMD5(string value)
        {
            return DecryptMD5(keyString, value);
        }

        /// <summary>
        /// decrypt value
        /// </summary>
        /// <param name="Passphrase"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string DecryptMD5(string Passphrase, string Message)
        {
            try
            {
                if (string.IsNullOrEmpty(Message))
                    return string.Empty;
                byte[] Results;
                System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

                // Step 1. We hash the passphrase using MD5
                // We use the MD5 hash generator as the result is a 128 bit byte array
                // which is a valid length for the TripleDES encoder we use below

                MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
                byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));

                // Step 2. Create a new TripleDESCryptoServiceProvider object
                TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider
                {

                    // Step 3. Setup the decoder
                    Key = TDESKey,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                };

                Message = Message.Replace(" ", "+"); // Replace space with plus sign in encrypted value if any.

                // Step 4. Convert the input string to a byte[]
                byte[] DataToDecrypt = Convert.FromBase64String(Message);

                // Step 5. Attempt to decrypt the string
                try
                {
                    ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                    Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
                }
                catch
                {
                    return "";
                }
                finally
                {
                    // Clear the TripleDes and Hashprovider services of any sensitive information
                    TDESAlgorithm.Clear();
                    HashProvider.Clear();
                }

                // Step 6. Return the decrypted string in UTF8 format
                return UTF8.GetString(Results);
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", "CommonUtility", System.Reflection.MethodBase.GetCurrentMethod().Name));
                return "";
            }

        }

        #endregion

        private static void SendException(String Message, ApplicationDbContext _db)
        {
            if (_db != null)
            {
                var _emailData = _db.EmailConfigurations.FirstOrDefault();
                if (_emailData != null)
                {
                    MailMessage mail = new MailMessage();
                    try
                    {
                        if (_emailData != null)
                        {
                            string FromEmail = _emailData.email_send_from;
                            string Pass = _emailData.password;

                            SmtpClient SmtpServer = new SmtpClient(_emailData.smtp);
                            mail.From = new MailAddress(FromEmail, "HR");
                            mail.To.Add("seema@avissol.com");
                            mail.To.Add("nagesh@avissol.com");
                            mail.Subject = "HRMS Exception";
                            mail.IsBodyHtml = true;
                            mail.Body = Message;
                            SmtpServer.Host = _emailData.smtp;
                            SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                            SmtpServer.UseDefaultCredentials = false;
                            SmtpServer.Port = _emailData.port;
                            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                         System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
                            { return true; };
                            SmtpServer.Credentials = new System.Net.NetworkCredential(FromEmail, Pass);
                            SmtpServer.EnableSsl = _emailData.ssl;
                            SmtpServer.Send(mail);
                        }
                    }
                    catch (Exception ex)
                    {
                        CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", "CommonUtility", System.Reflection.MethodBase.GetCurrentMethod().Name));
                    }
                }
            }
        }
    }
    public class HostType
    {
        public const string Localhost = "localhost";
    }
}

