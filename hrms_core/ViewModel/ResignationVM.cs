﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class ResignationVM:Resignation
    {
        public bool ExistResignation { get; set; }
        public string official_email { get; set; }
        public string hod_email { get; set; }
        public string username { get; set; }
        public string userpass { get; set; }
        public string reporting_email { get; set; }
        public string associate_name { get; set; }
        public DateTime DOJ { get; set; }
        public string department_name { get; set; }
        public List<string> additional_reporting_email { get; set; }
        public int NoticeDays { get; set; }
        public string encResignationID { get; set; }
        public ResignationVM()
        {
            additional_reporting_email = new List<string>();
        }
    }
    public class ResignationViewModel:ResignationVM
    {
        public List<ResignationVM> ResignationVMList { get; set; }
        public List<ResignationVM> ResignationList { get; set; }
        public ResignationViewModel()
        {
            ResignationVMList = new List<ResignationVM>();
            ResignationList = new List<ResignationVM>();
        }
    }
}
