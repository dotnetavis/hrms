﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class Header:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }         //primary key
        [Required(ErrorMessage = "Enter Header Name")]
        public string headername { get; set; }
       
        public bool isdeduction { get; set; }
        [ForeignKey("header_id")]
        public virtual ICollection<HeaderAssign> HeaderDetails { get; set; }
    }



}
