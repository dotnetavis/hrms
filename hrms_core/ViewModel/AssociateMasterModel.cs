﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class AssociateMasterModel : AssociateMaster
    {
        public List<AssociateMasterModel> AssociateMasterModelList { get; set; }
        public bool is_selected { get; set; }
        public int department_id{ get; set; }
        public string DesignationText { get; set; }
        public List<string> inTimes { get; set; }
        public List<string> outTimes { get; set; }
        public List<DateTime> Dates { get; set; }
        public List<String> LoginTimes { get; set; }
        public Attendancedata attendance { get; set; }
        public List<Attendancedata> attendanceData { get; set; }
        public AssociateMasterModel()
        {
            attendanceData = new List<Attendancedata>();
            AssociateMasterModelList = new List<AssociateMasterModel>();
            LoginTimes = new List<string>();
            Dates = new List<DateTime>();
        } 
    }
}

