﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.Models;

namespace hrms_core.ViewModel
{
    public class ProjectWiseReportVM
    { 
        public int associate_id { get; set; }
        [Required(ErrorMessage ="Select Project")]
        public int project_id { get; set; }
        public string project_name { get; set; }
        public string associate_name { get; set; }
        public string total_hours { get; set; }
        public  List<AssociateMaster> AssociateList { get; set; }
        public List<ProjectMaster> ProjectList { get; set; } 
      public List<ProjectWiseReportVM> ProjectWiseReportList { get; set; }
        public ProjectWiseReportVM()
        {
            AssociateList = new List<AssociateMaster>();
            ProjectList = new List<ProjectMaster>();
            ProjectWiseReportList = new List<ProjectWiseReportVM>();
        }
    }
}
