﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class MenuCreationVM : MenuCreationModel
    {
      public List<MenuCreationVM> menulist { get; set; }
        public MenuCreationVM ()
        {
            menulist = new List<MenuCreationVM>();
        }
    }
}
