﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class HRAttendanceDataVM : Attendancedata
    {
        public List<AttendanceDataVm> AttendanceDataVmList { get; set; }
        
        public int associateId { get; set; }
        public string username { get; set; }
        public string department { get; set; }
        public bool selectAll { get; set; }
        public string changedBy { get; set; }
        //public int associate_id { get; set; }
        public List<AssociateMasterModel> ListUserName { get; set; }
        //public List<string> inTimes { get; set; }
        //public List<string> outTimes { get; set; }
        public List<DateTime> Dates { get; set; }
        public List<String> LoginTimes { get; set; }
        public List<AssociateMasterModel> associateMasters { get; set; }
        public List<AssociateVM> AssociateVMList { get; set; }
        public HRAttendanceDataVM()
        {
            
            ListUserName = new List<AssociateMasterModel>();
            LoginTimes = new List<string>();
            AssociateVMList = new List<AssociateVM>();
            Dates = new List<DateTime>();
            AttendanceDataVmList = new List<AttendanceDataVm>();
            associateMasters = new List<AssociateMasterModel>();
        }
    }
}
