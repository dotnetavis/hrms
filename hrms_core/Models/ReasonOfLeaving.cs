﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class ReasonOfLeaving:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage ="Enter Reason")]
        public string reasontext { get; set; }
        [ForeignKey("employee_reason_id")]
        public virtual ICollection<AssociateRelieve> AssociateRelieve_employeeReason { get; set; }
        [ForeignKey("employer_reason_id")]
        public virtual ICollection<AssociateRelieve> AssociateRelieve_employerReason { get; set; }
    }
}
