﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace hrms_core.ViewModel.Services
{
    public class CityServices: ICityServices
    {
        private readonly ApplicationDbContext _db;
        public CityServices(ApplicationDbContext options) => _db = options;
        /// <summary>
        /// Get the City Data,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public List<CityModel> GetCity()
        {
            CityModel model = new CityModel();
            try
            {
                model.CityModelList = (from City in _db.Citys.Where(x=>x.isactive == true)
                                        join state in _db.States on City.state_id equals state.id
                                        select new CityModel
                                        {
                                            id = City.id,
                                            citytext = City.citytext,
                                            state_id = City.state_id,
                                            state_text = state.statetext,
                                            isactive = City.isactive,
                                            createdon = City.createdon,
                                            createdby = City.createdby,
                                            updatedon = City.updatedon,
                                            updatedby = City.updatedby
                                        }).OrderByDescending(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.CityModelList;
        }

        /// <summary>
        /// Get the City Data Only Active,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public List<CityModel> GetCity_Active()
        {
            CityModel model = new CityModel();
            try
            {
                model.CityModelList = (from City in _db.Citys.Where(x => x.isactive == true)
                                       join state in _db.States.Where(x => x.isactive == true) on City.state_id equals state.id
                                       select new CityModel
                                       {
                                           id = City.id,
                                           citytext = City.citytext,
                                           state_id = City.state_id,
                                           state_text = state.statetext,
                                           isactive = City.isactive,
                                           createdon = City.createdon,
                                           createdby = City.createdby,
                                           updatedon = City.updatedon,
                                           updatedby = City.updatedby
                                       }).OrderBy(x => x.citytext).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.CityModelList;
        }

        /// <summary>
        /// Get the City Data By StateId Only Active,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <param name="StateId"></param>
        /// <returns></returns>
        public List<CityModel> GetCity_ByStateId(int StateId)
        {
            CityModel model = new CityModel();
            try
            {
                model.CityModelList = (from City in _db.Citys.Where(x => x.isactive == true && x.state_id==StateId)
                                       join state in _db.States.Where(x => x.isactive == true) on City.state_id equals state.id
                                       select new CityModel
                                       {
                                           id = City.id,
                                           citytext = City.citytext,
                                           state_id = City.state_id,
                                           state_text = state.statetext,
                                           isactive = City.isactive,
                                           createdon = City.createdon,
                                           createdby = City.createdby,
                                           updatedon = City.updatedon,
                                           updatedby = City.updatedby
                                       }).OrderBy(x => x.citytext).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.CityModelList;
        }
        /// <summary>
        /// Save City
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string SaveCity(CityModel model)
        {
            string Result = string.Empty;
            City city = new City();
            try
            {
                bool Same_Name = _db.Citys.Any(x => x.citytext.ToLower().Trim() == model.citytext.ToLower().Trim());
                if(Same_Name)
                {
                    Result = MessageHelper.City_Duplicate;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            city.state_id = model.state_id;
                            city.citytext = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(model.citytext.ToLower());
                            city.createdby = model.createdby;
                            city.createdbyname = model.createdbyname;
                            city.createdon = DateTime.Now;
                            city.updatedby = model.updatedby;
                            city.updatedbyname = model.updatedbyname;
                            city.updatedon = DateTime.Now;
                            city.isactive = true;
                            _db.Citys.Add(city);
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.City_AddSuccess;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.City_AddError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.City_AddError;
            }
            return Result;
        }

        public string UpdateCity(CityModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Citys.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                { 
                    bool Same_Name = _db.Citys.Any(x => x.citytext.ToLower().Trim() == model.citytext.ToUpper().Trim() && x.id != model.id);
                if(Same_Name)
                {
                    Result = MessageHelper.City_Duplicate;
                }
                    else
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            try
                            {
                                data.state_id = model.state_id;
                                data.citytext = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(model.citytext.ToLower());
                                data.createdby = model.createdby;
                                data.createdbyname = model.createdbyname;
                                data.createdon = DateTime.Now;
                                data.updatedby =model.updatedby;
                                data.updatedbyname = model.updatedbyname;
                                data.updatedon = DateTime.Now;
                                data.isactive = true;
                                _db.SaveChanges();
                                scope.Complete();
                                Result = MessageHelper.City_AddSuccess;
                            }
                            catch (Exception ex)
                            {
                                scope.Dispose();
                                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                Result = MessageHelper.City_AddError;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.City_AddError;
            }
            return Result;
        }

        public string EnableCity(CityModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Citys.FirstOrDefault(x => x.id == model.id);
                if(data!=null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = true;
                            data.updatedon = DateTime.Now;
                            data.updatedby = model.updatedby;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.City_Enable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }

        public string DisableCity(CityModel model)
        {
            string Result = string.Empty;
            try
            {
                var data = _db.Citys.FirstOrDefault(x => x.id == model.id);
                if (data != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            data.isactive = false;
                            data.updatedon = DateTime.Now;
                            data.updatedby = model.updatedby;
                            _db.SaveChanges();
                            scope.Complete();
                            Result = MessageHelper.City_Disable;
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                            Result = MessageHelper.OtherError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.OtherError;
            }
            return Result;
        }
    }
}
         