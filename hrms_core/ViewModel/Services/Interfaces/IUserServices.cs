﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.EF;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IUserServices
    {
        List<UserModel> GetUserDetails(UserModel user);
        List<UserModel> GetUserDetailsByGUID(string Guid);
        List<UserModel> SavePassword(UserModel user);
    }
}
