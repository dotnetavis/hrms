﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class LeaveAppMain:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }     //primary key
        public int leaveapp_id { get; set; }
        public DateTime leave_date { get; set; }
        public double totalleave { get; set; }



    }
}
