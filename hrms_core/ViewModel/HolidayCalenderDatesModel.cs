﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class HolidayCalenderDatesModel: HolidayCalenderDates
    {
        public List<HolidayCalenderDatesModel> HolidayCalenderDatesModelList { get; set; }
        public HolidayCalenderDatesModel()
        {
            HolidayCalenderDatesModelList = new List<HolidayCalenderDatesModel>();
        }
    }
}
