﻿using System;
using System.Linq;
using hrms_core.EF;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace hrms_core.Controllers
{
    public class ProjectController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IProjectServices projectServices => new ProjectServices(_db);
        private IDepartmentServices departmentServices => new departmentServices(_db);
        public ProjectController(ApplicationDbContext options) => _db = options;

        [HttpGet]
        public IActionResult Index(int? id)
        {
            ProjectMasterModel model = new ProjectMasterModel();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                model.ProjectMasterModelList = projectServices.GetProjectList();
                model.DepartmentList = departmentServices.GetDepartment_Active();
                model.expectedcompletiondate = DateTime.Now;
                model.completiondate = DateTime.Now;
                model.datestart = DateTime.Now;
                //model.DepartmentList.Insert(0, new DepartmentModel { id = 0, departmenttext = "--Select--" });
                if (id > 0 && id != null)
                {
                    if (model.ProjectMasterModelList.Count > 0 && model.ProjectMasterModelList != null)
                    {
                        var data = model.ProjectMasterModelList.FirstOrDefault(x => x.id == id);
                        if (data != null)
                        {
                            model.id = data.id;
                            model.projecttext = data.projecttext;
                            model.datestart =Convert.ToDateTime(data.datestart.ToString("yyyy-MM-dd"));
                            //model.departmentid = data.departmentid;
                            model.expectedcompletiondate = data.expectedcompletiondate;
                            model.completiondate = data.completiondate;
                            model.isactive = data.isactive;
                            model.createdby = data.createdby;
                            model.createdon = data.createdon;
                            model.updatedby = model.updatedby;
                            model.updatedon = model.updatedon;
                        }

                        var projectdeptdata = _db.projectdeptassigns.Where(x => x.project_id == id).ToList();
                        if (projectdeptdata.Count > 0)
                        {
                            foreach (var item in model.DepartmentList)
                            {
                                var dept_data = projectdeptdata.FirstOrDefault(x => x.department_id == item.id);
                                if (dept_data != null)
                                {
                                    item.is_selected = dept_data.is_selected;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(ProjectMasterModel model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.associate_id;
                model.createdbyname = SesMdl.associate_name;
                model.updatedby = SesMdl.associate_id;
                model.updatedbyname = SesMdl.associate_name;
                if (model.id > 0)
                {
                    TempData["msg"] = projectServices.UpdateProject(model);
                }
                else
                {
                    TempData["msg"] = projectServices.SaveProject(model);
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("Index", "Project", new { id = 0 });
        }

        [HttpPost]
        public IActionResult DisableProject(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            ProjectMasterModel model = new ProjectMasterModel();
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            model.id = id;
            model.updatedby = 1;
            TempData["msg"] = projectServices.DisableProject(model);
            return Json(true);
        }

        [HttpPost]
        public IActionResult EnableProject(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            ProjectMasterModel model = new ProjectMasterModel();
            model.createdby = SesMdl.associate_id;
            model.createdbyname = SesMdl.associate_name;
            model.updatedby = SesMdl.associate_id;
            model.updatedbyname = SesMdl.associate_name;
            model.id = id;
            model.updatedby = 1;
            TempData["msg"] = projectServices.EnableProject(model);
            return Json(true);
        }

        [HttpGet]
        public IActionResult ProjectWiseReport()
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                ProjectWiseReportVM report = new ProjectWiseReportVM();
                return View(report);
            }
            }

        [HttpPost]
        public IActionResult ProjectWiseReport(ProjectWiseReportVM report, DateTime? FromDate, DateTime? TODate)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (FromDate == null)
                    FromDate = DateTime.Now.Date;
                if (TODate == null)
                    TODate = DateTime.Now.Date;

                    
                return View(report);
            }
           
        }

    }
}