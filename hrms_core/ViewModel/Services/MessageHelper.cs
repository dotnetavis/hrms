﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services
{
    public static class MessageHelper
    {
        #region ConfigurationSetting
        public const string ConfigurationSetting_AddSuccess = "Configuration Setting Changes Saved Successfully.";
        public const string ConfigurationSetting_AddError = "Configuration Setting Changes not Saved.";
        #endregion

        #region EmailConfigurationSetting
        public const string EmailConfiguration_AddSuccess = "Email Configuration Changes Saved Successfully.";
        public const string EmailConfiguration_AddError = "Email Configuration Changes not Saved.";
        #endregion

        #region Designation
        public const string Designation_AddSuccess = "Designation Changes Saved Successfully.";
        public const string Designation_AddError = "Designation Changes not Saved.";
        public const string Designation_Duplicate = "Designation Already Exist.";
        public const string Designation_Disable = "Designation Disabled Successfully.";
        public const string Designation_Enable = "Designation Enabled Successfully.";
        #endregion

        #region Department
        public const string Department_AddSuccess = "Department Changes Saved Successfully.";
        public const string Department_AddError = "Department Changes not Saved.";
        public const string Department_Duplicate = "Department Already Exist.";
        public const string Department_Disable = "Department Disabled Successfully.";
        public const string Department_Enable = "Department Enabled Successfully.";
        #endregion

        #region Daily Activity Report
        public const string DAR_AddSuccess = "DAR Changes Saved Successfully.";
        public const string DAR_AddError = "DAR Changes not Saved.";
        public const string DAR_Duplicate = "DAR Already Exist.";
        public const string DAR_Disable = "DAR Disabled Successfully.";
        public const string DAR_Enable = "DAR Enabled Successfully.";
        #endregion

        #region Role
        public const string Role_AddSuccess = "Role Changes Saved Successfully.";
        public const string Role_AddError = "Role Changes not Saved.";
        public const string Role_Duplicate = "Role Already Exist.";
        public const string Role_Disable = "Role Disabled Successfully.";
        public const string Role_Enable = "Role Enabled Successfully.";
        #endregion

        #region Header Name
        public const string header_update = "Header Updated.";
        public const string header_adderror = "Header Changes not Saved.";
        public const string header_duplicate = "Header Name Already Exists.";
        public const string header_addsuccess = "Header Changes Saved Successfully.";
        public const string Header_Disable = "Header Disabled Successfully.";
        public const string Header_Enable = "Header Enabled Successfully.";

        #endregion

        #region Shift Master
        public const string shift_update = "Shift Master Updated.";
        public const string shift_adderror = "Changes not Saved.";
        public const string shift_duplicate = "Name Already Exists.";
        public const string shift_addsuccess = "Changes Saved Successfully.";
        #endregion

        #region Project
        public const string Project_AddSuccess = "Project Changes Saved Successfully.";
        public const string Project_AddError = "Project Changes not Saved.";
        public const string Project_Duplicate = "Project Already Exists.";
        public const string Project_Disable = "Project Disabled Successfully.";
        public const string Project_Enable = "Project Enabled Successfully.";
        #endregion
        public const string PaySlip_Duplicate = "Pay Slip Already Exists.";
        public const string PaySlip_AddSuccess = "Pay Slip Saved Successfully.";
        public const string PaySlip_AddError = "Pay Slip Changes not Saved.";
        public const string EmailSent = "Mail Sent Successfully";
        public const string EmailError = "Something went wrong, Mail not Sent";

        #region CommonMessages
        public const string OtherError = "Something went wrong while Processing the Data, Please check Logs.";
        #endregion

        #region Forgot Password
        public const string Forgot_PasswordCreated = "New Password Created Successfully";
        #endregion

        #region TaskMaster
        public const string Task_Duplicate = "Task Already Exists.";
        public const string Task_AddSuccess = "Task Changes Saved Successfully.";
        public const string Task_AddError = "Task Changes not Saved.";
        public const string Task_Enable = "Task Enabled Successfully.";
        #endregion

        #region Associate
        public const string Associate_AddSuccess = "Associate Changes Saved Successfully.";
        public const string Associate_AddError = "Associate Changes not Saved.";
        public const string Associate_Duplicate = "Associate Already Exist.";
        public const string Associate_Disable = "Associate Disabled Successfully.";
        public const string Associate_Enable = "Associate Enabled Successfully.";
        #endregion
        #region Qualification
        public const string qualification_update = "Qualification Updated.";
        public const string qualification_adderror = "Qualification Changes not Saved.";
        public const string qualification_duplicate = "Qualification Name Already Exists.";
        public const string qualification_addsuccess = "Qualification Changes Saved Successfully.";
        public const string qualification_Disable = "Qualification Disabled Successfully.";
        public const string qualification_Enable = "Qualification Enabled Successfully.";

        #endregion
        #region Policy
        public const string Policy_Duplicate = "Policy Already Exists For This Year.";
        public const string Policy_AddSuccess = "Policy Added Successfully";
        public const string Policy_Update = "Policy updated SAuccessfully";
        public const string Policy_AddError = "Policy Changes Not Saved.";
        public const string Policy_Enable = "Policy Enabled Successfully";
        public const string Policy_Disabled = "Policy Disabled Successfully";
        #endregion

        #region Leave
        public const string Leave_AddError = "Leave Already Exists.";
        public const string Leave_AddSuccess = "Leave Saved Successfully.";
        public const string Leave_Duplicate = "Leave Already Exists";
        public const string Leave_UpdateError = "Leave Changes Not Saved.";
        public const string Leave_UpdateSuccess = "Leave Updated Successfully.";
        public const string Leave_AddDetails = "Fill the mandatory fields";

        #endregion
        #region Opening Leave
        public const string OpeningLeave_AddError = "Opening Leave Already Exists.";
        public const string OpeningLeave_AddSuccess = "Opening Leave Saved Successfully.";
        public const string OpeningLeave_Duplicate = "Opening Leave Already Exists";
        public const string OpeningLeave_UpdateError = "Opening Leave Changes Not Saved.";
        public const string OpeningLeave_UpdateSuccess = "Opening Leave Updated Successfully.";
        public const string OpeningLeave_AddDetails = "Fill the mandatory fields";
        #endregion
        #region Employee Dashboard
        public const string Employee_AddSuccess = "Changes Saved Successfully.";
        public const string Employee_AddError = "Changes not Saved.";
        public const string Employee_Duplicate = "Already Exists.";
        public const string Employee_Disable = "Disabled Successfully.";
        public const string Employee_Enable = "Enabled Successfully.";
        #endregion
        #region Profile Page
        public const string ProfilePage_AddSuccess = "Profile Page Changes Saved Successfully.";
        public const string ProfilePage_AddError = "Profile Page Changes not Saved.";
        public const string ProfilePage_Duplicate = "Profile Page Already Exist.";
        public const string ProfilePage_Disable = "Profile Page Disabled Successfully.";
        public const string ProfilePage_Enable = "Profile Page Enabled Successfully.";
        #endregion

        #region Country 
        public const string Country_AddSuccess = "Country Changes Saved Successfully.";
        public const string Country_AddError = "Country Changes not Saved.";
        public const string Country_Duplicate = "Country Already Exist.";
        public const string Country_Disable = "Country Disabled Successfully.";
        public const string Country_Enable = "Country Enabled Successfully.";
        #endregion

        #region State 
        public const string State_AddSuccess = "State Changes Saved Successfully.";
        public const string State_AddError = "State Changes not Saved.";
        public const string State_Duplicate = "State Already Exist.";
        public const string State_Disable = "State Disabled Successfully.";
        public const string State_Enable = "State Enabled Successfully.";
        #endregion

        #region City
        public const string City_AddSuccess = "City Changes Saved Successfully.";
        public const string City_AddError = "City Changes not Saved.";
        public const string City_Duplicate = "City Already Exist.";
        public const string City_Disable = "City Disabled Successfully.";
        public const string City_Enable = "City Enabled Successfully.";
        #endregion

        #region HolidayCalender
        public const string HolidayCalender_AddSuccess = "Holiday Changes Saved Successfully.";
        public const string HolidayCalender_AddError = "Holiday Changes not Saved.";
        public const string HolidayCalender_Duplicate = "Holiday Already Exist.";
        public const string HolidayCalender_Disable = "Holiday Disabled Successfully.";
        public const string HolidayCalender_Enable = "Holiday Enabled Successfully.";
        #endregion

        #region Resignation
        public const string Resignation_AddSuccess = "Your Resignation Submitted Successfully.";
        public const string Resignation_AddError = "Your Resignation not Submitted Successfully!!";
        public const string Resignation_Duplicate = "You Already Submitted your Resignation.";
        public const string Resignation_SaveChanges = "Changes Saved Successfully.";
        public const string Resignation_ApprovedSuccess = "Approved Successfully.";
        public const string Resignation_RejectSuccess = "Reject Successfully.";
        #endregion
    }

}
