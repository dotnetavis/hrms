﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IEmployeeDashboardServices
    {
        string SaveEmployeeDetails(EmployeeDashboardModel model);
        List<EmployeeDashboardModel> GetEmployeeDetails();
        string UpdateEmployeeDeatails(EmployeeDashboardModel model);
        string DisableEmployee(EmployeeDashboardModel model);
        string EnableEmployee(EmployeeDashboardModel model);
        List<AssociateMasterModel> GetEmployeeByDepartmentId(int DepartmentId);
        string GetDesignationByAssociateId(int AssociateId);
        List<EmployeeDashboardModel> GetEmployeeDetailsByID(int associateid);
    }
}
