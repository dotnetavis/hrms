﻿using hrms_core.EF;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using NUglify.Helpers;
using Microsoft.EntityFrameworkCore.Internal;
using System.Transactions;
using hrms_core.Models;

namespace hrms_core.ViewModel.Services
{
    public class HRAttendanceService : IHRAttendance
    {
        private readonly ApplicationDbContext _db;
        public HRAttendanceService(ApplicationDbContext options) => _db = options;
        public List<AssociateMasterModel> GetUsers(int? associateid)
        {
            AssociateMasterModel model = new AssociateMasterModel();
            try
            {
                if (!associateid.HasValue)
                {
                    model.AssociateMasterModelList = _db.AssociateMasters.Where(x => x.isactive == true)
                .Select(x => new AssociateMasterModel
                {
                    id = x.id,
                    associate_name = x.associate_name,
                    applicable_to = x.applicable_to,
                    application_date = x.application_date,
                    associate_code = x.associate_code,
                    createdby = x.createdby,
                    createdon = x.createdon,
                    updatedby = x.updatedby,
                    updatedon = x.updatedon,
                    isactive = x.isactive
                }).OrderByDescending(m => m.updatedon).ToList();
                }
                else
                {
                    AssociateMasterModel associate = new AssociateMasterModel();
                    var associatemodel = _db.AssociateMasters.FirstOrDefault(x => x.id == associateid);
                    associate = new AssociateMasterModel()
                    {
                        id = associatemodel.id,
                        associate_name = associatemodel.associate_name,
                        applicable_to = associatemodel.applicable_to,
                        application_date = associatemodel.application_date,
                        associate_code = associatemodel.associate_code,
                        createdby = associatemodel.createdby,
                        createdon = associatemodel.createdon,
                        updatedby = associatemodel.updatedby,
                        updatedon = associatemodel.updatedon,
                        isactive = associatemodel.isactive
                    };
                    model.AssociateMasterModelList.Add(associate);
                }

            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            var DistinctData = model.AssociateMasterModelList.DistinctBy(x => x.id).ToList();
            return DistinctData;
            // return null;
        }
        public List<AttendanceDataVm> GetAttendanceDatas(int associate_code, DateTime FromDate, DateTime TODate)
        {
            AttendanceDataVm model = new AttendanceDataVm();
            try
            {
                model.AttendanceDataVmList = (from att in _db.attendancedata.Where(x => x.attendancedate.Date >= FromDate.Date && x.attendancedate.Date <= TODate.Date)
                                              join asso in _db.AssociateMasters on associate_code equals asso.id
                                              select new AttendanceDataVm()
                                              {
                                                  associate_code = asso.associate_code,
                                                  username = asso.associate_name,
                                                  intime = att.intime.ToString(),
                                                  attendancedate = att.attendancedate.Date,
                                                  outtime = att.outtime.ToString(),
                                                  logintime = att.logintime
                                              }).ToList();
                return model.AttendanceDataVmList;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return null;
            }
        }
        public List<DateTime> GetDates(DateTime FromDate, DateTime TODate)
        {
            AttendanceDataVm model = new AttendanceDataVm();
            var dates = new List<DateTime>();

            for (var dt = FromDate; dt <= TODate; dt = dt.AddDays(1))
            {
                dates.Add(dt);
            }
            return dates;
        }
        public List<String> GetLoginTimes(int associate_code, DateTime FromDate, DateTime TODate)
        {
            try
            {
                AttendanceDataVm model = new AttendanceDataVm
                {
                    AttendanceDataVmList = GetAttendanceDatas(associate_code, FromDate, TODate)
                };
                for (DateTime i = FromDate; i <= TODate;)
                {
                    var login = _db.attendancedata.Where(x => x.attendancedate == i.Date && x.id == associate_code).FirstOrDefault().logintime;

                    model.LoginTimes.Add(login.ToString());
                }
                return model.LoginTimes;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return null;
            }

        }
        public bool changeAttendance(HRAttendanceDataVM model)
        {
            bool Result=false;
            Attendancedata attendancedata = new Attendancedata();
            AttendanceChanges attendanceChanges = new AttendanceChanges();
            if (model != null)
            {
                if (model.AttendanceDataVmList != null)
                {
                    var data = model.AttendanceDataVmList.Where(x => x.is_selected == true).ToList();
                    foreach (var item in data)
                    {
                        if (item.id > 0)
                        {
                            //update
                            attendancedata = _db.attendancedata.FirstOrDefault(x => x.id == item.id);
                            if (attendancedata != null)
                            {
                                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                                {
                                    try
                                    {
                                        if(attendancedata.intime!=item.intime || attendancedata.outtime != item.outtime)
                                        {
                                            attendanceChanges.attendancedate = attendancedata.attendancedate;
                                            attendanceChanges.associate_code = attendancedata.associate_code;
                                            attendanceChanges.associate_name = item.associateName;
                                            attendanceChanges.beforeintime = attendancedata.intime;
                                            attendanceChanges.beforeouttime = attendancedata.outtime;
                                            attendanceChanges.afterintime = item.intime;
                                            attendanceChanges.afterouttime = item.outtime;
                                            attendanceChanges.changed_by = model.changedBy;
                                            attendanceChanges.changed_on = DateTime.Now;
                                            _db.attendancechanges.Add(attendanceChanges);
                                            attendancedata.attendancedate = item.attendancedate;
                                            attendancedata.associate_code = item.associate_code;
                                            attendancedata.intime = item.intime;
                                            attendancedata.outtime = item.outtime;
                                            if (Convert.ToDateTime(item.intime) <= Convert.ToDateTime(item.outtime))
                                            {
                                                attendancedata.logintime = Convert.ToString(Convert.ToDateTime(item.outtime) - Convert.ToDateTime(item.intime));
                                            }
                                            else
                                            {
                                                attendancedata.logintime = Convert.ToString(Convert.ToDateTime(item.outtime) - Convert.ToDateTime(item.intime) + DateTime.MaxValue.TimeOfDay);
                                                attendancedata.logintime = attendancedata.logintime.Substring(0, 8);
                                            }
                                            _db.SaveChanges();
                                            scope.Complete();
                                            Result = true;
                                        }
                                        
                                    }
                                    catch (Exception ex)
                                    {
                                        scope.Dispose();
                                        CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                        Result = false;
                                        return Result;
                                    }
                                }
                            }
                            
                        }
                        else
                        {
                            //insert
                            try
                            {

                                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                                {
                                    try
                                    {
                                        attendanceChanges.attendancedate = item.attendancedate;
                                        attendanceChanges.associate_code = item.associate_code;
                                        attendanceChanges.associate_name = item.associateName;
                                        attendanceChanges.beforeintime = "00:00:00";
                                        attendanceChanges.beforeouttime = "00:00:00";
                                        attendanceChanges.afterintime = item.intime;
                                        attendanceChanges.afterouttime = item.outtime;
                                        attendanceChanges.changed_by = model.changedBy;
                                        attendanceChanges.changed_on = DateTime.Now;
                                        _db.attendancechanges.Add(attendanceChanges);
                                        attendancedata.attendancedate = item.attendancedate;
                                        attendancedata.associate_code = item.associate_code;
                                        attendancedata.intime = item.intime;
                                        attendancedata.outtime = item.outtime;
                                        if (Convert.ToDateTime(item.intime) <=Convert.ToDateTime(item.outtime))
                                        {
                                            attendancedata.logintime = Convert.ToString(Convert.ToDateTime(item.outtime) - Convert.ToDateTime(item.intime));
                                        }
                                        else
                                        {
                                            attendancedata.logintime = Convert.ToString(Convert.ToDateTime(item.outtime) - Convert.ToDateTime(item.intime) + DateTime.MaxValue.TimeOfDay);
                                            attendancedata.logintime = attendancedata.logintime.Substring(0, 8);
                                        }
                                        _db.attendancedata.Add(attendancedata);
                                        //country.countrytext = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(model.countrytext.ToLower()); ;
                                        //country.createdby = model.createdby;
                                        //country.createdbyname = model.createdbyname;
                                        //country.createdon = DateTime.Now;
                                        //country.updatedby = model.updatedby;
                                        //country.updatedbyname = model.updatedbyname;
                                        //country.updatedon = DateTime.Now;
                                        //country.isactive = true;
                                        //_db.Countrys.Add(country);
                                        _db.SaveChanges();
                                        scope.Complete();
                                        Result = true;
                                    }
                                    catch (Exception ex)
                                    {
                                        scope.Dispose();
                                        CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                        Result = false;
                                        return Result;
                                    }
                                }
                            }

                            catch (Exception ex)
                            {
                                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                                Result = false;
                                return false;
                            }
                            
                        }
                    }
                }
            }
            return Result;
        }
    }
}
