﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface ITaskCreationServices
    {
        void getproductlist();
        void getassociatelist(int projectid);
        void insertTaskDetaiils();
        void getTaskMasterDetaiils();
        void editTaskMasterDetaiils(int taskid);
        void updateTaskMasterDetaiils();
        void updateTaskMasterStatus(int taskid);
    }
}
