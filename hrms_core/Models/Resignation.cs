﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class Resignation:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        [ForeignKey("associate_id")]
        public int associate_id { get; set; }
        [Required(ErrorMessage = "Reason Required")]
        public string resignation_text { get; set; }
        public string reply_comment { get; set; }
        public int approved_by { get; set; }
        public bool? is_approved { get; set; }
        public string approved_by_name { get; set; }
    }
}
