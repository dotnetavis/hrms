﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IEmailService
    {
        bool SendEmailAsync(string email, string subject, string message);
    }
}
