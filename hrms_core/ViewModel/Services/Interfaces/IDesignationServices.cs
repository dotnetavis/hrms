﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    public interface IDesignationServices
    {
        /// <summary>
        /// Get the Configuration Data,
        /// Created By : Bilal 
        ///On Date: 22/05/2018
        /// </summary>
        /// <returns></returns>
        List<DesignationModel> GetDesignation();

        /// <summary>
        /// Save the Configuration Data,
        /// Created By : Bilal 
        ///On Date: 22/05/2018
        /// </summary>
        /// <returns></returns>
        string SaveDesignation(DesignationModel model);

        /// <summary>
        /// Update the Configuration Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        string UpdateDesignation(DesignationModel model);

        /// <summary>
        /// Disable the Designation Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        string DisableDesignation(DesignationModel model);

        /// <summary>
        /// Enable the Designation Data,
        /// Created By : Bilal 
        ///On Date: 23/05/2018
        /// </summary>
        /// <returns></returns>
        string EnableDesignation(DesignationModel model);

        /// <summary>
        /// Get the Designation Data,
        /// Created By : Bilal 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        List<DesignationModel> GetDesignation_Active();
    }
}
