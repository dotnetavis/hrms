﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class AssociateAttendanceModel: AssociateAttendance
    {
        public List<AssociateAttendanceModel> AssociateAttendanceModelList { get; set; }
        public AssociateAttendanceModel()
        {
            AssociateAttendanceModelList = new List<AssociateAttendanceModel>();
        }
    }
}
