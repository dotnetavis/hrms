﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class LeaveTypeMaster:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }         //primary key
        [Required(ErrorMessage = "Enter Leave Code")]
        public string leave_code { get; set; }
        [Required(ErrorMessage = "Enter Leave Name")]
        public string leavetext { get; set; }
        [Required(ErrorMessage = "Enter Minimum Days")]
        public string min_days { get; set; }
        [Required(ErrorMessage = "Enter Maximum Days")]
        public string max_days { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = "Select Leave Apply")]
        public string leave_apply { get; set; }
        [Required(ErrorMessage = "Enter Before Minimum")]
        public string before_min { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = "Select Leave Type")]
        public string leave_type { get; set; }
        public bool is_default { get; set; }
        public bool carry_forward { get; set; }
        public bool leave_balance_check { get; set; }
        public bool holiday_inclusive { get; set; }
      
        [ForeignKey("leavetype_id")]
        public virtual ICollection<LeaveApplication> LeaveApplicationDetails { get; set; }
    }
}
