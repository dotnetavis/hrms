﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services.Interfaces
{
    interface IShiftServices
    {
       void getShiftMasterList();
       void insertShiftMasterList();
       void editShiftMasterList(int shift_id);
       void updateShiftMasterList();
       void updateShiftStatus();
        List<ShiftModel> GetShift();

    }
}
