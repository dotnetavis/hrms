﻿using hrms_core.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class AssociateVM : CommonAttributeModel
    {
        public List<DesignationModel> DesignationModelList { get; set; }
        public List<DepartmentModel> DepartmentModelList { get; set; }
        public List<RoleMasterModel> RoleList { get; set; }

        public List<AssociateAdditionalInfoModel> AssociateAdditionalInfoModelList { get; set; }
        public List<AssociateQualificationModel> AssociateQualificationModelList { get; set; }
        public List<AssociateSkillModel> AssociateSkillModelList { get; set; }
        public List<AssociateExperienceModel> AssociateExperienceModelList { get; set; }
        public List<AssociateMainModel> AssociateMainModelList { get; set; }
        public List<CountryModel> PresentCountryModelList { get; set; }
        public List<StateModel> PresentStateModelList { get; set; }
        public List<CityModel> PresentCityModelList { get; set; }
        
        public List<CountryModel> PermanentCountryModelList { get; set; }
        public List<StateModel> PermanentStateModelList { get; set; }
        public List<CityModel> PermanentCityModelList { get; set; }

        public AssociateMasterModel associateMaster { get; set; }
        public AssociateMainModel associateMain { get; set; }
        public AssociateAdditionalInfoModel associateAdditionalInfo { get; set; }
        public AssociateAttachedInfoModel associateAttachedInfo { get; set; }
        public QualificationModel qualificationModel { get; set; }
        public AssociateQualificationModel associateQualification { get; set; }
        public AssociateShiftModel associateShift { get; set; }
        public AssociatePersonalModel associatePersonal { get; set; }
        public AssociateSkillModel associateSkill { get; set; }
        public AssociateExperienceModel associateExperience { get; set; }
        public ProfilePageModel profilepage { get; set; }
        public EmployeeDashboardModel employeeDashboard { get; set; }
        public List<EmployeeDashboardModel> EmployeeDashboardModelList { get; set; }

        public int associate_id { get; set; }
        public string associate_code { get; set; }
        public string associate_name { get; set; }
        public string designation { get; set; }
        public string department { get; set; }
        public int applicable_leave { get; set; }
        public string email { get; set; }
        public string contact { get; set; }
        public string extension { get; set; }
        public DateTime DOJ { get; set; }
        public int view { get; set; }
        public int mail_Hod { get; set; }
        public Boolean IS_HOD { get; set; }
        public string Remarks { get; set; }
      

        public List<AssociateVM> AssociateVMList { get; set; }
        public List<AssociateVM> AssociateVMInactiveList { get; set; }
        public List<AssociateVM> AssociateExtension { get; set; }

        public AssociateVM()
        {
            EmployeeDashboardModelList = new List<EmployeeDashboardModel>();
            DesignationModelList = new List<DesignationModel>();
            DepartmentModelList = new List<DepartmentModel>();
            //AssociateMasterModelList = new List<AssociateMasterModel>();
            //QualificationModelList = new List<QualificationModel>();
            AssociateMainModelList = new List<AssociateMainModel>();
            AssociateAdditionalInfoModelList = new List<AssociateAdditionalInfoModel>();
            AssociateQualificationModelList = new List<AssociateQualificationModel>();
            AssociateSkillModelList = new List<AssociateSkillModel>();
            AssociateExperienceModelList = new List<AssociateExperienceModel>();

            PresentCountryModelList = new List<CountryModel>();
            PresentStateModelList = new List<StateModel>();
            PresentCityModelList = new List<CityModel>();

            PermanentCountryModelList = new List<CountryModel>();
            PermanentStateModelList = new List<StateModel>();
            PermanentCityModelList = new List<CityModel>();

            associateMaster = new AssociateMasterModel();
            associateMain = new AssociateMainModel();
            qualificationModel = new QualificationModel();
            associateAdditionalInfo = new AssociateAdditionalInfoModel();
            associateAttachedInfo = new AssociateAttachedInfoModel();
            associateQualification = new AssociateQualificationModel();
            associateShift = new AssociateShiftModel();
            associatePersonal = new AssociatePersonalModel();
            associateSkill = new AssociateSkillModel();
            associateExperience = new AssociateExperienceModel();
            RoleList = new List<RoleMasterModel>();
            AssociateVMList = new List<AssociateVM>();
            AssociateVMInactiveList = new List<AssociateVM>();
            AssociateExtension = new List<AssociateVM>();
            profilepage = new ProfilePageModel();
            employeeDashboard = new EmployeeDashboardModel();

        }
    }

}
