﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class ProfilePageModel:ProfilePage
    {
        public List<DesignationModel> DesignationModelList { get; set; }
        public List<DepartmentModel> DepartmentModelList { get; set; }
        public List<QualificationModel> QualificationModelList { get; set; }

        public List<CountryModel> PresentCountryModelList { get; set; }
        public List<StateModel> PresentStateModelList { get; set; }
        public List<CityModel> PresentCityModelList { get; set; }
        public List<ProfilePageModel> profilePageList { get; set; }
        public List<CountryModel> PermanentCountryModelList { get; set; }
        public List<StateModel> PermanentStateModelList { get; set; }
        public List<CityModel> PermanentCityModelList { get; set; }

        public List<EmployeeDashboardModel> EmployeeDashboardModelList { get; set; }
        public AssociateMasterModel associateMaster { get; set; }
        public AssociateMainModel associateMain { get; set; }
        public AssociateAdditionalInfoModel associateAdditionalInfo { get; set; }
        public AssociateAttachedInfoModel associateAttachedInfo { get; set; }
        public ProfilePageModel ProfilePage { get; set; }

        public AssociateQualificationModel associateQualification { get; set; }
        public AssociateShiftModel associateShift { get; set; }
        public AssociatePersonalModel associatePersonal { get; set; }
        public AssociateSkillModel associateSkill { get; set; }
        public AssociateExperienceModel associateExperience { get; set; }
        public ProfilePageModel()
        {
            DesignationModelList = new List<DesignationModel>();
            DepartmentModelList = new List<DepartmentModel>();
            QualificationModelList = new List<QualificationModel>();
            PresentCountryModelList = new List<CountryModel>();
            PresentStateModelList = new List<StateModel>();
            PresentCityModelList = new List<CityModel>();

            EmployeeDashboardModelList = new List<EmployeeDashboardModel>();

            PermanentCountryModelList = new List<CountryModel>();
            PermanentStateModelList = new List<StateModel>();
            PermanentCityModelList = new List<CityModel>();
            associateMaster = new AssociateMasterModel();
            associateMain = new AssociateMainModel();

            associateAdditionalInfo = new AssociateAdditionalInfoModel();
            associateAttachedInfo = new AssociateAttachedInfoModel();
            associateQualification = new AssociateQualificationModel();
            associateShift = new AssociateShiftModel();
            associatePersonal = new AssociatePersonalModel();
            associateSkill = new AssociateSkillModel();
            associateExperience = new AssociateExperienceModel();
            profilePageList = new List<ProfilePageModel>();

            
        }
    }
    
}
