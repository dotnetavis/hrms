﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class EmailConfigurationModel: EmailConfiguration
    {
        [Compare("password",ErrorMessage ="Password did not Match")]
        public string confirm_password { get; set; }
        public List<EmailConfigurationModel> EmailConfigurationModelList { get; set; }
        public EmailConfigurationModel()
        {
            EmailConfigurationModelList = new List<EmailConfigurationModel>();
        }
    }
}
