﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hrms_core.EF;
using hrms_core.ViewModel;
using hrms_core.ViewModel.Services;
using hrms_core.ViewModel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace hrms_core.Controllers
{
    public class MenuCreationController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IMenuCreationServices menucreationServices => new MenuCreationServices(_db);
        public MenuCreationController(ApplicationDbContext options) => _db = options;
        [HttpGet]
        public IActionResult Index(int? id)
        {
            MenuCreationVM vm_obj = new MenuCreationVM();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                vm_obj.createdby = SesMdl.associate_id;
                vm_obj.createdbyname = SesMdl.associate_name;
                vm_obj.updatedby = SesMdl.associate_id;
                vm_obj.updatedbyname = SesMdl.associate_name;
                vm_obj = menucreationServices.GetAllMenus();
                if (id != null && id > 0)
                {
                    var _data = menucreationServices.GetMenuById(id.Value);
                    vm_obj.action_name = _data.action_name;
                    vm_obj.controller_name = _data.controller_name;
                    vm_obj.menu_id = _data.menu_id;
                    vm_obj.menu_name = _data.menu_name;
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(vm_obj);
        }


        [HttpPost]
        public IActionResult Index(MenuCreationVM model, bool IsAPI = false)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);

            if (IsAPI)
            {
                try
                {
                    StringBuilder str = new StringBuilder();
                    if (String.IsNullOrWhiteSpace(model.menu_name))
                    {
                        str.Append("Menu Name");
                    }
                    if (String.IsNullOrWhiteSpace(model.action_name))
                    {
                        if (str.Length != 0)
                            str.Append(", ");
                        str.Append("Action Name");
                    }
                    if (String.IsNullOrWhiteSpace(model.controller_name))
                    {
                        if (str.Length != 0)
                            str.Append(", ");
                        str.Append("Controller Name");
                    }
                    if (String.IsNullOrWhiteSpace(model.module_name))
                    {
                        if (str.Length != 0)
                            str.Append(", ");
                        str.Append("Module Name");
                    }

                    if (str.Length > 0)
                    {
                        str.Append(" Fields are Mandatory.");
                        return Json(Convert.ToString(str));
                    }

                    else
                    {
                        bool _Result = menucreationServices.AddMenu(model);
                        if (_Result)
                            return Json("Menu Created Successfully.");

                        else
                            return Json("Menu Creation Failed.");
                    }
                }
                catch (Exception ex)
                {
                    CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                    return Json("Unhandled Exception");
                }
            }
            else
            {
                if (SesMdl != null)
                {
                    try
                    {
                        StringBuilder str = new StringBuilder();
                        if (String.IsNullOrWhiteSpace(model.menu_name))
                        {
                            str.Append("Menu Name");
                        }
                        if (String.IsNullOrWhiteSpace(model.action_name))
                        {
                            if (str.Length != 0)
                                str.Append(", ");
                            str.Append("Action Name");
                        }
                        if (String.IsNullOrWhiteSpace(model.controller_name))
                        {
                            if (str.Length != 0)
                                str.Append(", ");
                            str.Append("Controller Name");
                        }
                        if (String.IsNullOrWhiteSpace(model.module_name))
                        {
                            if (str.Length != 0)
                                str.Append(", ");
                            str.Append("Module Name");
                        }

                        if (str.Length > 0)
                        {
                            str.Append(" Fields are Mandatory.");
                            return Json(Convert.ToString(str));
                        }

                        else
                        {
                            bool _Result = menucreationServices.AddMenu(model);
                            if (_Result)
                                return Json("Menu Created Successfully.");

                            else
                                return Json("Menu Creation Failed.");
                        }
                    }
                    catch (Exception ex)
                    {
                        CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                        return Json("Unhandled Exception");
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }

        }

    }
}