﻿using hrms_core.EF;
using hrms_core.Models;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace hrms_core.ViewModel.Services
{
    public class ConfigurationServices : IConfigurationServices
    {
        private readonly ApplicationDbContext _db;
        public ConfigurationServices(ApplicationDbContext options) => _db = options;
        /// <summary>
        /// Get the Configuration Data,
        /// Created By : Bilal 
        ///On Date: 18/05/2018
        /// </summary>
        /// <returns></returns>
        public List<ConfigurationModel> GetConfigurationSettings()
        {
            ConfigurationModel model = new ConfigurationModel();
            try
            {
                model.ConfigurationList = _db.Configurations
                    .Select(x => new ConfigurationModel
                    {
                        id = x.id,
                        company_name = x.company_name,
                        address = x.address,
                        associate_prefix = x.associate_prefix,
                        contact_no = x.contact_no,
                        createdon = x.createdon,
                        createdby = x.createdby,
                        createdbyname = x.createdbyname,
                        updatedon = x.updatedon,
                        updatedby = x.updatedby,
                        updatedbyname = x.updatedbyname,
                        email_id = x.email_id,
                        max_numeric_character = x.max_numeric_character
                    }).ToList();

            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.ConfigurationList;
        }

        /// <summary>
        /// Save the Configuration Data,
        /// Created By : Bilal 
        ///On Date: 18/05/2018
        /// </summary>
        /// <returns></returns>
        public string SaveConfigurationSettings(ConfigurationModel model)
        {
            string Result = string.Empty;
            Configuration configuration = new Configuration();
            try
            {
                configuration.company_name = model.company_name;
                configuration.max_numeric_character = model.max_numeric_character;
                configuration.email_id = model.email_id;
                configuration.associate_prefix = model.associate_prefix;
                configuration.address = model.address;
                configuration.contact_no = model.contact_no;
                configuration.createdon = model.createdon;
                configuration.createdby = model.createdby;
                configuration.createdbyname = model.createdbyname;
                configuration.updatedon = model.updatedon;
                configuration.updatedby = model.updatedby;
                configuration.updatedbyname = model.updatedbyname;
                _db.Configurations.Add(configuration);
                _db.SaveChanges();
                Result = MessageHelper.ConfigurationSetting_AddSuccess;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.ConfigurationSetting_AddError;
            }
            return Result;
        }

        /// <summary>
        /// Update the Configuration Data,
        /// Created By : Bilal 
        ///On Date: 10/08/2018
        /// </summary>
        /// <returns></returns>
        public string UpdateConfigurationSettings(ConfigurationModel model)
        {
            string Result = string.Empty;
            try
            {
                var _configData = _db.Configurations.ToList();
                if (_configData != null)
                {
                    var configuration = _configData.FirstOrDefault();
                    if (configuration != null)
                    {
                        configuration.company_name = model.company_name;
                        configuration.max_numeric_character = model.max_numeric_character;
                        configuration.email_id = model.email_id;
                        configuration.associate_prefix = model.associate_prefix;
                        configuration.address = model.address;
                        configuration.contact_no = model.contact_no;
                        //configuration.createdon = model.createdon;
                        //configuration.createdby = model.createdby;
                        //configuration.createdbyname = model.createdbyname;
                        configuration.updatedon = DateTime.Now;
                        configuration.updatedby = model.updatedby;
                        configuration.updatedbyname = model.updatedbyname;
                        configuration.isactive = true;
                        _db.SaveChanges();
                    }
                }
                Result = MessageHelper.ConfigurationSetting_AddSuccess;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.ConfigurationSetting_AddError;
            }
            return Result;
        }

        /// <summary>
        /// Get the Email Configuration Data,
        /// Created By : Bilal 
        ///On Date: 18/05/2018
        /// </summary>
        /// <returns></returns>
        public List<EmailConfigurationModel> GetConfigurationEmail()
        {
            EmailConfigurationModel model = new EmailConfigurationModel();
            try
            {
                model.EmailConfigurationModelList = _db.EmailConfigurations
                    .Select(x => new EmailConfigurationModel
                    {
                        id = x.id,
                        email_send_from = x.email_send_from,
                        password = x.password,
                        smtp = x.smtp,
                        port = x.port,
                        ssl = x.ssl,
                        createdon = x.createdon,
                        createdby = x.createdby,
                        createdbyname = x.createdbyname,
                        updatedon = x.updatedon,
                        updatedby = x.updatedby,
                        updatedbyname = x.updatedbyname,
                    }).OrderBy(x => x.updatedon).ToList();
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
            }
            return model.EmailConfigurationModelList;
        }

        /// <summary>
        /// Save the Email Configuration Data,
        /// Created By : Bilal 
        ///On Date: 18/05/2018
        /// </summary>
        /// <returns></returns>
        public string SaveEmailConfiguration(EmailConfigurationModel model)
        {
            string Result = string.Empty;
            EmailConfiguration _emailConfiguration = new EmailConfiguration();
            try
            {
                _emailConfiguration.email_send_from = model.email_send_from;
                _emailConfiguration.password = model.confirm_password;
                _emailConfiguration.smtp = model.smtp;
                _emailConfiguration.port = model.port;
                _emailConfiguration.ssl = model.ssl;
                _emailConfiguration.createdon = DateTime.Now;
                _emailConfiguration.createdby = 1;
                _emailConfiguration.createdbyname = "SuperAdmin";
                _emailConfiguration.updatedon = DateTime.Now;
                _emailConfiguration.updatedby = 1;
                _emailConfiguration.isactive = true;
                _emailConfiguration.updatedbyname = "SuperAdmin";
                _db.EmailConfigurations.Add(_emailConfiguration);
                _db.SaveChanges();
                Result = MessageHelper.EmailConfiguration_AddSuccess;
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                Result = MessageHelper.EmailConfiguration_AddError;
            }
            return Result;
        }

        /// <summary>
        /// Update the Email Configuration Data,
        /// Created By : Bilal 
        ///On Date: 10/08/2018
        /// </summary>
        /// <returns></returns>
        public string UpdateEmailConfiguration(EmailConfigurationModel model)
        {
            string Result = string.Empty;
            //EmailConfiguration _emailConfiguration = new EmailConfiguration();
            try
            {
                var _emailConfig = _db.EmailConfigurations.ToList();
                if (_emailConfig != null && _emailConfig.Count > 0)
                {
                    var _emailConfiguration = _emailConfig.FirstOrDefault();
                    if (_emailConfiguration != null)
                    {
                        _emailConfiguration.email_send_from = model.email_send_from;
                        _emailConfiguration.password = model.confirm_password;
                        _emailConfiguration.smtp = model.smtp;
                        _emailConfiguration.port = model.port;
                        _emailConfiguration.ssl = model.ssl;
                        //_emailConfiguration.createdon = DateTime.Now;
                        //_emailConfiguration.createdby = model.createdby;
                        //_emailConfiguration.createdbyname = model.createdbyname;
                        _emailConfiguration.updatedon = DateTime.Now;
                        _emailConfiguration.updatedby = model.updatedby;
                        _emailConfiguration.isactive = true;
                        _emailConfiguration.updatedbyname = model.updatedbyname;
                        _db.SaveChanges();
                        Result = MessageHelper.EmailConfiguration_AddSuccess;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, "ConfigurationServices=>UpdateEmailConfiguration", _db);
                Result = MessageHelper.EmailConfiguration_AddError;
            }
            return Result;
        }

        /// <summary>
        /// Sent Email To SuperAdmin,
        /// Created By : Bilal 
        ///On Date: 02/08/2018
        /// </summary>
        /// <returns></returns>
        public string SendMail(string ToMail, string Subject, string Body, string AssociateName)
        {
            string str = string.Empty;
            try
            {
                var EmailConfig = _db.EmailConfigurations.ToList();
                string FromEmail = string.Empty;
                string Pass = string.Empty;
                string Smtp = string.Empty;
                bool Ssl = false;
                int Port = 0;
                if (EmailConfig.Count > 0)
                {
                    var _emailData = EmailConfig.FirstOrDefault();
                    if (_emailData != null)
                    {
                        FromEmail = _emailData.email_send_from;
                        Pass = _emailData.password;
                        Smtp = _emailData.smtp;
                        Ssl = _emailData.ssl;
                        Port = _emailData.port;
                    }
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient(Smtp);

                    mail.From = new MailAddress(FromEmail);
                    mail.To.Add(ToMail);
                    mail.Subject = Subject;
                    mail.IsBodyHtml = true;
                    mail.Body = Body;

                    SmtpServer.Host = Smtp;
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.Port = Port;
                    SmtpServer.Credentials = new System.Net.NetworkCredential(FromEmail, Pass);
                    SmtpServer.EnableSsl = Ssl;
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                 System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };
                    SmtpServer.Send(mail);
                    str = MessageHelper.EmailSent;
                }
                return str;
            }
            catch (Exception ex)
            {
                str = MessageHelper.EmailError;
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return str;
            }
        }
    }
}
