﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class PayElement:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public virtual AssociateMaster associate { get; set; }
        public double basic { get; set; }
        public double hra { get; set; }
        public double special_skill_allowance { get; set; }
        public double mobile_allowance { get; set; }
        public double conveyance_allowance { get; set; }
        public double management_allowance { get; set; }
        public double book_periodicals { get; set; }
        public double driver_allowance { get; set; }
        public double medical_allowance { get; set; }
        public double gross_salary { get; set; }
        public double pf { get; set; }
        public double esi { get; set; }
        public double total_deduction { get; set; }
        public double carry_home_salary { get; set; }
        public double ctc { get; set; }
        public bool is_current { get; set; }
    }
}


  
        