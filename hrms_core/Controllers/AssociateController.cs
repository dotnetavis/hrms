﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using hrms_core.Models;
using hrms_core.ViewModel;
using hrms_core.EF;
using hrms_core.ViewModel.Services.Interfaces;
using hrms_core.ViewModel.Services;
using System.Transactions;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace hrms_core.Controllers
{
    public class AssociateController : Controller
    {
        private readonly ApplicationDbContext _db;
        public IConfiguration Configuration { get; }
        private IAssociateServices associateServices => new AssociateServices(_db);
        private IDesignationServices designationServices => new DesignationServices(_db);
        private IDepartmentServices departmentServices => new departmentServices(_db);
        private IQualificationServices qualificationServices => new QualificationServices(_db);
        private ICountryServices countryServices => new CountryServices(_db);
        private IStateServices stateServices => new StateServices(_db);
        private ICityServices cityServices => new CityServices(_db);
        private IRoleServices roleServices => new RoleServices(_db);

        public AssociateController(ApplicationDbContext db, IConfiguration configuration)
        {
            _db = db;
            Configuration = configuration;
        }

        public IActionResult AssociateBucket()
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            AssociateVM associate = new AssociateVM();
            associate.createdby = SesMdl.associate_id;
            associate.createdbyname = SesMdl.associate_name;
            associate.updatedby = SesMdl.associate_id;
            associate.updatedbyname = SesMdl.associate_name;
            associate.AssociateVMList = (from master in _db.AssociateMasters.Where(x => x.isactive == true && x.is_super_admin == false)
                                         join main in _db.AssociateMains
                                         on master.id equals main.associate_id
                                         select new AssociateVM
                                         {
                                             associate_id = master.id,
                                             associate_code = master.associate_code,
                                             associate_name = master.associate_name,
                                             designation = _db.Designations.FirstOrDefault(x => x.id == main.designation_id).designationtext,
                                             department = _db.Departments.FirstOrDefault(x => x.id == main.department_id).departmenttext,

                                             contact = main.mobileno,
                                             DOJ = master.application_date,
                                         }).ToList();

            associate.AssociateVMInactiveList = (from master in _db.AssociateMasters.Where(x => x.isactive == false && x.is_super_admin == false)
                                                 join main in _db.AssociateMains
                                                 on master.id equals main.associate_id
                                                 select new AssociateVM
                                                 {
                                                     associate_id = master.id,
                                                     associate_code = master.associate_code,
                                                     associate_name = master.associate_name,
                                                     designation = _db.Designations.FirstOrDefault(x => x.id == main.designation_id).designationtext,
                                                     department = _db.Departments.FirstOrDefault(x => x.id == main.department_id).departmenttext,
                                                     contact = main.mobileno,
                                                     DOJ = master.application_date,
                                                 }).ToList();
            return View(associate);
        }
        [HttpGet]
        public IActionResult Index(int? id, int? view)
        {
            AssociateVM model = new AssociateVM();
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                if (view == 1)
                {
                    model.view = 1;
                }
                else
                {
                    model.view = 0;
                }
                model.createdby = SesMdl.user_id;
                model.updatedby = SesMdl.user_id;
                model.createdbyname = SesMdl.username;
                model.updatedbyname = SesMdl.username;
                model.associateMaster.application_date = DateTime.Now.Date;
                #region Bind Designation Dropdown
                model.DesignationModelList = designationServices.GetDesignation_Active();
                model.DesignationModelList.Insert(0, new DesignationModel { id = 0, designationtext = "--Select--" });
                #endregion

                #region Bind Department Dropdown
                model.DepartmentModelList = departmentServices.GetDepartment_Active();
                model.DepartmentModelList.Insert(0, new DepartmentModel { id = 0, departmenttext = "--Select--" });
                #endregion

                #region Bind Reporting_Person(Associate) Dropdown
                model.associateMaster.AssociateMasterModelList = associateServices.GetAssociateList_Active();
                model.associateMaster.AssociateMasterModelList.Insert(0, new AssociateMasterModel { id = 0, associate_name = "--Select--" });
                #endregion

                #region Bind Qualification Dropdown
                model.qualificationModel.QualificationModelList = qualificationServices.GetQualification_Active();
                model.qualificationModel.QualificationModelList.Insert(0, new QualificationModel { id = 0, qualificationtext = "--Select--" });
                #endregion
                #region Bind Country Dropdown
                model.PresentCountryModelList = countryServices.GetCountry_Active();
                model.PermanentCountryModelList = countryServices.GetCountry_Active();
                model.PresentCountryModelList.Insert(0, new CountryModel { id = 0, countrytext = "--Select--" });
                 model.PermanentCountryModelList.Insert(0, new CountryModel { id = 0, countrytext = "--Select--" });
                #endregion

                #region Bind Role DropDown
                model.RoleList = roleServices.GetRoleList();
                model.RoleList.Insert(0, new RoleMasterModel { id = 0, role_name = "--Select--" });
                #endregion
                model.PresentStateModelList.Insert(0, new StateModel { id = 0, statetext = "--Select--" });
                model.PermanentStateModelList.Insert(0, new StateModel { id = 0, statetext = "--Select--" });
                model.PresentCityModelList.Insert(0, new CityModel { id = 0, citytext = "--Select--" });
                model.PermanentCityModelList.Insert(0, new CityModel { id = 0, citytext = "--Select--" });
                if (id != null && id > 0)
                {
                    #region Master
                    model.associateMaster = _db.AssociateMasters.Where(x => x.id == id )
                   .Select(x => new AssociateMasterModel
                   {
                       id = x.id,
                       associate_name = x.associate_name,
                       applicable_to = x.applicable_to,
                       application_date = x.application_date,
                       associate_code = x.associate_code,
                       createdby = x.createdby,
                       createdon = x.createdon,
                       updatedby = x.updatedby,
                       updatedon = x.updatedon,
                       isactive = x.isactive,
                       AssociateMasterModelList = associateServices.GetAssociateList_Active()
                   }).FirstOrDefault();
                    #endregion
                    #region Main
                    model.associateMain = (from master in _db.AssociateMasters.Where(x => x.id == id )
                                           join main in _db.AssociateMains
                                           on master.id equals main.associate_id
                                           select new AssociateMainModel
                                           {
                                               id = main.id,
                                               associate_id = main.associate_id,
                                               designation_id = main.designation_id,
                                               department_id = main.department_id,
                                               is_hod = main.is_hod,
                                               extensionno = main.extensionno,
                                               mobileno = main.mobileno,
                                               personal_email = main.personal_email,
                                               official_email = main.official_email,
                                               account_no = main.account_no,
                                               bankname = main.bankname,
                                               ifsc = main.ifsc,
                                               doc = (main.doc == null ? default(DateTime) : main.doc),
                                               qualification_id = main.qualification_id,
                                               shiftid = main.shiftid,
                                               pf_no = main.pf_no,
                                               esi_no = main.esi_no,
                                               applicable_leave = main.applicable_leave,
                                               shift_type = main.shift_type,
                                               role_id = main.role_id,
                                               reportingperson_id = main.reportingperson_id,
                                           }).FirstOrDefault();
                    #endregion
                    #region Personal
                    model.associatePersonal = (from master in _db.AssociateMasters.Where(x => x.id == id )
                                               join personal in _db.AssociatePersonals
                                               on master.id equals personal.associate_id
                                               select new AssociatePersonalModel
                                               {
                                                   id = personal.id,
                                                   associate_id = personal.associate_id,
                                                   pan_card = personal.pan_card,
                                                   dob = personal.dob,
                                                   anniversary = personal.anniversary,
                                                   father_husband_name = personal.father_husband_name,
                                                   mothername = personal.mothername,
                                                   present_address = personal.present_address,
                                                   present_city_id = personal.present_city_id,
                                                   present_country_id = personal.present_country_id,
                                                   present_state_id = personal.present_state_id,
                                                   permanent_address = personal.permanent_address,
                                                   permanent_city_id = personal.permanent_city_id,
                                                   permanent_country_id = personal.permanent_country_id,
                                                   permanent_state_id = personal.permanent_state_id,
                                                   bloodgroup = personal.bloodgroup,
                                                   adhar_card_no = personal.adhar_card_no,
                                                   gender = personal.gender,
                                                   maritalstatus = personal.maritalstatus
                                               }).FirstOrDefault();
                    #endregion

                    if (model.associatePersonal != null)
                    {
                        #region Bind State
                        model.PermanentStateModelList = stateServices.GetState_ByCountryID(model.associatePersonal.permanent_country_id);
                        model.PresentStateModelList = stateServices.GetState_ByCountryID(model.associatePersonal.present_country_id.Value);

                        model.PresentStateModelList.Insert(0, new StateModel { id = 0, statetext = "--Select--" });
                        model.PermanentStateModelList.Insert(0, new StateModel { id = 0, statetext = "--Select--" });
                        #endregion

                        #region Bind City
                        model.PresentCityModelList = cityServices.GetCity_ByStateId(model.associatePersonal.present_state_id.Value);
                        model.PermanentCityModelList = cityServices.GetCity_ByStateId(model.associatePersonal.permanent_state_id);
                        model.PresentCityModelList.Insert(0, new CityModel { id = 0, citytext = "--Select--" });
                        model.PermanentCityModelList.Insert(0, new CityModel { id = 0, citytext = "--Select--" });
                        #endregion
                    }
                    #region Qualification

                    model.AssociateQualificationModelList = (from qualification in _db.AssociateQualifications.Where(x => x.associate_id == model.associateMain.associate_id && x.isactive == true)
                                                             select new AssociateQualificationModel
                                                             {
                                                                 id = qualification.id,
                                                                 associate_id = qualification.associate_id,
                                                                 qualification_id = qualification.qualification_id,
                                                                 qualification_year = qualification.qualification_year,
                                                                 institute_name = qualification.institute_name,
                                                                 percentage = qualification.percentage,
                                                                 QualificationModelList = qualificationServices.GetQualification_Active(),
                                                             }).ToList();
                    #endregion
                    #region Experience
                    model.AssociateExperienceModelList = (from experience in _db.AssociateExperiences.Where(x => x.associate_id == model.associateMain.associate_id && x.isactive == true)
                                                          select new AssociateExperienceModel
                                                          {
                                                              id = experience.id,
                                                              associate_id = experience.associate_id,
                                                              previous_company = experience.previous_company,
                                                              from_date = experience.from_date,
                                                              to_date = experience.to_date,
                                                              gross_salary = experience.gross_salary,
                                                              carryhome_salary = experience.carryhome_salary,
                                                              ctc = experience.ctc,
                                                              extra_allowance = experience.extra_allowance,
                                                              designation_id = experience.designation_id,
                                                              DesignationModelList = designationServices.GetDesignation_Active()
                                                          }).ToList();
                    #endregion

                    #region Skill
                    model.AssociateSkillModelList = (from skill in _db.AssociateSkills.Where(x => x.associate_id == model.associateMain.associate_id && x.isactive == true)
                                                     select new AssociateSkillModel
                                                     {
                                                         id = skill.id,
                                                         associate_id = skill.associate_id,
                                                         skillcode = skill.skillcode,
                                                         skill_description = skill.skill_description,
                                                         remarks = skill.remarks,
                                                     }).ToList();
                    #endregion

                    #region Additional
                    model.AssociateAdditionalInfoModelList = (from additional in _db.AssociateAdditionalInfos.Where(x => x.associate_id == model.associateMain.associate_id && x.isactive == true)
                                                              select new AssociateAdditionalInfoModel
                                                              {
                                                                  id = additional.id,
                                                                  associate_id = additional.associate_id,
                                                                  reporting_person_id = additional.reporting_person_id,
                                                                  remark = additional.remark,
                                                                  mail_to_hod = additional.mail_to_hod,
                                                                  AssociateMasterModelList = associateServices.GetAssociateList_Active(),
                                                                  AssociateAdditionalInfoModelList = associateServices.GetAssociateInfoList_Active(),
                                                              }).ToList();

                    if (model.AssociateAdditionalInfoModelList != null && model.AssociateAdditionalInfoModelList.Count > 0)
                    {
                        if (model.AssociateAdditionalInfoModelList.FirstOrDefault().mail_to_hod == true)
                        {
                            model.mail_Hod = 1;
                        }
                        else
                        {
                            model.mail_Hod = 0;
                        }
                        model.Remarks = model.AssociateAdditionalInfoModelList.FirstOrDefault().remark;
                    }
                    #endregion
                    var data = stateServices.GetState_ByCountryID(id.Value);
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult Index(AssociateVM model)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl != null)
            {
                model.createdby = SesMdl.user_id;
                model.updatedby = SesMdl.user_id;
                model.createdbyname = SesMdl.username;
                model.updatedbyname = SesMdl.username;
                model.associateMain.is_hod = model.IS_HOD;
                model.associateAdditionalInfo.remark = model.Remarks;

                List<string> validations = new List<string>();
                string message = string.Empty;
                if (model.associateMain.department_id == 0)
                {
                    validations.Add("Department");
                }
                if (model.associateMain.designation_id == 0)
                {
                    validations.Add("Designation");
                }
                if (model.associateMain.mobileno == null)
                {
                    validations.Add("Contact No");
                }
                if (model.associateMain.personal_email == null)
                {
                    validations.Add("Personal Email");
                }
                if (model.associateMain.official_email == null)
                {
                    validations.Add("Official Email");
                }
                if (model.associateMain.qualification_id == 0)
                {
                    validations.Add("Qualification");
                }
                if (model.associateMain.role_id == 0)
                {
                    validations.Add("Role");
                }
                if (model.associatePersonal.dob == null)
                {
                    validations.Add("Date of Birth");
                }
                if (model.associatePersonal.permanent_address == null)
                {
                    validations.Add("Permanent Address");
                }
                if (model.associatePersonal.permanent_country_id == 0)
                {
                    validations.Add("Permanent Country");
                }
                if (model.associatePersonal.permanent_state_id == 0)
                {
                    validations.Add("Permanent State");
                }
                if (model.associatePersonal.permanent_city_id == 0)
                {
                    validations.Add("Permanent City");
                }
                if (model.associatePersonal.gender == "Select")
                {
                    validations.Add("Gender");
                }
                if (model.associatePersonal.maritalstatus == "Select")
                {
                    validations.Add("Marital Status");
                }

                if (model.associateMain.official_email == model.associateMain.personal_email)
                {
                    validations.Add("Official and Personal Email Address should be different");
                }

                #region Bind Designation Dropdown
                model.DesignationModelList = designationServices.GetDesignation_Active();
                model.DesignationModelList.Insert(0, new DesignationModel { id = 0, designationtext = "--Select--" });
                #endregion

                #region Bind Department Dropdown
                model.DepartmentModelList = departmentServices.GetDepartment_Active();
                model.DepartmentModelList.Insert(0, new DepartmentModel { id = 0, departmenttext = "--Select--" });
                #endregion

                #region Bind Reporting_Person(Associate) Dropdown
                model.associateMaster.AssociateMasterModelList = associateServices.GetAssociateList_Active();
                model.associateMaster.AssociateMasterModelList.Insert(0, new AssociateMasterModel { id = 0, associate_name = "--Select--" });
                #endregion

                #region Bind Qualification Dropdown
                model.qualificationModel.QualificationModelList = qualificationServices.GetQualification_Active();
                model.qualificationModel.QualificationModelList.Insert(0, new QualificationModel { id = 0, qualificationtext = "--Select--" });
                #endregion

                #region Bind Country Dropdown
                model.PresentCountryModelList = countryServices.GetCountry_Active();
                model.PermanentCountryModelList = countryServices.GetCountry_Active();
                model.PresentCountryModelList.Insert(0, new CountryModel { id = 0, countrytext = "--Select--" });
                model.PermanentCountryModelList.Insert(0, new CountryModel { id = 0, countrytext = "--Select--" });
                #endregion

                if (model.associatePersonal.permanent_country_id != 0)
                {

                    #region Bind State Dropdown
                    model.PermanentStateModelList = stateServices.GetState_ByCountryID(model.associatePersonal.permanent_country_id);
                    model.PermanentStateModelList.Insert(0, new StateModel { id = 0, statetext = "--Select--" });
                    model.PresentStateModelList.Insert(0, new StateModel { id = 0, statetext = "--Select--" });
                    #endregion
                    if (model.associatePersonal.permanent_state_id != 0)
                    {
                        #region Bind City Dropdown
                        model.PermanentCityModelList = cityServices.GetCity_ByStateId(model.associatePersonal.permanent_state_id);
                        model.PresentCityModelList.Insert(0, new CityModel { id = 0, citytext = "--Select--" });
                        model.PermanentCityModelList.Insert(0, new CityModel { id = 0, citytext = "--Select--" });
                        #endregion
                    }
                    else
                    {
                        model.PresentCityModelList.Insert(0, new CityModel { id = 0, citytext = "--Select--" });
                        model.PermanentCityModelList.Insert(0, new CityModel { id = 0, citytext = "--Select--" });
                    }
                }
                else
                {
                    model.PermanentStateModelList.Insert(0, new StateModel { id = 0, statetext = "--Select--" });
                    model.PresentStateModelList.Insert(0, new StateModel { id = 0, statetext = "--Select--" });
                    model.PresentCityModelList.Insert(0, new CityModel { id = 0, citytext = "--Select--" });
                    model.PermanentCityModelList.Insert(0, new CityModel { id = 0, citytext = "--Select--" });
                }
                #region Bind Role DropDown
                model.RoleList = roleServices.GetRoleList();
                model.RoleList.Insert(0, new RoleMasterModel { id = 0, role_name = "--Select--" });
                #endregion
                foreach (var item in model.AssociateQualificationModelList)
                {
                    if (item != null)
                    {
                        item.QualificationModelList = qualificationServices.GetQualification_Active();
                        item.QualificationModelList.Insert(0, new QualificationModel { id = 0, qualificationtext = "--Select--" });
                    }
                }
                foreach (var item in model.AssociateExperienceModelList)
                {
                    if (item != null)
                    {
                        item.DesignationModelList = designationServices.GetDesignation_Active();
                        model.DesignationModelList.Insert(0, new DesignationModel { id = 0, designationtext = "--Select--" });
                    }
                }
                foreach (var item in model.AssociateAdditionalInfoModelList)
                {
                    if (item != null)
                    {
                        item.AssociateMasterModelList = associateServices.GetAssociateList_Active();
                        item.AssociateMasterModelList.Insert(0, new AssociateMasterModel { id = 0, associate_name = "--Select--" });
                    }
                }

                if (validations == null || validations.Count == 0)
                {
                    if (model != null)
                    {
                        if (model.AssociateQualificationModelList != null && model.AssociateQualificationModelList.Count != 0)
                        {
                            foreach (var item in model.AssociateQualificationModelList)
                            {
                                if (item != null)
                                {
                                    if (item.qualification_year == null || item.percentage == 0 || item.institute_name == null)
                                    {
                                        TempData["msg"] = "Enter Complete Details";
                                        return View(model);
                                    }
                                }
                            }
                        }
                        if (model.AssociateSkillModelList != null && model.AssociateSkillModelList.Count != 0)
                        {
                            foreach (var item in model.AssociateSkillModelList)
                            {
                                if (item != null)
                                {
                                    if (item.skillcode == null || item.skill_description == null)
                                    {
                                        TempData["msg"] = "Enter Complete Details";
                                        return View(model);
                                    }
                                }

                            }
                        }
                        if (model.AssociateAdditionalInfoModelList != null && model.AssociateAdditionalInfoModelList.Count != 0)
                        {
                            foreach (var item in model.AssociateAdditionalInfoModelList)
                            {
                                if (item != null)
                                {

                                    if (item.reporting_person_id == 0)
                                    {
                                        TempData["msg"] = "Enter Complete Details";
                                        return View(model);
                                    }
                                }
                            }
                        }
                        if (model.AssociateExperienceModelList != null && model.AssociateExperienceModelList.Count != 0)
                        {
                            foreach (var item in model.AssociateExperienceModelList)
                            {
                                if (item != null)
                                {
                                    if (item.previous_company == null || item.carryhome_salary == 0 || item.to_date == null || item.from_date == null || item.extra_allowance == null || item.gross_salary == 0)
                                    {
                                        TempData["msg"] = "Enter Complete Details";
                                        return View(model);
                                    }
                                }

                            }
                        }
                        int id = 0;
                        string result;
                        bool _result;
                        if (model.associateMaster.id > 0)
                        {
                            result = associateServices.UpdateAssociateData(model);
                        }
                        else
                        {
                            id = associateServices.SaveAssociateData(model);
                            result = MessageHelper.Associate_AddSuccess;
                            if (id > 0)
                            {
                                model.associateMain.personal_email = _db.AssociateMains.FirstOrDefault(x => x.associate_id == id).personal_email;
                                model.associate_code = _db.AssociateMasters.FirstOrDefault(x => x.id == id).associate_code;
                                model.associate_id = id;

                                _result = associateServices.SendAssociateDetails(model, HttpContext, Configuration);
                                if (_result == true)
                                {
                                    TempData["msg"] = "Associate Successfully Created";
                                }
                            }
                        }
                        TempData["msg"] = result;
                    }
                }
                else
                {
                    message = "Please Enter the complete details. You have missed the following fields : ";
                    foreach (var item in validations)
                    {
                        message += item + ",";
                    }
                    TempData["msg"] = message;
                    return View(model);
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("Index", "Associate", new { id=0});
        }
        [HttpPost]
        public JsonResult GetHodByDepartment(int DepartmentId)
        {
            AssociateVM model = new AssociateVM();
            model.DepartmentModelList = departmentServices.GetDepartment_Active();
            if (model.DepartmentModelList.Count > 0 && model.DepartmentModelList != null)
            {
                model.DepartmentModelList = model.DepartmentModelList.Where(x => x.id == DepartmentId && x.isactive == true).ToList();
            }
            return Json(model.DepartmentModelList);
        }
        [HttpPost]
        public JsonResult GetStateByCountryId(int CountryId)
        {
            AssociateVM model = new AssociateVM();
            model.PresentStateModelList = stateServices.GetState_ByCountryID(CountryId);
            return Json(model.PresentStateModelList);
        }
        [HttpPost]
        public JsonResult GetCityByStateId(int StateId)
        {
            AssociateVM model = new AssociateVM();
            model.PresentCityModelList = cityServices.GetCity_ByStateId(StateId);
            return Json(model.PresentCityModelList);
        }
        public PartialViewResult SkillDetailsPartial()
        { 
            AssociateSkillModel model = new AssociateSkillModel();
            return PartialView("_SkillDetail", model);
        }
        public PartialViewResult QualificationDetailsPartial()
        {
            AssociateQualificationModel model = new AssociateQualificationModel();
            model.QualificationModelList = qualificationServices.GetQualification_Active();
            model.QualificationModelList.Insert(0, new QualificationModel { id = 0, qualificationtext = "--Select--" });
            return PartialView("_QualificationDetail", model);
        }
        public PartialViewResult ExperienceDetailsPartial()
        {
            AssociateExperienceModel model = new AssociateExperienceModel();
            model.DesignationModelList = designationServices.GetDesignation_Active();
            model.DesignationModelList.Insert(0, new DesignationModel { id = 0, designationtext = "--Select--" });
            model.from_date = DateTime.Now;
            model.to_date = DateTime.Now;
            return PartialView("_ExperienceDetails", model);
        }
        public PartialViewResult ReportingDetailsPartial()
        {
            AssociateAdditionalInfoModel model = new AssociateAdditionalInfoModel();
            model.AssociateMasterModelList = associateServices.GetAssociateList_Active();
            if (model.AssociateMasterModelList == null)
            {
                model.AssociateMasterModelList = new List<AssociateMasterModel>();
            }
            model.AssociateMasterModelList.Insert(0, new AssociateMasterModel { id = 0, associate_name = "--Select--" });
            return PartialView("_ReportingDetails", model);
        }

        /// <summary>
        /// Author : 
        /// Date : 16-Aug-2019 
        /// UpdatedBy : 04-Nov-2019
        /// Updated code with deactivating id in AssociateMain Table
        /// </summary>
        /// <returns></returns>
        public IActionResult DeleteAssociate(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            try
            {
                var _data = _db.AssociateMasters.FirstOrDefault(x => x.id == id);
                var _dataMain = _db.AssociateMains.FirstOrDefault(x => x.associate_id == id);
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    _data.isactive = false;
                    _dataMain.isactive = false;
                    _db.SaveChanges();
                    scope.Complete();
                }
                TempData["msg"] = "Associate Succesfully Deleted";
                return RedirectToAction("AssociateBucket", "Associate");
            }
            catch(Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return null;
            }
            
        }
        public JsonResult GetHod(int id)
        {

            string HODNAME = string.Empty;
            try
            {
                var data = (from main in _db.AssociateMains.Where(x => x.department_id == id && x.is_hod == true && x.isactive == true)
                            join master in _db.AssociateMasters
                            on main.associate_id equals master.id
                            join dept in _db.Departments
                            on main.department_id equals dept.id
                            select new AssociateVM()
                            {
                                associate_name = master.associate_name
                            }).ToList();
                if (data != null && data.Count > 0)
                {

                    return Json(data);
                }
                else
                {
                    return Json(null);
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return Json(null);
            }
        }
        public JsonResult GetApplicableLeave(int id)
        {
            try
            {
                var data = _db.Departments.Where(x => x.id == id && x.isactive == true).ToList();
                if (data != null && data.Count > 0)
                {
                    return Json(data);
                }
                else
                {
                    return Json(null);
                }
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                return Json(null);
            }
        }

        public IActionResult DownloadExtension()
        {
            AssociateVM associate = new AssociateVM();
            associate.AssociateExtension = (from master in _db.AssociateMasters.Where(x => x.isactive == true)
                                            join main in _db.AssociateMains
                                            on master.id equals main.associate_id
                                            select new AssociateVM()
                                            {
                                                associate_id = master.id,
                                                associate_name = master.associate_name,
                                                extension = main.extensionno
                                            }).ToList();

            return View(associate);
        }
        public IActionResult ActivateAssociate(int id)
        {
            SessionModel SesMdl = HttpContext.Session.GetObjectFromJson<SessionModel>(SessionVariables.SessionData);
            if (SesMdl == null)
            {
                return RedirectToAction("Login", "Account");
            }
            var _data = _db.AssociateMasters.FirstOrDefault(x => x.id == id);
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                _data.isactive = true;

                _db.SaveChanges();
                scope.Complete();
            }
            TempData["msg"] = "Associate Succesfully Resumed";
            return RedirectToAction("AssociateBucket", "Associate");
        }
    }
}