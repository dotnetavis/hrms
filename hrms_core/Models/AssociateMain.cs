﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class AssociateMain:CommonAttributeModel
    {    
        [Key]                 
        public int id { get; set; }       //primary key
        [ForeignKey("associate_id")]
        public int associate_id { get; set; }

        [Range(1, Int32.MaxValue, ErrorMessage = ("Select Designation"))]
        [ForeignKey("designation_id")]
        
        public int designation_id { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = ("Select Department"))]
        [ForeignKey("department_id")]
       
        public int department_id { get; set; }
        public bool is_hod { get; set; }
        public string extensionno { get; set; }
        [Required(ErrorMessage ="Mobile No. Required")]
        public string mobileno { get; set; }
        [Required(ErrorMessage = "Personal Email Required")]
        public string personal_email { get; set; }
        [Required(ErrorMessage = "Official Email Required")]
        public string official_email { get; set; }
        public string account_no { get; set; }
        public string bankname { get; set; }
        public string ifsc { get; set; }
        public DateTime? doc { get; set; }

        //TODO: Why Range Required
        [Range(1,999999, ErrorMessage = ("Qualification Required"))]
        [ForeignKey("qualification_id")]
        public int qualification_id { get; set; }
        public int shiftid { get; set; }
        public string pf_no { get; set; }
        public string esi_no { get; set; }
        public int applicable_leave { get; set; }
        public int shift_type { get; set; }
        [ForeignKey("role_id")]
        [Range(1, Int32.MaxValue, ErrorMessage = ("Select Role"))]
        public int role_id { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = ("Select Reporting Person"))]
        public int? reportingperson_id { get; set; }   //foreign key

    }
}
