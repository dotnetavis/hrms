﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using hrms_core.Models;

namespace hrms_core.Models
{
    public class CompanyPolicy:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public string policy_content { get; set; }
        public int policy_year { get; set; }
    }
}
