﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class Department:CommonAttributeModel
    {
        
        public int id { get; set; }      //primary key
        [Required(ErrorMessage ="Department Name Required" )]
        public string departmenttext { get; set; }
        [Required(ErrorMessage = "Applicable Leave Required")]
        public int applicable_leave { get; set; }
        [ForeignKey("department_id")]
        public virtual ICollection<AssociateMain> AssociateMainDetails { get; set; }
        //public virtual AssociateMaster hod { get; set; }  //foreign key

    }
}
