﻿using hrms_core.EF;
using hrms_core.ViewModel.Services.Interfaces;
using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace hrms_core.ViewModel.Services
{
    public class ForgotPasswordServices : IForgotPasswordServices
    {
        private readonly ApplicationDbContext _db;
        public ForgotPasswordServices(ApplicationDbContext options) => _db = options;

        public bool SendForgetPassword(UserModel userModel, HttpContext httpContext, IConfiguration configuration)
        {
            bool result;
            #region GetData
            var _data = _db.Users.Where(x => x.username == userModel.username && x.isactive == true).FirstOrDefault();
            var Data = (from user in _db.Users.Where(x => x.username == _data.username)
                        join master in _db.AssociateMasters.Where(x => x.isactive == true) on user.username equals master.associate_code
                        join main in _db.AssociateMains.Where(x => x.isactive == true) on master.id equals main.associate_id
                        select new AssociateMainModel
                        {
                            personal_email = main.personal_email,
                            userfullname = master.associate_name,
                            username = master.associate_code,
                            userpass = user.userpass
                        }).FirstOrDefault();
            Guid obj = Guid.NewGuid();
            #endregion
            var _emailData = _db.EmailConfigurations.FirstOrDefault();
            MailMessage mail = new MailMessage();
            try
            {
                string str = string.Empty;
                

                
                if (Data != null)
                {
                    StringBuilder URL = new StringBuilder();
                    URL.Append(String.Format("{0}://{1}", httpContext.Request.Scheme, httpContext.Request.Host.Host));
                    URL.Append(String.Format(":{0}", httpContext.Request.Host.Port));
                    URL.Append(String.Format("/{0}/{1}?Guid={2}", "Account", "ResetPassword", obj));


                    #region Mailing Section

                
                    if (_emailData!=null)
                    {
                        string FromEmail = _emailData.email_send_from;
                        string Pass = _emailData.password;
                       
                        SmtpClient SmtpServer = new SmtpClient(_emailData.smtp);
                        mail.From = new MailAddress(FromEmail);
                        mail.To.Add(Data.personal_email);
                        mail.IsBodyHtml = true;
                        mail.Subject = Data.userfullname.ToUpper() + ", Forgot Password Recovery Mail";
                        mail.Body = "Password Reset link is given below: ";
                        if (_data.userpass != "")
                        {
                            mail.Body = string.Format("Hi <strong>" + Data.userfullname + "</strong>,<br/><br/>Your UserName : {0} <br/> Your Password : {1}<br><a href='" + Convert.ToString(URL) + "'>Click Here to Set New Password<a/>", Data.username,CommonUtility.GetDecryptMD5(Data.userpass));
                        }
                        else
                        {
                            mail.Body = string.Format("{0} is not an authrorized username, Please check with the management", Data.username);
                        }

                        SmtpServer.Host = _emailData.smtp;
                        SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                        SmtpServer.UseDefaultCredentials = false;
                        SmtpServer.Port = _emailData.port;
                        SmtpServer.Credentials = new System.Net.NetworkCredential(FromEmail, Pass);
                        SmtpServer.EnableSsl = _emailData.ssl;
                        System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                 System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };
                        SmtpServer.Send(mail);
                        str = MessageHelper.EmailSent;
                        #endregion

                        _data.guid = obj.ToString();
                        _data.updatedon = DateTime.Now;
                        _db.SaveChanges();
                    }
                   result=true;
                }
                else
                {

                    result = false;
                }
              
            }
            catch (Exception ex)
            {
                CommonUtility.WriteErrorLogs(ex, String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name), _db);
                result = false;
               
            }
            string status = string.Empty;
            if (result)
                status = "Success";
            else
                status = "Failed";
            CommonUtility.WriteMailLogs("From =>" + _emailData.email_send_from + "  Date=>" + DateTime.Now.ToShortDateString() + " Body=>" + mail.Body + " Status=>" + status, "SendForgetPassword");
            return result;
        }
    }
}
