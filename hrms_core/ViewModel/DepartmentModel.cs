﻿using hrms_core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.ViewModel
{
    public class DepartmentModel : Department
    {
        public virtual AssociateMaster AssociateMaster { get; set; }
        public List<DepartmentModel> DepartmentModelList { get; set; }
        public List<AssociateMasterModel> AssociateMasterModelList { get; set; }
        public string associate_name { get; set; }
        public bool is_selected { get; set; }
        public DepartmentModel()
        {
            DepartmentModelList = new List<DepartmentModel>();
            AssociateMasterModelList = new List<AssociateMasterModel>();
        }
    }
}
