﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class Attendancedata
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage = "AttendanceDate is required")]
        public DateTime attendancedate { get; set; }
        public string associate_code { get; set; }
        [Required(ErrorMessage ="InTime is required")]
        public string intime { get; set; }
        [Required(ErrorMessage = "OutTime is required")]
        public string outtime { get; set; }
        public string logintime { get; set; }
        //public int employeeid { get; set; }
    }
}
