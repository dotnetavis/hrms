﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hrms_core.Models
{
    public class Payslip:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }                           //primary key
        public virtual AssociateMaster associate { get; set; }  //Foriegn Key
        [Required(ErrorMessage = "Enter Month Year")]
        public DateTime monthyear { get; set; }
        public int totalammount { get; set; }
    }
}
